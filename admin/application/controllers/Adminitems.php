<?php
require_once APPPATH . 'core/Base_Controller.php'; //Load Base Controller
defined('BASEPATH') OR exit('No direct script access allowed');

class Adminitems extends Base_Controller 
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Kolkata');
		$this->load->model("item_models"); // load Item moels
		if(!$this->session->userdata('__ci_last_regenerate') || $this->session->userdata('user_type') != 1){
			$this->session->set_flashdata('error', 'You Are not Allowed to access this file...!');
			redirect('login');
		}
	}
	
 	public function item_tracking_old()
	{
		$rs = $this->db->query("select * from tbl_item_sales");
		$array = $rs->result_array();

		foreach($array as $row){
			$level_type = $row['level_type'];
			if($level_type=='4'){
				$data[] = array
						(
							'imei'  		=> $row['imei'],
							'ref_id'   		=> $row['nd_id'],
							'ref_code'   	=> $row['nd_code'],
							'tis_id'   		=> $row['is_id'],
							'level_type'   	=> '1',
						    'track_status'  => '1',
							'date'   		=> date('Y-m-d h:i:s', strtotime($row['nd_date']))
							
						);
				$data[] = array
						(
							'imei'  		=> $row['imei'],
							'ref_id'   		=> $row['d_id'],
							'ref_code'   	=> $row['d_code'],
							'tis_id'   		=> $row['is_id'],
							'level_type'   	=> '2',
						    'track_status'  => '1',
							'date'   		=> date('Y-m-d h:i:s', strtotime($row['d_date']))
							
						);
				$data[] = array
						(
							'imei'  		=> $row['imei'],
							'ref_id'   		=> $row['rt_id'],
							'ref_code'   	=> $row['rt_code'],
							'tis_id'   		=> $row['is_id'],
							'level_type'   	=> '3',
						    'track_status'  => '1',
							'date'   		=> date('Y-m-d h:i:s', strtotime($row['rt_date']))
							
						);
				$data[] = array
						(
							'imei'  		=> $row['imei'],
							'ref_id'   		=> $row['c_id'],
							'ref_code'   	=> $row['c_code'],
							'tis_id'   		=> $row['is_id'],
							'level_type'   	=> '4',
						    'track_status'  => '1',
							'date'   		=> date('Y-m-d h:i:s', strtotime($row['c_date']))
							
						);										
			}elseif($level_type=='3'){
				$data[] = array
						(
							'imei'  		=> $row['imei'],
							'ref_id'   		=> $row['nd_id'],
							'ref_code'   	=> $row['nd_code'],
							'tis_id'   		=> $row['is_id'],
							'level_type'   	=> '1',
						    'track_status'  => '1',
							'date'   		=> date('Y-m-d h:i:s', strtotime($row['nd_date']))
							
						);
				$data[] = array
						(
							'imei'  		=> $row['imei'],
							'ref_id'   		=> $row['d_id'],
							'ref_code'   	=> $row['d_code'],
							'tis_id'   		=> $row['is_id'],
							'level_type'   	=> '2',
						    'track_status'  => '1',
							'date'   		=> date('Y-m-d h:i:s', strtotime($row['d_date']))
							
						);
				$data[] = array
						(
							'imei'  		=> $row['imei'],
							'ref_id'   		=> $row['rt_id'],
							'ref_code'   	=> $row['rt_code'],
							'tis_id'   		=> $row['is_id'],
							'level_type'   	=> '3',
						    'track_status'  => '1',
							'date'   		=> date('Y-m-d h:i:s', strtotime($row['rt_date']))
							
						);
			}elseif($level_type=='2'){
					$data[] = array
						(
							'imei'  		=> $row['imei'],
							'ref_id'   		=> $row['nd_id'],
							'ref_code'   	=> $row['nd_code'],
							'tis_id'   		=> $row['is_id'],
							'level_type'   	=> '1',
						    'track_status'  => '1',
							'date'   		=> date('Y-m-d h:i:s', strtotime($row['nd_date']))
							
						);
					$data[] = array
						(
							'imei'  		=> $row['imei'],
							'ref_id'   		=> $row['d_id'],
							'ref_code'   	=> $row['d_code'],
							'tis_id'   		=> $row['is_id'],
							'level_type'   	=> '2',
						    'track_status'  => '1',
							'date'   		=> date('Y-m-d h:i:s', strtotime($row['d_date']))
						);
				}elseif($level_type=='1'){
					$data[] = array
						(
							'imei'  		=> $row['imei'],
							'ref_id'   		=> $row['nd_id'],
							'ref_code'   	=> $row['nd_code'],
							'tis_id'   		=> $row['is_id'],
							'level_type'   	=> '1',
						    'track_status'  => '1',
							'date'   		=> date('Y-m-d h:i:s', strtotime($row['nd_date']))
						);
				}

		}
			$this->db->trans_begin(); //trans start
			foreach ($data as $row)
			{	 
			     if($row['imei']!='')
				 {	 
					$this->db->insert("tbl_item_tracking",$row);
				 }
			}	
		if ($this->db->trans_status() === FALSE){
		    $this->db->trans_rollback(); //rolback
		   	echo "fail";
		}else{
		    $this->db->trans_commit(); //commit
		 echo "success";
		}	
	}

	public function uploads_items()
	{
		$this->renderView('Admin/Items/upload_items');
	}

	public function upload_items()
	{
		
				$this->load->library('excel');
				
				$rs = $this->db->query("select imei from tbl_items where status='1'");
					$array = $rs->result_array();
					
				$imei_array = array_column($array, 'imei');	 // for converting in 1 array format
		            $imei_array1 =  array_flip($imei_array);
				if (isset($_POST)) 
				{
					//print_r($_FILES); die;
				   $path = $_FILES["product_info"]["tmp_name"];
				   $object = PHPExcel_IOFactory::load($path);
				   foreach($object->getWorksheetIterator() as $worksheet)
				   {
					   $highestRow = $worksheet->getHighestRow();
					   $highestColumn = $worksheet->getHighestColumn();
					  
					    for($row=2; $row<=$highestRow; $row++)
					    {
							$companycode 		= $worksheet->getCellByColumnAndRow(0, $row)->getValue();
							$companyname 		= $worksheet->getCellByColumnAndRow(1, $row)->getValue(); 
							// $udate 			= $worksheet->getCellByColumnAndRow(2, $row)->getValue();
							$date 				= date('Y-m-d', PHPExcel_Shared_Date::ExcelToPHP($worksheet->getCellByColumnAndRow(2, $row)->getValue()));
							$itemcode 			= $worksheet->getCellByColumnAndRow(3, $row)->getValue();
							$imei 				= $worksheet->getCellByColumnAndRow(4, $row)->getValue();
							
							$imei_code  =	trim($imei); // remove spaces from both sides of string
							$length = strlen($imei_code);
							if($length > '10'){
								   	if(array_key_exists($imei_code, $imei_array1))
									{
										$duplicate[] = array
											(
												
												'imei'   				=> $imei_code
															
											);
									}else{
													$data[] = array
												(
													'company_code'  		=> $companycode,
													'company_name'   		=> $companyname,
													'upload_date'   		=> $date,
													'item_code'   			=> $itemcode,
												    'imei'   				=> $imei_code,
													'inserted_on'   		=> date('Y-m-d h:i:s')
													
												);
										  }
								
							}else{
									$invalid_imei_length[] = array
											(
												
												'imei'   				=> $imei_code
															
											);

								 }	  
					}
				}
					if(isset($data))
					{
						foreach ($data as $row)
							{	 
							     if($row['imei']!='')
								 {	 
									$this->db->insert("tbl_items",$row);
								 }
							}	
					}		 
						
					if(isset($data))
					{    	
						    $page_data['acceptedprodct']	=	$data;
						
					}

					if(isset($duplicate))
					{    	
						    $page_data['rejectedprodct']	=	$duplicate;
						
					}
					
					if(isset($invalid_imei_length))
					{    	
						    $page_data['invalidimei']		=	$invalid_imei_length;
						
					}
				 	
				 	$this->renderView('Admin/Items/upload_items',$page_data);
				}
				
		}

    public function add_items()
	{
		$this->renderView('Admin/Items/add_items');
	}
    
    public function insert_items()
	{
		$rs 			= $this->db->query("select imei from tbl_items where status='1'");
		$array 			= $rs->result_array();
		$imei_array 	= array_column($array, 'imei');	 // for converting in 1 array format
		$imei_array1 	=  array_flip($imei_array);

		if (isset($_POST)) 
		{
			$companycode 		= $_POST['company_code'];
			$companyname 		= $_POST['company_name'];
			$date 				= date("Y-m-d", strtotime($_POST['upload_date']));
			$itemcode 			= $_POST['item_code'];
			$imei 				= $_POST['imei'];
			$imei_code 	=	trim($imei); // remove spaces from both sides of string
			$length = strlen($imei_code);
			if($length > '10'){
					 if(array_key_exists($imei_code, $imei_array1))
					 {
						$this->session->set_flashdata('error','IMEI Number Already Exist!');
						$this->renderView('Admin/Items/add_items');
					 }
					 else
					 {
					    $data[] = array
						(
							'company_code'  		=> $companycode,
							'company_name'   		=> $companyname,
							'upload_date'   		=> $date,
							'item_code'   			=> $itemcode,
							'imei'   				=> $imei_code,
							'inserted_on'   		=> date('Y-m-d h:i:s')
						);
						
						foreach ($data as $row)
						{	 
									   	 
							$this->db->insert("tbl_items",$row);
										
						}
						$this->session->set_flashdata('success',' Item Added Successfully');
						$this->renderView('Admin/Items/add_items');
					 }
				
			}else{
					$invalid_imei_length[] = array
					(
						
						'imei'   				=> $imei_code
									
					);
						$this->session->set_flashdata('error','Item not Uploaded. Invalid IMEI Number!');
						$this->renderView('Admin/Items/add_items');	
			}	  
		}	 
	}	
	

	/* Without ci pagination 
	public function item_list()
		{
			
			$select	 = array('item_id','company_code','company_name','upload_date','item_code','item_name','imei');
			$where = array('status'=> '1');
			$pagedata['results'] = $this->item_models->GetAllItemValues('tbl_items', $where, $select);
			$pagedata['delete_link'] = 'Adminitems/delete_item';
			$this->renderView('Admin/Items/item_list',$pagedata);
		}*/


	//generate to excel	
	public function generate_item_excel($param1){
		// create file name
		$fileName = 'ItemList'.'-data-'.date('d-M-Y').'.xlsx';   
		// load excel library
		$this->load->library('excel');
		$info = $param1;
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->setActiveSheetIndex(0);
		// set Header
		$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Company Code');
		$objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Company Name');
		$objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Uploading Date');
		$objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Item Code');
		$objPHPExcel->getActiveSheet()->SetCellValue('E1', 'IMEI');
		$objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Status');
		// set Row
		$rowCount = 2;
	
		foreach ($info as $element) {
			$objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $element['company_code']);
			$objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $element['company_name']);
			$objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, date('d-M-y', strtotime($element['upload_date'])));
			$objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $element['item_code']);
			$objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $element['imei']);
				$pay_status = ($element['item_status'] == '0') ? 'Unsold' : 'Sold';
			$objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $pay_status);
			$rowCount++;
		}
		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
		$objWriter->save('uploads/admin/excel/'.$fileName);
		// download file
		header("Content-Type: application/vnd.ms-excel");
		redirect(base_url('uploads/admin/excel/'.$fileName));
	}			


    	public function item_list()
		{			
            $select	 = array('item_id','company_code','company_name','upload_date','item_code','item_name','imei','item_status');
			$where = array('status'=> '1');
			$pagedata['delete_link'] = 'Adminitems/delete_item';
			$status ='';
			$imei ='';
			if($this->session->userdata('imei') || $this->session->userdata('status')){
				$this->session->userdata('imei');
				$this->session->userdata('status');
			}
        
          //Pagination Start
			$config = array();
	        $config["base_url"] = site_url() . "/Adminitems/item_list";
	        $config["total_rows"] = $this->base_models->get_count('item_id','tbl_items', $where);
	        $config["per_page"] = 10;
	        $config["uri_segment"] = 3;
	        $this->pagination->initialize($config);
	        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
	        $pagedata["links"] = $this->pagination->create_links();
	        $pagedata['results'] = $this->base_models->get_pagination('tbl_items', $where,'item_id',$config["per_page"], $page);     
           	//Pagination End

           	$imei = (@$imei) ? $imei : '';
           	$sstatus = (@$status) ? $status : null;
            @strcmp($status,$sstatus);
			$pagedata['select']=array('status'=>$status,'imei'=>$imei);  
	        $this->renderView('Admin/Items/item_list',$pagedata);
		}

		// with ci pagination in php
		public function item_list_sess()
		{

           // print_r($_POST);
			$select	 = array('item_id','company_code','company_name','upload_date','item_code','item_name','imei','item_status');
			$where = array('status'=> '1');
			$pagedata['delete_link'] = 'Adminitems/delete_item';
			$status ='';
			$imei ='';
			//Filter Process
		
           if(@$_POST['submit']=='filter' || @$_POST['submit']=='createxls')
           {
           		/*$ranges = explode('-',$this->input->post('daterange'));
				$fdate = date('Y-m-d', strtotime($ranges[0])).' 00:00:00';
				$todate = date('Y-m-d', strtotime($ranges[1])).' 23:59:00';*/
           	  	$fdate = (@$this->input->post('fromdate')) ? $this->input->post('fromdate') : '';
           	  	$todate = (@$this->input->post('todate')) ? $this->input->post('todate') : '';
           	  	$imei_no = (@$this->input->post('imei')) ? $this->input->post('imei') : '';
           		$imei = trim($imei_no);
              $where = array();
               if($this->input->post('fromdate') != '' && $this->input->post('todate') != ''){
						$where = "upload_date BETWEEN '$fdate' AND '$todate'";
					}

           	    $status = (@$this->input->post('status')!= null) ? $this->input->post('status') : '';
               $array_items = $this->session->set_userdata(array("imei"=>$imei,"status"=>$status));
                if($imei !=''){
                	$filter =  array('imei'=> $imei);
                	$where = @array_merge($where,$filter);	
                }  
                if($status !=''){
               		$filter =  array('item_status'=>$status);
                	$where = @array_merge($where,$filter);	
                } 
             
           }else{
 				if($this->session->userdata('imei') != NULL){
					$imei = $this->session->userdata('imei'); 
					$filter =  array('imei'=> $imei);
					$where = array_merge($where,$filter);
 				} 
 				if($this->session->userdata('status') != NULL){
					$status = $this->session->userdata('status'); 
					$filter =  array('item_status'=>$status);
					$where = array_merge($where,$filter);
 				}
           	}
           		/*echo '<pre>';
			print_r($where); die();*/
           	 if(@$_POST['submit']=='createxls')
           	  {

           	  //	$select	 = array('item_id','company_code','company_name','upload_date','item_code','item_name','imei');
				//$where .= array('status'=> '1');
				$data['data'] = $this->item_models->GetAllItemValues('tbl_items', $where, $select);
		     /* echo '<pre>';
		       print_r($data['data']);*/
				//Export xls
					$this->generate_item_excel($data['data']);			
					

              }
			//End Filter Process
        
          //Pagination Start
			$config = array();
	        $config["base_url"] = site_url() . "/Adminitems/item_list_sess";
	        $config["total_rows"] = $this->base_models->get_count('item_id','tbl_items', $where);
	        $config["per_page"] = 10;
	        $config["uri_segment"] = 3;
	        $this->pagination->initialize($config);
	        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
	        $pagedata["links"] = $this->pagination->create_links();
	        $pagedata['results'] = $this->base_models->get_pagination('tbl_items', $where,'item_id',$config["per_page"], $page);     
           	//Pagination End

           	$imei = (@$imei) ? $imei : '';

           	$sstatus = (@$status) ? $status : null;
            @strcmp($status,$sstatus);
	       $pagedata['select']=array('status'=>$status,'imei'=>$imei);  
	        $this->renderView('Admin/Items/item_list',$pagedata);
		}	


	public function edit_item()
	{
		$id = base64_decode($_GET['id']); 
		$select	 = array('item_id','company_code','company_name','upload_date','item_code','item_name','imei');
		$where = array('item_id'=> $id);
		$pagedata['data']=$this->item_models->GetAllItemValues('tbl_items', $where, $select);
		$this->renderView('Admin/Items/edit_items',$pagedata);
	}

	public function update_items()
	{
		if (isset($_POST)) 
		{
			$item_id = $this->input->post('item_id');
			$update_array	=	array(
										'company_code'  		=> $this->input->post('company_code'),
										'company_name'   		=> $this->input->post('company_name'),
										'upload_date'   		=> date("Y-m-d", strtotime($this->input->post('upload_date'))),
										'item_code'   			=> $this->input->post('item_code'),
										//'item_name'   			=> $this->input->post('item_name'),
										'imei'   				=> $this->input->post('imei'),
										'updated_on'			=> date("Y-m-d H:i:s")
									);
			$where_array	=	 array('item_id'=>$item_id);

			if($this->base_models->update_records('tbl_items',$update_array,$where_array) == true){
					$this->session->set_flashdata('success','Edited successfully');
				}else{
					$this->session->set_flashdata('error','Not added Please try again');
				}
		}
			redirect(base_url('Adminitems/edit_item/?id='.base64_encode($item_id)));	
	}	
			public function delete_item()
			{
				$id = $_GET['id'];
				$current_date = date("Y-m-d H:i:s");
				$update_array = array(
									'status'=>'2',
									'deleted_on'=>$current_date
									);
				$where_array = array('item_id'=>$id);
				if($this->base_models->update_records('tbl_items',$update_array,$where_array) == true){
					$data['status'] = 'success';
					$data['message'] = 'Successfully deleted';
				}else{
					$data['status'] = 'error';
					$data['message'] = 'Somting went worng please try again';
				}
				echo json_encode($data);
				die();
			}
   
       
    public function check_imei_exist()
	{
		$rs 			= $this->db->query("select imei from tbl_items where status='1'");
		$array 			= $rs->result_array();
		$imei_array 	= array_column($array, 'imei');	 // for converting in 1 array format
		$imei_array1 	= array_flip($imei_array);

		$imei 	        = $_POST['imei'];

		$imei_code 		= trim($imei); // remove spaces from both sides of string

			 if(array_key_exists($imei_code, $imei_array1))
			 {
				echo 'error';
			 }
			 else
			 {
			 	echo 'success';
			 }	
	}	 

	//---------- ND ------------//
	public function national_distributor()
	{		
		$select = array('nd_id','nd_code','fname','mname','lname','lname','username','email','email2','contact','contact2','pan_no','gst_no','status','state','city','address','acnt_name','acnt_email','acnt_contact','inserted_on','updated_on','(select date_time from recent_login_user where user_code = nd_code AND type = "1" Order by id desc limit 1) as last_login');
		$pagedata['results'] = $this->base_models->GetAllValues('ndistributor',null, $select);
		echo '<pre>';
		print_r($pagedata['results']);
		die();
		$pagedata['delete_link'] = 'Adminmaster/delete_nd';
		$this->renderView('Admin/Master/national_distributor',$pagedata);
	}
	
	
	
	public function insert_nd()
	{
		$this->form_validation->set_rules('fname', 'First name', 'trim|required');
		$this->form_validation->set_rules('mname', 'Middle Name', 'trim|required');
		$this->form_validation->set_rules('lname', 'Last Name', 'trim|required');
		$this->form_validation->set_rules('username', 'User Name', 'trim|required');
		$this->form_validation->set_rules('inputPassword', 'Password', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'trim');
		$this->form_validation->set_rules('contact', 'Mobile No.', 'trim|required|numeric');
		$this->form_validation->set_rules('type', 'User Type', 'trim|required');
		$this->form_validation->set_rules('status', 'Status', 'trim|required');
		$current_date = date("Y-m-d H:i:s");
	
		$error='';
			// if (empty($_FILES['image']['name'][0])){
				// $this->form_validation->set_rules('image', 'Image', 'required');
			// }
			$data['file_name'] = '';
            if(!empty($_FILES['image']['name'])){
                $config['upload_path'] = 'uploads/admin/users/';
                $config['allowed_types'] = 'gif|jpg|png';
                $this->upload->initialize($config);
				if($this->upload->do_upload('image')){
					$data = $this->upload->data();
				}else{
                    $imageerrors = $this->upload->display_errors();
					$this->form_validation->set_message('image', $imageerrors);					
                }
			}
			
			if($this->form_validation->run())
			{					
				$insert_array=array(
						'fname'=>$this->input->post('fname'),
						'mname'=>$this->input->post('mname'),
						'lname'=>$this->input->post('lname'),
						'username'=>$this->input->post('username'),
						'password'=>md5($this->input->post('inputPassword')),
						'email'=>$this->input->post('email'),
						'contact'=> $this->input->post('contact'),
						'type'=>$this->input->post('type'),
						'status'=>$this->input->post('status'),
						'profile_pic'=>$data['file_name'],
						'inserted_on'=>date("Y-m-d H:i:s")
					);
					//print_r($insert_array);exit;
					if($this->base_models->add_records('wwc_admin',$insert_array)){
						$this->session->set_flashdata('success','Added successfully');
						redirect(base_url('admin/users'));
					}else{
						$this->session->set_flashdata('error','Not added Please try again');
						//redirect(base_url('admin/add_user'));
					}
			}
				$this->renderView('Admin/add-user');
	}
	
	public function edit_nd()
	{
		$id = base64_decode($_GET['id']);
		$pagedata['data']=$this->base_models->get_users('',$id);
		$this->renderView('Admin/edit-user',$pagedata);
	}
	
	public function update_nd()
	{
		$id = base64_decode($_GET['id']);		
		if($id==''){
			redirect(base_url('admin/users')); 
		}
		
		$this->form_validation->set_rules('fname', 'First name', 'trim|required');
		$this->form_validation->set_rules('mname', 'Middle Name', 'trim|required');
		$this->form_validation->set_rules('lname', 'Last Name', 'trim|required');
		$this->form_validation->set_rules('username', 'User Name', 'trim|required');
		$this->form_validation->set_rules('inputPassword', 'Password', 'trim');
		$this->form_validation->set_rules('email', 'Email', 'trim');
		$this->form_validation->set_rules('contact', 'Mobile No.', 'trim|required|numeric');
		$this->form_validation->set_rules('type', 'User Type', 'trim|required');
		$this->form_validation->set_rules('status', 'Status', 'trim|required');
		$current_date = date("Y-m-d H:i:s");
	
		$error='';
			// if (empty($_FILES['image']['name'][0])){
				// $this->form_validation->set_rules('image', 'Image', 'required');
			// }
			// $data['file_name'] = '';
            // if(!empty($_FILES['image']['name'])){
                // $config['upload_path'] = 'uploads/admin/users/';
                // $config['allowed_types'] = 'gif|jpg|png';
                // $this->upload->initialize($config);
				// if($this->upload->do_upload('image')){
					// $data = $this->upload->data();
				// }else{
                    // $imageerrors = $this->upload->display_errors();
					// $this->form_validation->set_message('image', $imageerrors);					
                // }
			// }
			
			if($this->input->post('inputPassword') != ''){
				$password = md5($this->input->post('inputPassword'));
			}else{
				$password = $this->input->post('oldpassword');
			}	
			if($this->form_validation->run())
			{				
				$update_array=array(
						'fname'=>$this->input->post('fname'),
						'mname'=>$this->input->post('mname'),
						'lname'=>$this->input->post('lname'),
						'username'=>$this->input->post('username'),
						'password'=>$password,
						'email'=>$this->input->post('email'),
						'contact'=> $this->input->post('contact'),
						'type'=>$this->input->post('type'),
						'status'=>$this->input->post('status'),
						// 'profile_pic'=>$data['file_name'],
						'updated_on'=>date("Y-m-d H:i:s")
					);
				$where_array = array('id'=>$id);
				//print_r($insert_array);exit;
				if($this->base_models->update_records('wwc_admin',$update_array,$where_array) == true){
					$this->session->set_flashdata('success','Edited successfully');
				}else{
					$this->session->set_flashdata('error','Not added Please try again');
				}
			}
				redirect(base_url('admin/edit_user/?id='.base64_encode($id)));
			
		// $pagedata['data']=$this->base_models->get_users('',$id);
		// $this->renderView('Admin/edit-user',$pagedata);
	}
			
	public function delete_nd()
	{
		$id = $_GET['id'];
		$current_date = date("Y-m-d H:i:s");
		$update_array = array(
							'status'=>'2',
							'deleted_on'=>$current_date
							);
		$where_array = array('id'=>$id);
		if($this->base_models->update_records('wwc_admin',$update_array,$where_array) == true){
			$data['status'] = 'success';
			$data['message'] = 'Successfully deleted';
		}else{
			$data['status'] = 'error';
			$data['message'] = 'Somting went worng please try again';
		}
		echo json_encode($data);
		die();
	}
	//---------- END ND ------------//
	
		
}
