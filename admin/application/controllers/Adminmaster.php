<?php
require_once APPPATH . 'core/Base_Controller.php'; //Load Base Controller
defined('BASEPATH') OR exit('No direct script access allowed');

class Adminmaster extends Base_Controller 
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Kolkata');
		
		if(!$this->session->userdata('__ci_last_regenerate') || $this->session->userdata('user_type') != 1){
			$this->session->set_flashdata('error', 'You Are not Allowed to access this file...!');
			redirect('login');
		}
	}
	public function retailer_tsm_mapping()
	{
		$this->renderView('Admin/Master/retailer_tsm_mapping');
	}


	public function upload_retailer_tsm_mapping()
	{
		$this->load->library('excel');
		
		$rs = $this->db->query("select atsm_code from  atsm where level_type='1'");
		$array = $rs->result_array();
		$atsm_array = array_column($array, 'atsm_code');	 // for converting in 1 array format
		$atsm_array1 =  array_flip($atsm_array);
		
		$rt = $this->db->query("select rt_code from retailer where status='1'");
		$arrayrt = $rt->result_array();
		$rt_array = array_column($arrayrt, 'rt_code');	 // for converting in 1 array format
		$rt_array1 =  array_flip($rt_array);

		if (isset($_POST)) 
		{
			//print_r($_FILES); die;
		   $path = $_FILES["product_info"]["tmp_name"];
		   $object = PHPExcel_IOFactory::load($path);
		   foreach($object->getWorksheetIterator() as $worksheet)
		   {
			   $highestRow = $worksheet->getHighestRow();
			   $highestColumn = $worksheet->getHighestColumn();
			  
				for($row=2; $row<=$highestRow; $row++)
				{
					$rt_id 		= $worksheet->getCellByColumnAndRow(0, $row)->getValue();
					$atsm_id 	= $worksheet->getCellByColumnAndRow(1, $row)->getValue(); 
					$rt_code  	=	trim($rt_id); // remove spaces from both sides of string
					$atsm_code  =	trim($atsm_id); // remove spaces from both sides of string
					
					if(array_key_exists($atsm_code, $atsm_array1)){
						if(array_key_exists($rt_code, $rt_array1)){
							$atsm = $this->db->query("select atsm_id from  atsm where atsm_code='$atsm_code'");
							$atsm_res = $atsm->result_array();
							$atsm_id=	$atsm_res[0]['atsm_id'];
							$data[] = array
								(
									'atsm_id'  			=> $atsm_id,
									'rt_code'   	=> $rt_code
									
								);
						}else{
							$invalid_rt1[] = array('rt' => $rt_code);
						}						
					}else{
							$invalid_atsm1[] = array
							(
								'atsm'  => $atsm_code
							);
					}
				}
			}
			if(isset($data))
			{
				foreach ($data as $row)
				{	
					$update_array = array('tsm_id'=>$row['atsm_id'],'updated_on'=>date('Y-m-d h:i:s'));
					$where_array = array('rt_code'=>$row['rt_code']);
					$this->base_models->update_records('retailer',$update_array,$where_array); 
				}	
			}		 
			if(isset($data)){    	
				$page_data['acceptedprodct']	=	$data;
			}

			if(isset($invalid_rt1)){    	
				$page_data['invalid_rt']	=	$invalid_rt1;
			}

			if(isset($invalid_atsm1)){    	
				$page_data['invalid_atsm']	=	$invalid_atsm1;
			}
			$this->renderView('Admin/Master/retailer_tsm_mapping',$page_data);
		}				
	}

	//---------- ND ------------//
	public function national_distributor()
	{	
		$this->load->model("adminmaster_models");
		$select = array('nd_id','nd_code','firmname','fname','mname','lname','lname','username','email','email2','contact','contact2','pan_no','gst_no','status','state','city','address','acnt_name','acnt_email','acnt_contact','inserted_on','updated_on','(select date_time from recent_login_user where user_code = nd_code AND type = "1" Order by id desc limit 1) as last_login','(select state_name from area where state = state_id Order by area_id desc limit 1) as state_name','(select city_name from area where city = city_id Order by area_id desc limit 1) as city_name');
		$where = array('status !=' => '2');
		
		//Pagination Start
		$config = array();
		$config["base_url"] = site_url() . "/Adminmaster/national_distributor";
		$config["total_rows"] = $this->base_models->get_count('nd_id','ndistributor', $where);
		$config["per_page"] = 10;
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$pagedata["links"] = $this->pagination->create_links();
		$pagedata['results'] = $this->adminmaster_models->get_national_distributor($select,'ndistributor', $where,'nd_id',$config["per_page"], $page);     
		//Pagination End
		
		$pagedata['delete_link'] = 'Adminmaster/delete_nd';
		$this->renderView('Admin/Master/national_distributor',$pagedata);
	}
	
	public function add_nd()
	{
		$pagedata['state_list']=$this->base_models->get_state();
		$pagedata['city_list']=$this->base_models->get_city();
		$this->renderView('Admin/Master/add-nd',$pagedata);
	}
	
	public function get_city_by_id()
	{
			$state_id		=	$_POST['state_id'];	
			$rs 	=	 $this->db->query("select city_id,city_name from area where state_id='$state_id' group by city_id");
			$array  = 	 $rs->result_array();
			echo ' <div class="form-group col-sm-2">
						<label for="city" class="control-label">City</label>
						<select class="form-control select2" id="city" name="city" data-error="City required" required>
							<option value="">Select</option>';
							
							 foreach($array as $city){
							  echo '<option value="' . $city["city_id"] . ' ">' . $city["city_name"] . '</option>';
							 } 
					echo	'</select>
						<div class="help-block with-errors"></div>
					  </div>';
	}
	public function insert_nd()
	{
		$this->form_validation->set_rules('firmname', 'Firm name', 'trim|required');
		$this->form_validation->set_rules('fname', 'First name', 'trim|required');
		$this->form_validation->set_rules('lname', 'Last Name', 'trim|required');
		$this->form_validation->set_rules('status', 'Status', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'trim|required');
		$this->form_validation->set_rules('contact', 'Mobile No.', 'trim|required|numeric');
		$this->form_validation->set_rules('state', 'State', 'trim|required');
		$this->form_validation->set_rules('city', 'City', 'trim|required');
		$this->form_validation->set_rules('address', 'Address', 'trim|required');
		$this->form_validation->set_rules('pan', 'PAN', 'trim|required');
		$this->form_validation->set_rules('gst', 'GST', 'trim|required');
		$this->form_validation->set_rules('inputPassword', 'Password', 'trim|required');
		$current_date = date("Y-m-d H:i:s");
	
		$error='';
			// if (empty($_FILES['image']['name'][0])){
				// $this->form_validation->set_rules('image', 'Image', 'required');
			// }
			$data['file_name'] = '';
            // if(!empty($_FILES['image']['name'])){
                // $config['upload_path'] = 'uploads/nd/profile/';
                // $config['allowed_types'] = 'gif|jpg|png';
                // $this->upload->initialize($config);
				// if($this->upload->do_upload('image')){
					// $data = $this->upload->data();
				// }else{
                    // $imageerrors = $this->upload->display_errors();
					// $this->form_validation->set_message('image', $imageerrors);					
                // }
			// }
			
			if($this->form_validation->run())
			{					
				$insert_array=array(
						'firmname'=>$this->input->post('firmname'),
						'fname'=>$this->input->post('fname'),
						'mname'=>$this->input->post('mname'),
						'lname'=>$this->input->post('lname'),
						'status'=>$this->input->post('status'),
						'email'=>$this->input->post('email'),
						'email2'=>$this->input->post('email2'),
						'contact'=> $this->input->post('contact'),
						'contact2'=> $this->input->post('contact2'),
						'state'=> $this->input->post('state'),
						'city'=> $this->input->post('city'),
						'address'=> $this->input->post('address'),
						'pan_no'=>strtoupper($this->input->post('pan')),
						'gst_no'=>strtoupper($this->input->post('gst')),
						'acnt_name'=>$this->input->post('acnt_name'),
						'acnt_email'=>$this->input->post('acnt_email'),
						'acnt_contact'=>$this->input->post('acnt_contact'),
						'password'=>md5($this->input->post('inputPassword')),
						// 'profile_pic'=>$data['file_name'],
						'admin_id'=>$this->session->userdata('id'),
						'inserted_on'=>date("Y-m-d H:i:s")
					);
					//print_r($insert_array);exit;
					if($this->base_models->add_records('ndistributor',$insert_array)){
						$this->session->set_flashdata('success','Added successfully');
						redirect(site_url('/Adminmaster/national_distributor'));
					}else{
						$this->session->set_flashdata('error','Not added Please try again');
						//redirect(base_url('admin/add_user'));
					}
			}
				$this->renderView('Admin/Master/add-nd');
	}
	
	public function edit_nd()
	{
		$id = base64_decode($_GET['id']);
		$pagedata['data']=$this->base_models->GetSingleDetails('ndistributor', array('nd_id' => $id), $select = "*");
		$state_id=	$pagedata['data']->state;
		$pagedata['state_list']=$this->base_models->get_state();
		$pagedata['city_list']=$this->base_models->get_city_by_state($state_id);
		
		$this->renderView('Admin/Master/edit-nd',$pagedata);
	}
	
	public function update_nd()
	{
		$id = base64_decode($_GET['id']);		
		if($id==''){
			redirect(site_url('/Adminmaster/national_distributor')); 
		}
		
		$this->form_validation->set_rules('firmname', 'Firm name', 'trim|required');
		$this->form_validation->set_rules('fname', 'First name', 'trim|required');
		$this->form_validation->set_rules('lname', 'Last Name', 'trim|required');
		$this->form_validation->set_rules('status', 'Status', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'trim|required');
		$this->form_validation->set_rules('contact', 'Mobile No.', 'trim|required|numeric');
		$this->form_validation->set_rules('state', 'State', 'trim|required');
		$this->form_validation->set_rules('city', 'City', 'trim|required');
		$this->form_validation->set_rules('address', 'Address', 'trim|required');
		$this->form_validation->set_rules('pan', 'PAN', 'trim|required');
		$this->form_validation->set_rules('gst', 'GST', 'trim|required');
		if($this->input->post('inputPassword') != ''){
			$password = md5($this->input->post('inputPassword'));
		}else{
			$password = $this->input->post('oldpassword');
		}
		$current_date = date("Y-m-d H:i:s");
	
		$error='';
			// if (empty($_FILES['image']['name'][0])){
				// $this->form_validation->set_rules('image', 'Image', 'required');
			// }
			// $data['file_name'] = '';
            // if(!empty($_FILES['image']['name'])){
                // $config['upload_path'] = 'uploads/admin/users/';
                // $config['allowed_types'] = 'gif|jpg|png';
                // $this->upload->initialize($config);
				// if($this->upload->do_upload('image')){
					// $data = $this->upload->data();
				// }else{
                    // $imageerrors = $this->upload->display_errors();
					// $this->form_validation->set_message('image', $imageerrors);					
                // }
			// }
			
			if($this->input->post('inputPassword') != ''){
				$password = md5($this->input->post('inputPassword'));
			}else{
				$password = $this->input->post('oldpassword');
			}	
			if($this->form_validation->run())
			{				
				$update_array=array(
						'firmname'=>$this->input->post('firmname'),
						'fname'=>$this->input->post('fname'),
						'mname'=>$this->input->post('mname'),
						'lname'=>$this->input->post('lname'),
						'status'=>$this->input->post('status'),
						'email'=>$this->input->post('email'),
						'email2'=>$this->input->post('email2'),
						'contact'=> $this->input->post('contact'),
						'contact2'=> $this->input->post('contact2'),
						'state'=> $this->input->post('state'),
						'city'=> $this->input->post('city'),
						'address'=> $this->input->post('address'),
						'pan_no'=>strtoupper($this->input->post('pan')),
						'gst_no'=>strtoupper($this->input->post('gst')),
						'acnt_name'=>$this->input->post('acnt_name'),
						'acnt_email'=>$this->input->post('acnt_email'),
						'acnt_contact'=>$this->input->post('acnt_contact'),
						'password'=>$password,
						// 'profile_pic'=>$data['file_name'],
						'updated_on'=>date("Y-m-d H:i:s")
					);
				$where_array = array('nd_id'=>$id);
				//print_r($insert_array);exit;
				if($this->base_models->update_records('ndistributor',$update_array,$where_array) == true){
					$this->session->set_flashdata('success','Edited successfully');
				}else{
					$this->session->set_flashdata('error','Not added Please try again');
				}
			}
				redirect(site_url('/Adminmaster/edit_nd/?id='.base64_encode($id)));
			
		// $pagedata['data']=$this->base_models->get_users('',$id);
		// $this->renderView('Admin/edit-user',$pagedata);
	}
			
	public function delete_nd()
	{
		$id = $_GET['id'];
		$current_date = date("Y-m-d H:i:s");
		$update_array = array(
							'status'=>'2',
							'deleted_on'=>$current_date
							);
		$where_array = array('nd_id'=>$id);
		if($this->base_models->update_records('ndistributor',$update_array,$where_array) == true){
			$data['status'] = 'success';
			$data['message'] = 'Successfully deleted';
		}else{
			$data['status'] = 'error';
			$data['message'] = 'Somting went worng please try again';
		}
		echo json_encode($data);
		die();
	}
	
	public function change_status()
	{
		$current_date = date("Y-m-d H:i:s");
		$update_array = array(
						$_POST['col']=>$_POST['status'],
						'updated_on'=>$current_date
						//'lastUpdateUser'=>$this->session->userdata('id')
						);
		$where_array = array('nd_id'=>$_POST['id']);
		
		if($this->base_models->update_records('ndistributor',$update_array,$where_array)){
			echo "success";
			// $this->session->set_flashdata('success','Status change successfully');
		}else{
			// $this->session->set_flashdata('error','Error while changing status');
			echo "failed";
		}
	}
	//---------- END ND ------------//	
	
	//---------- Retailer ------------//
	public function retailer()
	{		
		$this->load->model("adminmaster_models");
		$select = array('rt_id','rt_code','d_id','tsm_id','firmname','fname','mname','lname','username','email','email2','contact','contact2','pan_no','gst_no','status','state','city','address','acnt_name','acnt_email','acnt_contact','inserted_on','updated_on','tsm_id','(select atsm_code from atsm where atsm_id = tsm_id Order by atsm_id desc limit 1) as rt_tsm_code','(select fname from atsm where atsm_id = tsm_id Order by atsm_id desc limit 1) as rt_tsm_fname','(select lname from atsm where atsm_id = tsm_id Order by atsm_id desc limit 1) as rt_tsm_lname','(select date_time from recent_login_user where user_code = rt_code AND type = "1" Order by id desc limit 1) as last_login','(select state_name from area where state = state_id Order by area_id desc limit 1) as state_name','(select city_name from area where city = city_id Order by area_id desc limit 1) as city_name','(select taluka_name from area where taluka = taluka_id Order by area_id desc limit 1) as taluka_name');
		$where = array('status !=' => '2');
		
		//Pagination Start
		$config = array();
		$config["base_url"] = site_url() . "/Adminmaster/retailer";
		$config["total_rows"] = $this->base_models->get_count('rt_id','retailer', $where);
		$config["per_page"] = 10;
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$pagedata["links"] = $this->pagination->create_links();
		$pagedata['results'] = $this->adminmaster_models->retailer_list($select,'retailer', $where,'rt_id',$config["per_page"], $page);		
		//Pagination End
		
		$pagedata['delete_link'] = 'Dmaster/delete_rt';
		$this->renderView('Admin/Master/retailer',$pagedata);
	}
	
	public function retailer_sess()
	{
		$this->load->model("adminmaster_models");
		$select = array('rt_id','rt_code','d_id','tsm_id','firmname','fname','mname','lname','username','email','email2','contact','contact2','pan_no','gst_no','status','state','city','address','acnt_name','acnt_email','acnt_contact','inserted_on','updated_on','tsm_id','(select atsm_code from atsm where atsm_id = tsm_id Order by atsm_id desc limit 1) as rt_tsm_code','(select fname from atsm where atsm_id = tsm_id Order by atsm_id desc limit 1) as rt_tsm_fname','(select lname from atsm where atsm_id = tsm_id Order by atsm_id desc limit 1) as rt_tsm_lname','(select date_time from recent_login_user where user_code = rt_code AND type = "1" Order by id desc limit 1) as last_login','(select state_name from area where state = state_id Order by area_id desc limit 1) as state_name','(select city_name from area where city = city_id Order by area_id desc limit 1) as city_name','(select taluka_name from area where taluka = taluka_id Order by area_id desc limit 1) as taluka_name');
		$where = array('status !=' => '2');
		
		$rt_code = null;
		$filter = array();
		//Filter Process	
		if(@$_POST['submit']=='filter' || @$_POST['submit']=='createxls'){
			$rt_code_post = (@$this->input->post('rt_code')) ? $this->input->post('rt_code') : '';
			$rt_code = trim($rt_code_post);
			$array_items = $this->session->set_userdata(array("rt_code"=>$rt_code));
		}else{
			if($this->session->userdata('rt_code') != NULL){
				$rt_code = $this->session->userdata('rt_code');
			}
		}
		if($rt_code != null){
			$filter = array("rt_code"=>$rt_code);
		}
		$where = array_merge($where,$filter);
		
		 
		if(@$_POST['submit']=='createxls'){
			$data['data'] = $this->adminmaster_models->retailer_list($select,'retailer', $where,'rt_id');
			$this->generate_retailer_excel($data['data']);		
		}
		//End Filter Process
	
		//Pagination Start
		$config = array();
		$config["base_url"] = site_url() . "/Adminmaster/retailer_sess";
		$config["total_rows"] = $this->base_models->get_count('rt_id','retailer', $where);
		$config["per_page"] = 10;
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$pagedata["links"] = $this->pagination->create_links();
		$pagedata['results'] = $this->adminmaster_models->retailer_list($select,'retailer', $where,'rt_id',$config["per_page"], $page);		
		//Pagination End
		$pagedata['delete_link'] = 'Dmaster/delete_rt';
		$rt_code = (@$rt_code) ? $rt_code : '';
		$pagedata['select']=array('rt_code'=>$rt_code);  
		$this->renderView('Admin/Master/retailer',$pagedata);
	}
	
	//generate to excel	
	public function generate_retailer_excel($param1){
		// create file name
		$fileName = 'Retailers'.'-data-'.date('d-M-Y').'.xlsx';   
		// load excel library
		$this->load->library('excel');
		$info = $param1;
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->setActiveSheetIndex(0);
		
		// set Header		
		$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'RT Code');
		$objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Firm Name');
		$objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Name');
		$objPHPExcel->getActiveSheet()->SetCellValue('D1', 'User Name');
		$objPHPExcel->getActiveSheet()->SetCellValue('E1', 'TSM Code');
		$objPHPExcel->getActiveSheet()->SetCellValue('F1', 'TSM Name');
		$objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Primary Email');
		$objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Secondary Email');
		$objPHPExcel->getActiveSheet()->SetCellValue('I1', 'Primary Contact');
		$objPHPExcel->getActiveSheet()->SetCellValue('J1', 'Secondary Contact');
		$objPHPExcel->getActiveSheet()->SetCellValue('K1', 'PAN');
		$objPHPExcel->getActiveSheet()->SetCellValue('L1', 'GST');
		$objPHPExcel->getActiveSheet()->SetCellValue('M1', 'State');
		$objPHPExcel->getActiveSheet()->SetCellValue('N1', 'Taluka');
		$objPHPExcel->getActiveSheet()->SetCellValue('O1', 'City');
		$objPHPExcel->getActiveSheet()->SetCellValue('P1', 'Address');
		
		// set Row
		$rowCount = 2;
	
		foreach ($info as $element) {
			// $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, date('d-M-y', strtotime($element['rt_date'])));
			$objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $element['rt_code']);
			$objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount,  $element['firmname']);
			$objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $element['fname'].' '.$element['mname'].' '.$element['lname']);
			$objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $element['username']);
			$objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $element['rt_tsm_code']);
			$objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $element['rt_tsm_fname'].' '.$element['rt_tsm_lname']);
			$objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $element['email']);
			$objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $element['email2']);
			$objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $element['contact']);
			$objPHPExcel->getActiveSheet()->SetCellValue('J' . $rowCount, $element['contact2']);
			$objPHPExcel->getActiveSheet()->SetCellValue('K' . $rowCount, $element['pan_no']);
			$objPHPExcel->getActiveSheet()->SetCellValue('L' . $rowCount, $element['gst_no']);
			$objPHPExcel->getActiveSheet()->SetCellValue('M' . $rowCount, $element['state_name']);
			$objPHPExcel->getActiveSheet()->SetCellValue('N' . $rowCount, $element['taluka_name']);
			$objPHPExcel->getActiveSheet()->SetCellValue('O' . $rowCount, $element['city_name']);
			$objPHPExcel->getActiveSheet()->SetCellValue('P' . $rowCount, $element['address']);			
			$rowCount++;
		}
		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
		$objWriter->save('uploads/admin/excel/'.$fileName);
		// download file
		header("Content-Type: application/vnd.ms-excel");
		redirect(base_url('uploads/admin/excel/'.$fileName));
	}
	
	public function retailer_list()
	{		
		$this->load->model("adminmaster_models");
		$select = array('rt_id','rt_code','firmname as rt_firmname','tsm_id','inserted_on','(select atsm_code from atsm where atsm_id = tsm_id Order by atsm_id desc limit 1) as rt_tsm_code','(select fname from atsm where atsm_id = tsm_id Order by atsm_id desc limit 1) as rt_tsm_fname','(select lname from atsm where atsm_id = tsm_id Order by atsm_id desc limit 1) as rt_tsm_lname','(select taluka_name from area where taluka = taluka_id Order by area_id desc limit 1) as rt_taluka_name','d_id as did','(select d_code from distributor where did = d_id) as d_code','(select firmname from distributor where did = d_id) as d_firnname','(select city from distributor where did = d_id) as d_city_id','(select city_name from area where city_id = d_city_id Order by area_id desc limit 1) as d_city_name','(select nd_id from distributor where did = d_id) as ndid','(select nd_code from ndistributor where ndid = nd_id) as nd_code','(select firmname from ndistributor where ndid = nd_id) as nd_firnname','(select state from ndistributor where ndid = nd_id) as nd_state_id','(select state_name from area where state_id = nd_state_id Order by area_id desc limit 1) as nd_state_name');
		$where = array('status !=' => '2');
		
		//Pagination Start
		$config = array();
		$config["base_url"] = site_url() . "/Adminmaster/retailer_list";
		$config["total_rows"] = $this->base_models->get_count('rt_id','retailer', $where);
		$config["per_page"] = 10;
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$pagedata["links"] = $this->pagination->create_links();
		$pagedata['results'] = $this->adminmaster_models->retailer_list($select,'retailer', $where,'rt_id',$config["per_page"], $page); 
		//echo '<pre>';print_r($pagedata['results']); die;
		//Pagination End
		
		$this->renderView('Admin/Master/retailer_list',$pagedata);
	}
	
	public function retailer_list_sess()
	{
		$this->load->model("adminmaster_models");
		$select = array('rt_id','rt_code','firmname as rt_firmname','tsm_id','inserted_on','(select atsm_code from atsm where atsm_id = tsm_id Order by atsm_id desc limit 1) as rt_tsm_code','(select fname from atsm where atsm_id = tsm_id Order by atsm_id desc limit 1) as rt_tsm_fname','(select lname from atsm where atsm_id = tsm_id Order by atsm_id desc limit 1) as rt_tsm_lname','(select taluka_name from area where taluka = taluka_id Order by area_id desc limit 1) as rt_taluka_name','d_id as did','(select d_code from distributor where did = d_id) as d_code','(select firmname from distributor where did = d_id) as d_firnname','(select city from distributor where did = d_id) as d_city_id','(select city_name from area where city_id = d_city_id Order by area_id desc limit 1) as d_city_name','(select nd_id from distributor where did = d_id) as ndid','(select nd_code from ndistributor where ndid = nd_id) as nd_code','(select firmname from ndistributor where ndid = nd_id) as nd_firnname','(select state from ndistributor where ndid = nd_id) as nd_state_id','(select state_name from area where state_id = nd_state_id Order by area_id desc limit 1) as nd_state_name','status');
		$where = array('status !=' => '2');
		
		$rt_code = null;
		$filter = array();
		//Filter Process	
		if(@$_POST['submit']=='filter' || @$_POST['submit']=='createxls'){
			$rt_code_post = (@$this->input->post('rt_code')) ? $this->input->post('rt_code') : '';
			$rt_code = trim($rt_code_post);
			$array_items = $this->session->set_userdata(array("rt_code"=>$rt_code));
		}else{
			if($this->session->userdata('rt_code') != NULL){
				$rt_code = $this->session->userdata('rt_code');
			}
		}
		if($rt_code != null){
			$filter = array("rt_code"=>$rt_code);
		}
		$where = array_merge($where,$filter);
		
		 
		if(@$_POST['submit']=='createxls'){
			$data['data'] = $this->adminmaster_models->retailer_list($select,'retailer', $where,'rt_id'); 
			$this->generate_retailer_list_excel($data['data']);		
		}
		//End Filter Process
	
		//Pagination Start
		$config = array();
		$config["base_url"] = site_url() . "/Adminmaster/retailer_list_sess";
		$config["total_rows"] = $this->base_models->get_count('rt_id','retailer', $where);
		$config["per_page"] = 10;
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$pagedata["links"] = $this->pagination->create_links();
		$pagedata['results'] = $this->adminmaster_models->retailer_list($select,'retailer', $where,'rt_id',$config["per_page"], $page); 	
		//Pagination End
		$pagedata['delete_link'] = 'Dmaster/delete_rt';
		$rt_code = (@$rt_code) ? $rt_code : '';
		$pagedata['select']=array('rt_code'=>$rt_code);  
		$this->renderView('Admin/Master/retailer_list',$pagedata);
	}
	
	//generate to excel	
	public function generate_retailer_list_excel($param1){
		// create file name
		$fileName = 'Retailerslist'.'-data-'.date('d-M-Y').'.xlsx';   
		// load excel library
		$this->load->library('excel');
		$info = $param1;
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->setActiveSheetIndex(0);
		
		// set Header		
		$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Retailer Code');
		$objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Retailer FirmName');
		$objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Retailer Taluka');
		$objPHPExcel->getActiveSheet()->SetCellValue('D1', 'TSM Code');
		$objPHPExcel->getActiveSheet()->SetCellValue('E1', 'TSM Name');
		$objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Distributor Code');
		$objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Distributor FirmName');
		$objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Distributor City');
		$objPHPExcel->getActiveSheet()->SetCellValue('I1', 'ND Code');
		$objPHPExcel->getActiveSheet()->SetCellValue('J1', 'ND FirmName');
		$objPHPExcel->getActiveSheet()->SetCellValue('K1', 'ND State');
		$objPHPExcel->getActiveSheet()->SetCellValue('L1', 'Status');
		$objPHPExcel->getActiveSheet()->SetCellValue('M1', 'Created Date');
		
		// set Row
		$rowCount = 2;
	
		foreach ($info as $element) {
			$objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $element['rt_code']);
			$objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount,  $element['rt_firmname']);
			$objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $element['rt_taluka_name']);
			$objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $element['rt_tsm_code']);
			$objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $element['rt_tsm_fname'].' '.$element['rt_tsm_lname']);
			$objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $element['d_code']);
			$objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $element['d_firnname']);
			$objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $element['d_city_name']);
			$objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $element['nd_code']);
			$objPHPExcel->getActiveSheet()->SetCellValue('J' . $rowCount, $element['nd_firnname']);
			$objPHPExcel->getActiveSheet()->SetCellValue('K' . $rowCount, $element['nd_state_name']);
			$status = ($element['status'] == '1') ? 'Active' : 'Inactive';
			$objPHPExcel->getActiveSheet()->SetCellValue('L' . $rowCount, $status);
			$objPHPExcel->getActiveSheet()->SetCellValue('M' . $rowCount, date('d-M-Y', strtotime($element['inserted_on'])));
			$rowCount++;
		}
		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
		$objWriter->save('uploads/admin/excel/'.$fileName);
		// download file
		header("Content-Type: application/vnd.ms-excel");
		redirect(base_url('uploads/admin/excel/'.$fileName));
	}
	//---------- END Retailer ------------//	
	
	//---------- Distributor ------------//
	public function distributor()
	{	
		$this->load->model("adminmaster_models");
		$select = array('d_id','d_code','firmname','fname','mname','lname','lname','username','email','email2','contact','contact2','pan_no','gst_no','status','state','city','address','acnt_name','acnt_email','acnt_contact','inserted_on','updated_on','(select date_time from recent_login_user where user_code = d_code AND type = "1" Order by id desc limit 1) as last_login','(select state_name from area where state = state_id Order by area_id desc limit 1) as state_name','(select city_name from area where city = city_id Order by area_id desc limit 1) as city_name');
		$where = array('status !=' => '2');
		
		//Pagination Start
		$config = array();
		$config["base_url"] = site_url() . "/Adminmaster/distributor";
		$config["total_rows"] = $this->base_models->get_count('d_id','distributor', $where);
		$config["per_page"] = 10;
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$pagedata["links"] = $this->pagination->create_links();
		$pagedata['results'] = $this->adminmaster_models->distributor_list($select,'distributor', $where,'d_id',$config["per_page"], $page);		
		//Pagination End
	
		$pagedata['delete_link'] = 'Ndmaster/delete_d';
		$this->renderView('Admin/Master/distributor',$pagedata);
	}
	
	public function distributor_list()
	{		
		$this->load->model("adminmaster_models");		
		$select = array('d_id','d_code','firmname as d_firmname','(select city_name from area where city = city_id Order by area_id desc limit 1) as d_city_name','nd_id as nid','(select nd_code from ndistributor where nid = nd_id) as nd_code','(select firmname from ndistributor where nid = nd_id) as nd_firnname','(select state from ndistributor where nid = nd_id) as nd_state','(select state_name from area where state_id = nd_state Order by area_id desc limit 1) as nd_state_name');
		$where = array('status !=' => '2');
		
		//Pagination Start
		$config = array();
		$config["base_url"] = site_url() . "/Adminmaster/distributor_list";
		$config["total_rows"] = $this->base_models->get_count('d_id','distributor', $where);
		$config["per_page"] = 10;
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$pagedata["links"] = $this->pagination->create_links();
		$pagedata['results'] = $this->adminmaster_models->distributor_list($select,'distributor', $where,'d_id',$config["per_page"], $page);     
		
		//Pagination End
		$this->renderView('Admin/Master/distributor_list',$pagedata);
	}
	//---------- END Distributor ------------//
}
