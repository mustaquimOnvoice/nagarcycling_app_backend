<?php
require_once APPPATH . 'core/Base_Controller.php'; //Load Base Controller
defined('BASEPATH') OR exit('No direct script access allowed');

class Adminstock extends Base_Controller 
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Kolkata');
		$this->load->model("adminstock_model"); // load model
		if(!$this->session->userdata('__ci_last_regenerate') || $this->session->userdata('user_type') != 1){
			$this->session->set_flashdata('error', 'You Are not Allowed to access this file...!');
			redirect('login');
		}
	}
	
	//generate to excel	
	public function generate_stock_excel($param1){
		// create file name
		$fileName = 'StockReport'.'-data-'.date('d-M-Y').'.xlsx';   
		// load excel library
		$this->load->library('excel');
		$info = $param1;
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->setActiveSheetIndex(0);
		// set Header
		$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Code');
		$objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Firm Name');
		$objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Level');
		$objPHPExcel->getActiveSheet()->SetCellValue('D1', 'IMEI');
		$objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Item Code');
		$objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Date');
		// set Row
		$rowCount = 2;
	
		foreach ($info as $element) {
			$objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $element['code']);
			$objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $element['firmname']);
			$objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $element['level']);
			$objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $element['imei']);
			$objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $element['item_code']);
			$objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, date('d-M-Y', strtotime($element['stock_date'])));
			$rowCount++;
		}
		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
		$objWriter->save('uploads/admin/excel/'.$fileName);
		// download file
		header("Content-Type: application/vnd.ms-excel");
		redirect(base_url('uploads/admin/excel/'.$fileName));
	}		

	public function admin()
	{			
		$imei = null;
		if($this->session->userdata('imei')){
			$this->session->userdata('imei');
		}
	
	  //Pagination Start
		$config = array();
		$config["base_url"] = site_url() . "/Adminstock/admin";
		$config["total_rows"] = $this->adminstock_model->get_all_stock_count();
		$config["per_page"] = 10;
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$pagedata["links"] = $this->pagination->create_links();
		$pagedata['results'] = $this->adminstock_model->get_all_stock($imei,$config["per_page"], $page); 

		//Pagination End
		$imei = (@$imei) ? $imei : '';
		$pagedata['select']=array('imei'=>$imei);  
		$this->renderView('Admin/Stock/admin',$pagedata);
	}
		
	// with ci pagination in php
	public function admin_sess()
	{
		$imei = null;
		
		//Filter Process	
		if(@$_POST['submit']=='filter' || @$_POST['submit']=='createxls'){
			$imei_no = (@$this->input->post('imei')) ? $this->input->post('imei') : '';
			$imei = trim($imei_no);
			$array_items = $this->session->set_userdata(array("imei"=>$imei));
		}else{
			if($this->session->userdata('imei') != NULL){
				$imei = $this->session->userdata('imei');
			}
		}
		
		if(@$_POST['submit']=='createxls'){
			$data['data'] = $this->adminstock_model->get_all_stock();
			// die($this->db->last_query());
			$this->generate_stock_excel($data['data']);		
		}
		//End Filter Process
	
		//Pagination Start
		$config = array();
		$config["base_url"] = site_url() . "/Adminstock/admin_sess";
		$config["total_rows"] = $this->adminstock_model->get_all_stock_count($imei);
		$config["per_page"] = 10;
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$pagedata["links"] = $this->pagination->create_links();
		$pagedata['results'] = $this->adminstock_model->get_all_stock($imei,$config["per_page"], $page);  
		//Pagination End

		$imei = (@$imei) ? $imei : '';
		$pagedata['select']=array('imei'=>$imei);  
		$this->renderView('Admin/Stock/admin',$pagedata);
	}	
		
}
