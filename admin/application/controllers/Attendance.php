<?php
require_once APPPATH . 'core/Base_Controller.php';
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class attendance extends Base_Controller {
	public function __construct() {
		parent::__construct ();
		if(!$this->session->userdata('__ci_last_regenerate') || $this->session->userdata('user_type') != 1){
			$this->session->set_flashdata('error', 'You Are not Allowed to access this file...!');
			redirect('login');
		}
		$this->load->model('api_model');		
    }

    function index(){
		$atsm_id = trim(base64_decode($_GET['id']));
		$atsm_code = trim(base64_decode($_GET['code']));
		$year_month = (@$_GET['date']) ? trim($_GET['date']) : date('d-M-Y');		
		//if get date is greater than todays date then take todays date
		if(strtotime($year_month) > strtotime(date('d-M-Y'))){ 
			$year_month = date('d-M-Y');
		}
		
		// if empty redirect to ATSM list
		if(empty($atsm_id) || empty($atsm_code) || empty($year_month)){
			redirect('Ndmaster/atsm');
		}
		
		$response['today_date'] = $year_month;
		$response['lastday'] = date('d',strtotime($year_month));
		$year = date('Y',strtotime($year_month));
		$month = date('m',strtotime($year_month));
		$lastday = $response['lastday'];
		
		if(!empty($atsm_id) && !empty($atsm_code) && !empty($year_month)){			
			$response ['message'] = "done";
			$response ['result'] =  "Monthly attendence";
			
			$u=1;//pending
			$v=1;//half day
			$w=1;//present false
			$y=1;//present true (total_absent)
			$z=1;//weekoff
			$response['total_pending'] = 0;
			$response['total_half_day'] = 0;
			$response['total_present'] = 0;
			$response['total_absent'] = 0;
			$response['total_weekoff'] = 0;
			$att = array();
			for ($x = 1; $x <= $lastday; $x++) {
			    $attendance[$x]['pm_time'] = '00:00:00';
				$attendance_date = "$year-$month-$x";
				$attendance[$x]= $this->api_model->check_tsm_attendance('atsm_attendance',$atsm_code,$attendance_date);
				// $attendance[$x]['date'] = strtotime($attendance_date.' '.$attendance[$x]['am_time'])*1000;
				$attendance[$x]['date'] = $attendance_date;
				$attendance[$x]['half_day'] = 'false';
				$attendance[$x]['pending'] = 'false';
				
				if($attendance[$x]['absent'] == 'false' && $attendance[$x]['weekoff'] == 'false'){
					$last_time = date('H:i:s',strtotime($attendance[$x]['am_time']) + 60*60*8); //check 8hrs if not half day
					$last_time_to_absent = date('H:i:s',strtotime($attendance[$x]['am_time']) + 60*60*5); //check 5hrs if not half day
					
					if(!empty($attendance[$x]['pm_time'])){
						if($attendance[$x]['pm_time'] < $last_time_to_absent){// check if not compected 5hrs 
							$attendance[$x]['absent'] = 'true';
						}elseif($attendance[$x]['pm_time'] < $last_time || $attendance[$x]['am_time'] > '11:00:00'){//half day - check if not compected 8hrs | login after 11am
							$response['total_half_day'] = $v++;
							$attendance[$x]['half_day'] = 'true';
						}elseif($attendance[$x]['half_day'] == 'false'){//present day
							$response['total_present'] = $w++;
						}
					}
					
					// if($attendance[$x]['half_day'] == 'false'){//present day
						// $response['total_present'] = $w++;
					// }
					
					if($attendance[$x]['pm_time'] == '' && !empty($attendance[$x]['am_time'])){// check in but not check out 
						$response['total_pending'] = $u++;
						$attendance[$x]['pending'] = 'true';
					}
				}
				if($attendance[$x]['absent'] == 'true'){
					$response['total_absent'] = $y++;
				}
				if($attendance[$x]['weekoff'] == 'true'){
					$response['total_weekoff'] = $z++;
				}
				array_push($att,$attendance[$x]);
			}
				$response['attendance'] = $att;
		}		
		$response['name'] = trim(base64_decode($_GET['name']));
		$response['atsm_id'] = trim(base64_decode($_GET['id']));
		$response['atsm_code'] = trim(base64_decode($_GET['code']));
		$response['month'] = $month;
		$response['year'] = $year;
		$this->renderView('Admin/Attendance/attendance_monthly',$response);		
		// $this->load->view('Admin/Attendance/attendance',$pagedate);
    }
	
	function attendance_black(){
		$this->load->view('Admin/Attendance/attendance_black');
	}
}
?>