<?php
require_once APPPATH . 'core/Base_Controller.php'; //Load Base Controller
defined('BASEPATH') OR exit('No direct script access allowed');

class Dactivate extends Base_Controller 
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Kolkata');
		$this->load->model("d_activate_models"); // load nd sales models 
		if(!$this->session->userdata('__ci_last_regenerate') || $this->session->userdata('user_type') != 4){
			$this->session->set_flashdata('error', 'You Are not Allowed to access this file...!');
			redirect('login');
		}
	}
	// Start Singel IMEI Activation
	public function single_imei()
	{	
		$d_id =	 $this->session->userdata('id');
	
	
		$imei ='';	
		if($this->session->userdata('imei') ){
			$this->session->userdata('imei');
		}
		//Pagination Start
		$config = array();
		$config["base_url"] = site_url() . "/Dactivate/single_imei";
		$config["total_rows"] = $this->d_activate_models->get_single_imei_rt_cnt($d_id);

		$config["per_page"] = 10;
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$pagedata["links"] = $this->pagination->create_links();
		$pagedata['results'] = $this->d_activate_models->get_single_imei_rt($d_id,$config["per_page"], $page);     
		//print_r($pagedata["links"]);die;
		//Pagination End
			
		$imei = (@$imei) ? $imei : '';
		$pagedata['select']=array('imei'=>$imei);  
		$this->renderView('Distributor/Activation/single_activation',$pagedata);
	}

	// with ci pagination in php
	public function single_imei_sess()
	{
	   $d_id =	 $this->session->userdata('id');
	
	
		$imei ='';	
		if($this->session->userdata('imei') ){
			$this->session->userdata('imei');
		}
		//Pagination Start
		$config = array();
		$config["base_url"] = site_url() . "/Dactivate/single_imei_sess";
		$config["total_rows"] = $this->d_activate_models->get_single_imei_rt_cnt($d_id);
		$config["per_page"] = 10;
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$pagedata["links"] = $this->pagination->create_links();
		$pagedata['results'] = $this->d_activate_models->get_single_imei_rt($d_id,$config["per_page"], $page);     
		//print_r($pagedata["links"]);die;
		//Pagination End
			
		$imei = (@$imei) ? $imei : '';
		$pagedata['select']=array('imei'=>$imei);  
		$this->renderView('Distributor/Activation/single_activation',$pagedata);
	}	

	public function updatesingleimei()
	{
		$current_date = date("Y-m-d H:i:s");
		$cilent_name = $this->input->post('cilent_name');
		$cilent_mob = $this->input->post('cilent_mob');
		$imei_code = trim($this->input->post('imei_code'));
		$d_id = $this->session->userdata('id');
		$this->db->trans_begin();// transaction start
		
			$cl         = $this->db->query("select c_id,c_code from client where contact='$cilent_mob'");
			$client_array = $cl->result_array();
			if(count($client_array)>0){ // checks mobile number register or not
				$c_id = $client_array[0]['c_id'];
				$c_code = $client_array[0]['c_code'];
			}
			else{
				$cldata  = array
				(
				'd_id' 		  	=> $d_id,
				'fname'   			=> $cilent_name,
				'contact'   		=> $cilent_mob,
				'inserted_on'   	=> date('Y-m-d h:i:s')
				);
			$this->db->insert("client",$cldata);
			$cl   	      = $this->db->query("select c_id,c_code from client where contact='$cilent_mob'");
			$client_array = $cl->result_array();
			$c_id = $client_array[0]['c_id'];
			$c_code = $client_array[0]['c_code'];
			}
		
		//insert tbl_sales_to_c
		$rs         = $this->db->query("select item_id,item_code from tbl_items where imei='$imei_code'");
		$item_array = $rs->result_array();
		$item_id = $item_array[0]['item_id'];
		$item_code  = $item_array[0]['item_code'];
		$date =	date('Y-m-d h:i:s');

		$data  = array('item_id'  	=> $item_id,
					'item_code	'   => $item_code,
					'imei'   		=> $imei_code,
					'c_id'   		=> $c_id,
					'c_code'    	=> $c_code,
					'upload_date'   => $date,
					'inserted_on'   => date('Y-m-d h:i:s')
					);
		$this->db->insert("tbl_sales_to_c",$data);
		
		//update tbl_item_sales
		$update_array	=	array(
			'level_type'  	=> '4',
			'c_id'      => $c_id,
			'c_code'	  => $c_code,
			'c_date'   => $date,
			'updated_on'	=> date("Y-m-d H:i:s")
			);
		$where_array	=	array('imei'=> $imei_code);
		$this->base_models->update_records('tbl_item_sales',$update_array,$where_array);
		
		//update tbl_sales_to_rt
		$update_array = array(
						'item_status'=>'1',
						'updated_on'=>$current_date
						);
		$where_array = array('imei'=>$imei_code);
		$this->base_models->update_records('tbl_sales_to_rt',$update_array,$where_array);
		
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback(); //rollback
			$data['status'] = 'fail';
		}else{
			$this->db->trans_commit(); //commit
			$data['status'] = 'success';
		}
		echo json_encode($data);
			
	}
 // End Singel IMEI Activation

   public function sale_to_rt()
	{
			$select	 = array('rt_id','rt_code','firmname');
			$where = array('status !=' => '2','d_id' => $this->session->userdata('id'));
			$data['data'] = $this->d_sales_models->GetAllItemValues('retailer', $where, $select);
		$this->renderView('Distributor/Sale/sale_items',$data);
	}

   public function activation()
	{
		$this->renderView('Distributor/Activation/activation_excel_form');
	}
	
	public function upload_activation_excel()
	{
		$d_id =	 $this->session->userdata('id');	
		$this->load->library('excel');
		
		$rt = $this->db->query("select rt_code from retailer where d_id='$d_id'");
		$array_rt = $rt->result_array();
 		$rt_array = array_column($array_rt, 'rt_code');	 // for converting in 1 array format
        $rt_array1 =  array_flip($rt_array);
				if (isset($_POST)) 
				{
					//print_r($_FILES); die;
				   $path = $_FILES["acivation_excel"]["tmp_name"];
				   $object = PHPExcel_IOFactory::load($path);
				   foreach($object->getWorksheetIterator() as $worksheet)
				   {
					   $highestRow = $worksheet->getHighestRow();
					   $highestColumn = $worksheet->getHighestColumn();
					  
					    for($row=2; $row<=$highestRow; $row++)
					    {
							$rt_code1 		= $worksheet->getCellByColumnAndRow(0, $row)->getValue();
							$imei 		= $worksheet->getCellByColumnAndRow(1, $row)->getValue(); 
							$cname1 			= $worksheet->getCellByColumnAndRow(2, $row)->getValue();
							$cnum1 			= $worksheet->getCellByColumnAndRow(3, $row)->getValue();
							$rt_code	=	trim($rt_code1);
							if(array_key_exists($rt_code, $rt_array1))
								{
									$imei_code  =	trim($imei); // remove spaces from both sides of string
									$rt_code  =	trim($rt_code1); // remove spaces from both sides of string
									$cname  =	trim($cname1); // remove spaces from both sides of string
									$cnum  =	trim($cnum1); // remove spaces from both sides of string
									$length = strlen($imei_code);
										if($length > '10'){
											$sql 		= $this->db->query("select imei from tbl_sales_to_rt where rt_code='$rt_code'");
											$array_item = $sql->result_array();
									 		$item_array = array_column($array_item, 'imei');	 // for converting in 1 array format
									        $item_array1=  array_flip($item_array);

											$rs = $this->db->query("select imei from tbl_sales_to_rt where item_status='1'");
											$array = $rs->result_array();
											$imei_array = array_column($array, 'imei');	 // for converting in 1 array format
									        $imei_array1 =  array_flip($imei_array);

											if(array_key_exists($imei_code, $item_array1)) // for item is in rt list or not
											{
											   	if(array_key_exists($imei_code, $imei_array1))
												{
													$sold[] = array
														(
															
															'imei'   				=> $imei_code
																		
														);
												}else{
															$cl         = $this->db->query("select c_id,c_code from client where contact='$cnum'");
															$client_array = $cl->result_array();
															if(count($client_array)>0){ // checks mobile number register or not
																$c_id = $client_array[0]['c_id'];
																$c_code = $client_array[0]['c_code'];
															}
															else{
																		$cldata  = array
																		(
																			'd_id' 		  	=> $d_id,
																			'fname'   			=> $cname,
																			'contact'   		=> $cnum,
																			'inserted_on'   	=> date('Y-m-d h:i:s')
																		);
																		$this->db->insert("client",$cldata);
																		$cl   	      = $this->db->query("select c_id,c_code from client where contact='$cnum'");
																		$client_array = $cl->result_array();
																		$c_id = $client_array[0]['c_id'];
																		$c_code = $client_array[0]['c_code'];
																}	
															
															$rs         = $this->db->query("select item_id,item_code from tbl_items where imei='$imei_code'");
															$item_array = $rs->result_array();
															$item_id 	= 	$item_array[0]['item_id'];
															$item_code  = 	$item_array[0]['item_code'];
															$date 	 =	date('Y-m-d h:i:s');
															
															$data[]  = array
																(
																	'item_id'  			=> $item_id,
																	'item_code	'   	=> $item_code,
																	'imei'   			=> $imei_code,
																	'c_id'   			=> $c_id,
																	'c_code'    		=> $c_code,
																	'upload_date'   	=> $date,
																	'inserted_on'   	=> date('Y-m-d h:i:s')
																);
													  }
											}else{
													$item_not_in_rt_list[] = array
														(
															
															'imei' => $imei_code
																		
														);
												 }	
										}else{
												$invalid_imei_length[] = array
														(
															
															'imei' => $imei_code
																		
														);
											 }	
												   
								}else{
								
										$invalid_rt[] = array
														(
															
															'imei' => $imei
																		
														);
									}
					}
		}
					$this->db->trans_begin(); //trans start
					if(isset($data))
					{
						foreach ($data as $row)
							{	 
							     if($row['imei']!='')
								 {	 
								 	$this->db->insert("tbl_sales_to_c",$row);
								 
								 	
								 	$update_array	=	array(
										'level_type'  		=> '4',
										'c_id'   		    => $row['c_id'],
										'c_code'	   		=> $row['c_code'],
										'c_date'   			=> $row['upload_date'],
										'updated_on'		=> date("Y-m-d H:i:s")
									);
									$where_array	=	 array('imei'=> $row['imei']);
									$this->base_models->update_records('tbl_item_sales',$update_array,$where_array);
									$update_array1	=	array(
										'item_status'  		=> '1',
										'updated_on'		=> date("Y-m-d H:i:s")
									);
									$this->base_models->update_records('tbl_sales_to_rt',$update_array1,$where_array);	
								 }
							}	
					}		 
						if ($this->db->trans_status() === FALSE){
					    $this->db->trans_rollback(); //rolback
					    $page_data['status'] = 'Something went wrong please try again..!';
					    $page_data['message'] = $this->db->_error_message();
					    $this->session->set_flashdata('error','Something went wrong please try again..!');
					}else{
					    $this->db->trans_commit(); //commit
					    $page_data['status'] = 'Query run successfully';
					    //  $this->session->set_flashdata('success','Query run successfully');
					}
						if(isset($data))
						{    	
							    $page_data['acceptedprodct']	=	$data;
							
						}

						if(isset($sold))
						{    	
							    $page_data['sold']	=	$sold;
							
						}
						if(isset($item_not_in_rt_list))
						{    	
							    $page_data['itemnotinlist']	=	$item_not_in_rt_list;
							
						}
						if(isset($invalid_imei_length))
						{    	
							    $page_data['invalidimei']	=	$invalid_imei_length;
							
						}
					 	if(isset($invalid_rt))
						{    	
							    $page_data['invalid_rt']	=	$invalid_rt;
							
						}
						/*echo '<pre>';
						print_r($page_data); die;*/
					 	$this->renderView('Distributor/Activation/activation_excel_form',$page_data);
				}
	}
}
