<?php
require_once APPPATH . 'core/Base_Controller.php'; //Load Base Controller
defined('BASEPATH') OR exit('No direct script access allowed');

class Dmaster extends Base_Controller 
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Kolkata');
		
		if(!$this->session->userdata('__ci_last_regenerate') || $this->session->userdata('user_type') != 1 && $this->session->userdata('user_type') != 2 && $this->session->userdata('user_type') != 4){
			$this->session->set_flashdata('error', 'You Are not Allowed to access this file...!');
			redirect('login');
		}
	}
	
	//---------- Retailer ------------//
	//generate to excel	
	public function generate_retailer_excel($param1){
		// create file name
		$fileName = 'RetailerList'.'-data-'.date('d-M-Y').'.xlsx';   
		// load excel library
		$this->load->library('excel');
		$info = $param1;
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->setActiveSheetIndex(0);
		// set Header
		$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'RT Code');
		$objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Firm Name');
		$objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Name');
		$objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Dealer Code');
		$objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Primary email');
		$objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Secondary email');
		$objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Primary Contact');
		$objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Secondary Contact');
		$objPHPExcel->getActiveSheet()->SetCellValue('I1', 'PAN');
		$objPHPExcel->getActiveSheet()->SetCellValue('J1', 'GST');
		$objPHPExcel->getActiveSheet()->SetCellValue('K1', 'State');
		$objPHPExcel->getActiveSheet()->SetCellValue('L1', 'City');
		$objPHPExcel->getActiveSheet()->SetCellValue('M1', 'Taluka');
		$objPHPExcel->getActiveSheet()->SetCellValue('N1', 'Address');
		$objPHPExcel->getActiveSheet()->SetCellValue('O1', 'TSM');
		// set Row
		$rowCount = 2;
	
		foreach ($info as $element) {
			$objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $element['rt_code']);
			$objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $element['firmname']);
			$objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $element['fname'].' '.$element['mname'].' '.$element['lname']);
			$objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $element['d_code']);
			$objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $element['email']);
			$objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $element['email2']);
			$objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $element['contact']);
			$objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $element['contact2']);
			$objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $element['pan_no']);
			$objPHPExcel->getActiveSheet()->SetCellValue('J' . $rowCount, $element['gst_no']);
			$objPHPExcel->getActiveSheet()->SetCellValue('K' . $rowCount, $element['state_name']);
			$objPHPExcel->getActiveSheet()->SetCellValue('L' . $rowCount, $element['city_name']);
			$objPHPExcel->getActiveSheet()->SetCellValue('M' . $rowCount, $element['taluka_name']);
			$objPHPExcel->getActiveSheet()->SetCellValue('N' . $rowCount, $element['address']);
			$objPHPExcel->getActiveSheet()->SetCellValue('O' . $rowCount, $element['atsm_code']);
			$rowCount++;
		}
		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
		$objWriter->save('uploads/admin/excel/'.$fileName);
		// download file
		header("Content-Type: application/vnd.ms-excel");
		redirect(base_url('uploads/admin/excel/'.$fileName));
	}
	
	public function retailer()
	{		
		$this->load->model("adminmaster_models");
		$select = array('rt_id','rt_code','d_id as did','tsm_id','firmname','fname','mname','lname','username','email','email2','contact','contact2','pan_no','gst_no','status','state','city','address','acnt_name','acnt_email','acnt_contact','inserted_on','updated_on','(select atsm_code from atsm where tsm_id = atsm_id Order by atsm_id desc limit 1) as atsm_code','(select d_code from distributor where did = d_id Order by d_id desc limit 1) as d_code','(select firmname from distributor where did = d_id Order by d_id desc limit 1) as d_firmname','(select date_time from recent_login_user where user_code = rt_code AND type = "1" Order by id desc limit 1) as last_login','(select state_name from area where state = state_id Order by area_id desc limit 1) as state_name','(select city_name from area where city = city_id Order by area_id desc limit 1) as city_name','(select taluka_name from area where taluka = taluka_id Order by area_id desc limit 1) as taluka_name');
		$where = array('status !=' => '2','d_id' => $this->session->userdata('id'));
		
		if(@$_POST['submit']=='createxls'){
			$data['data'] = $this->adminmaster_models->retailer_list($select,'retailer', $where,'rt_id');
			$this->generate_retailer_excel($data['data']);
		}
		
		//Pagination Start
		$config = array();
		$config["base_url"] = site_url() . "/Dmaster/retailer";
		$config["total_rows"] = $this->base_models->get_count('rt_id','retailer', $where);
		$config["per_page"] = 10;
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$pagedata["links"] = $this->pagination->create_links();
		$pagedata['results'] = $this->adminmaster_models->retailer_list($select,'retailer', $where,'rt_id',$config["per_page"], $page);     
		//Pagination End
		$pagedata['delete_link'] = 'Dmaster/delete_rt';
		
		$this->renderView('Distributor/Master/retailer',$pagedata);
	}
	
	public function add_rt()
	{
		$pagedata['state_list']=$this->base_models->get_state();
		$pagedata['city_list']=$this->base_models->get_city();
		$pagedata['taluka_list']=$this->base_models->get_taluka_by_state_and_city($this->session->userdata('state'),$this->session->userdata('city'));
		$pagedata['state_id']=$this->session->userdata('state');
		$pagedata['city_id']=$this->session->userdata('city');
		$pagedata['tsm_list']=$this->base_models->GetAllValues('atsm', array('level_type' => '1'), array('atsm_id','atsm_code','fname','lname'));
		$this->renderView('Distributor/Master/add-rt',$pagedata);
	}
	
	public function insert_rt()
	{
		$this->form_validation->set_rules('firmname', 'Firm name', 'trim|required');
		$this->form_validation->set_rules('fname', 'First name', 'trim|required');
		$this->form_validation->set_rules('lname', 'Last Name', 'trim|required');
		$this->form_validation->set_rules('status', 'Status', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'trim|required');
		$this->form_validation->set_rules('contact', 'Mobile No.', 'trim|required|numeric');
		$this->form_validation->set_rules('state', 'State', 'trim|required');
		$this->form_validation->set_rules('taluka', 'Taluka', 'trim|required');
		$this->form_validation->set_rules('city', 'City', 'trim|required');
		$this->form_validation->set_rules('address', 'Address', 'trim|required');
		$this->form_validation->set_rules('pan', 'PAN', 'trim|required');
		$this->form_validation->set_rules('gst', 'GST', 'trim|required');
		$this->form_validation->set_rules('inputPassword', 'Password', 'trim|required');
		$current_date = date("Y-m-d H:i:s");
	
		$error='';
			// if (empty($_FILES['image']['name'][0])){
				// $this->form_validation->set_rules('image', 'Image', 'required');
			// }
			$data['file_name'] = '';
            // if(!empty($_FILES['image']['name'])){
                // $config['upload_path'] = 'uploads/nd/profile/';
                // $config['allowed_types'] = 'gif|jpg|png';
                // $this->upload->initialize($config);
				// if($this->upload->do_upload('image')){
					// $data = $this->upload->data();
				// }else{
                    // $imageerrors = $this->upload->display_errors();
					// $this->form_validation->set_message('image', $imageerrors);					
                // }
			// }
			
			if($this->form_validation->run())
			{					
				$insert_array=array(
						'firmname'=>$this->input->post('firmname'),
						'fname'=>$this->input->post('fname'),
						'mname'=>$this->input->post('mname'),
						'lname'=>$this->input->post('lname'),
						'status'=>$this->input->post('status'),
						'email'=>$this->input->post('email'),
						'email2'=>$this->input->post('email2'),
						'contact'=> $this->input->post('contact'),
						'contact2'=> $this->input->post('contact2'),
						'state'=> $this->input->post('state'),
						'taluka'=> $this->input->post('taluka'),
						'city'=> $this->input->post('city'),
						'address'=> $this->input->post('address'),
						'tsm_id'=> $this->input->post('tsm_id'),
						'pan_no'=>strtoupper($this->input->post('pan')),
						'gst_no'=>strtoupper($this->input->post('gst')),
						// 'acnt_name'=>$this->input->post('acnt_name'),
						// 'acnt_email'=>$this->input->post('acnt_email'),
						// 'acnt_contact'=>$this->input->post('acnt_contact'),
						'password'=>md5($this->input->post('inputPassword')),
						// 'profile_pic'=>$data['file_name'],
						'd_id'=>$this->session->userdata('id'),
						'inserted_on'=>date("Y-m-d H:i:s")
					);
					//print_r($insert_array);exit;
					if($this->base_models->add_records('retailer',$insert_array)){
						$this->session->set_flashdata('success','Added successfully');
						redirect(site_url('Dmaster/retailer'));
					}else{
						$this->session->set_flashdata('error','Not added Please try again');
						//redirect(base_url('admin/add_user'));
					}
			}
				$this->renderView('Distributor/Master/add-rt');
	}
	
	public function edit_rt()
	{
		$id = base64_decode($_GET['id']);
		$pagedata['data']=$this->base_models->GetSingleDetails('retailer', array('rt_id' => $id), $select = "*");
		$pagedata['state_list']=$this->base_models->get_state();
		$pagedata['city_list']=$this->base_models->get_city();
		if($this->session->userdata('user_type') == 1){
			$pagedata['taluka_list']=$this->base_models->get_taluka();
		}else{
			$pagedata['taluka_list']=$this->base_models->get_taluka_bycityid($this->session->userdata('city'));			
		}
		$pagedata['tsm_list']=$this->base_models->GetAllValues('atsm', array('level_type' => '1'), array('atsm_id','atsm_code','fname','lname'));
		$this->renderView('Distributor/Master/edit-rt',$pagedata);
	}
	
	public function update_rt()
	{
		$id = base64_decode($_GET['id']);		
		if($id==''){
			redirect(base_url('Dmaster/retailer')); 
		}
		$this->form_validation->set_rules('firmname', 'Firm name', 'trim|required');
		$this->form_validation->set_rules('fname', 'First name', 'trim|required');
		$this->form_validation->set_rules('lname', 'Last Name', 'trim|required');
		$this->form_validation->set_rules('status', 'Status', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'trim|required');
		$this->form_validation->set_rules('contact', 'Mobile No.', 'trim|required|numeric');
		$this->form_validation->set_rules('state', 'State', 'trim|required');
		$this->form_validation->set_rules('taluka', 'Taluka', 'trim|required');
		$this->form_validation->set_rules('city', 'City', 'trim|required');
		$this->form_validation->set_rules('address', 'Address', 'trim|required');
		$this->form_validation->set_rules('pan', 'PAN', 'trim|required');
		$this->form_validation->set_rules('gst', 'GST', 'trim|required');
		if($this->input->post('inputPassword') != ''){
			$password = md5($this->input->post('inputPassword'));
		}else{
			$password = $this->input->post('oldpassword');
		}
		$current_date = date("Y-m-d H:i:s");
	
		$error='';
			// if (empty($_FILES['image']['name'][0])){
				// $this->form_validation->set_rules('image', 'Image', 'required');
			// }
			// $data['file_name'] = '';
            // if(!empty($_FILES['image']['name'])){
                // $config['upload_path'] = 'uploads/admin/users/';
                // $config['allowed_types'] = 'gif|jpg|png';
                // $this->upload->initialize($config);
				// if($this->upload->do_upload('image')){
					// $data = $this->upload->data();
				// }else{
                    // $imageerrors = $this->upload->display_errors();
					// $this->form_validation->set_message('image', $imageerrors);					
                // }
			// }
			
			// if($this->input->post('inputPassword') != ''){
				// $password = md5($this->input->post('inputPassword'));
			// }else{
				// $password = $this->input->post('oldpassword');
			// }	
			if($this->form_validation->run())
			{				
				$update_array=array(
						'firmname'=>$this->input->post('firmname'),
						'fname'=>$this->input->post('fname'),
						'mname'=>$this->input->post('mname'),
						'lname'=>$this->input->post('lname'),
						'status'=>$this->input->post('status'),
						'email'=>$this->input->post('email'),
						'email2'=>$this->input->post('email2'),
						'contact'=> $this->input->post('contact'),
						'contact2'=> $this->input->post('contact2'),
						'state'=> $this->input->post('state'),
						'taluka'=> $this->input->post('taluka'),
						'city'=> $this->input->post('city'),
						'address'=> $this->input->post('address'),
						'tsm_id'=> $this->input->post('tsm_id'),
						'pan_no'=>strtoupper($this->input->post('pan')),
						'gst_no'=>strtoupper($this->input->post('gst')),
						// 'acnt_name'=>$this->input->post('acnt_name'),
						// 'acnt_email'=>$this->input->post('acnt_email'),
						// 'acnt_contact'=>$this->input->post('acnt_contact'),
						'password'=>$password,
						// 'profile_pic'=>$data['file_name'],
						'updated_on'=>date("Y-m-d H:i:s")
					);
				$where_array = array('rt_id'=>$id);
				//print_r($insert_array);exit;
				if($this->base_models->update_records('retailer',$update_array,$where_array) == true){
					$this->session->set_flashdata('success','Edited successfully');
				}else{
					$this->session->set_flashdata('error','Not added Please try again');
				}
			}
				redirect(site_url('Dmaster/edit_rt?id='.base64_encode($id)));
			
		// $pagedata['data']=$this->base_models->get_users('',$id);
		// $this->renderView('Admin/edit-user',$pagedata);
	}
			
	public function delete_rt()
	{
		$id = $_GET['id'];
		$current_date = date("Y-m-d H:i:s");
		$update_array = array(
							'status'=>'2',
							'deleted_on'=>$current_date
							);
		$where_array = array('rt_id'=>$id);
		if($this->base_models->update_records('retailer',$update_array,$where_array) == true){
			$data['status'] = 'success';
			$data['message'] = 'Successfully deleted';
		}else{
			$data['status'] = 'error';
			$data['message'] = 'Somting went worng please try again';
		}
		echo json_encode($data);
		die();
	}
	
	public function change_status()
	{
		$current_date = date("Y-m-d H:i:s");
		$update_array = array(
						$_POST['col']=>$_POST['status'],
						'updated_on'=>$current_date
						//'lastUpdateUser'=>$this->session->userdata('id')
						);
		$where_array = array('rt_id'=>$_POST['id']);
		
		if($this->base_models->update_records('retailer',$update_array,$where_array)){
			echo "success";
			// $this->session->set_flashdata('success','Status change successfully');
		}else{
			// $this->session->set_flashdata('error','Error while changing status');
			echo "failed";
		}
	}
	//---------- END D ------------//	
		
}
