<?php
require_once APPPATH . 'core/Base_Controller.php'; //Load Base Controller
defined('BASEPATH') OR exit('No direct script access allowed');

class Dsales extends Base_Controller 
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Kolkata');
		$this->load->model("d_sales_models"); // load nd sales models 
		if(!$this->session->userdata('__ci_last_regenerate') || $this->session->userdata('user_type') != 4){
			$this->session->set_flashdata('error', 'You Are not Allowed to access this file...!');
			redirect('login');
		}
	}
public function faulty_return()
	{		
		$this->renderView('Distributor/Sale/faulty_return');
	}
	public function return_faulty_items()
	{	
		
		$all_imei 	= $_POST['imei'];
		$reason 	= $_POST['reason'];
		$arr_imei 	= preg_split('/\r\n|[\r\n]/', $all_imei);
		
		$d_id =$this->session->userdata('id');
		      // array For product is in the listof d  or not 
				$rs = $this->db->query("select imei from tbl_sales_to_d where d_id='$d_id'");
				$array = $rs->result_array();
				$imei_array = array_column($array, 'imei');	 // for converting in 1 array format
		        $imei_array1 =  array_flip($imei_array);
				
				// array for product sold 
		        $rsold = $this->db->query("select imei from tbl_sales_to_d where item_status='1'");
				$arraysold = $rsold->result_array();
				$sold_imei_array = array_column($arraysold, 'imei');	 // for converting in 1 array format
		        $sold_imei_array1 =  array_flip($sold_imei_array);
		           
		        // array for product is in faulty list or not 
		        $faulty = $this->db->query("select imei from tbl_item_faulty");
				$arrayfaulty = $faulty->result_array();
				$faulty_imei_array = array_column($arrayfaulty, 'imei');	 // for converting in 1 array format
		        $faulty_imei_array1 =  array_flip($faulty_imei_array);

				if (!empty($arr_imei)) 
				{
					$i=0;
				  foreach($arr_imei as $row)
				   {
					    $imei 	    = 	$arr_imei[$i]; 
						$imei_code  =	trim($imei); // remove spaces from both sides of string
						
						   if(array_key_exists($imei_code, $imei_array1))
							{
							
									if(array_key_exists($imei_code, $faulty_imei_array1))
									{
										$fault[] = array
										    (
												'imei' => $imei_code
											);
											 $this->session->set_flashdata('error','Given IMEI Number Already in Faulty List!');	
											 
									}
									else{	
											$d = $this->db->query("select d_code,nd_id from distributor where d_id='$d_id'");
											$d_array = $d->result_array();
											$d_code  = $d_array[0]['d_code'];
											$nd_id   = $d_array[0]['nd_id'];

											$nd = $this->db->query("select nd_code from ndistributor where nd_id='$nd_id'");
											$nd_array = $nd->result_array();
											$nd_code  = $nd_array[0]['nd_code'];
											$data[]   = array
												(
													'level_type' 		=>'1',
													'imei'   			=> $imei_code,
													'reason'   			=> $reason,
													'nd_id'   			=> $nd_id,
													'nd_code'   		=> $nd_code,
													'd_id'   			=> $d_id,
													'd_code'   			=> $d_code,
													'd_date'			=> date("Y-m-d H:i:s"),
													'inserted_on'		=> date("Y-m-d H:i:s")
												);
										}	
								
							}
							else
							{
								$notinlist[] = array
									(
										'imei'=> $imei_code
									);
								 $this->session->set_flashdata('error','Given IMEI Number Not In Your Sale List!');	
								
							}		  
						$i++;
					}
					
					$this->db->trans_begin(); //trans start
					if(!empty($data))
					{
						foreach ($data as $row)
							{	 
							     if($row['imei']!='')
								 {	
								 	$this->db->insert("tbl_item_faulty",$row);
								 }
							}	
							$this->session->set_flashdata('success','You Have Done Faulty Entry Successfully ');
					}

					if ($this->db->trans_status() === FALSE){
					    $this->db->trans_rollback(); //rolback
					    $page_data['status'] = 'Something went wrong please try again..!';
					    $page_data['message'] = $this->db->_error_message();
					    //$this->session->set_flashdata('error','Something went wrong please try again..!');
					}else{
					    $this->db->trans_commit(); //commit
					    $page_data['status'] = 'Query run successfully';
					      //$this->session->set_flashdata('success','You Have Done Faulty Entry Successfully ');
					}	

				$this->renderView('Distributor/Sale/faulty_return',$page_data);
				}
				
		}
	public function faulty_return_list_of_d()
	{	
		$select	 = array('if_id','imei','reason','d_date','nd_date','nd_status');
		$where = array('d_status ' => '1','d_id' => $this->session->userdata('id'));
		//$pagedata['delete_link'] = 'Adminitems/delete_item';
	
		
		//Pagination Start
		$config = array();
		$config["base_url"] = site_url() . "/Dsales/faulty_return_list_of_d";
		$config["total_rows"] = $this->base_models->get_count('if_id','tbl_item_faulty', $where);
		$config["per_page"] = 10;
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$pagedata["links"] = $this->pagination->create_links();
		$pagedata['results'] = $this->base_models->get_pagination('tbl_item_faulty', $where,'if_id',$config["per_page"], $page);     
		//Pagination End

		$imei = (@$imei) ? $imei : '';
		$pagedata['select']=array('imei'=>$imei);  
		$this->renderView('Distributor/Sale/faulty_return_list_of_d',$pagedata);
	}

	public function faulty_return_list()
	{	
		$d_id =	 $this->session->userdata('id');
	
	
		$imei ='';	
		if($this->session->userdata('imei') ){
			$this->session->userdata('imei');
		}
		//Pagination Start
		$config = array();
		$config["base_url"] = site_url() . "/Dsales/faulty_return_list";
		$config["total_rows"] = $this->d_sales_models->get_faulty_list_cnt($d_id);
		$config["per_page"] = 10;
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$pagedata["links"] = $this->pagination->create_links();
		$pagedata['results'] =  $this->d_sales_models->get_faulty_list($d_id,$config["per_page"], $page);     
		//echo'<pre>';print_r($pagedata["results"]);die;
		//Pagination End
			
		$imei = (@$imei) ? $imei : '';
		$pagedata['select']=array('imei'=>$imei);  
		$this->renderView('Distributor/Sale/faulty_list',$pagedata);
	}
     
    public function updatefaultyimeid()
	{	
		
		$imei 	= $_POST['imei'];
		$reason 	= $_POST['reason'];
		$d_status 	= $_POST['d_status'];
		$level_type	=	($_POST['d_status']=="1" ?  "1" : "2" );
		
		$d_id =$this->session->userdata('id');

		$d = $this->db->query("select nd_id from distributor where d_id='$d_id'");
		$d_array = $d->result_array();
		$nd_id 	 = $d_array[0]['nd_id'];

		$nd = $this->db->query("select nd_code from ndistributor where nd_id='$nd_id'");
		$nd_array = $nd->result_array();
		$nd_code  = $nd_array[0]['nd_code'];
		$update_array[]   = array
			(
				'level_type' 		=>$level_type,
				'imei'   			=> $imei,
				'reason'   			=> $reason,
				'nd_id'   			=> $nd_id,
				'nd_code'   		=> $nd_code,
				'd_status'   		=> $d_status,
				'd_date'			=> date("Y-m-d H:i:s")
			);
		
		$this->db->trans_begin(); //trans start
		if(!empty($update_array))
		{
			foreach ($update_array as $row)
			{	 
			    if($row['imei']!='')
				{	
					$where_array = array('imei'=>$row['imei']);
					$this->base_models->update_records('tbl_item_faulty',$row,$where_array);
				}
			}	
			
		}

		if ($this->db->trans_status() === FALSE){
		    $this->db->trans_rollback(); //rolback
		   	$data['status'] = 'fail';
		}else{
		    $this->db->trans_commit(); //commit
		  $data['status'] = 'success';
		}	

		echo json_encode($data);
				
	}

	// Sale Return By Excel
	public function sale_return_by_excel()
	{
		$this->renderView('Distributor/Sale/sale_return_by_excel');
	}
	public function upload_sale_return_by_excel()
	{
		$d_id =	 $this->session->userdata('id');	
		$this->load->library('excel');
		
		$rt = $this->db->query("select rt_code from retailer where d_id='$d_id'");
		$array_rt = $rt->result_array();
 		$rt_array = array_column($array_rt, 'rt_code');	 // for converting in 1 array format
        $rt_array1 =  array_flip($rt_array);
				
				if (isset($_POST)) 
				{
					//print_r($_FILES); die;
				   $path = $_FILES["sale_return"]["tmp_name"];
				   $object = PHPExcel_IOFactory::load($path);
				   foreach($object->getWorksheetIterator() as $worksheet)
				   {
					   $highestRow = $worksheet->getHighestRow();
					   $highestColumn = $worksheet->getHighestColumn();
					  
					    for($row=2; $row<=$highestRow; $row++)
					    {
							$rt_code1 		= $worksheet->getCellByColumnAndRow(0, $row)->getValue();
							$imei 			= $worksheet->getCellByColumnAndRow(1, $row)->getValue();
							$rt_code		= trim($rt_code1);
							if(array_key_exists($rt_code, $rt_array1))
								{
									$imei_code  =	trim($imei); // remove spaces from both sides of string
									$length = strlen($imei_code);
										if($length > '10'){
											 // array For product is in the listof d  or not 
											$rs = $this->db->query("select imei from tbl_sales_to_rt where rt_code='$rt_code'");
											$array = $rs->result_array();
											$imei_array = array_column($array, 'imei');	 // for converting in 1 array format
									        $imei_array1 =  array_flip($imei_array);
											
											// array for product sold 
									        $rsold = $this->db->query("select imei from tbl_sales_to_rt where item_status='1'");
											$arraysold = $rsold->result_array();
											$sold_imei_array = array_column($arraysold, 'imei');	 // for converting in 1 array format
									        $sold_imei_array1 =  array_flip($sold_imei_array);
												
												if(array_key_exists($imei_code, $imei_array1)) // for item is in d list or not
												{

												   	if(array_key_exists($imei_code, $sold_imei_array1))
													{
														$sold[] = array
															(
																
																'imei'   				=> $imei_code
																			
															);
													}else{
															$data[]  = array
																(
																	'imei'   			=> $imei_code
																);
														  }
												}else{
														$item_not_in_d_list[] = array
															(
																
																'imei' => $imei_code
																			
															);
													 }	
											}else{
													$invalid_imei_length[] = array
															(
																
																'imei' => $imei_code
																			
															);
												 }	
													   
									}else{
											$invalid_rt[] = array
															(
																
																'imei' => $imei
																			
															);
										}
					}
		}
						$this->db->trans_begin(); //trans start
					if(!empty($data))
					{
						foreach ($data as $row)
							{	 
							     if($row['imei']!='')
								 {	
								 	if($this->base_models->delete_records('tbl_sales_to_rt',array('imei'=>$row['imei'])) == true){
									// For Code return Tracking imei
								 	$imei  	= $row['imei'];	
								 	$tis         = $this->db->query("select is_id,d_id,d_code from tbl_item_sales where imei='$imei'");
									$tis_array = $tis->result_array();
									$tis_id 	= 	$tis_array[0]['is_id'];
									$ref_id 	= 	$tis_array[0]['d_id'];
									$ref_code 	= 	$tis_array[0]['d_code'];
									
									$arr_tracking = array(
											'imei'  		=> $row['imei'],
											'ref_id'   		=> $ref_id,
											'ref_code'   	=> $ref_code,
											'tis_id'   		=> $tis_id,
											'level_type'   	=> '2',
										    'track_status'  => '2',
											'date'   		=> date('Y-m-d h:i:s')
										);
									$this->db->insert("tbl_item_tracking",$arr_tracking);
									 	$update_array	=	array(
											'level_type'  		=> '2',
											'rt_id'   		    => '0',
											'rt_code'   		=> null,
											'rt_state_id'   	=> null,
											'rt_city_id'   		=> null,
											'rt_taluka_id'   	=> null,
											'rt_date'   		=> null,
											'updated_on'		=> date("Y-m-d H:i:s")
										);
										$where_array	=	 array('imei'=> $row['imei']);
										$this->base_models->update_records('tbl_item_sales',$update_array,$where_array);
										$update_array1	=	array(
											'item_status'  		=> '0',
											'updated_on'		=> date("Y-m-d H:i:s")
										);
										$this->base_models->update_records('tbl_sales_to_d',$update_array1,$where_array);	
									}						 
								 }
							}	
					}

					if ($this->db->trans_status() === FALSE){
					    $this->db->trans_rollback(); //rolback
					    $page_data['status'] = 'Something went wrong please try again..!';
					    $page_data['message'] = $this->db->_error_message();
					    $this->session->set_flashdata('error','Something went wrong please try again..!');
					}else{
					    $this->db->trans_commit(); //commit
					    $page_data['status'] = 'Query run successfully';
					    //  $this->session->set_flashdata('success','Query run successfully');
					}	

						if(isset($data))
						{    	
							    $page_data['acceptedprodct']	=	$data;
							
						}

						if(isset($sold))
						{    	
							    $page_data['sold']	=	$sold;
							
						}
						if(isset($item_not_in_d_list))
						{    	
							    $page_data['itemnotinlist']	=	$item_not_in_d_list;
							
						}
						if(isset($invalid_imei_length))
						{    	
							    $page_data['invalidimei']	=	$invalid_imei_length;
							
						}
					 	if(isset($invalid_rt))
						{    	
							    $page_data['invalid_rt']	=	$invalid_rt;
							
						}
						/*echo '<pre>';
						print_r($page_data); die;*/
					 	$this->renderView('Distributor/Sale/sale_return_by_excel',$page_data);
				}
				
	}
	//End Sale Return By Excel
	public function sale_return()
	{
		$select	 = array('rt_id','rt_code','firmname');
		$where = array('status !=' => '2','d_id' => $this->session->userdata('id'));
		$data['data'] = $this->d_sales_models->GetAllItemValues('retailer', $where, $select);
		$this->renderView('Distributor/Sale/sale_return',$data);
	}
	public function return_sale_items()
	{	
		$rt_id 	= $_POST['rt_id'];
		$all_imei 	= $_POST['imei'];
		$arr_imei 	= preg_split('/\r\n|[\r\n]/', $all_imei);
		
		$d_id =$this->session->userdata('id');
		      // array For product is in the listof d  or not 
				$rs = $this->db->query("select imei from tbl_sales_to_rt where rt_id='$rt_id'");
				$array = $rs->result_array();
				$imei_array = array_column($array, 'imei');	 // for converting in 1 array format
		        $imei_array1 =  array_flip($imei_array);
				
				// array for product sold 
		        $rsold = $this->db->query("select imei from tbl_sales_to_rt where item_status='1'");
				$arraysold = $rsold->result_array();
				$sold_imei_array = array_column($arraysold, 'imei');	 // for converting in 1 array format
		        $sold_imei_array1 =  array_flip($sold_imei_array);
		           

				if (!empty($arr_imei)) 
				{
					$i=0;
				  foreach($arr_imei as $row)
				   {
					    $imei 	     = $arr_imei[$i]; 
						$imei_code  =	trim($imei); // remove spaces from both sides of string
						
						   if(array_key_exists($imei_code, $imei_array1))
							{
								if(array_key_exists($imei_code, $sold_imei_array1))
								{
									$sold[] = array
									    (
											'imei' => $imei_code
										);
								}
								else
								{
									$data[]  = array
										(
											'imei'   			=> $imei_code,
										);
								}
							}
							else
							{
								$notinlist[] = array
									(
										'imei'=> $imei_code
									);
							}		  
						$i++;
					}
					
					$this->db->trans_begin(); //trans start
					if(!empty($data))
					{
						foreach ($data as $row)
							{	 
							     if($row['imei']!='')
								 {	
								 	if($this->base_models->delete_records('tbl_sales_to_rt',array('imei'=>$row['imei'])) == true){
									 	
									 	// For Code return Tracking imei
								 	$imei  	= $row['imei'];	
								 	$tis         = $this->db->query("select is_id,d_id,d_code from tbl_item_sales where imei='$imei'");
									$tis_array = $tis->result_array();
									$tis_id 	= 	$tis_array[0]['is_id'];
									$ref_id 	= 	$tis_array[0]['d_id'];
									$ref_code 	= 	$tis_array[0]['d_code'];
									
									$arr_tracking = array(
											'imei'  		=> $row['imei'],
											'ref_id'   		=> $ref_id,
											'ref_code'   	=> $ref_code,
											'tis_id'   		=> $tis_id,
											'level_type'   	=> '2',
										    'track_status'  => '2',
											'date'   		=> date('Y-m-d h:i:s')
										);
									$this->db->insert("tbl_item_tracking",$arr_tracking);
									 	$update_array	=	array(
											'level_type'  		=> '2',
											'rt_id'   		    => '0',
											'rt_code'   		=> null,
											'rt_state_id'   	=> null,
											'rt_city_id'   		=> null,
											'rt_taluka_id'   	=> null,
											'rt_date'   		=> null,
											'updated_on'		=> date("Y-m-d H:i:s")
										);
										$where_array	=	 array('imei'=> $row['imei']);
										$this->base_models->update_records('tbl_item_sales',$update_array,$where_array);
										$update_array1	=	array(
											'item_status'  		=> '0',
											'updated_on'		=> date("Y-m-d H:i:s")
										);
										$this->base_models->update_records('tbl_sales_to_d',$update_array1,$where_array);	
									}						 
								 }
							}	
					}

					if ($this->db->trans_status() === FALSE){
					    $this->db->trans_rollback(); //rolback
					    $page_data['status'] = 'Something went wrong please try again..!';
					    $page_data['message'] = $this->db->_error_message();
					    $this->session->set_flashdata('error','Something went wrong please try again..!');
					}else{
					    $this->db->trans_commit(); //commit
					    $page_data['status'] = 'Query run successfully';
					    //  $this->session->set_flashdata('success','Query run successfully');
					}	

				

						
					if(!empty($data))
					{    	
						$page_data['acceptedprodct']	=	$data;
					}

					if(!empty($notinlist))
					{    	
	    				$page_data['rejectedprodct']	=	$notinlist;
					}

					if(!empty($sold))
					{    	
					    $page_data['soldprodct']		=	$sold;
					}
				 	
				 	$select	 = array('rt_id','rt_code','firmname');
					$where = array('status !=' => '2','d_id' => $this->session->userdata('id'));
					 $page_data['select']=array('rt_id'=>$rt_id); 
					$page_data['data'] = $this->d_sales_models->GetAllItemValues('retailer', $where, $select);
				 	$this->renderView('Distributor/Sale/sale_return',$page_data);
				}
				
		}


   public function sale_to_rt()
	{
			$select	 = array('rt_id','rt_code','firmname');
			$where = array('status !=' => '2','d_id' => $this->session->userdata('id'));
			$data['data'] = $this->d_sales_models->GetAllItemValues('retailer', $where, $select);
		$this->renderView('Distributor/Sale/sale_items',$data);
	}

   public function upload_excel_for_sale()
	{
		$this->renderView('Distributor/Sale/upload_excel_for_sale');
	}
	
	public function upload_sale_to_rt()
	{
		$d_id =	 $this->session->userdata('id');	
		$this->load->library('excel');
		
		$rt = $this->db->query("select rt_code from retailer where d_id='$d_id'");
		$array_rt = $rt->result_array();
 		$rt_array = array_column($array_rt, 'rt_code');	 // for converting in 1 array format
        $rt_array1 =  array_flip($rt_array);

        $sql = $this->db->query("select imei from tbl_sales_to_d where d_id='$d_id'");
		$array_item = $sql->result_array();
 		$item_array = array_column($array_item, 'imei');	 // for converting in 1 array format
        $item_array1 =  array_flip($item_array);

		$rs = $this->db->query("select imei from tbl_sales_to_d where item_status='1'");
		$array = $rs->result_array();
		$imei_array = array_column($array, 'imei');	 // for converting in 1 array format
        $imei_array1 =  array_flip($imei_array);

				if (isset($_POST)) 
				{
					//print_r($_FILES); die;
				   $path = $_FILES["sale_to_rt"]["tmp_name"];
				   $object = PHPExcel_IOFactory::load($path);
				   foreach($object->getWorksheetIterator() as $worksheet)
				   {
					   $highestRow = $worksheet->getHighestRow();
					   $highestColumn = $worksheet->getHighestColumn();
					  
					    for($row=2; $row<=$highestRow; $row++)
					    {
							$rt_code1 		= $worksheet->getCellByColumnAndRow(0, $row)->getValue();
							//$companyname 		= $worksheet->getCellByColumnAndRow(1, $row)->getValue(); 
							// $udate 			= $worksheet->getCellByColumnAndRow(2, $row)->getValue();
							//$date 				= date('Y-m-d', PHPExcel_Shared_Date::ExcelToPHP($worksheet->getCellByColumnAndRow(2, $row)->getValue()));
							//$itemcode 			= $worksheet->getCellByColumnAndRow(3, $row)->getValue();
							$imei 				= $worksheet->getCellByColumnAndRow(2, $row)->getValue();
							$rt_code	=	trim($rt_code1);
							if(array_key_exists($rt_code, $rt_array1))
								{
									$imei_code  =	trim($imei); // remove spaces from both sides of string
									$length = strlen($imei_code);
										if($length > '10'){
											if(array_key_exists($imei_code, $item_array1)) // for item is in d list or not
											{
											   	if(array_key_exists($imei_code, $imei_array1))
												{
													$sold[] = array
														(
															
															'imei'   				=> $imei_code
																		
														);
												}else{
															$rs         = $this->db->query("select item_id,item_code from tbl_items where imei='$imei_code'");
															$item_array = $rs->result_array();
															$item_id 	= 	$item_array[0]['item_id'];
															$item_code  = 	$item_array[0]['item_code'];

															$d = $this->db->query("select rt_id,state,city from retailer where rt_code='$rt_code'");
															$d_array = $d->result_array();
															$rt_id 	 = 	$d_array[0]['rt_id'];
															$state   = 	$d_array[0]['state'];
															$city    = 	$d_array[0]['city'];
															$date 	 =	date('Y-m-d h:i:s');
															$data[]  = array
																(
																	'item_id'  			=> $item_id,
																	'item_code	'   	=> $item_code,
																	'imei'   			=> $imei_code,
																	'rt_id'   			=> $rt_id,
																	'rt_code'    		=> $rt_code,
																	'upload_date'   	=> $date,
																	'inserted_on'   	=> date('Y-m-d h:i:s')
																);
													  }
											}else{
													$item_not_in_d_list[] = array
														(
															
															'imei' => $imei_code
																		
														);
												 }	
										}else{
												$invalid_imei_length[] = array
														(
															
															'imei' => $imei_code
																		
														);
											 }	
												   
								}else{
										$invalid_rt[] = array
														(
															
															'imei' => $imei
																		
														);
									}
					}
		}
					if(isset($data))
					{
						foreach ($data as $row)
							{	 
							     if($row['rt_id']!='')
								 {	 
								 	$this->db->insert("tbl_sales_to_rt",$row);
								 	// For Code Tracking imei
								 	$imei  	= $row['imei'];	
								 	$tis         = $this->db->query("select is_id from tbl_item_sales where imei='$imei'");
									$tis_array = $tis->result_array();
									$tis_id 	= 	$tis_array[0]['is_id'];
									
									$arr_tracking = array(
											'imei'  		=> $row['imei'],
											'ref_id'   		=> $row['rt_id'],
											'ref_code'   	=> $row['rt_code'],
											'tis_id'   		=> $tis_id,
											'level_type'   	=> '3',
										    'track_status'  => '1',
											'date'   		=> date('Y-m-d h:i:s')
										);
									$this->db->insert("tbl_item_tracking",$arr_tracking);
									// End Code Tracking imei
								 	$rt_id = $row['rt_id'];
								 	$rt = $this->db->query("select state,city,taluka from retailer where rt_id='$rt_id'");
									$rt_array = $rt->result_array();
									$state  = 	$rt_array[0]['state'];
									$city  	= 	$rt_array[0]['city'];
									$taluka  	= 	$rt_array[0]['taluka'];
								 	$update_array	=	array(
										'level_type'  		=> '3',
										'rt_id'   		    => $row['rt_id'],
										'rt_code'   		=> $row['rt_code'],
										'rt_state_id'   	=> $state,
										'rt_city_id'   		=> $city,
										'rt_taluka_id'   	=> $taluka,
										'rt_date'   		=> $row['upload_date'],
										'updated_on'		=> date("Y-m-d H:i:s")
									);
									$where_array	=	 array('imei'=> $row['imei']);
									$this->base_models->update_records('tbl_item_sales',$update_array,$where_array);
									$update_array1	=	array(
										'item_status'  		=> '1',
										'updated_on'		=> date("Y-m-d H:i:s")
									);
									$this->base_models->update_records('tbl_sales_to_d',$update_array1,$where_array);	
								 }
							}	
					}		 
						if ($this->db->trans_status() === FALSE){
					    $this->db->trans_rollback(); //rolback
					    $page_data['status'] = 'Something went wrong please try again..!';
					    $page_data['message'] = $this->db->_error_message();
					    $this->session->set_flashdata('error','Something went wrong please try again..!');
					}else{
					    $this->db->trans_commit(); //commit
					    $page_data['status'] = 'Query run successfully';
					    //  $this->session->set_flashdata('success','Query run successfully');
					}
						if(isset($data))
						{    	
							    $page_data['acceptedprodct']	=	$data;
							
						}

						if(isset($sold))
						{    	
							    $page_data['sold']	=	$sold;
							
						}
						if(isset($item_not_in_d_list))
						{    	
							    $page_data['itemnotinlist']	=	$item_not_in_d_list;
							
						}
						if(isset($invalid_imei_length))
						{    	
							    $page_data['invalidimei']	=	$invalid_imei_length;
							
						}
					 	if(isset($invalid_rt))
						{    	
							    $page_data['invalid_rt']	=	$invalid_rt;
							
						}
						/*echo '<pre>';
						print_r($page_data); die;*/
					 	$this->renderView('Distributor/Sale/upload_excel_for_sale',$page_data);
				}
				
	}


	public function insert_sale_items()
	{	
		
		$rt_id 	= $_POST['rt_id'];
		$time = date("h:i:s");
	   	$datef 		= date("Y-m-d", strtotime($_POST['date'])); 
	    $date= $datef.' '.$time;
		$all_imei 	= $_POST['imei'];
		$arr_imei 	= preg_split('/\r\n|[\r\n]/', $all_imei);
		$d_id =$this->session->userdata('id');
		      // array For product is in the listof d  or not 
				$rs = $this->db->query("select imei from tbl_sales_to_d where d_id='$d_id'");
					$array = $rs->result_array();
					
				$imei_array = array_column($array, 'imei');	 // for converting in 1 array format
		            $imei_array1 =  array_flip($imei_array);
				
				// array for product sold or not
		         $rsold = $this->db->query("select imei from tbl_sales_to_d where item_status='1'");
				 $arraysold = $rsold->result_array();
					
				$sold_imei_array = array_column($arraysold, 'imei');	 // for converting in 1 array format
		        $sold_imei_array1 =  array_flip($sold_imei_array);
		           

				if (!empty($arr_imei)) 
				{
					$i=0;
				  foreach($arr_imei as $row)
				   {
					    $imei 	     = $arr_imei[$i]; 
						 $imei_code  =	trim($imei); // remove spaces from both sides of string
						
						   if(array_key_exists($imei_code, $imei_array1))
							{
								if(array_key_exists($imei_code, $sold_imei_array1))
								{
									$sold[] = array
									    (
											'imei' => $imei_code
										);
								}
								else
								{
									$rs         = $this->db->query("select item_id,item_code from tbl_items where imei='$imei_code'");
									$item_array = $rs->result_array();
									$item_id 	= 	$item_array[0]['item_id'];
									$item_code  = 	$item_array[0]['item_code'];

									$d = $this->db->query("select rt_code,state,city from retailer where rt_id='$rt_id'");
									$d_array = $d->result_array();
									$rt_code = 	$d_array[0]['rt_code'];
									$state   = 	$d_array[0]['state'];
									$city    = 	$d_array[0]['city'];
									$data[]  = array
										(
											'item_id'  			=> $item_id,
											'item_code	'   	=> $item_code,
											'imei'   			=> $imei_code,
											'rt_id'   			=> $rt_id,
											'rt_code'    		=> $rt_code,
											'upload_date'   	=> $date,
											'inserted_on'   	=> date('Y-m-d h:i:s')
										);
								}
							}
							else
							{
								$duplicate[] = array
									(
										'imei'=> $imei_code
									);
							}		  
						$i++;
					}
					
					$this->db->trans_begin(); //trans start
					if(!empty($data))
					{
						foreach ($data as $row)
							{	 
							     if($row['rt_id']!='')
								 {	 
								 	$this->db->insert("tbl_sales_to_rt",$row);
								 		// For Code Tracking imei
								 	$imei  	= $row['imei'];	
								 	$tis         = $this->db->query("select is_id from tbl_item_sales where imei='$imei'");
									$tis_array = $tis->result_array();
									$tis_id 	= 	$tis_array[0]['is_id'];
									
									$arr_tracking = array(
											'imei'  		=> $row['imei'],
											'ref_id'   		=> $row['rt_id'],
											'ref_code'   	=> $row['rt_code'],
											'tis_id'   		=> $tis_id,
											'level_type'   	=> '3',
										    'track_status'  => '1',
											'date'   		=> date('Y-m-d h:i:s')
										);
									$this->db->insert("tbl_item_tracking",$arr_tracking);
								 	$rt_id = $row['rt_id'];
								 	$rt = $this->db->query("select state,city,taluka from retailer where rt_id='$rt_id'");
									$rt_array = $rt->result_array();
									$state  = 	$rt_array[0]['state'];
									$city  	= 	$rt_array[0]['city'];
									$taluka  	= 	$rt_array[0]['taluka'];
								 	$update_array	=	array(
										'level_type'  		=> '3',
										'rt_id'   		    => $row['rt_id'],
										'rt_code'   		=> $row['rt_code'],
										'rt_state_id'   	=> $state,
										'rt_city_id'   		=> $city,
										'rt_taluka_id'   	=> $taluka,
										'rt_date'   		=> $row['upload_date'],
										'updated_on'		=> date("Y-m-d H:i:s")
									);
									$where_array	=	 array('imei'=> $row['imei']);
									$this->base_models->update_records('tbl_item_sales',$update_array,$where_array);
									$update_array1	=	array(
										'item_status'  		=> '1',
										'updated_on'		=> date("Y-m-d H:i:s")
									);
									$this->base_models->update_records('tbl_sales_to_d',$update_array1,$where_array);	
								 }
							}	
					}

					if ($this->db->trans_status() === FALSE){
					    $this->db->trans_rollback(); //rolback
					    $page_data['status'] = 'Something went wrong please try again..!';
					    $page_data['message'] = $this->db->_error_message();
					    $this->session->set_flashdata('error','Something went wrong please try again..!');
					}else{
					    $this->db->trans_commit(); //commit
					    $page_data['status'] = 'Query run successfully';
					    //  $this->session->set_flashdata('success','Query run successfully');
					}	

				

						
					if(!empty($data))
					{    	
						$page_data['acceptedprodct']	=	$data;
					}

					if(!empty($duplicate))
					{    	
	    				$page_data['rejectedprodct']	=	$duplicate;
					}

					if(!empty($sold))
					{    	
					    $page_data['soldprodct']		=	$sold;
					}
				 	
				 	$select	 = array('rt_id','rt_code','firmname');
					$where = array('status !=' => '2','d_id' => $this->session->userdata('id'));
					$page_data['data'] = $this->d_sales_models->GetAllItemValues('retailer', $where, $select);
				 	$this->renderView('Distributor/Sale/sale_items',$page_data);
				}
				
		}

    


//generate to excel	
	public function generate_sale_excel($param1){
		// create file name
		$fileName = 'ItemSalesToRetailer'.'-data-'.date('d-M-Y').'.xlsx';   
		// load excel library
		$this->load->library('excel');
		$info = $param1;
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->setActiveSheetIndex(0);
		// set Header
		$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Sale Date');
		$objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Retailer Code');
		$objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Retailer Firm Name');
		$objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Item Code');
		$objPHPExcel->getActiveSheet()->SetCellValue('E1', 'IMEI');
		$objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Status');
		
		// set Row
		$rowCount = 2;
	
		foreach ($info as $element) {
			$objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, date('d-M-y', strtotime($element['upload_date'])));
			$objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $element['rt_code']);
			$rt_code = $element['rt_code'];
			$sql = $this->db->query("select firmname from retailer where rt_code = '$rt_code'");
			$d_array = $sql->result_array();
			$objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $d_array[0]['firmname']);
			$objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount,  $element['item_code']);
			$objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount,  $element['imei']);
			$pay_status = ($element['item_status'] == '0') ? 'Unsold' : 'Sold';
			$objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $pay_status);
			
			$rowCount++;
		}
		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
		$objWriter->save('uploads/admin/excel/'.$fileName);
		// download file
		header("Content-Type: application/vnd.ms-excel");
		redirect(base_url('uploads/admin/excel/'.$fileName));
	}			

	public function sale_list()
	{	
		
		$select	 = array('rt_id','rt_code','firmname');
		$where = array('d_id' => $this->session->userdata('id'));
		$pagedata['rt_list'] = $this->d_sales_models->GetAllItemValues('retailer', $where, $select);
 

        $select	 = array('strt_id','item_code','imei','rt_code','item_status','upload_date');
		$where = array();
		$pagedata['delete_link'] = 'Dsales/delete_sale_item';
		$status ='';
		$imei ='';
		$rt_id ='';
		if($this->session->userdata('imei') || $this->session->userdata('status')|| $this->session->userdata('rt_id')){
			$this->session->userdata('imei');
			$this->session->userdata('status');
			$this->session->userdata('rt_id');
		}
    
      //Pagination Start
		$config = array();
        $config["base_url"] = base_url() . "Dsales/sale_list";
        $config["total_rows"] = $this->base_models->get_count('strt_id','tbl_sales_to_rt', $where);
        $config["per_page"] = 10;
        $config["uri_segment"] = 3;
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $pagedata["links"] = $this->pagination->create_links();
        $pagedata['results'] = $this->base_models->get_pagination('tbl_sales_to_rt', $where,'strt_id',$config["per_page"], $page);     
       	//Pagination End

       	$imei = (@$imei) ? $imei : '';
       	$sstatus = (@$status) ? $status : null;
       	@strcmp($status,$sstatus);
       	$rt_id1 = (@$rt_id) ? $rt_id : null;
       	@strcmp($d_id,$d_id1);
        $pagedata['select']=array('status'=>$status,'imei'=>$imei,'rt_id'=>$rt_id);  
        $this->renderView('Distributor/Sale/sale_list',$pagedata);
	}


	// with ci pagination in php
	public function sale_list_sess()
	{

		$select	 = array('rt_id','rt_code','firmname');
		$where1 = array('d_id' => $this->session->userdata('id'));
		$pagedata['rt_list'] = $this->d_sales_models->GetAllItemValues('retailer', $where1, $select);
 

        $select	 = array('strt_id','item_code','imei','rt_code','item_status','upload_date');
		$where = array();
		$pagedata['delete_link'] = 'Dsales/delete_sale_item';
		$status ='';
		$imei ='';
		//Filter Process
	
       if(@$_POST['submit']=='filter' || @$_POST['submit']=='createxls')
       {
       	  	$imei_no = (@$this->input->post('imei')) ? $this->input->post('imei') : '';
       		$imei = trim($imei_no);

       	    $status = (@$this->input->post('status')!= null) ? $this->input->post('status') : '';
          
           $rt_id = (@$this->input->post('rt_id')!= null) ? $this->input->post('rt_id') : '';
           $array_items = $this->session->set_userdata(array("imei"=>$imei,"status"=>$status,"rt_id"=>$rt_id));
            if($imei !=''){
            	$filter =  array('imei'=> $imei);
            	$where = array_merge($where,$filter);	
            }  
            if($status !=''){
           		$filter =  array('item_status'=>$status);
            	$where = array_merge($where,$filter);	
            } 
             if($rt_id !=''){
           		$filter =  array('rt_id'=>$rt_id);
            	$where = array_merge($where,$filter);	
         
       }else{
            } 
				if($this->session->userdata('imei') != NULL){
				$imei = $this->session->userdata('imei'); 
				$filter =  array('imei'=> $imei);
				$where = array_merge($where,$filter);
				} 
				if($this->session->userdata('status') != NULL){
				$status = $this->session->userdata('status'); 
				$filter =  array('item_status'=>$status);
				$where = array_merge($where,$filter);
				}
				if($this->session->userdata('rt_id') != NULL){
				$rt_id = $this->session->userdata('rt_id'); 
				$filter =  array('rt_id'=>$rt_id);
				$where = array_merge($where,$filter);
				}
       	}
       	     // for secondpage
       			if($this->session->userdata('imei') != NULL){
				$imei = $this->session->userdata('imei'); 
				$filter =  array('imei'=> $imei);
				$where = array_merge($where,$filter);
				} 
				if($this->session->userdata('status') != NULL){
				$status = $this->session->userdata('status'); 
				$filter =  array('item_status'=>$status);
				$where = array_merge($where,$filter);
				}
				if($this->session->userdata('rt_id') != NULL){
				$rt_id = $this->session->userdata('rt_id'); 
				$filter =  array('rt_id'=>$rt_id);
				$where = array_merge($where,$filter);
				}
       		/*echo '<pre>';
		print_r($where); die();*/
       	 if(@$_POST['submit']=='createxls')
       	  {

       	  //	$select	 = array('item_id','company_code','company_name','upload_date','item_code','item_name','imei');
			//$where .= array('status'=> '1');

			$data['data'] = $this->d_sales_models->GetAllItemValues('tbl_sales_to_rt', $where, $select);
	   /*   echo '<pre>';
	       print_r($data['data']);die;*/
			//Export xls
				$this->generate_sale_excel($data['data']);			
				

          }
		//End Filter Process
    
      //Pagination Start
		$config = array();
        $config["base_url"] = base_url() . "Dsales/sale_list_sess";
        $config["total_rows"] = $this->base_models->get_count('strt_id','tbl_sales_to_rt', $where);
        $config["per_page"] = 10;
        $config["uri_segment"] = 3;
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $pagedata["links"] = $this->pagination->create_links();
        $pagedata['results'] = $this->base_models->get_pagination('tbl_sales_to_rt', $where,'strt_id',$config["per_page"], $page);     
       	//Pagination End
	//	echo $this->db->last_query();die;
       	$imei = (@$imei) ? $imei : '';

       	$sstatus = (@$status) ? $status : null;
        @strcmp($status,$sstatus);
        $rt_id1 = (@$rt_id) ? $rt_id : null;
       	@strcmp($rt_id,$rt_id1);
       @$pagedata['select']=array('status'=>$status,'imei'=>$imei,'rt_id'=>$rt_id);  
       $this->renderView('Distributor/Sale/sale_list',$pagedata);
	}	



	public function edit_sale_item()
	{
		$select	 = array('rt_id','rt_code','firmname');
		$where = array('d_id' => $this->session->userdata('id'));
		$pagedata['rt_list'] = $this->d_sales_models->GetAllItemValues('retailer', $where, $select);

		$id = base64_decode($_GET['id']); 
		$select	 = array('strt_id','rt_id','upload_date','imei');
		$where = array('strt_id'=> $id);
		$pagedata['data']=$this->d_sales_models->GetAllItemValues('tbl_sales_to_rt', $where, $select);
		$this->renderView('Distributor/Sale/edit_sale_items',$pagedata);
	}


	public function update_sale_items()
	{
		if (isset($_POST)) 
		{

			$imei = trim($this->input->post('imei'));
			$rt_id = $this->input->post('rt_id');
			$datef 	= date("Y-m-d", strtotime($_POST['upload_date'])); 
			$time = date("h:i:s");
	   		$date= $datef.' '.$time;

			$item_id = $this->input->post('strt_id');
			
			$select	 = array('rt_code');
			$where = array('rt_id'=> $rt_id);
			$arr_rt	=$this->d_sales_models->GetAllItemValues('retailer', $where, $select);
            $rt_code =$arr_rt[0]['rt_code'];    
			$update_array	=	array(
										'rt_id'  		=> $this->input->post('rt_id'),
										'rt_code'  		=> $rt_code,
										'updated_on'	=> date("Y-m-d H:i:s")
									);
			$where_array	=	 array('imei'=>$imei);
            
            $this->base_models->update_records('tbl_sales_to_rt',$update_array,$where_array);
			
			$rt_id1	 =	$this->input->post('rt_id');
			$rt 	 =	$this->db->query("select state,city,taluka from retailer where rt_id='$rt_id'");
			$rt_array= $rt->result_array();
			$state   = 	$rt_array[0]['state'];
			$city  	 = 	$rt_array[0]['city'];
			$taluka  = 	$rt_array[0]['taluka'];
			$update_array1	=	array(
										'rt_id'  		=> $this->input->post('rt_id'),
										'rt_code'  		=> $rt_code,
										'rt_state_id'   => $state,
										'rt_city_id'   	=> $city,
										'rt_taluka_id'  => $taluka,
										'rt_date'  		=> $date,
										'updated_on'	=> date("Y-m-d H:i:s")
									);
			$where_array1	=	 array('imei'=>$imei);


			if($this->base_models->update_records('tbl_item_sales',$update_array1,$where_array1) == true){
					$this->session->set_flashdata('success','Edited successfully');
				}else{
					$this->session->set_flashdata('error','Not added Please try again');
				}
		}
			redirect(base_url('Dsales/edit_sale_item/?id='.base64_encode($item_id)));	
	}	
			public function delete_sale_item()
			{
				 $imei = trim($_GET['id']); 
				$current_date = date("Y-m-d H:i:s");

				$update_array1 = array(
									'level_type'=>'0',
									'nd_id'=>'0',
									'nd_code'=>null,
									'nd_date'=>null,
									'nd_state_id' => null,
									'nd_city_id'  => null,
									'updated_on'=>$current_date
									);
				$where_array1 = array('imei'=>$imei);
				$this->base_models->update_records('tbl_item_sales',$update_array1,$where_array1);

				$update_array = array(
									'item_status'=>'0',
									'updated_on'=>$current_date
									);
				$where_array = array('imei'=>$imei);
				if($this->base_models->update_records('tbl_items',$update_array,$where_array) == true){
					$this->base_models->delete_records('tbl_sales_to_nd',array('imei'=>$imei));//delete item from tbl item sale to nd
					   $this->session->set_flashdata('success','Item Deleted Successfully From Sale List');
				}else{
				  $this->session->set_flashdata('error','Item Not Deleted Successfully From Sale List');
				}
			redirect('Adminsales/sale_list/');
			}
   
       
    public function check_imei_exist()
	{
		$rs 			= $this->db->query("select imei from tbl_items where status='1'");
		$array 			= $rs->result_array();
		$imei_array 	= array_column($array, 'imei');	 // for converting in 1 array format
		$imei_array1 	= array_flip($imei_array);

		$imei 	        = $_POST['imei'];

		$imei_code 		= trim($imei); // remove spaces from both sides of string

			 if(array_key_exists($imei_code, $imei_array1))
			 {
				echo 'error';
			 }
			 else
			 {
			 	echo 'success';
			 }	
	}	 

	//---------- ND ------------//
	public function national_distributor()
	{		
		$select = array('nd_id','nd_code','fname','mname','lname','lname','username','email','email2','contact','contact2','pan_no','gst_no','status','state','city','address','acnt_name','acnt_email','acnt_contact','inserted_on','updated_on','(select date_time from recent_login_user where user_code = nd_code AND type = "1" Order by id desc limit 1) as last_login');
		$pagedata['results'] = $this->base_models->GetAllValues('ndistributor',null, $select);
		echo '<pre>';
		print_r($pagedata['results']);
		die();
		$pagedata['delete_link'] = 'Adminmaster/delete_nd';
		$this->renderView('Admin/Master/national_distributor',$pagedata);
	}
	
	
	
	public function insert_nd()
	{
		$this->form_validation->set_rules('fname', 'First name', 'trim|required');
		$this->form_validation->set_rules('mname', 'Middle Name', 'trim|required');
		$this->form_validation->set_rules('lname', 'Last Name', 'trim|required');
		$this->form_validation->set_rules('username', 'User Name', 'trim|required');
		$this->form_validation->set_rules('inputPassword', 'Password', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'trim');
		$this->form_validation->set_rules('contact', 'Mobile No.', 'trim|required|numeric');
		$this->form_validation->set_rules('type', 'User Type', 'trim|required');
		$this->form_validation->set_rules('status', 'Status', 'trim|required');
		$current_date = date("Y-m-d H:i:s");
	
		$error='';
			// if (empty($_FILES['image']['name'][0])){
				// $this->form_validation->set_rules('image', 'Image', 'required');
			// }
			$data['file_name'] = '';
            if(!empty($_FILES['image']['name'])){
                $config['upload_path'] = 'uploads/admin/users/';
                $config['allowed_types'] = 'gif|jpg|png';
                $this->upload->initialize($config);
				if($this->upload->do_upload('image')){
					$data = $this->upload->data();
				}else{
                    $imageerrors = $this->upload->display_errors();
					$this->form_validation->set_message('image', $imageerrors);					
                }
			}
			
			if($this->form_validation->run())
			{					
				$insert_array=array(
						'fname'=>$this->input->post('fname'),
						'mname'=>$this->input->post('mname'),
						'lname'=>$this->input->post('lname'),
						'username'=>$this->input->post('username'),
						'password'=>md5($this->input->post('inputPassword')),
						'email'=>$this->input->post('email'),
						'contact'=> $this->input->post('contact'),
						'type'=>$this->input->post('type'),
						'status'=>$this->input->post('status'),
						'profile_pic'=>$data['file_name'],
						'inserted_on'=>date("Y-m-d H:i:s")
					);
					//print_r($insert_array);exit;
					if($this->base_models->add_records('wwc_admin',$insert_array)){
						$this->session->set_flashdata('success','Added successfully');
						redirect(base_url('admin/users'));
					}else{
						$this->session->set_flashdata('error','Not added Please try again');
						//redirect(base_url('admin/add_user'));
					}
			}
				$this->renderView('Admin/add-user');
	}
	
	public function edit_nd()
	{
		$id = base64_decode($_GET['id']);
		$pagedata['data']=$this->base_models->get_users('',$id);
		$this->renderView('Admin/edit-user',$pagedata);
	}
	
	public function update_nd()
	{
		$id = base64_decode($_GET['id']);		
		if($id==''){
			redirect(base_url('admin/users')); 
		}
		
		$this->form_validation->set_rules('fname', 'First name', 'trim|required');
		$this->form_validation->set_rules('mname', 'Middle Name', 'trim|required');
		$this->form_validation->set_rules('lname', 'Last Name', 'trim|required');
		$this->form_validation->set_rules('username', 'User Name', 'trim|required');
		$this->form_validation->set_rules('inputPassword', 'Password', 'trim');
		$this->form_validation->set_rules('email', 'Email', 'trim');
		$this->form_validation->set_rules('contact', 'Mobile No.', 'trim|required|numeric');
		$this->form_validation->set_rules('type', 'User Type', 'trim|required');
		$this->form_validation->set_rules('status', 'Status', 'trim|required');
		$current_date = date("Y-m-d H:i:s");
	
		$error='';
			// if (empty($_FILES['image']['name'][0])){
				// $this->form_validation->set_rules('image', 'Image', 'required');
			// }
			// $data['file_name'] = '';
            // if(!empty($_FILES['image']['name'])){
                // $config['upload_path'] = 'uploads/admin/users/';
                // $config['allowed_types'] = 'gif|jpg|png';
                // $this->upload->initialize($config);
				// if($this->upload->do_upload('image')){
					// $data = $this->upload->data();
				// }else{
                    // $imageerrors = $this->upload->display_errors();
					// $this->form_validation->set_message('image', $imageerrors);					
                // }
			// }
			
			if($this->input->post('inputPassword') != ''){
				$password = md5($this->input->post('inputPassword'));
			}else{
				$password = $this->input->post('oldpassword');
			}	
			if($this->form_validation->run())
			{				
				$update_array=array(
						'fname'=>$this->input->post('fname'),
						'mname'=>$this->input->post('mname'),
						'lname'=>$this->input->post('lname'),
						'username'=>$this->input->post('username'),
						'password'=>$password,
						'email'=>$this->input->post('email'),
						'contact'=> $this->input->post('contact'),
						'type'=>$this->input->post('type'),
						'status'=>$this->input->post('status'),
						// 'profile_pic'=>$data['file_name'],
						'updated_on'=>date("Y-m-d H:i:s")
					);
				$where_array = array('id'=>$id);
				//print_r($insert_array);exit;
				if($this->base_models->update_records('wwc_admin',$update_array,$where_array) == true){
					$this->session->set_flashdata('success','Edited successfully');
				}else{
					$this->session->set_flashdata('error','Not added Please try again');
				}
			}
				redirect(base_url('admin/edit_user/?id='.base64_encode($id)));
			
		// $pagedata['data']=$this->base_models->get_users('',$id);
		// $this->renderView('Admin/edit-user',$pagedata);
	}
			
	public function delete_nd()
	{
		$id = $_GET['id'];
		$current_date = date("Y-m-d H:i:s");
		$update_array = array(
							'status'=>'2',
							'deleted_on'=>$current_date
							);
		$where_array = array('id'=>$id);
		if($this->base_models->update_records('wwc_admin',$update_array,$where_array) == true){
			$data['status'] = 'success';
			$data['message'] = 'Successfully deleted';
		}else{
			$data['status'] = 'error';
			$data['message'] = 'Somting went worng please try again';
		}
		echo json_encode($data);
		die();
	}
	//---------- END ND ------------//
	
		
}
