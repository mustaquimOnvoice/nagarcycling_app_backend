<?php
require_once APPPATH . 'core/Base_Controller.php'; //Load Base Controller
defined('BASEPATH') OR exit('No direct script access allowed');

class National_distributor extends Base_Controller 
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Kolkata');
		
		if(!$this->session->userdata('__ci_last_regenerate') || $this->session->userdata('user_type') != 3){
			$this->session->set_flashdata('error', 'You Are not Allowed to access this file...!');
			redirect('login');
		}
	}
	
	//-- Anuron ERP--//
	
	public function dashboard()
	{		
		$this->renderView('National_distributor/dashboard');
	}
	
	//END Anuron ERP--//			
}
