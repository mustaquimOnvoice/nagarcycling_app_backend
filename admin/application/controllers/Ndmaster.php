<?php
require_once APPPATH . 'core/Base_Controller.php'; //Load Base Controller
defined('BASEPATH') OR exit('No direct script access allowed');

class Ndmaster extends Base_Controller 
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Kolkata');
		
		if(!$this->session->userdata('__ci_last_regenerate') || $this->session->userdata('user_type') != 1 && $this->session->userdata('user_type') != 2 && $this->session->userdata('user_type') != 3){
			$this->session->set_flashdata('error', 'You Are not Allowed to access this file...!');
			redirect('login');
		}
	}
	
	//---------- D ------------//
	public function distributor()
	{		
		$this->load->model("adminmaster_models");
		$select = array('d_id','d_code','firmname','fname','mname','lname','lname','username','email','email2','contact','contact2','pan_no','gst_no','status','state','city','address','acnt_name','acnt_email','acnt_contact','inserted_on','updated_on','(select date_time from recent_login_user where user_code = d_code AND type = "1" Order by id desc limit 1) as last_login','(select state_name from area where state = state_id Order by area_id desc limit 1) as state_name','(select city_name from area where city = city_id Order by area_id desc limit 1) as city_name');
		$where = array('status !=' => '2','nd_id' => $this->session->userdata('id'));
		
		//Pagination Start
		$config = array();
		$config["base_url"] = base_url() . "Ndmaster/distributor";
		$config["total_rows"] = $this->base_models->get_count('d_id','distributor', $where);
		$config["per_page"] = 10;
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$pagedata["links"] = $this->pagination->create_links();
		$pagedata['results'] = $this->adminmaster_models->distributor_list($select,'distributor', $where,'d_id',$config["per_page"], $page);	
		//Pagination End
		
		$pagedata['delete_link'] = 'Ndmaster/delete_d';
		$this->renderView('National_distributor/Master/distributor',$pagedata);
	}
	
	public function add_d()
	{
		$id = $this->session->userdata('id');
		$pagedata['data']=$this->base_models->GetSingleDetails('ndistributor', array('nd_id' => $id), $select = "*");
		$state_id=	$pagedata['data']->state;
		$pagedata['state_list']=$this->base_models->get_state();
		$pagedata['city_list']=$this->base_models->get_city_by_state($state_id);
		$pagedata['state_id']=$this->session->userdata('state');
		$this->renderView('National_distributor/Master/add-d',$pagedata);
	}
	
	public function insert_d()
	{
		$this->form_validation->set_rules('firmname', 'Firm name', 'trim|required');
		$this->form_validation->set_rules('fname', 'First name', 'trim|required');
		$this->form_validation->set_rules('lname', 'Last Name', 'trim|required');
		// $this->form_validation->set_rules('status', 'Status', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'trim|required');
		$this->form_validation->set_rules('contact', 'Mobile No.', 'trim|required|numeric');
		$this->form_validation->set_rules('state', 'State', 'trim|required');
		$this->form_validation->set_rules('city', 'City', 'trim|required');
		$this->form_validation->set_rules('address', 'Address', 'trim|required');
		$this->form_validation->set_rules('pan', 'PAN', 'trim|required');
		$this->form_validation->set_rules('gst', 'GST', 'trim|required');
		$this->form_validation->set_rules('inputPassword', 'Password', 'trim|required');
		$current_date = date("Y-m-d H:i:s");
	
		$error='';
			// if (empty($_FILES['image']['name'][0])){
				// $this->form_validation->set_rules('image', 'Image', 'required');
			// }
			$data['file_name'] = '';
            // if(!empty($_FILES['image']['name'])){
                // $config['upload_path'] = 'uploads/nd/profile/';
                // $config['allowed_types'] = 'gif|jpg|png';
                // $this->upload->initialize($config);
				// if($this->upload->do_upload('image')){
					// $data = $this->upload->data();
				// }else{
                    // $imageerrors = $this->upload->display_errors();
					// $this->form_validation->set_message('image', $imageerrors);					
                // }
			// }
			
			if($this->form_validation->run())
			{					
				$insert_array=array(
						'firmname'=>$this->input->post('firmname'),
						'fname'=>$this->input->post('fname'),
						'mname'=>$this->input->post('mname'),
						'lname'=>$this->input->post('lname'),
						'status'=>'0',
						'email'=>$this->input->post('email'),
						'email2'=>$this->input->post('email2'),
						'contact'=> $this->input->post('contact'),
						'contact2'=> $this->input->post('contact2'),
						'state'=> $this->input->post('state'),
						'city'=> $this->input->post('city'),
						'address'=> $this->input->post('address'),
						'pan_no'=>strtoupper($this->input->post('pan')),
						'gst_no'=>strtoupper($this->input->post('gst')),
						'acnt_name'=>$this->input->post('acnt_name'),
						'acnt_email'=>$this->input->post('acnt_email'),
						'acnt_contact'=>$this->input->post('acnt_contact'),
						'password'=>md5($this->input->post('inputPassword')),
						// 'profile_pic'=>$data['file_name'],
						'nd_id'=>$this->session->userdata('id'),
						'inserted_on'=>date("Y-m-d H:i:s")
					);
					//print_r($insert_array);exit;
					if($this->base_models->add_records('distributor',$insert_array)){
						$this->session->set_flashdata('success','Added successfully');
						redirect(site_url('Ndmaster/distributor'));
					}else{
						$this->session->set_flashdata('error','Not added Please try again');
						//redirect(base_url('admin/add_user'));
					}
			}
				$this->renderView('National_distributor/Master/add-nd');
	}
	
	public function edit_d()
	{
		$id = base64_decode($_GET['id']);
		$pagedata['data']=$this->base_models->GetSingleDetails('distributor', array('d_id' => $id), $select = "*");
		$state_id=	$pagedata['data']->state;
		$pagedata['state_list']=$this->base_models->get_state();
		$pagedata['city_list']=$this->base_models->get_city_by_state($state_id);	
		$this->renderView('National_distributor/Master/edit-d',$pagedata);
	}
	
	public function update_d()
	{
		$id = base64_decode($_GET['id']);		
		if($id==''){
			redirect(base_url('Ndmaster/national_distributor')); 
		}
		
		$this->form_validation->set_rules('firmname', 'Firm name', 'trim|required');
		$this->form_validation->set_rules('fname', 'First name', 'trim|required');
		$this->form_validation->set_rules('lname', 'Last Name', 'trim|required');
		$this->form_validation->set_rules('status', 'Status', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'trim|required');
		$this->form_validation->set_rules('contact', 'Mobile No.', 'trim|required|numeric');
		$this->form_validation->set_rules('state', 'State', 'trim|required');
		$this->form_validation->set_rules('city', 'City', 'trim|required');
		$this->form_validation->set_rules('address', 'Address', 'trim|required');
		$this->form_validation->set_rules('pan', 'PAN', 'trim|required');
		$this->form_validation->set_rules('gst', 'GST', 'trim|required');
		if($this->input->post('inputPassword') != ''){
			$password = md5($this->input->post('inputPassword'));
		}else{
			$password = $this->input->post('oldpassword');
		}
		$current_date = date("Y-m-d H:i:s");
	
		$error='';
			// if (empty($_FILES['image']['name'][0])){
				// $this->form_validation->set_rules('image', 'Image', 'required');
			// }
			// $data['file_name'] = '';
            // if(!empty($_FILES['image']['name'])){
                // $config['upload_path'] = 'uploads/admin/users/';
                // $config['allowed_types'] = 'gif|jpg|png';
                // $this->upload->initialize($config);
				// if($this->upload->do_upload('image')){
					// $data = $this->upload->data();
				// }else{
                    // $imageerrors = $this->upload->display_errors();
					// $this->form_validation->set_message('image', $imageerrors);					
                // }
			// }
			
			if($this->input->post('inputPassword') != ''){
				$password = md5($this->input->post('inputPassword'));
			}else{
				$password = $this->input->post('oldpassword');
			}	
			if($this->form_validation->run())
			{				
				$update_array=array(
						'firmname'=>$this->input->post('firmname'),
						'fname'=>$this->input->post('fname'),
						'mname'=>$this->input->post('mname'),
						'lname'=>$this->input->post('lname'),
						'status'=>$this->input->post('status'),
						'email'=>$this->input->post('email'),
						'email2'=>$this->input->post('email2'),
						'contact'=> $this->input->post('contact'),
						'contact2'=> $this->input->post('contact2'),
						'state'=> $this->input->post('state'),
						'city'=> $this->input->post('city'),
						'address'=> $this->input->post('address'),
						'pan_no'=>strtoupper($this->input->post('pan')),
						'gst_no'=>strtoupper($this->input->post('gst')),
						'acnt_name'=>$this->input->post('acnt_name'),
						'acnt_email'=>$this->input->post('acnt_email'),
						'acnt_contact'=>$this->input->post('acnt_contact'),
						'password'=>$password,
						// 'profile_pic'=>$data['file_name'],
						'updated_on'=>date("Y-m-d H:i:s")
					);
				$where_array = array('d_id'=>$id);
				//print_r($insert_array);exit;
				if($this->base_models->update_records('distributor',$update_array,$where_array) == true){
					$this->session->set_flashdata('success','Edited successfully');
				}else{
					$this->session->set_flashdata('error','Not added Please try again');
				}
			}
				redirect(site_url('Ndmaster/edit_d?id='.base64_encode($id)));
			
		// $pagedata['data']=$this->base_models->get_users('',$id);
		// $this->renderView('Admin/edit-user',$pagedata);
	}
			
	public function delete_d()
	{
		$id = $_GET['id'];
		$current_date = date("Y-m-d H:i:s");
		$update_array = array(
							'status'=>'2',
							'deleted_on'=>$current_date
							);
		$where_array = array('d_id'=>$id);
		if($this->base_models->update_records('distributor',$update_array,$where_array) == true){
			$data['status'] = 'success';
			$data['message'] = 'Successfully deleted';
		}else{
			$data['status'] = 'error';
			$data['message'] = 'Somting went worng please try again';
		}
		echo json_encode($data);
		die();
	}
	
	public function change_status()
	{
		$current_date = date("Y-m-d H:i:s");
		$update_array = array(
						$_POST['col']=>$_POST['status'],
						'updated_on'=>$current_date
						//'lastUpdateUser'=>$this->session->userdata('id')
						);
		$where_array = array('d_id'=>$_POST['id']);
		
		if($this->base_models->update_records('distributor',$update_array,$where_array)){
			echo "success";
			// $this->session->set_flashdata('success','Status change successfully');
		}else{
			// $this->session->set_flashdata('error','Error while changing status');
			echo "failed";
		}
	}
	//---------- END D ------------//	
	
	//---------- ASM/TSM ------------//
	//generate to excel	
	public function generate_report_excel($param2){
		// create file name
		$fileName = 'TSMList-data-'.date('d-M-Y').'.xlsx';   
		// load excel library
		$this->load->library('excel');
		$info = $param2;
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->setActiveSheetIndex(0);
		// set Header
		$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Code');
		$objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Name');      
		$objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Parent ASM');      
		//$objPHPExcel->getActiveSheet()->SetCellValue('D1', 'ND Code');      
		//$objPHPExcel->getActiveSheet()->SetCellValue('E1', 'ND Firm Name');      
		$objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Primary  Email');      
		$objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Secondary  Email');      
		$objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Primary  Contact');      
		$objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Secondary  Contact');      
		$objPHPExcel->getActiveSheet()->SetCellValue('H1', 'PAN');      
		$objPHPExcel->getActiveSheet()->SetCellValue('I1', 'State');      
		$objPHPExcel->getActiveSheet()->SetCellValue('J1', 'City');      
		$objPHPExcel->getActiveSheet()->SetCellValue('K1', 'Address');      
		$objPHPExcel->getActiveSheet()->SetCellValue('L1', 'Status');      
		// set Row
		$rowCount = 2;
	
		foreach ($info as $element) {
			$objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $element['atsm_code']);
			$objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $element['fname'].' '.$element['lname']);
			$objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $element['level_type'] == '1' ? $element['upline_code'] : '-');
			//$objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $element['nd_code']);
			//$objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $element['nd_firmname']);
			$objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $element['email']);
			$objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $element['email2']);
			$objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $element['contact']);
			$objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $element['contact2']);
			$objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $element['pan_no']);
			$objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $element['state_name']);
			$objPHPExcel->getActiveSheet()->SetCellValue('J' . $rowCount, $element['city_name']);
			$objPHPExcel->getActiveSheet()->SetCellValue('K' . $rowCount, $element['address']);
			$objPHPExcel->getActiveSheet()->SetCellValue('L' . $rowCount, ($element['status']== 0) ? 'Inactive' : 'Active');
			$rowCount++;
		}
			
		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
		$objWriter->save('uploads/admin/excel/'.$fileName);
		// download file
		header("Content-Type: application/vnd.ms-excel");
		redirect(base_url('uploads/admin/excel/'.$fileName));
	}
	
	public function uploads_tsm_excel()
	{
		if($this->session->userdata('user_type') == 3){
			redirect(site_url('Ndmaster/distributor'));
		}		
		$this->renderView('National_distributor/Master/upload_tsm');
	}
	
	public function upload_tsm()
	{		
		$this->load->library('excel');
		// print_r($_FILES['tsm_info']['name']);
		// die();
		if (!empty($_FILES['tsm_info']['name'])) 
		{
		   $path = $_FILES["tsm_info"]["tmp_name"];
		   $object = PHPExcel_IOFactory::load($path);
		   foreach($object->getWorksheetIterator() as $worksheet)
		   {
			   $highestRow = $worksheet->getHighestRow();
			   $highestColumn = $worksheet->getHighestColumn();
			  
				for($row=2; $row<=$highestRow; $row++)
				{
					$tsm_name 		= $worksheet->getCellByColumnAndRow(0, $row)->getValue();
					$asm_code 		= $worksheet->getCellByColumnAndRow(1, $row)->getValue(); 
					$email 			= $worksheet->getCellByColumnAndRow(2, $row)->getValue(); 
					$email2 		= $worksheet->getCellByColumnAndRow(3, $row)->getValue(); 
					$contact 		= $worksheet->getCellByColumnAndRow(4, $row)->getValue(); 
					$contact2 		= $worksheet->getCellByColumnAndRow(5, $row)->getValue(); 
					$pan_no 		= $worksheet->getCellByColumnAndRow(6, $row)->getValue(); 
					$state 			= $worksheet->getCellByColumnAndRow(7, $row)->getValue(); 
					$city 			= $worksheet->getCellByColumnAndRow(8, $row)->getValue(); 
					$address 		= $worksheet->getCellByColumnAndRow(9, $row)->getValue(); 
					
					@$upline_id 			= $this->base_models->GetSingleDetails('atsm', array('atsm_code'=>$asm_code), 'atsm_id')->atsm_id;					
					@$state_id 			= $this->base_models->GetSingleDetails('area', array('state_name'=>$state), 'state_id')->state_id;
					@$city_id 			= $this->base_models->GetSingleDetails('area', array('city_name'=>$city), 'city_id')->city_id;
					
					$data[] = array('upline_id'  		=> $upline_id,
									'level_type'   		=> '1',
									'fname'   			=> $tsm_name,
									'email'   			=> $email,
									'email2'   			=> $email2,
									'contact'   		=> $contact,
									'contact2'   		=> $contact2,
									'pan_no'   			=> $pan_no,
									'state'   			=> $state_id,
									'city'   			=> $city_id,
									'address'   		=> $address,
									'inserted_on'   	=> date('Y-m-d h:i:s')									
									);	  
				}
			}
			if($this->db->insert_batch('atsm', $data)){
				$this->session->set_flashdata('success','Added successfully');
			}else{
				$this->session->set_flashdata('error','Not added Please try again');
			}				
		}		
			redirect('Ndmaster/uploads_tsm_excel');
	}
	
	public function atsm()
	{		
		if($this->session->userdata('user_type') == 3){
			redirect(site_url('Ndmaster/distributor'));
		}
		
		$this->load->model("adminmaster_models");
		$select = array('atsm_id','atsm_code','nd_id as ndid','level_type','upline_id as upid','firmname','fname','mname','lname','lname','username','email','email2','contact','contact2','pan_no','gst_no','status','state','city','address','acnt_name','acnt_email','acnt_contact','inserted_on','updated_on','(select atsm_code from atsm where upid = atsm_id Order by atsm_id desc limit 1) as upline_code','(select nd_code from ndistributor where ndid = nd_id Order by nd_id desc limit 1) as nd_code','(select firmname from ndistributor where ndid = nd_id Order by nd_id desc limit 1) as nd_firmname','(select date_time from recent_login_user where user_code = atsm_code AND type = "1" Order by id desc limit 1) as last_login','(select state_name from area where state = state_id Order by area_id desc limit 1) as state_name','(select city_name from area where city = city_id Order by area_id desc limit 1) as city_name','(select taluka_name from area where taluka = taluka_id Order by area_id desc limit 1) as taluka_name','(select image_url from images where ref_code = atsm_id AND type = "0" Order by id desc limit 1) as image');
		if($this->session->userdata('user_type') == 1){
			$where = array('status !=' => '2');			
		}else{
			$where = array('status !=' => '2','nd_id' => $this->session->userdata('id'));
		}
		$upline_id = '0';
		if(!empty($_POST['upline_id'])){
			$upline_id = $this->input->post('upline_id');
			$filter= array('upline_id' => $upline_id);
			$where = array_merge($where,$filter);
		}
		
		if(@$_POST['submit']=='createxls'){
			$data['data'] = $this->adminmaster_models->atsm_list($select,'atsm', $where,'atsm_id');		
			$this->generate_report_excel($data['data']);			
		}
		//Pagination Start
		$config = array();
		$config["base_url"] = site_url() . "/Ndmaster/atsm";
		$config["total_rows"] = $this->base_models->get_count('atsm_id','atsm', $where);
		$config["per_page"] = 10;
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$pagedata["links"] = $this->pagination->create_links();
		$pagedata['results'] = $this->adminmaster_models->atsm_list($select,'atsm', $where,'atsm_id',$config["per_page"], $page);
		//Pagination End
		$pagedata['asm_list']=$this->base_models->GetAllValues('atsm', array('level_type' => '0'), array('atsm_id','atsm_code','fname','lname'));
		// $pagedata['delete_link'] = 'Ndmaster/delete_atsm';		
		$pagedata['select'] = array('upline_id' => $upline_id);		
		$this->renderView('National_distributor/Master/atsm',$pagedata);
	}
	
	public function add_atsm()
	{
		if($this->session->userdata('user_type') == 3){
			redirect(site_url('Ndmaster/distributor'));
		}
		
		$pagedata['state_list']=$this->base_models->get_state();
		$pagedata['city_list']=$this->base_models->get_city();
		// $pagedata['taluka_list']=$this->base_models->get_taluka_bycityid($this->session->userdata('city'));
		$pagedata['taluka_list']=$this->base_models->get_taluka();
		//$pagedata['state_id']=$this->session->userdata('state');
		$pagedata['asm_list']=$this->base_models->GetAllValues('atsm', array('level_type' => '0'), array('atsm_id','atsm_code','fname','lname'));
		$this->renderView('National_distributor/Master/add-atsm',$pagedata);
	}
	
	public function insert_atsm()
	{		
		$this->form_validation->set_rules('upline_id', 'Parent ASM', 'trim');
		$this->form_validation->set_rules('fname', 'First name', 'trim|required');
		$this->form_validation->set_rules('lname', 'Last Name', 'trim|required');
		// $this->form_validation->set_rules('status', 'Status', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'trim|required');
		$this->form_validation->set_rules('contact', 'Mobile No.', 'trim|required|numeric');
		$this->form_validation->set_rules('state', 'State', 'trim|required');
		$this->form_validation->set_rules('city', 'City', 'trim|required');
		$this->form_validation->set_rules('address', 'Address', 'trim|required');
		$this->form_validation->set_rules('pan', 'PAN', 'trim|required');
		$this->form_validation->set_rules('inputPassword', 'Password', 'trim|required');
		$current_date = date("Y-m-d H:i:s");
	
	// echo '<pre>';
	// print_r($_POST);
	// die();
		$error='';
			// if (empty($_FILES['image']['name'][0])){
				// $this->form_validation->set_rules('image', 'Image', 'required');
			// }
			$data['file_name'] = '';
            // if(!empty($_FILES['image']['name'])){
                // $config['upload_path'] = 'uploads/nd/profile/';
                // $config['allowed_types'] = 'gif|jpg|png';
                // $this->upload->initialize($config);
				// if($this->upload->do_upload('image')){
					// $data = $this->upload->data();
				// }else{
                    // $imageerrors = $this->upload->display_errors();
					// $this->form_validation->set_message('image', $imageerrors);					
                // }
			// }
			
			if($this->input->post('upline_id') != '0'){
				$level_type = '1';							
			}else{
				$level_type = '0';
			}
			
			if($this->form_validation->run())
			{					
				$insert_array=array(
						'upline_id'=>$this->input->post('upline_id'),
						'fname'=>$this->input->post('fname'),
						'mname'=>$this->input->post('mname'),
						'lname'=>$this->input->post('lname'),
						'status'=>$this->input->post('status'),
						'email'=>$this->input->post('email'),
						'email2'=>$this->input->post('email2'),
						'contact'=> $this->input->post('contact'),
						'contact2'=> $this->input->post('contact2'),
						'state'=> $this->input->post('state'),
						'city'=> $this->input->post('city'),
						'address'=> $this->input->post('address'),
						'pan_no'=>strtoupper($this->input->post('pan')),
						// 'gst_no'=>strtoupper($this->input->post('gst')),
						// 'acnt_name'=>$this->input->post('acnt_name'),
						// 'acnt_email'=>$this->input->post('acnt_email'),
						// 'acnt_contact'=>$this->input->post('acnt_contact'),
						'password'=>md5($this->input->post('inputPassword')),
						// 'profile_pic'=>$data['file_name'],
						'nd_id'=>$this->session->userdata('id'),
						'level_type'=>$level_type,
						'inserted_on'=>date("Y-m-d H:i:s")
					);
					//print_r($insert_array);exit;
					if($this->base_models->add_records('atsm',$insert_array)){
						$this->session->set_flashdata('success','Added successfully');
						redirect(site_url('Ndmaster/atsm'));
					}else{
						$this->session->set_flashdata('error','Not added Please try again');
						//redirect(base_url('admin/add_user'));
					}
			}
			$this->renderView('National_distributor/Master/add-atsm');
	}
	
	public function edit_atsm()
	{
		if($this->session->userdata('user_type') == 3){
			redirect(site_url('Ndmaster/distributor'));
		}
		
		$id = base64_decode($_GET['id']);
		$pagedata['state_list']=$this->base_models->get_state();
		$pagedata['city_list']=$this->base_models->get_city();
		// $pagedata['taluka_list']=$this->base_models->get_taluka_bycityid($this->session->userdata('city'));
		$pagedata['state_id']=$this->session->userdata('state');
		$pagedata['data']=$this->base_models->GetSingleDetails('atsm', array('atsm_id' => $id), $select = "*");
		$pagedata['asm_list']=$this->base_models->GetAllValues('atsm', array('level_type' => '0'), array('atsm_id','atsm_code','fname','lname'));		
		$this->renderView('National_distributor/Master/edit-atsm',$pagedata);
	}
	
	public function update_atsm()
	{
		$id = base64_decode($_GET['id']);		
		if($id==''){
			redirect(base_url('Ndmaster/national_distributor')); 
		}
				
		$this->form_validation->set_rules('upline_id', 'Upline', 'trim');
		$this->form_validation->set_rules('fname', 'First name', 'trim|required');
		$this->form_validation->set_rules('lname', 'Last Name', 'trim|required');
		// $this->form_validation->set_rules('status', 'Status', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'trim|required');
		$this->form_validation->set_rules('contact', 'Mobile No.', 'trim|required|numeric');
		$this->form_validation->set_rules('state', 'State', 'trim|required');
		$this->form_validation->set_rules('city', 'City', 'trim|required');
		$this->form_validation->set_rules('address', 'Address', 'trim|required');
		$this->form_validation->set_rules('pan', 'PAN', 'trim|required');
		
		if($this->input->post('inputPassword') != ''){
			$password = md5($this->input->post('inputPassword'));
		}else{
			$password = $this->input->post('oldpassword');
		}
		$current_date = date("Y-m-d H:i:s");
	
		$error='';
			// if (empty($_FILES['image']['name'][0])){
				// $this->form_validation->set_rules('image', 'Image', 'required');
			// }
			// $data['file_name'] = '';
            // if(!empty($_FILES['image']['name'])){
                // $config['upload_path'] = 'uploads/admin/users/';
                // $config['allowed_types'] = 'gif|jpg|png';
                // $this->upload->initialize($config);
				// if($this->upload->do_upload('image')){
					// $data = $this->upload->data();
				// }else{
                    // $imageerrors = $this->upload->display_errors();
					// $this->form_validation->set_message('image', $imageerrors);					
                // }
			// }
			
			if($this->input->post('inputPassword') != ''){
				$password = md5($this->input->post('inputPassword'));
			}else{
				$password = $this->input->post('oldpassword');
			}

			if($this->input->post('upline_id') != '0'){
				$level_type = '1';							
			}else{
				$level_type = '0';
			}
			
			if($this->form_validation->run())
			{				
				$update_array=array(
						'upline_id'=>$this->input->post('upline_id'),
						'fname'=>$this->input->post('fname'),
						'mname'=>$this->input->post('mname'),
						'lname'=>$this->input->post('lname'),
						'status'=>$this->input->post('status'),
						'email'=>$this->input->post('email'),
						'email2'=>$this->input->post('email2'),
						'contact'=> $this->input->post('contact'),
						'contact2'=> $this->input->post('contact2'),
						'state'=> $this->input->post('state'),
						'city'=> $this->input->post('city'),
						'address'=> $this->input->post('address'),
						'pan_no'=>strtoupper($this->input->post('pan')),
						'password'=>$password,
						'level_type'=>$level_type,
						// 'profile_pic'=>$data['file_name'],
						'updated_on'=>date("Y-m-d H:i:s")
					);
				$where_array = array('atsm_id'=>$id);
				//print_r($insert_array);exit;
				if($this->base_models->update_records('atsm',$update_array,$where_array) == true){
					$this->session->set_flashdata('success','Edited successfully');
				}else{
					$this->session->set_flashdata('error','Not added Please try again');
				}
			}
				redirect(site_url('Ndmaster/edit_atsm?id='.base64_encode($id)));
			
		// $pagedata['data']=$this->base_models->get_users('',$id);
		// $this->renderView('Admin/edit-user',$pagedata);
	}
			
	public function delete_atsm()
	{
		$id = $_GET['id'];
		$current_date = date("Y-m-d H:i:s");
		$update_array = array(
							'status'=>'2',
							'deleted_on'=>$current_date
							);
		$where_array = array('d_id'=>$id);
		if($this->base_models->update_records('distributor',$update_array,$where_array) == true){
			$data['status'] = 'success';
			$data['message'] = 'Successfully deleted';
		}else{
			$data['status'] = 'error';
			$data['message'] = 'Somting went worng please try again';
		}
		echo json_encode($data);
		die();
	}
	//---------- END ASM/TSM ------------//	
	
	
	//---------- Retailer Start ------------//
	public function retailer_list()
	{		
		$this->load->model("adminmaster_models");
		
		$nd_id = $this->session->userdata('id');
		$under_nd_d = $this->base_models->Custome_quary("SELECT d_id FROM distributor where nd_id = $nd_id"); //select under distributor
		$did_ids = implode(',', array_column($under_nd_d,'d_id'));//make it to string
		$total_rows = $this->base_models->Custome_quary("SELECT count(rt_id) as counting FROM retailer WHERE d_id IN ($did_ids)");
		$select = "Select rt_id,rt_code,firmname as rt_firmname,tsm_id,(select atsm_code from atsm where atsm_id = tsm_id Order by atsm_id desc limit 1) as rt_tsm_code,(select fname from atsm where atsm_id = tsm_id Order by atsm_id desc limit 1) as rt_tsm_fname,(select lname from atsm where atsm_id = tsm_id Order by atsm_id desc limit 1) as rt_tsm_lname,(select taluka_name from area where taluka = taluka_id Order by area_id desc limit 1) as rt_taluka_name,d_id as did,(select d_code from distributor where did = d_id) as d_code,(select firmname from distributor where did = d_id) as d_firnname,(select city from distributor where did = d_id) as d_city_id,(select city_name from area where city_id = d_city_id Order by area_id desc limit 1) as d_city_name,(select nd_id from distributor where did = d_id) as ndid,(select nd_code from ndistributor where ndid = nd_id) as nd_code,(select firmname from ndistributor where ndid = nd_id) as nd_firnname,(select state from ndistributor where ndid = nd_id) as nd_state_id,(select state_name from area where state_id = nd_state_id Order by area_id desc limit 1) as nd_state_name";
		
		if(@$_POST['submit']=='createxls'){
			$data['data'] = $this->base_models->Custome_quary("$select FROM retailer WHERE d_id IN ($did_ids)");
			$this->generate_retailer_list_excel($data['data']);		
		}
		
		$rt_code_post = (@$this->input->post('rt_code')) ? $this->input->post('rt_code') : '';
		if($rt_code_post != ''){
			$rt_code = trim($rt_code_post);
			$select = array('rt_id','rt_code','firmname as rt_firmname','tsm_id','(select atsm_code from atsm where atsm_id = tsm_id Order by atsm_id desc limit 1) as rt_tsm_code','(select fname from atsm where atsm_id = tsm_id Order by atsm_id desc limit 1) as rt_tsm_fname','(select lname from atsm where atsm_id = tsm_id Order by atsm_id desc limit 1) as rt_tsm_lname','(select taluka_name from area where taluka = taluka_id Order by area_id desc limit 1) as rt_taluka_name','d_id as did','(select d_code from distributor where did = d_id) as d_code','(select firmname from distributor where did = d_id) as d_firnname','(select city from distributor where did = d_id) as d_city_id','(select city_name from area where city_id = d_city_id Order by area_id desc limit 1) as d_city_name','(select nd_id from distributor where did = d_id) as ndid','(select nd_code from ndistributor where ndid = nd_id) as nd_code','(select firmname from ndistributor where ndid = nd_id) as nd_firnname','(select state from ndistributor where ndid = nd_id) as nd_state_id','(select state_name from area where state_id = nd_state_id Order by area_id desc limit 1) as nd_state_name');
			$where = array('status !=' => '2');					
			$filter = array();
			
			//Filter Process	
			if($rt_code != null){
				$rt_code_post = (@$rt_code) ? $rt_code : '';
				$rt_code = trim($rt_code_post);
				$array_items = $this->session->set_userdata(array("rt_code"=>$rt_code));
				$filter = array("rt_code"=>$rt_code);
			}			
			$where = array_merge($where,$filter);		
			//End Filter Process
		
			//Pagination Start
			$config = array();
			$config["base_url"] = site_url() . "/Ndmaster/retailer_list";
			$config["total_rows"] = $this->base_models->get_count('rt_id','retailer', $where);
			$config["per_page"] = 10;
			$config["uri_segment"] = 3;
			$this->pagination->initialize($config);
			$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
			$pagedata["links"] = $this->pagination->create_links();
			$pagedata['results'] = $this->adminmaster_models->retailer_list($select,'retailer', $where,'rt_id',$config["per_page"], $page);
			
		}else{		
			//Pagination Start
			$config = array();
			$config["base_url"] = site_url() . "/Ndmaster/retailer_list";
			$config["total_rows"] = $total_rows[0]['counting'];
			$config["per_page"] = 10;
			$config["uri_segment"] = 3;
			$this->pagination->initialize($config);
			$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
			$pagedata["links"] = $this->pagination->create_links();
				$limit = $config["per_page"];
				$results = $this->base_models->Custome_quary("$select FROM retailer WHERE d_id IN ($did_ids) limit $limit offset $page");
			$pagedata['results'] = $results; 
			//Pagination End
		}
		
		$pagedata['delete_link'] = 'Dmaster/delete_rt';
		$rt_code = (@$rt_code) ? $rt_code : '';
		$pagedata['select']=array('rt_code'=>$rt_code);
		$this->renderView('National_distributor/Master/retailer_list',$pagedata);
	}
	
	//generate to excel	
	public function generate_retailer_list_excel($param1){
		// create file name
		$fileName = 'Retailerslist'.'-data-'.date('d-M-Y').'.xlsx';   
		// load excel library
		$this->load->library('excel');
		$info = $param1;
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->setActiveSheetIndex(0);
		
		// set Header		
		$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Retailer Code');
		$objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Retailer FirmName');
		$objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Retailer Taluka');
		$objPHPExcel->getActiveSheet()->SetCellValue('D1', 'TSM Code');
		$objPHPExcel->getActiveSheet()->SetCellValue('E1', 'TSM Name');
		$objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Distributor Code');
		$objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Distributor FirmName');
		$objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Distributor City');
		$objPHPExcel->getActiveSheet()->SetCellValue('I1', 'ND Code');
		$objPHPExcel->getActiveSheet()->SetCellValue('J1', 'ND FirmName');
		$objPHPExcel->getActiveSheet()->SetCellValue('K1', 'ND State');
		
		// set Row
		$rowCount = 2;
	
		foreach ($info as $element) {
			$objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $element['rt_code']);
			$objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount,  $element['rt_firmname']);
			$objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $element['rt_taluka_name']);
			$objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $element['rt_tsm_code']);
			$objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $element['rt_tsm_fname'].' '.$element['rt_tsm_lname']);
			$objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $element['d_code']);
			$objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $element['d_firnname']);
			$objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $element['d_city_name']);
			$objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $element['nd_code']);
			$objPHPExcel->getActiveSheet()->SetCellValue('J' . $rowCount, $element['nd_firnname']);
			$objPHPExcel->getActiveSheet()->SetCellValue('K' . $rowCount, $element['nd_state_name']);
			$rowCount++;
		}
		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
		$objWriter->save('uploads/admin/excel/'.$fileName);
		// download file
		header("Content-Type: application/vnd.ms-excel");
		redirect(base_url('uploads/admin/excel/'.$fileName));
	}
	//---------- END Retailer ------------//
		
}
