<?php
require_once APPPATH . 'core/Base_Controller.php'; //Load Base Controller
defined('BASEPATH') OR exit('No direct script access allowed');

class Ndstock extends Base_Controller 
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Kolkata');
		$this->load->model("ndstock_models"); // load Item moels
		if(!$this->session->userdata('__ci_last_regenerate') || $this->session->userdata('user_type') != 3){
			$this->session->set_flashdata('error', 'You Are not Allowed to access this file...!');
			redirect('login');
		}
	}
	
	
	//new stock under report
	//generate to excel	
	public function generate_stock_excel($param1){
		// create file name
		$fileName = 'StockReport'.'-data-'.date('d-M-Y').'.xlsx';   
		// load excel library
		$this->load->library('excel');
		$info = $param1;
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->setActiveSheetIndex(0);
		// set Header
		$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Code');
		$objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Firm Name');
		$objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Level');
		$objPHPExcel->getActiveSheet()->SetCellValue('D1', 'IMEI');
		$objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Item Code');
		$objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Date');
		// set Row
		$rowCount = 2;
	
		foreach ($info as $element) {
			$objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $element['code']);
			$objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $element['firmname']);
			$objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $element['level']);
			$objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $element['imei']);
			$objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $element['item_code']);
			$objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, date('d-M-Y', strtotime($element['stock_date'])));
			$rowCount++;
		}
		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
		$objWriter->save('uploads/admin/excel/'.$fileName);
		// download file
		header("Content-Type: application/vnd.ms-excel");
		redirect(base_url('uploads/admin/excel/'.$fileName));
	}
	
	public function nd()
	{			
		$imei = null;
		if($this->session->userdata('imei')){
			$this->session->userdata('imei');
		}
	
	  //Pagination Start
		$config = array();
		$config["base_url"] = site_url() . "/Ndstock/nd";
		$config["total_rows"] = $this->ndstock_models->get_all_stock_count();
		$config["per_page"] = 10;
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$pagedata["links"] = $this->pagination->create_links();
		$pagedata['results'] = $this->ndstock_models->get_all_stock($imei,$config["per_page"], $page);     
		//Pagination End
		$imei = (@$imei) ? $imei : '';
		$pagedata['select']=array('imei'=>$imei);  
		$this->renderView('National_distributor/Stock/nd',$pagedata);
	}
		
	// with ci pagination in php
	public function nd_sess()
	{
		$imei = null;
		
		//Filter Process	
		if(@$_POST['submit']=='filter' || @$_POST['submit']=='createxls'){
			$imei_no = (@$this->input->post('imei')) ? $this->input->post('imei') : '';
			$imei = trim($imei_no);
			$array_items = $this->session->set_userdata(array("imei"=>$imei));
		}else{
			if($this->session->userdata('imei') != NULL){
				$imei = $this->session->userdata('imei');
			}
		}
		
		if(@$_POST['submit']=='createxls'){
			$data['data'] = $this->ndstock_models->get_all_stock();
			// die($this->db->last_query());
			$this->generate_stock_excel($data['data']);		
		}
		//End Filter Process
	
		//Pagination Start
		$config = array();
		$config["base_url"] = site_url() . "/Ndstock/nd_sess";
		$config["total_rows"] = $this->ndstock_models->get_all_stock_count($imei);
		$config["per_page"] = 10;
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$pagedata["links"] = $this->pagination->create_links();
		$pagedata['results'] = $this->ndstock_models->get_all_stock($imei,$config["per_page"], $page);  
		//Pagination End

		$imei = (@$imei) ? $imei : '';
		$pagedata['select']=array('imei'=>$imei);  
		$this->renderView('National_distributor/Stock/nd',$pagedata);
	}
	//new stock end
	
	
    	public function stock_list()
		{			

            $select	 = array('stnd_id','item_id','item_code','imei','nd_id','nd_code','upload_date');
			$where = array('item_status'=> '0');
			$pagedata['delete_link'] = 'Adminitems/delete_item';
		
			$imei ='';
		
		  if($this->session->userdata('imei') ){
				$this->session->userdata('imei');
			}
          //Pagination Start
			$config = array();
	        $config["base_url"] = site_url() . "/Ndstock/stock_list";
	        $config["total_rows"] = $this->base_models->get_count('stnd_id','tbl_sales_to_nd', $where);
	        $config["per_page"] = 10;
	        $config["uri_segment"] = 3;
	        $this->pagination->initialize($config);
	        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
	        $pagedata["links"] = $this->pagination->create_links();
	        $pagedata['results'] = $this->base_models->get_pagination('tbl_sales_to_nd', $where,'stnd_id',$config["per_page"], $page);     
           	//Pagination End

           	$imei = (@$imei) ? $imei : '';
           	$pagedata['select']=array('imei'=>$imei);  
	        $this->renderView('National_distributor/Stock/stock_list',$pagedata);
		}

		// with ci pagination in php
		public function sale_list_sess()
		{

           // print_r($_POST);
			$select	 = array('stnd_id','item_id','item_code','imei','nd_id','nd_code','upload_date');
			$where = array('item_status'=> '0');


			if(@$_POST['submit']=='filter' || @$_POST['submit']=='createxls')
		       {
		       	  	$imei_no = (@$this->input->post('imei')) ? $this->input->post('imei') : '';
		       		$imei = trim($imei_no);

		       
		           $array_items = $this->session->set_userdata(array("imei"=>$imei));
		            if($imei !=''){
		            	$filter =  array('imei'=> $imei);
		            	$where = array_merge($where,$filter);	
		            }else{
		            } 
						if($this->session->userdata('imei') != NULL){
							$imei = $this->session->userdata('imei'); 
							$filter =  array('imei'=> $imei);
							$where = array_merge($where,$filter);
						} 
				}



			 if(@$_POST['submit']=='createxls')
           	  {

           	  //	$select	 = array('item_id','company_code','company_name','upload_date','item_code','item_name','imei');
				//$where .= array('status'=> '1');
				$data['data'] = $this->ndstock_models->GetAllItemValues('tbl_sales_to_nd', $where, $select);
		   /*   echo '<pre>';
		       print_r($data['data']); die;*/
				//Export xls
					$this->generate_unsold_item_excel($data['data']);			
					

              }

            $config = array();
	        $config["base_url"] = site_url() . "/Ndstock/sale_list_sess";
	        $config["total_rows"] = $this->base_models->get_count('stnd_id','tbl_sales_to_nd', $where);
	        $config["per_page"] = 10;
	        $config["uri_segment"] = 3;
	        $this->pagination->initialize($config);
	        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
	        $pagedata["links"] = $this->pagination->create_links();
	        $pagedata['results'] = $this->base_models->get_pagination('tbl_sales_to_nd', $where,'stnd_id',$config["per_page"], $page);     
           	//Pagination End

           	$imei = (@$imei) ? $imei : '';
           	$pagedata['select']=array('imei'=>$imei);  
	        $this->renderView('National_distributor/Stock/stock_list',$pagedata);
			
		}	

//generate to excel	
	public function generate_unsold_item_excel($param1){
		// create file name
		$fileName = 'ItemList'.'-data-'.date('d-M-Y').'.xlsx';   
		// load excel library
		$this->load->library('excel');
		$info = $param1;
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->setActiveSheetIndex(0);
		// set Header
		$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'IMEI');
		$objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Item Code');
		$objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Uploading Date');
		// set Row
		$rowCount = 2;
	
		foreach ($info as $element) {
			$objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $element['imei']);
			$objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $element['item_code']);
			$objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, date('d-M-y', strtotime($element['upload_date'])));
			$rowCount++;
		}
		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
		$objWriter->save('uploads/admin/excel/'.$fileName);
		// download file
		header("Content-Type: application/vnd.ms-excel");
		redirect(base_url('uploads/admin/excel/'.$fileName));
	}			
	
	// retailer stock
	public function rt_stock()
	{	
		$this->load->model("d_sales_models"); // load nd sales models 
		$select	 = array('rt_id','rt_code','firmname');
		// $where = array('d_id' => $this->session->userdata('id'));
		$where = array();
		$pagedata['rt_list'] = $this->d_sales_models->GetAllItemValues('retailer', $where, $select);
 

        $select	 = array('strt_id','item_code','imei','rt_code','item_status','upload_date');
		$where = array();
		$pagedata['delete_link'] = 'Ndstock/delete_sale_item';
		$status ='';
		$imei ='';
		$rt_id ='';
		if($this->session->userdata('imei') || $this->session->userdata('status')|| $this->session->userdata('rt_id')){
			$this->session->userdata('imei');
			$this->session->userdata('status');
			$this->session->userdata('rt_id');
		}
    
      //Pagination Start
		$config = array();
        $config["base_url"] = site_url() . "/Ndstock/rt_stock";
        $config["total_rows"] = $this->base_models->get_count('strt_id','tbl_sales_to_rt', $where);
        $config["per_page"] = 10;
        $config["uri_segment"] = 3;
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $pagedata["links"] = $this->pagination->create_links();
        $pagedata['results'] = $this->base_models->get_pagination('tbl_sales_to_rt', $where,'strt_id',$config["per_page"], $page);     
       	//Pagination End

       	$imei = (@$imei) ? $imei : '';
       	$sstatus = (@$status) ? $status : null;
       	@strcmp($status,$sstatus);
       	$rt_id1 = (@$rt_id) ? $rt_id : null;
       	@strcmp($d_id,$d_id1);
        $pagedata['select']=array('status'=>$status,'imei'=>$imei,'rt_id'=>$rt_id);  
        $this->renderView('National_distributor/Stock/rt_stock',$pagedata);
	}

	// with ci pagination in php
	public function rt_stock_sess()
	{
		$this->load->model("d_sales_models"); // load nd sales models 
		$select	 = array('rt_id','rt_code','firmname');
		// $where1 = array('d_id' => $this->session->userdata('id'));
		$where1 = array();
		$pagedata['rt_list'] = $this->d_sales_models->GetAllItemValues('retailer', $where1, $select);
		
        $select	 = array('strt_id','item_code','imei','rt_code','item_status','upload_date');
		$where = array();
		$pagedata['delete_link'] = 'Ndstock/delete_sale_item';
		$status ='';
		$imei ='';
		
		//Filter Process
       if(@$_POST['submit']=='filter' || @$_POST['submit']=='createxls')
       {
       	  	$imei_no = (@$this->input->post('imei')) ? $this->input->post('imei') : '';
       		$imei = trim($imei_no);

       	    $status = (@$this->input->post('status')!= null) ? $this->input->post('status') : '';
          
           $rt_id = (@$this->input->post('rt_id')!= null) ? $this->input->post('rt_id') : '';
           $array_items = $this->session->set_userdata(array("imei"=>$imei,"status"=>$status,"rt_id"=>$rt_id));
            if($imei !=''){
            	$filter =  array('imei'=> $imei);
            	$where = array_merge($where,$filter);	
            }  
            if($status !=''){
           		$filter =  array('item_status'=>$status);
            	$where = array_merge($where,$filter);	
            } 
            if($rt_id !=''){
           		$filter =  array('rt_id'=>$rt_id);
            	$where = array_merge($where,$filter);         
			}
			
			if($this->session->userdata('imei') != NULL){
				$imei = $this->session->userdata('imei'); 
				$filter =  array('imei'=> $imei);
				$where = array_merge($where,$filter);
			} 
			if($this->session->userdata('status') != NULL){
				$status = $this->session->userdata('status'); 
				$filter =  array('item_status'=>$status);
				$where = array_merge($where,$filter);
			}
			if($this->session->userdata('rt_id') != NULL){
				$rt_id = $this->session->userdata('rt_id'); 
				$filter =  array('rt_id'=>$rt_id);
				$where = array_merge($where,$filter);
			}
       	}
		 // for secondpage
		if($this->session->userdata('imei') != NULL){
			$imei = $this->session->userdata('imei'); 
			$filter =  array('imei'=> $imei);
			$where = array_merge($where,$filter);
		} 
		if($this->session->userdata('status') != NULL){
			$status = $this->session->userdata('status'); 
			$filter =  array('item_status'=>$status);
			$where = array_merge($where,$filter);
		}
		if($this->session->userdata('rt_id') != NULL){
			$rt_id = $this->session->userdata('rt_id'); 
			$filter =  array('rt_id'=>$rt_id);
			$where = array_merge($where,$filter);
		}
       		
		if(@$_POST['submit']=='createxls'){
       	  	$data['data'] = $this->d_sales_models->GetAllItemValues('tbl_sales_to_rt', $where, $select);
			$this->generate_sale_excel($data['data']);	
		}
		//End Filter Process
    
      //Pagination Start
		$config = array();
        $config["base_url"] = site_url() . "/Ndstock/rt_stock_sess";
        $config["total_rows"] = $this->base_models->get_count('strt_id','tbl_sales_to_rt', $where);
        $config["per_page"] = 10;
        $config["uri_segment"] = 3;
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $pagedata["links"] = $this->pagination->create_links();
        $pagedata['results'] = $this->base_models->get_pagination('tbl_sales_to_rt', $where,'strt_id',$config["per_page"], $page);     
       	//Pagination End
	
		$imei = (@$imei) ? $imei : '';
		$sstatus = (@$status) ? $status : null;
		@strcmp($status,$sstatus);
		$rt_id1 = (@$rt_id) ? $rt_id : null;
		@strcmp($rt_id,$rt_id1);
		@$pagedata['select']=array('status'=>$status,'imei'=>$imei,'rt_id'=>$rt_id);  
       $this->renderView('National_distributor/Stock/rt_stock',$pagedata);
	}	
	
	public function generate_sale_excel($param1){
		// create file name
		$fileName = 'StockOfRetailer'.'-data-'.date('d-M-Y').'.xlsx';   
		// load excel library
		$this->load->library('excel');
		$info = $param1;
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->setActiveSheetIndex(0);
		// set Header
		$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Sale Date');
		$objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Retailer Code');
		$objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Retailer Firm Name');
		$objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Item Code');
		$objPHPExcel->getActiveSheet()->SetCellValue('E1', 'IMEI');
		$objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Status');
		
		// set Row
		$rowCount = 2;
	
		foreach ($info as $element) {
			$objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, date('d-M-y', strtotime($element['upload_date'])));
			$objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $element['rt_code']);
			$rt_code = $element['rt_code'];
			$sql = $this->db->query("select firmname from retailer where rt_code = '$rt_code'");
			$d_array = $sql->result_array();
			$objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $d_array[0]['firmname']);
			$objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount,  $element['item_code']);
			$objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount,  $element['imei']);
			$pay_status = ($element['item_status'] == '0') ? 'Unsold' : 'Sold';
			$objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $pay_status);			
			$rowCount++;
		}
		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
		$objWriter->save('uploads/admin/excel/'.$fileName);
		// download file
		header("Content-Type: application/vnd.ms-excel");
		redirect(base_url('uploads/admin/excel/'.$fileName));
	}
	
		
}
