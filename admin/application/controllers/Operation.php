<?php
/*********************************
Note: Load this script in every page of admin
<!-- Sparkline chart JavaScript -->
<script src="<?php echo base_url();?>assets/plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/bower_components/jquery-sparkline/jquery.charts-sparkline.js"></script>
<script src="<?php echo base_url();?>assets/plugins/bower_components/toast-master/js/jquery.toast.js"></script>
<!-- Load Common/Admin Custome JS -->
<script src="<?php echo base_url();?>assets/js/common/common.js"></script>
<script src="<?php echo base_url();?>assets/js/admin/admin.js"></script>
**************************************/
require_once APPPATH . 'core/Base_Controller.php';
defined('BASEPATH') OR exit('No direct script access allowed');

class Operation extends Base_Controller 
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Kolkata');
		
		// user_type 1=admin, 2=HeadMarketing, 3=Marketing, 4=Accounts, 5=Operation, 6=Analyzer
		if(!$this->session->userdata('__ci_last_regenerate') || $this->session->userdata('user_type') != 5){
			$this->session->set_flashdata('error', 'You Are not Allowed to access this file...!');
			redirect('login');
		}

	}

	public function dashboard()
	{
		$pagedata['total_users'] = $this->base_models->count_users();
		$pagedata['total_clients'] = $this->base_models->count_clients();
		$pagedata['total_pending_adv'] = $this->base_models->count_adv('0');
		$pagedata['total_rejected_adv'] = $this->base_models->count_adv('1');
		$pagedata['total_approved_adv'] = $this->base_models->count_adv('2');
		$pagedata['total_publish_adv'] = $this->base_models->count_adv('3');
		$pagedata['total_pending_invoice'] = $this->base_models->count_invoice('2');
		$pagedata['total_approved_invoice'] = $this->base_models->count_invoice('1');
		
		$this->renderView('Operation/dashboard',$pagedata);
	}
	 	
		
}
