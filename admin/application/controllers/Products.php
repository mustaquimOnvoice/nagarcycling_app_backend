<?php
require_once APPPATH . 'core/Base_Controller.php'; //Load Base Controller
defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends Base_Controller 
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Kolkata');
		$this->load->model("item_models"); // load Item moels
		if(!$this->session->userdata('__ci_last_regenerate') || $this->session->userdata('user_type') != 1){
			$this->session->set_flashdata('error', 'You Are not Allowed to access this file...!');
			redirect('login');
		}
	}
	
	public function product_list()
	{			
		$select	 = array('*','(select image_url from images where ref_code = products.id and type = "5" order by id desc limit 1) as image');
		$where = array('prod_status'=> '1');
		$pagedata['delete_link'] = 'Products/delete_product';
			
		//Pagination Start
		$config = array();
		$config["base_url"] = site_url() . "/Products/product_list";
		$config["total_rows"] = $this->base_models->get_count('id','products', $where);
		$config["per_page"] = 10;
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$pagedata["links"] = $this->pagination->create_links();
		$pagedata['results'] = $this->base_models->get_pagination_data($select,'products', $where,'id',$config["per_page"], $page);
		//Pagination End
		$this->renderView('Admin/Products/product_list',$pagedata);
	}

    public function add_product()
	{
		$this->renderView('Admin/Products/add_product');
	}
    	
	public function insert_product()
	{
		$this->form_validation->set_rules('model', 'Model', 'trim|required');
		$this->form_validation->set_rules('tech_spec', 'Technical Specification', 'trim|required');
		$this->form_validation->set_rules('usps', 'USPS', 'trim|required');
		$this->form_validation->set_rules('dp', 'DP', 'trim|required');
		$this->form_validation->set_rules('mop', 'MOP', 'trim|required');
		$this->form_validation->set_rules('mrp', 'MRP', 'trim');
		$current_date = date("Y-m-d H:i:s");
	
		$error='';
			// if (empty($_FILES['image']['name'][0])){
				// $this->form_validation->set_rules('image', 'Image', 'required');
			// }			
			if($this->form_validation->run())
			{					
				$insert_array=array(
						'model'=>$this->input->post('model'),
						'tech_spec'=>$this->input->post('tech_spec'),
						'usps'=>$this->input->post('usps'),
						'dp'=>$this->input->post('dp'),
						'mop'=>$this->input->post('mop'),
						'mrp'=>$this->input->post('mrp'),
						'prod_status'=>'1',
						'inserted_on'=>$current_date
					);
					//print_r($insert_array);exit;
					if($this->base_models->add_records('products',$insert_array)){
						$insert_id = $this->db->insert_id();
						
						//upload image
						$data['file_name'] = '';
						if(!empty($_FILES['image']['name'])){
							$config['upload_path'] = 'uploads/tv/';
							$config['allowed_types'] = 'gif|jpg|png';
							$this->upload->initialize($config);
							if($this->upload->do_upload('image')){
								$data = $this->upload->data();
								$path = base_url().''.$config['upload_path'].''.$data['file_name'];
								$insert_image=array(
									'image_url'=>$path,
									'type'=>'5',
									'ref_code'=>$insert_id
								);
								$this->base_models->add_records('images',$insert_image);
							}else{
								$imageerrors = $this->upload->display_errors();
								$this->form_validation->set_message('image', $imageerrors);					
							}
						}						
						$this->session->set_flashdata('success','Added successfully');
						redirect(site_url('Products/product_list'));
					}else{
						$this->session->set_flashdata('error','Not added Please try again');
					}
			}
			redirect(site_url('Products/add_product'));
	}
	
	public function delete_product()
	{
		$id = $_GET['id'];
		$current_date = date("Y-m-d H:i:s");
		$update_array = array(
							'prod_status'=>'0',
							'deleted_on'=>$current_date
							);
		$where_array = array('id'=>$id);
		if($this->base_models->update_records('products',$update_array,$where_array) == true){
			$data['status'] = 'success';
			$data['message'] = 'Successfully deleted';
		}else{
			$data['status'] = 'error';
			$data['message'] = 'Somting went worng please try again';
		}
		echo json_encode($data);
		die();
	}
	
	public function edit_product()
	{
		$id = base64_decode($_GET['id']);
		$wherecondition = array('id' => $id);
		$pagedata['data']=$this->base_models->GetSingleDetails('products', $wherecondition,"*,(select image_url from images where type = 5 and ref_code = products.id ORDER BY id limit 1) as image");
		$this->renderView('Admin/Products/edit_product',$pagedata);
	}
	
	public function update_product()
	{
		$id = base64_decode($_GET['id']);		
		if($id==''){
			redirect(site_url('Products/product_list')); 
		}
		
		$this->form_validation->set_rules('model', 'Model', 'trim|required');
		$this->form_validation->set_rules('tech_spec', 'Technical Specification', 'trim|required');
		$this->form_validation->set_rules('usps', 'USPS', 'trim|required');
		$this->form_validation->set_rules('dp', 'DP', 'trim|required');
		$this->form_validation->set_rules('mop', 'MOP', 'trim|required');
		$this->form_validation->set_rules('mrp', 'MRP', 'trim');
		$this->form_validation->set_rules('prod_status', 'Status', 'trim');
		$current_date = date("Y-m-d H:i:s");
		$error='';
			
			if($this->form_validation->run())
			{
				if(!empty($_FILES['image']['name']))
				{
					$config1['upload_path'] = 'uploads/tv/';
					$config1['allowed_types'] = 'gif|jpg|png|jpeg';
					$this->upload->initialize($config1);
					if($this->upload->do_upload('image')){
						$data = $this->upload->data();
						$path = base_url().''.$config1['upload_path'].''.$data['file_name'];
						$insert_image=array(
							'image_url'=>$path,
							'type'=>'5',
							'ref_code'=>$id
						);
						if(!empty($this->input->post('image1'))){
							unlink($this->input->post('image1'));
						}
						$this->base_models->add_records('images',$insert_image);
					}else{
						$imageerrors = $this->upload->display_errors();
						$this->form_validation->set_message('image', $imageerrors);					
					}
				}else{
					$image = $config1['upload_path'].$this->input->post('image1');
				}
				
				$update_array=array(
						'model'=>$this->input->post('model'),
						'tech_spec'=>$this->input->post('tech_spec'),
						'usps'=>$this->input->post('usps'),
						'dp'=>$this->input->post('dp'),
						'mop'=>$this->input->post('mop'),
						'mrp'=>$this->input->post('mrp'),
						'prod_status'=>$this->input->post('prod_status'),
						'updated_on'=>date("Y-m-d H:i:s")
					);
				$where_array = array('id'=>$id);
				//print_r($insert_array);exit;
				if($this->base_models->update_records('products',$update_array,$where_array) == true){
					$this->session->set_flashdata('success','Edited successfully');
				}else{
					$this->session->set_flashdata('error','Not added Please try again');
				}
			}
		redirect(site_url('Products/edit_product/?id='.base64_encode($id)));
	}
	
	//Scheme
	public function scheme_list()
	{			
		$select	 = array('id','image_url as image','created_at','scheme_date');
		$where = array('type'=> '6');
		$pagedata['delete_link'] = 'Products/delete_scheme';
			
		//Pagination Start
		$config = array();
		$config["base_url"] = site_url() . "/Products/scheme_list";
		$config["total_rows"] = $this->base_models->get_count('id','images', $where);
		$config["per_page"] = 10;
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$pagedata["links"] = $this->pagination->create_links();
		$pagedata['results'] = $this->base_models->get_pagination_data($select,'images', $where,'id',$config["per_page"], $page);
		//Pagination End
		$this->renderView('Admin/Products/scheme_list',$pagedata);
	}

    public function add_scheme()
	{
		$this->renderView('Admin/Products/add_scheme');
	}
    	
	public function insert_scheme()
	{
		$this->form_validation->set_rules('scheme_date', 'Date', 'trim|required');
		$current_date = date("Y-m-d H:i:s");	
		$error='';
		if($this->form_validation->run())
		{					
			//upload image
			$data['file_name'] = '';
			if(!empty($_FILES['image']['name'])){
				$config['upload_path'] = 'uploads/tv/';
				$config['allowed_types'] = 'gif|jpg|png';
				$this->upload->initialize($config);
				if($this->upload->do_upload('image')){
					$data = $this->upload->data();
					$path = base_url().''.$config['upload_path'].''.$data['file_name'];
					$insert_image=array(
						'image_url'=>$path,
						'scheme_date'=>date('Y-m-d', strtotime($this->input->post('scheme_date'))),
						'type'=>'6',
						'ref_code'=>'0'
					);
					if($this->base_models->add_records('images',$insert_image) == true){
						$this->session->set_flashdata('success','Added successfully');
					}else{
						$this->session->set_flashdata('error','Not added Please try again');
					}
				}else{
					$imageerrors = $this->upload->display_errors();
					$this->form_validation->set_message('image', $imageerrors);					
				}
			}						
		}
		redirect(site_url('Products/scheme_list'));
	}
	
	public function delete_scheme()
	{
		$id = $_GET['id'];
		$current_date = date("Y-m-d H:i:s");
		$update_array = array(
							'type'=>'7',
							'deleted_on'=>$current_date
							);
		$where_array = array('id'=>$id);
		if($this->base_models->update_records('images',$update_array,$where_array) == true){
			$data['status'] = 'success';
			$data['message'] = 'Successfully deleted';
		}else{
			$data['status'] = 'error';
			$data['message'] = 'Somting went worng please try again';
		}
		echo json_encode($data);
		die();
	}
	
	public function edit_scheme()
	{
		$id = base64_decode($_GET['id']);
		$wherecondition = array('id' => $id);
		$pagedata['data']=$this->base_models->GetSingleDetails('images', $wherecondition,"*, image_url as image");
		$this->renderView('Admin/Products/edit_scheme',$pagedata);
	}
	
	public function update_scheme()
	{
		$id = base64_decode($_GET['id']);		
		if($id==''){
			redirect(site_url('Products/scheme_list')); 
		}
		
		$this->form_validation->set_rules('scheme_date', 'Date', 'trim|required');
		$this->form_validation->set_rules('type', 'Type', 'trim|required');
		$current_date = date("Y-m-d H:i:s");
		$error='';			
		if($this->form_validation->run())
		{
			// if(!empty($_FILES['image']['name']))
			// {
				$where_array = array('id'=>$id);
				$config1['upload_path'] = 'uploads/tv/';
				$config1['allowed_types'] = 'gif|jpg|png|jpeg';
				$this->upload->initialize($config1);
				
				if($this->upload->do_upload('image')){
					$data = $this->upload->data();
					$path = base_url().''.$config1['upload_path'].''.$data['file_name'];
					if(!empty($this->input->post('image1'))){
						unlink($this->input->post('image1'));
					}
				}else{
					$path = $this->input->post('image1');
				}
				
				$update_array=array(
					'image_url'=>$path,
					'type'=>$this->input->post('type'),
					'scheme_date'=>date('Y-m-d', strtotime($this->input->post('scheme_date'))),
					'ref_code'=>'0'
				);
				
				if($this->base_models->update_records('images',$update_array,$where_array)){
					$this->session->set_flashdata('success','Updated successfully');
				}else{
					$this->session->set_flashdata('error','Not updated Please try again');
				}
			// }else{
				// $this->session->set_flashdata('error','Not updated Please try again');
			// }			
		}
		redirect(site_url('Products/edit_scheme/?id='.base64_encode($id)));
	}
	
}
