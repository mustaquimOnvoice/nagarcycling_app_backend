<?php
require_once APPPATH . 'core/Base_Controller.php'; //Load Base Controller
defined('BASEPATH') OR exit('No direct script access allowed');

class Rtvisit extends Base_Controller 
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Kolkata');
		if(!$this->session->userdata('__ci_last_regenerate') || $this->session->userdata('user_type') != 1){
			$this->session->set_flashdata('error', 'You Are not Allowed to access this file...!');
			redirect('login');
		}
	}
	
	public function rt_visit_list()
	{
		$select	 = array('rv.rt_visit_id', 'rv.visit_date', 'rv.visit_time', 'rv.lati', 'rv.longi', 'rv.atsm_id', 'rv.atsm_code','rv.rt_code', '(select firmname from retailer where rt_id = rv.rt_id ORDER BY rt_id LIMIT 1) as rt_firmname', '(select fname from atsm where atsm_id = rv.atsm_id ORDER BY atsm_id LIMIT 1) as tsm_fname', '(select mname from atsm where atsm_id = rv.atsm_id ORDER BY atsm_id LIMIT 1) as tsm_mname', '(select lname from atsm where atsm_id = rv.atsm_id ORDER BY atsm_id LIMIT 1) as tsm_lname', '(select image_url from images where ref_code = rv.rt_visit_id AND type="3" ORDER BY id) as image');
		$where = array();
		
		//Pagination Start
		$config = array();
		$config["base_url"] = site_url() . "/Rtvisit/rt_visit_list";
		$config["total_rows"] = $this->base_models->get_count('rt_visit_id','rt_visit', $where);
		$config["per_page"] = 10;
		$config["uri_segment"] = 3;
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$pagedata["links"] = $this->pagination->create_links();
		$pagedata['results'] = $this->base_models->get_pagination_data($select,'rt_visit as rv',$where = NULL,$orderby= 'rv.rt_visit_id',$config["per_page"], $page);     
		//Pagination End		
		$this->renderView('Admin/Report/rt_visit_list',$pagedata);
	}
		
}
