<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller 
{
public function __construct()
	{
date_default_timezone_set('Asia/Kolkata');
		parent::__construct();
		$this->load->database();
		$id = $this->session->userdata('user_id');
		$user_type=$this->session->userdata('user_type');
		if(!$this->session->userdata('ci_session'))
			{
				redirect('login');
			}
		
		if($user_type==0)
			{
				redirect('login');
			}
		
		
	}

/***********************************************************************/
public function dashboard()
	{
		$id = $this->session->userdata('user_id');
		$cat_id=$this->db->query("select category_id from tbl_users where user_id=$id")->row()->category_id;
		$pagedata['count_cat']=$this->db->query("select * from tbl_uploads where category_id IN($cat_id) AND status=0")->num_rows();
		$this->load->view('User/dashboard',$pagedata);
	}
	
	

public function download_file()
	{
		$id 	= 	$this->session->userdata('user_id');
		$cat_id	=	$this->db->query("select category_id from tbl_users where user_id=$id")->row()->category_id;
		$id =	$this->db->query("SELECT * FROM tbl_uploads WHERE category_id IN ($cat_id) AND status = 0 order by file_id desc")->result_array();
		$pagedata['file_details'] = $id;
		$this->load->view('User/download_file',$pagedata);
	}	
	
}