<?php
require_once APPPATH . 'core/Base_Controller.php';
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class login extends Base_Controller {
	public function __construct() {
		parent::__construct ();
	}
	
    public function status() {
        $response ['message'] = "done";
        $response ['version'] =  "1.0";
        $response ['maintenance_mode'] =  false;
        $response ['data'] =  $this->Base_Models->GetSingleDetails('tbl_status', array('status' => '1') , array('(select image_url from images where type = "4" AND ref_code = tbl_status.id ORDER BY id DESC limit 1) as image'));
        $response ['data']->title = 'Anuron';
        $response ['data']->description = 'Anuron तो life ON';
		echo json_encode($response);
    }
	
	//Login ASM/TSM
	public function index() {
		$response ['message'] = "fail";
		$response ['result'] =  "Param is reqired";
		if( isset($_POST['atsm_code']) && isset($_POST['device_token']) && isset($_POST['password']) && isset($_POST['mobile_model']) && isset($_POST['os']) && isset($_POST['imei']) ){
			$atsm_code = $_POST['atsm_code'];
			$password = md5(trim($_POST['password']));
			
			$this->load->model("adminmaster_models");
			$select = array('atsm_id','atsm_code','os','mobile_model','imei','nd_id as ndid','level_type','upline_id as upid','firmname','fname','mname','lname','lname','username','email','email2','contact','contact2','pan_no','gst_no','status','state','city','address','acnt_name','acnt_email','acnt_contact','inserted_on','updated_on','(select atsm_code from atsm where upid = atsm_id Order by atsm_id desc limit 1) as upline_code','(select nd_code from ndistributor where ndid = nd_id Order by nd_id desc limit 1) as nd_code','(select firmname from ndistributor where ndid = nd_id Order by nd_id desc limit 1) as nd_firmname','(select date_time from recent_login_user where user_code = atsm_code AND type = "1" Order by id desc limit 1) as last_login','(select state_name from area where state = state_id Order by area_id desc limit 1) as state_name','(select city_name from area where city = city_id Order by area_id desc limit 1) as city_name','(select taluka_name from area where taluka = taluka_id Order by area_id desc limit 1) as taluka_name');
			$where = array('status !=' => '2','atsm_code' => $atsm_code,'password' => $password);		
			$temp1 = $this->adminmaster_models->atsm_list($select,'atsm', $where,'atsm_id','1');
			
				if(count($temp1)==0){
					$response ['message'] = "fail";
					$res = array('atsm_id'=> null,'atsm_code'=> null,'os'=> null,'mobile_model'=> null,'imei'=> null,'ndid'=> null,'level_type'=> null,'upid'=> null,'firmname'=> null,'fname'=> null,'mname'=> null,'lname'=> null,'username'=> null,'email'=> null,'email2'=> null,'contact'=> null,'contact'=> null,'contact2'=> null,'pan_no'=> null,'gst_no'=> null,'status'=> null,'state'=> null,'city'=> null,'address'=> null,'acnt_name'=> null,'acnt_email'=> null,'acnt_contact'=> null,'inserted_on'=> null,'updated_on'=> null,'upline_code'=> null,'nd_code'=> null,'nd_firmname'=> null,'last_login'=> null,'state_name'=> null,'city_name'=> null,'taluka_name'=> null,'image'=> null,);
					$response ['result'] = $res;
				}else{
					if($temp1[0]['imei'] == ''){//if empty - it will only update in first time
						$TableValues["device_token"] = $_POST['device_token'];
						$update = array ("device_token"=>$_POST['device_token'], "os"=>$_POST['os'], "mobile_model"=>$_POST['mobile_model'], "imei"=>$_POST['imei']);
						$this->Base_Models->UpadateValue ( "atsm",$update, array("atsm_code" => $atsm_code) );
						$response ['message'] = "done";
						$temp1[0]['os'] = $_POST['os'];
						$temp1[0]['mobile_model'] = $_POST['mobile_model'];				
						$temp1[0]['imei'] = $_POST['imei'];	
						foreach ($temp1 as $key => $value) {
							$image_url= $this->Base_Models->GetAllValues ( "images" ,array("ref_code"=>$value['atsm_code'] ,"type"=>"0"));
							if(!empty($image_url)){
								$temp1[$key]["image"] = $image_url[0]['image_url'];
							}else{
								$temp1[$key]["image"] = base_url().'uploads/avatar.png';
							}
						}				
						$response ['result'] = $temp1[0];
						
					}elseif($temp1[0]['imei'] != $_POST['imei']){						
						$response ['message'] = "fail";
						$response ['result'] = 'You cannot login from another mobile';
					}else{
						$response ['message'] = "done";
						$temp1[0]['os'] = $_POST['os'];
						$temp1[0]['mobile_model'] = $_POST['mobile_model'];				
						$temp1[0]['imei'] = $_POST['imei'];	
						foreach ($temp1 as $key => $value) {
							$image_url= $this->Base_Models->GetAllValues ( "images" ,array("ref_code"=>$value['atsm_code'] ,"type"=>"0"));
							if(!empty($image_url)){
								$temp1[$key]["image"] = $image_url[0]['image_url'];
							}else{
								$temp1[$key]["image"] = base_url().'uploads/avatar.png';
							}
						}				
						$response ['result'] = $temp1[0];
					}
				}
		}
		echo json_encode ($response);
	}
	
	public function test() {
		$postdata = file_get_contents("php://input");
		$request =  json_decode($postdata);
		
		$response ['message'] = "fail";
		$response ['result'] =  "Param is reqired";
		
		$_POST['atsm_code'] = $request->atsmCode;
		$_POST['device_token'] = $request->deviceToken;
		$_POST['mobile_model'] = $request->mobileModel;
		$_POST['password'] = $request->password;
		$_POST['os'] = $request->os;
		$_POST['imei'] = $request->imei;
		
		
		if( isset($_POST['atsm_code']) && isset($_POST['device_token']) && isset($_POST['password']) && isset($_POST['mobile_model']) && isset($_POST['os']) && isset($_POST['imei']) ){
			$atsm_code = $_POST['atsm_code'];
			$password = md5(trim($_POST['password']));
			
			$this->load->model("adminmaster_models");
			$select = array('atsm_id','atsm_code','os','mobile_model','imei','nd_id as ndid','level_type','upline_id as upid','firmname','fname','mname','lname','lname','username','email','email2','contact','contact2','pan_no','gst_no','status','state','city','address','acnt_name','acnt_email','acnt_contact','inserted_on','updated_on','(select atsm_code from atsm where upid = atsm_id Order by atsm_id desc limit 1) as upline_code','(select nd_code from ndistributor where ndid = nd_id Order by nd_id desc limit 1) as nd_code','(select firmname from ndistributor where ndid = nd_id Order by nd_id desc limit 1) as nd_firmname','(select date_time from recent_login_user where user_code = atsm_code AND type = "1" Order by id desc limit 1) as last_login','(select state_name from area where state = state_id Order by area_id desc limit 1) as state_name','(select city_name from area where city = city_id Order by area_id desc limit 1) as city_name','(select taluka_name from area where taluka = taluka_id Order by area_id desc limit 1) as taluka_name');
			$where = array('status !=' => '2','atsm_code' => $atsm_code,'password' => $password);		
			$temp1 = $this->adminmaster_models->atsm_list($select,'atsm', $where,'atsm_id','1');
			
				if(count($temp1)==0){
					$response ['message'] = "fail";
					$res = array('atsm_id'=> null,'atsm_code'=> null,'os'=> null,'mobile_model'=> null,'imei'=> null,'ndid'=> null,'level_type'=> null,'upid'=> null,'firmname'=> null,'fname'=> null,'mname'=> null,'lname'=> null,'username'=> null,'email'=> null,'email2'=> null,'contact'=> null,'contact'=> null,'contact2'=> null,'pan_no'=> null,'gst_no'=> null,'status'=> null,'state'=> null,'city'=> null,'address'=> null,'acnt_name'=> null,'acnt_email'=> null,'acnt_contact'=> null,'inserted_on'=> null,'updated_on'=> null,'upline_code'=> null,'nd_code'=> null,'nd_firmname'=> null,'last_login'=> null,'state_name'=> null,'city_name'=> null,'taluka_name'=> null,'image'=> null,);
					$response ['data'] = $res;
					$response ['result'] = 'UserName Password wrong';
				}else{
					if($temp1[0]['imei'] == ''){//if empty - it will only update in first time
						$TableValues["device_token"] = $_POST['device_token'];
						$update = array ("device_token"=>$_POST['device_token'], "os"=>$_POST['os'], "mobile_model"=>$_POST['mobile_model'], "imei"=>$_POST['imei']);
						$this->Base_Models->UpadateValue ( "atsm",$update, array("atsm_code" => $atsm_code) );
						$response ['message'] = "done";
						$temp1[0]['os'] = $_POST['os'];
						$temp1[0]['mobile_model'] = $_POST['mobile_model'];				
						$temp1[0]['imei'] = $_POST['imei'];	
						foreach ($temp1 as $key => $value) {
							$image_url= $this->Base_Models->GetAllValues ( "images" ,array("ref_code"=>$value['atsm_code'] ,"type"=>"0"));
							if(!empty($image_url)){
								$temp1[$key]["image"] = $image_url[0]['image_url'];
							}else{
								$temp1[$key]["image"] = base_url().'uploads/avatar.png';
							}
						}				
						$response ['data'] = $temp1[0];
						$response ['result'] = 'Successfully login';
						
					}elseif($temp1[0]['imei'] != $_POST['imei']){						
						$response ['message'] = "fail";
						$response ['result'] = 'You cannot login from another mobile';
					}else{
						$response ['message'] = "done";
						$temp1[0]['os'] = $_POST['os'];
						$temp1[0]['mobile_model'] = $_POST['mobile_model'];				
						$temp1[0]['imei'] = $_POST['imei'];	
						foreach ($temp1 as $key => $value) {
							$image_url= $this->Base_Models->GetAllValues ( "images" ,array("ref_code"=>$value['atsm_code'] ,"type"=>"0"));
							if(!empty($image_url)){
								$temp1[$key]["image"] = $image_url[0]['image_url'];
							}else{
								$temp1[$key]["image"] = base_url().'uploads/avatar.png';
							}
						}				
						$response ['data'] = $temp1[0];
						$response ['result'] = 'Successfully login';
					}
				}
		}
		echo json_encode ($response);
	}
}
?>