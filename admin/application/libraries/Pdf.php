<?php

// defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
/**
 *
 *
 * CodeIgniter PDF Library
 *
 * Generate PDF's in your CodeIgniter applications.
 *
 * @package CodeIgniter
 * @subpackage Libraries
 * @category Libraries
 * @author Chris Harvey
 * @license MIT License
 * @link https://github.com/chrisnharvey/CodeIgniter- PDF-Generator-Library
 *      
 *      
 *      
 *      
 */
require_once __DIR__.'/dompdf/autoload.inc.php';

use Dompdf\Dompdf;
use Dompdf\FontMetrics;
class Pdf extends DOMPDF {
	/**
	 * Get an instance of CodeIgniter
	 *
	 * @access protected
	 * @return void
	 */
	protected function ci() {
		return get_instance ();
	}
	
	/**
	 * Load a CodeIgniter view into domPDF
	 *
	 * @access public
	 * @param string $view
	 *        	The view to load
	 * @param array $data
	 *        	The view data
	 * @return void
	 */
	public function load_view($view, $data = array()) {
		//
		$dompdf = new Dompdf ();
		$html = $this->ci ()->load->view ( $view, $data, true );
		$dompdf->loadHtml ( $html );
		// (Optional) Setup the paper size and orientation
		$dompdf->setPaper ( 'A4', 'portrait' );
		// Render the HTML as PDF
		$dompdf->render ();
		$canvas = $dompdf->get_canvas ();
		$options = new \Dompdf\Options ();
		$fontMetrics = new FontMetrics ( $canvas, $options );
		$font = $fontMetrics->getFont ( $data ['font'] );
		if (isset ( $data ['page_count'] ) && $data ['page_count'] == true) {
			$canvas->page_text ( 520, 720, "{PAGE_NUM} of {PAGE_COUNT}", $font, 10, array (
					0,
					0,
					0 
			), 0, 1 );
		}
		// $canvas->page_text ( 110, 250 - 240, "TRIAL", $fontMetrics->getFont ( "verdana", "bold" ), 50, array (
		// 0.92,
		// 0.92,
		// 0.92
		// ), 0, 1 );
		// $canvas->
		// $canvas->image ( 'http://subodhancapacitor.unitglo.com/uploads/1111.PNG', 20, 500, 550, 100 );
		
		// $dompdf->page_text ( 110, $h - 240, "TRIAL", Font_Metrics::get_font ( "verdana", "bold" ), 110, array (
		// 0.92,
		// 0.92,
		// 0.92
		// ), 0, - 58 );
		// $time = time ();
		if ($data ['save_to_local'] != 2) {
			// Output the generated PDF to Browser
			$dompdf->stream ( $data ['file_path'], array (
					"Attachment" => $data ['save_to_local'] 
			) );
		}
		if (isset ( $data ['save_to_send'] ) && $data ['save_to_send'] == true)
			if (! file_exists ( $data ['file_path'] ))
				if (file_put_contents ( $data ['file_path'], $dompdf->output () )) {
					// header ( 'Location: ' . $data ['file_path'] );
					return "done";
				} else
					return "fail";
	}
}