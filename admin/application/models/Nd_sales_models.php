<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

class nd_sales_models extends CI_Model {
	
	function __construct() {
		/* Call the Model constructor */
		parent::__construct ();
	}
	// getting all Item values
	function GetAllItemValues($TableName, $wherecondition = null, $select = "*") {
		$this->db->select ( $select );
		if (isset ( $wherecondition ))
			$this->db->where ( $wherecondition );
		$this->db->from ( $TableName );
		 $this->db->order_by('d_id', 'DESC');
		$querys = $this->db->get ();
		return $querys->result_array ();
	}

	function get_users($select = '*', $id = '', $searchText = '', $page='', $segment='')
    {
        $this->db->select($select);
        $this->db->from('wwc_admin');
        if(!empty($searchText)) {
            $likeCriteria = "(username  LIKE '%".$searchText."%'
                            OR  email  LIKE '%".$searchText."%'
                            OR  contact  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
		if(!empty($id)) {
			$this->db->where(array('id'=>$id));
		}
        $this->db->where('status !=', 2);
        // $this->db->order_by('id', 'DESC');
		if(!empty($page)) {
			$this->db->limit($page, $segment);
		}
        $query = $this->db->get();
        
		if(!empty($id)) {
			$result = $query->row(); 
		}else{
			$result = $query->result();
		}
        return $result;
    }

    public function get_faulty_list($nd_id,$limit, $start){
		$select = "tif.imei,tif.reason, tif.d_date as return_date,(select item_code from tbl_items where imei = tif.imei Order by imei desc limit 1) as item_code,(select inserted_on from tbl_sales_to_d where imei = tif.imei Order by imei desc limit 1) as sale_date, (select d_code from distributor where d_id = tif.d_id Order by d_id desc limit 1) as d_code, (select firmname from distributor where d_id = tif.d_id Order by d_id desc limit 1) as tofirmname";
		$where = "tif.nd_id='$nd_id' and tif.level_type='1' and nd_status='0' ";			
		$this->db->select($select,FALSE)
				->from('tbl_item_faulty as tif');
				$this->db->where($where);
				$this->db->order_by('tif.nd_date','desc');
		if(isset ( $limit )){
			$this->db->limit( $limit, $start );
		}
		$res = $this->db->get();
		return $res->result_array();
	}
		public function get_faulty_list_cnt($nd_id){
		$select = "tif.imei";
		$where = "tif.nd_id='$nd_id'";			
		$this->db->select($select,FALSE)
				->from('tbl_item_faulty as tif');;
				$this->db->where($where);
				
		return $res = $this->db->get()->num_rows(); 
	}

}
?>