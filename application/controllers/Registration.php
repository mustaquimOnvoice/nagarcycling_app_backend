<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH . 'core/class.phpmailer.php';
require_once APPPATH . 'core/class.smtp.php';
class Registration extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function message_send_bulk($message1, $number) {
		//$numsStr = implode(',',$number);
		$postData = "{ 
			\"sender\": \"NGRCYC\", 
			\"route\": \"4\", 
			\"country\": \"91\", 
			\"unicode\": \"1\",
			\"sms\": [ 
						{ 
							\"message\": \"$message1\", 
							\"to\": [ $number ] 
						}
					] 
		}";
			
		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://api.msg91.com/api/v2/sendsms?country=91",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => $postData,
		  CURLOPT_SSL_VERIFYHOST => 0,
		  CURLOPT_SSL_VERIFYPEER => 0,
		  CURLOPT_HTTPHEADER => array(
			"authkey: 228445AqdIYICptZd5d36a66d",
			"content-type: application/json"
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		   "cURL Error #:" . $err;
		} else {
		  $response;
		}
	}
	public function index()
	{  
	  /* $km1 = $this->db->query("select user_id from tbl_user where category='1Km'")->num_rows();
	   $km3 = $this->db->query("select user_id from tbl_user where category='3Km'")->num_rows();
	   $km5 = $this->db->query("select user_id from tbl_user where category='5Km'")->num_rows();
	    
	    $pagedata['data']['remaining1km'] = 100-$km1;
	    $pagedata['data']['remaining3km'] = 100-$km3;
	    $pagedata['data']['remaining5km'] = 100-$km5;*/
	     $remainingallkm = $this->db->query("select user_id from tbl_user")->num_rows();
	      $pagedata['data']['remainingallkm'] = 300-$remainingallkm;
	    	
		$this->load->view('registration',$pagedata);
		$this->load->view('footer');
	}


	public function validmno()
	{  
	   $mno = $_POST['mno'];
		 $data = $this->db->query("select *  from tbl_registraton_nagarcycling2021 where mno='$mno'")->result_array();
		 $cnt = count($data);
		 if($cnt>'0'){
		 	echo 'exist';
		 }else{
		 	echo 'not exist';
		 }
	}

	public function validemail()
	{  

	   $email = $_POST['email'];
		 $data = $this->db->query("select *  from tbl_registraton_nagarcycling2021 where email='$email'")->result_array();
		
		 $cnt1 = count($data);
		 if($cnt1>'0'){
		 	echo 'exist';
		 }else{
		 	echo 'not exist';
		 }
	}
	
	public function confirm_form()
	{ 
	 
	/*	echo "<pre>";
		print_r($_POST);
		die;*/
		 
	   	$insert_array	=	array(
									'name'  						=> $this->input->post('name'),
									'email'   						=> $this->input->post('email'),
									'mno'   						=> $this->input->post('mobile_no'),
									'gender'   						=> $this->input->post('gender'),
									'dob'   						=> date('Y-m-d', strtotime($this->input->post('dob'))),
									'age'   						=> $this->input->post('age'),
									'category'     					=> $this->input->post('category'),
								    'blood_group'   				=> $this->input->post('blood_group'),
								  	'address'   					=> $this->input->post('address'),
									'city'   						=> $this->input->post('city'),
									'state'   						=> $this->input->post('state'),
									'pincode'   					=> $this->input->post('pincode'),
									'country'   					=> $this->input->post('country'),
									'tshirt_size'   				=> $this->input->post('t-shirt'),
									'emergency_contact_name'   		=> $this->input->post('emergency_name'),
									'emergency_contact_number'   	=> $this->input->post('emergency_number'),
								 );
	  
	  /* 	echo "<pre>";
		print_r($insert_array);
		die;*/

	   	$this->db->insert('tbl_registraton_nagarcycling2021_temp',$insert_array);
   
     
	   	$cat = $this->input->post('category');
	   	if($cat=='20KM'){
	   	   $price_array= array('price' => '1');
	   	}
	   	else if($cat=='50KM'){
	   	   $price_array= array('price' => '1');
	   	}
	   	else if($cat=='100KM'){
	   	   $price_array= array('price' => '1');
	   	}
	   $merge = array_merge($insert_array, $price_array);
	 
	   	$data['records'] = 	$merge;
	   	$this->load->view('confirmation_form',$data);
		$this->load->view('footer');
	}

	public function welcome(){

	}
		
	public function payment_success()
	{
    //  print_r($_POST);die;
	$email =  $_POST['udf2'];
	$phone =	$_POST['udf3'];
	$txnid =	$_POST['txnid'];
	/* $email =  "sak@gmail.com";
	$phone =	"7896541230";
	$txnid =	"810355";*/
	$datau = $this->db->query("select *  from tbl_registraton_nagarcycling2021 where mno='$phone' and email='$email' and txnId='$txnid' order by user_id desc")->result_array();
	$cntu = count($datau); 
	 		if($cntu>0){	 	
	     		$this->load->view('success');
				$this->load->view('footer');
			}else{	 
				$data = $this->db->query("select *  from tbl_registraton_nagarcycling2021_temp  where  `mno`='$phone' and  `email`='$email' order by `user_temp_id` desc")->result_array();
			
			/* echo '<pre>';
			print_r($data);
			die;*/
			$cnt = count($data);

			if($cnt>0){
						// code for bibcode
				
				$cat =$data[0]['category'];
				$result = $this->db->query("SELECT MAX(`bib_id`) AS `bib_id` FROM `tbl_registraton_nagarcycling2021` WHERE `category` = '$cat'")->row();
				$bib = $result->bib_id;
				
				if($bib=='' && $data[0]['category']=='20KM'){
					$bibid='2000';
				}else if($bib=='' && $data[0]['category']=='50KM'){
					$bibid='5000';
				}else if($bib=='' && $data[0]['category']=='100KM'){
					$bibid='10000';
				}else{
					$bibid=$bib;
				}
				$final_bib_id = $bibid+1;
				$insert_array1	=	array(
											'name'  						=> $data[0]['name'],
											'mno'   						=>$data[0]['mno'],
											'email'   						=> $data[0]['email'],
											'dob'   						=> date("Y-m-d", strtotime($data[0]['dob'])),
											'category'     					=>$data[0]['category'],
											'state'   						=> $data[0]['state'],
											'address'   					=> $data[0]['address'],
											'city'   						=> $data[0]['city'],
											'pincode'   					=> $data[0]['pincode'],
											'country'   					=> $data[0]['country'],
											'gender'   						=> $data[0]['gender'],
											'blood_group'   				=> $data[0]['blood_group'],
											'tshirt_size'   				=> $data[0]['tshirt_size'],
											'emergency_contact_name'   		=> $data[0]['emergency_contact_name'],
											'emergency_contact_number'   	=>$data[0]['emergency_contact_number'],
											'age'   						=> $data[0]['age'],
											'txnId'  					 	=> $txnid,
											'bib_id' 					  	=> $final_bib_id,												
											);

				$pagedata['data']=$insert_array1;
				$this->db->insert('tbl_registraton_nagarcycling2021',$insert_array1);
			// $emailinvoice = $this->load->view('invoicePdf',$pagedata,true);
			
				$pagedata1['data'] = $this->db->query("select *  from tbl_registraton_nagarcycling2021 where mno='$phone' and email='$email' and bib_id='$final_bib_id'")->result_array();
			
			
				/*  $emailinvoice =  '<html lang="en">
					<head>
						<title></title>
						<meta charset="utf-8">
						<meta http-equiv="X-UA-Compatible" content="IE=edge">
						<meta name="viewport" content="width=device-width, initial-scale=1">
						<meta name="description" content="">
						<meta name="author" content="">
						<link rel="icon" type="image/png" sizes="16x16" href="<?php // echo base_url();?>assets/plugins/images/favicon.ico">
						<script src="https://www.w3schools.com/lib/w3data.js"></script>
					</head>
					<body>
					<table border="1" width="100%" align="center" style="padding:10px 10px">
							<thead>
								<tr><th colspan="3" style="background-color:#ff0000";><h1 style="color:#fff" ><b>Ahmednagar Cyclothon 2020</b></h1></th>
								</tr>
								<tr>
								<th colspan="3">
									<p align="left"> You Have Successfully Registered For the Ahmednagar Cyclothon 2020.</p>   <br>
									<p align="left">This is a computer generated Invoice and does not require the signature. If you have any further queries kindly visit our website.</p>  <br>
									<p align="left">Your Registration Invoice No, Date and BIB No. are mentioned below:</p>   
								</th>
								</tr>
								<tr>
								<th ><h5><b>Invoice No: <br>'.$txnid.'</b></h5></th>
								<th ><h5><b>Invoice Date: <br>'.date("d-M-Y").'</b></h5></th>
								<th ><h5><b>BIB No: <?php  <br>'.$final_bib_id.'</b></h5></th>
								</tr>
							
							</thead>
						</table> <table width="100%" align="center" style="padding:10px 10px">
							<tr>
								<td>
									<address>
									<h3>To,</h3>
									<h4 >'. $data["0"]["name"].',</h4>
									<p >'.$data["0"]["address"].','.$data["0"]["city"].','.$data["0"]["state"].'</p>
									</address>
								</td>
								<td align="right">
									<address>
									<h4 style="padding-right: 37px;"> <b> Ahmednagar Cyclothon 2020</b></h4>
									<p>S.No 103, Ekta Colony, <br/>
										Kedgaon Devi Road, <br/>
										Ahmednagar - 414005
									</p>
									</address>
								</td>
							</tr>
						</table>
					<table border="1" width="100%" align="center" style="padding:10px 10px">
						<thead>
							<tr>
								<th width="5%">#</th>
								<th width="55%">Race Category</th>
								<th width="15%">No. of Registration</th>
								<th width="25%">Amount</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td width="5%">1</td>
								<td width="55%">
										Category - '.$data["0"]["category"].'  <br>
									
								</td>
								<td width="15%" align="center">1 </td>
								<td width="25%" align="center">'. $ca =($data["0"]["category"] =="20KM" ? "600": ($data["0"]["category"] =="50KM" ? "700":"900")).'</td>
							</tr>
						</tbody>
					</table><table width="100%" align="center" style="padding:10px 10px">
						<thead>
							<tr>
								<th></th>
							</tr>
						</thead>
					<tbody>
						
							<tr>
								<td align="right">
									<address>
										<h5 class="font-bold">Ahmednagar Cyclothon 2020,</h5>
										<p class="text-muted">Authorised Signatory</p>
									</address>
								</td>
							</tr>
						</tbody>    
						</table>
						<hr width="100%" align="center">
						<table width="100%" align="center">
							<tr>
								<td>This is a computer generated Invoice and does not require the signature. If you have any question regarding this invoice please email us at support@nagarcycling.com</td>
							</tr>
						</table>
					</body>
					</html>' ;   */
			
			
				$this->load->library('html2pdf');
				
				//Set folder to save PDF to
				$this->html2pdf->folder('./assets/pdfs/');
				
				//Set the filename to save/download as
				$this->html2pdf->filename('Invoice.pdf');
				
				//Set the paper defaults
				$this->html2pdf->paper('a4', 'portrait');
				
			/*  $data = array(
					'title' => 'PDF Created',
					'message' => 'Hello World!'
				);*/
			
				//Load html view
				$this->html2pdf->html($this->load->view('invoicePdf',$pagedata1,TRUE));
				$path = $this->html2pdf->create('save'); 
					$name = $data[0]['name']; 
					$ride = $data[0]['category'];
					$mno = $phone;	
					
					$emailinvoice = "Congratulations! You Have Successfully Registered For India's Biggest Cyclothon- Ahmednagar Cyclothon 2021,Season 4.";
					$msg = "Congratulations! You Have Successfully Registered For India's Biggest Cyclothon- Ahmednagar Cyclothon 2021, Season 4. Your BIB ID is: $final_bib_id  Ride Category: $ride. Keep Riding! ";
					$this->message_send_bulk($msg, $mno);
					$insert_array=array(
									'number'=>$mno,
									'message'=>$msg
									);
					$this->db->insert('sms_sent',$insert_array);

			
					$subj = "Registration Successful For Ahmednagar Cyclothon 2021";
						$emailmsg = "You Have Done Successfully Registration For Ahmednagar Cyclothon 2021";
				
									$developmentMode=false;
							$mail = new PHPMailer ($developmentMode);

								if ($developmentMode) {
								$mail->SMTPOptions = [
									'ssl'=> [
									'verify_peer' => false,
									'verify_peer_name' => false,
									'allow_self_signed' => true
									]
								];
								}
					$mail->CharSet   = "UTF-8";	
					$mail->IsSMTP (); // set mailer to use SMTP
					$mail->SMTPAuth = true; // turn on SMTP authentication
					$mail->Host = "mail.nagarcycling.com"; // specify main and backup server
					$mail->Port = 587; // set the port to use
					$mail->SMTPAuth = true; // turn on SMTP authentication
					$mail->Username = "support@nagarcycling.com"; // your SMTP username or your gmail username
					$mail->Password = "nagarcycling@123"; // your SMTP password or your gmail password
					
					$from = "support@nagarcycling.com"; // Reply to this email
					$name = "Ahmednagar Cyclothone 2021"; // Recipient's name
					$mail->From = $from;
					$mail->FromName = "Ahmednagar Cyclothone 2021"; // Name to indicate where the email came from when the recepient received
					
					
					
					$mail->AddAddress ( $email, $name );
					$mail->AddReplyTo ( "support@nagarcycling.com", "Ahmednagar Cyclothon 2021" );
					//$mail->AddCC("support@urjaacademy.com","Admin");
					$mail->IsHTML ( true ); // send as HTML 
					//print_r($this->input->post('message'));exit;
					$mail->Subject =  $subj;
					$mail->Body = $emailinvoice;
					$mail->addAttachment($path); 
					$mail->Send();

				$Path = './assets/pdfs/Invoice.pdf';
				unlink($Path);
				//$mail->Send();
				$insert_array11=array(
					'email'=>$email,
					'subject'=>  $subj,
					'message'=>'invoice'
					);
				$this->db->insert('email_sent',$insert_array11);

				$this->load->view('success');
				$this->load->view('footer');
			}
    	}
	}

	public function payment_success_redirect(){
		$this->load->view('success');
		$this->load->view('footer');
	}

	public function payment_success_app() {
		if(!empty($_POST)){
			//die('l');die;
			$temp_id =$_POST['udf1'];
			$email =$_POST['udf2'];
			$phone =$_POST['udf3'];
			$txnid =$_POST['txnid'];
			$amount =$_POST['amount'];
			// $email =  "mustaquim.sayyed@onevoice.co.in";
			// $phone =	"8087586743";
			// $txnid =	"771092iu";
			// $temp_id =	"6";
			// $amount = 500;

			$datau = $this->db->query("select *  from tbl_registraton_nagarcycling2021 where mno='$phone' and email='$email' and txnId='$txnid' order by user_id desc")->result_array();
			$cntu = count($datau);
				if($cntu>0){	 	
					$this->load->view('success');
					$this->load->view('footer');
				}else{
					$data = $this->db->query("select *  from tbl_registraton_nagarcycling2021_temp where user_temp_id = '$temp_id'")->result_array();
					$cnt = count($data);

					if($cnt>0){
						// code for bibcode					
						$cat = $data[0]['category'];
						$result = $this->db->query("SELECT MAX(`bib_id`) AS `bib_id` FROM `tbl_registraton_nagarcycling2021` WHERE `category` = '$cat'")->row();
						$bib = $result->bib_id;
						
						if($bib=='' && ($data[0]['category']=='20KM' || $data[0]['category']=='20km')){
							$bibid='2000';
						}else if($bib=='' && ($data[0]['category']=='50KM' || $data[0]['category']=='50km')){
							$bibid='5000';
						}else if($bib=='' && ($data[0]['category']=='100KM' || $data[0]['category']=='100km')){
							$bibid='10000';
						}else{
							$bibid=$bib;
						}
						$final_bib_id = $bibid+1;
						$insert_array1	=	array(
													'name'  						=> $data[0]['name'],
													'mno'   						=> $data[0]['mno'],
													'email'   						=> $data[0]['email'],
													'dob'   						=> date("Y-m-d", strtotime($data[0]['dob'])),
													'category'     					=> $data[0]['category'],
													'state'   						=> $data[0]['state'],
													'address'   					=> $data[0]['address'],
													'city'   						=> $data[0]['city'],
													'pincode'   					=> $data[0]['pincode'],
													'country'   					=> $data[0]['country'],
													'gender'   						=> $data[0]['gender'],
													'blood_group'   				=> $data[0]['blood_group'],
													'tshirt_size'   				=> $data[0]['tshirt_size'],
													'emergency_contact_name'   		=> $data[0]['emergency_contact_name'],
													'emergency_contact_number'   	=> $data[0]['emergency_contact_number'],
													'age'   						=> $data[0]['age'],
													'txnId'  					 	=> $txnid,
													'total_amount'					=> $amount,
													'bib_id' 					  	=> $final_bib_id,
													'app_user_id' 					=> $data[0]['app_user_id']											
													);

						$pagedata['data']=$insert_array1;
						$this->db->insert('tbl_registraton_nagarcycling2021',$insert_array1);
					// $emailinvoice = $this->load->view('invoicePdf',$pagedata,true);
					
						$pagedata1['data'] = $this->db->query("select *  from tbl_registraton_nagarcycling2021 where mno='$phone' and email='$email' and bib_id='$final_bib_id'")->result_array();
					
						$this->load->library('html2pdf');					
						//Set folder to save PDF to
						$this->html2pdf->folder('./assets/pdfs/');					
						//Set the filename to save/download as
						$this->html2pdf->filename('Invoice.pdf');					
						//Set the paper defaults
						$this->html2pdf->paper('a4', 'portrait');					
					
						$pagedata1['data']['amount'] = $amount;
						//Load html view
						$this->html2pdf->html($this->load->view('appInvoicePdf',$pagedata1,TRUE));
						$path = $this->html2pdf->create('save'); 
						$name = $data[0]['name']; 
						$ride = $data[0]['category'];
						$mno = $phone;	
						
						$emailinvoice = "Congratulations! You Have Successfully Registered For India's Biggest Cyclothon- Ahmednagar Cyclothon 2021,Season 4.";
						$msg = "Congratulations! You Have Successfully Registered For India's Biggest Cyclothon- Ahmednagar Cyclothon 2021, Season 4. Your BIB ID is: $final_bib_id  Ride Category: $ride. Keep Riding! ";
						$this->message_send_bulk($msg, $mno);
						$insert_array=array(
										'number'=>$mno,
										'message'=>$msg
										);
						$this->db->insert('sms_sent',$insert_array);

					
						$subj = "Registration Successful For Ahmednagar Cyclothon 2021";
						$emailmsg = "You Have Done Successfully Registration For Ahmednagar Cyclothon 2021";			
						$developmentMode=false;
						$mail = new PHPMailer ($developmentMode);
							if ($developmentMode) {
								$mail->SMTPOptions = [
									'ssl'=> [
									'verify_peer' => false,
									'verify_peer_name' => false,
									'allow_self_signed' => true
									]
								];
							}
						$mail->CharSet   = "UTF-8";	
						$mail->IsSMTP (); // set mailer to use SMTP
						$mail->SMTPAuth = true; // turn on SMTP authentication
						$mail->Host = "mail.nagarcycling.com"; // specify main and backup server
						$mail->Port = 587; // set the port to use
						$mail->SMTPAuth = true; // turn on SMTP authentication
						$mail->Username = "support@nagarcycling.com"; // your SMTP username or your gmail username
						$mail->Password = "nagarcycling@123"; // your SMTP password or your gmail password
						
						$from = "support@nagarcycling.com"; // Reply to this email
						$name = "Ahmednagar Cyclothone 2021"; // Recipient's name
						$mail->From = $from;
						$mail->FromName = "Ahmednagar Cyclothone 2021"; // Name to indicate where the email came from when the recepient received
						
						
						
						$mail->AddAddress ( $email, $name );
						$mail->AddReplyTo ( "support@nagarcycling.com", "Ahmednagar Cyclothon 2021" );
						//$mail->AddCC("support@urjaacademy.com","Admin");
						$mail->IsHTML ( true ); // send as HTML 
						//print_r($this->input->post('message'));exit;
						$mail->Subject =  $subj;
						$mail->Body = $emailinvoice;
						$mail->addAttachment($path); 
						$mail->Send();

						$Path = './assets/pdfs/Invoice.pdf';
						unlink($Path);
						//$mail->Send();
						$insert_array11=array(
							'email'=>$email,
							'subject'=>  $subj,
							'message'=>'invoice'
							);
						$this->db->insert('email_sent',$insert_array11);

						$this->load->view('success');
						$this->load->view('footer');
					}
				}
		}else{
			redirect('https://www.nagarcycling.com/');
		}
	}

	public function payment_fail()
	{  
		$this->load->view('failure');
		$this->load->view('footer');		
	}






	public function testing()
	{  
		 
      
   /*  $email =  $_POST['udf2'];
	  $phone =	$_POST['udf3'];
	  $txnid =	$_POST['txnid'];*/
	  $txnid =	"448956";
	  $phone ="9762379774";
	$final_bib_id ="2417";
		 $data = $this->db->query("select *  from tbl_registraton_nagarcycling2021 where mno='9762379774' and email='danish@onevoicetransmedia.com' and bib_id='5001'")->result_array();
	$pagedata['data'] = $this->db->query("select *  from tbl_registraton_nagarcycling2021 where mno='9762379774' and email='danish@onevoicetransmedia.com' and bib_id='5001'")->result_array();
	// $data = $this->db->query("select *  from tbl_user where mno='$phone' and email='$email' and bib_id='3002'")->result_array();

       
		
    
 
       
  
        //Load the library
	    $this->load->library('html2pdf');
	    
	    //Set folder to save PDF to
	    $this->html2pdf->folder('./assets/pdfs/');
	    
	    //Set the filename to save/download as
	    $this->html2pdf->filename('Invoice.pdf');
	    
	    //Set the paper defaults
	    $this->html2pdf->paper('a4', 'portrait');
	    
	  /*  $data = array(
	    	'title' => 'PDF Created',
	    	'message' => 'Hello World!'
	    );*/
	    
	    //Load html view
	    $this->html2pdf->html($this->load->view('invoicePdf',$pagedata,TRUE));
	    
	    $path = $this->html2pdf->create('save');
       
       
       
       
       
       
       
       
       
       
        
		 $emailinvoice = "Congratulations! You Have Successfully Registered For India's Biggest Cyclothon- Ahmednagar Cyclothon 2021,Season 4.";
	//	  $emailinvoice = "<h1>Test 1 of PHPMailer html</h1><p>This is a test</p>";
			$name = $data[0]['name'];
			$ride = $data[0]['category'];
			$final_bib_id = $data[0]['bib_id'];
		    $mno = $phone;	
		    //$msg = "Dear $name, You Have Successfully Registered For Ahmednagar Cycothone 2020. Your Ride - $ride,Your BIB Id:$final_bib_id. For more queries, contact us on 86002 44000 ";
		       $msg = "Congratulations! You Have Successfully Registered For India's Biggest Cyclothon- Ahmednagar Cyclothon 2021,Season 4. Your BIB ID is: $final_bib_id  Ride Category: $ride Kindly Riding! ";
		/*	$this->message_send_bulk($msg, $mno);*/
		/*	$insert_array=array(
							'number'=>$mno,
							'message'=>$msg
							);
			$this->db->insert('sms_sent',$insert_array);*/

	   
	  			 $subj = "Registration Successful For Ahmednagar Cycothone 2021";
				$emailmsg = "You Have Done Successfully Registration For Ahmednagar Cycothone 2021";
		
							$developmentMode=false;
					$mail = new PHPMailer ($developmentMode);

						if ($developmentMode) {
						$mail->SMTPOptions = [
							'ssl'=> [
							'verify_peer' => false,
							'verify_peer_name' => false,
							'allow_self_signed' => true
							]
						];
						}
			$mail->CharSet   = "UTF-8";	
			$mail->IsSMTP (); // set mailer to use SMTP
			$mail->SMTPAuth = true; // turn on SMTP authentication
			$mail->Host = "mail.nagarcycling.com"; // specify main and backup server
	        $mail->Port = 587; // set the port to use
	        $mail->SMTPAuth = true; // turn on SMTP authentication
	        $mail->Username = "support@nagarcycling.com"; // your SMTP username or your gmail username
	        $mail->Password = "nagarcycling@123"; // your SMTP password or your gmail password
	        
	        $from = "support@nagarcycling.com"; // Reply to this email
			$name = "Ahmednagar Cyclothon 2021"; // Recipient's name
			$mail->From = $from;
			$mail->FromName = "Ahmednagar Cyclothon 2021"; // Name to indicate where the email came from when the recepient received
			
		
  			
   				$mail->AddAddress ("danish@onevoicetransmedia.com", "DAMMY" );
			//	$mail->AddAddress ("surajk4437@gmail.com" );
				$mail->AddReplyTo ( "support@nagarcycling.com", "Ahmednagar Cyclothon 2021" );
				$mail->IsHTML(true); // send as HTML 
				//print_r($this->input->post('message'));exit;
				$mail->Subject =  $subj;
				$mail->Body = $emailinvoice;
		     $mail->addAttachment($path); 
			
	//	print_r($mail);die;	
		$mail->Send();
		
		$Path = './assets/pdfs/Invoice.pdf';
		unlink($Path);
			/*	$insert_array=array(
					'email'=>$email,
					'subject'=>  $subj,
					'message'=>'invoice'
					);
				$this->db->insert('email_sent',$insert_array);*/
			

		   	$this->load->view('success',$data);
			$this->load->view('footer');
		}

	












	public function testingold()
	{  
		 
      
   /*  $email =  $_POST['udf2'];
	  $phone =	$_POST['udf3'];
	  $txnid =	$_POST['txnid'];*/
	   $email =  "danish@onevoicetransmedia.com";
	  $phone =	"9762379774";
	//  $txnid =	"810322";
	 $data = $this->db->query("select *  from tbl_registraton_nagarcycling2021 where mno='9762379774' and email='danish@onevoicetransmedia.com' and bib_id='5001'")->result_array();
	$pagedata['data'] = $this->db->query("select *  from tbl_registraton_nagarcycling2021 where mno='9762379774' and email='danish@onevoicetransmedia.com' and bib_id='5001'")->result_array();
	// $data = $this->db->query("select *  from tbl_user where mno='$phone' and email='$email' and bib_id='3002'")->result_array();

       
		
    //   $emailinvoice = $this->load->view('invoicePdftest',$pagedata,TRUE);  
       
       
       $emailinvoice =  '<html lang="en">
<head>
    <title></title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php // echo base_url();?>assets/plugins/images/favicon.ico">
    <script src="https://www.w3schools.com/lib/w3data.js"></script>
</head>
<body>
<table border="1" width="100%" align="center" style="padding:10px 10px">
        <thead>
            <tr><th colspan="3" style="background-color:#ff0000";><h1 style="color:#fff" ><b>Ahmednagar Cyclothon 2020</b></h1></th>
            </tr>
            <tr>
            <th colspan="3">
                <p align="left"> You Have Successfully Registered For the Ahmednagar Cyclothon 2020.</p>   <br>
                <p align="left">This is a computer generated Invoice and does not require the signature.If you have any further queries kindly visit our website</p>  <br>
                <p align="left">Your Registration Invoice No, Date and BIB No. are mentioned below:</p>   
            </th>
            </tr>
            <tr>
            <th ><h5><b>Invoice No: <br>'.$data["0"]["txnId"].'</b></h5></th>
            <th ><h5><b>Invoice Date: <br>'.date("d-M-Y").'</b></h5></th>
            <th ><h5><b>BIB No: <?php  <br>'.$data["0"]["bib_id"].'</b></h5></th>
            </tr>
           
        </thead>
    </table> <table width="100%" align="center" style="padding:10px 10px">
        <tr>
            <td>
                <address>
                <h3>To,</h3>
                <h4 >'. $data["0"]["name"].',</h4>
                  <p >'.$data["0"]["address"].','.$data["0"]["city"].','.$data["0"]["state"].'</p>
                </address>
            </td>
            <td align="right">
                <address>
                  <h4 style="padding-right: 37px;"> <b> Ahmednagar Cyclothon 2020</b></h4>
                  <p>Titan Showroom, Beside The Castle, <br/>
                    Near Professor Colony Chowk, Savedi, <br/>
                    Ahmednagar - 414003
                 </p>
                </address>
            </td>
        </tr>
    </table>
<table border="1" width="100%" align="center" style="padding:10px 10px">
       <thead>
           <tr>
               <th width="5%">#</th>
               <th width="55%">Race Category</th>
               <th width="15%">No. of Registration</th>
               <th width="25%">Amount</th>
           </tr>
       </thead>
       <tbody>
           <tr>
               <td width="5%">1</td>
               <td width="55%">
                       Category - '.$data["0"]["category"].'  <br>
                 
               </td>
               <td width="15%" align="center">1 </td>
             <td width="25%" align="center">'. $ca =($data["0"]["category"] =="20KM" ? "600": ($data["0"]["category"] =="50KM" ? "700":"900")).'</td>
           </tr>
            <tr>
               <td width="100%">1</td>
           </tr>
       </tbody>
   </table><table width="100%" align="center" style="padding:10px 10px">
     <thead>
           <tr>
               <th></th>
           </tr>
       </thead>
   <tbody>
      
        <tr>
            <td align="right">
                <address>
                    <h5 class="font-bold">Ahmednagar Cyclothone 2020,</h5>
                    <p class="text-muted">Authorised Signatory</p>
                </address>
            </td>
        </tr>
    </tbody>    
    </table>
    <hr width="50%" align="center">
    <table width="50%" align="center">
        <tr>
            <td>This is a computer generated Invoice and does not require the signature. If you have any question regarding this invoice please email us at support@nagarcycling.com</td>
        </tr>
    </table>
</body>
</html>' ;   
       
 
       
  
       
       
       
       
       
       
       
       
       
       
       
        
	//	 $emailinvoice = "TEST BODY ";
	//	  $emailinvoice = "<h1>Test 1 of PHPMailer html</h1><p>This is a test</p>";
			$name = $data[0]['name'];
			$ride = $data[0]['category'];
			$final_bib_id = $data[0]['bib_id'];
		    $mno = $phone;	
		    //$msg = "Dear $name, You Have Successfully Registered For Ahmednagar Cycothone 2020. Your Ride - $ride,Your BIB Id:$final_bib_id. For more queries, contact us on 86002 44000 ";
		       $msg = "Congratulations! You Have Successfully Registered For India's Biggest Cyclothon- Ahmednagar Cyclothon 2020,Season 3. Your BIB ID is: $final_bib_id  Ride Category: $ride Kindly Riding! ";
			$this->message_send_bulk($msg, $mno);
		/*	$insert_array=array(
							'number'=>$mno,
							'message'=>$msg
							);
			$this->db->insert('sms_sent',$insert_array);*/

	   
	  			 $subj = "Registration Successful For Ahmednagar Cycothone 2020";
				$emailmsg = "You Have Done Successfully Registration For Ahmednagar Cycothone 2020";
		
							$developmentMode=false;
					$mail = new PHPMailer ($developmentMode);

						if ($developmentMode) {
						$mail->SMTPOptions = [
							'ssl'=> [
							'verify_peer' => false,
							'verify_peer_name' => false,
							'allow_self_signed' => true
							]
						];
						}
			$mail->CharSet   = "UTF-8";	
			$mail->IsSMTP (); // set mailer to use SMTP
			$mail->SMTPAuth = true; // turn on SMTP authentication
			$mail->Host = "mail.nagarcycling.com"; // specify main and backup server
	        $mail->Port = 587; // set the port to use
	        $mail->SMTPAuth = true; // turn on SMTP authentication
	        $mail->Username = "support@nagarcycling.com"; // your SMTP username or your gmail username
	        $mail->Password = "nagarcycling@123"; // your SMTP password or your gmail password
	        
	        $from = "support@nagarcycling.com"; // Reply to this email
			$name = "Urja Kids Run"; // Recipient's name
			$mail->From = $from;
			$mail->FromName = "Urja Kids Run"; // Name to indicate where the email came from when the recepient received
			
			
  			
   				$mail->AddAddress ($email, $name );
			//	$mail->AddAddress ("surajk4437@gmail.com" );
				$mail->AddReplyTo ( "support@urjaacademy.com", "Urja Kids Run" );
				$mail->IsHTML(true); // send as HTML 
				//print_r($this->input->post('message'));exit;
				$mail->Subject =  $subj;
				$mail->Body = $emailinvoice;
			
	//	print_r($mail);die;	
		$mail->Send();
		
		
			/*	$insert_array=array(
					'email'=>$email,
					'subject'=>  $subj,
					'message'=>'invoice'
					);
				$this->db->insert('email_sent',$insert_array);*/
			

		   	$this->load->view('success',$data);
			$this->load->view('footer');
		}

	

}
