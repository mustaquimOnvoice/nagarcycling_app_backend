<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH . 'core/class.phpmailer.php';
class Registration extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function message_send_bulk($message1, $number) {
		//$numsStr = implode(',',$number);
		$postData = "{ 
			\"sender\": \"URJRUN\", 
			\"route\": \"4\", 
			\"country\": \"91\", 
			\"unicode\": \"1\",
			\"sms\": [ 
						{ 
							\"message\": \"$message1\", 
							\"to\": [ $number ] 
						}
					] 
		}";
			
		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://api.msg91.com/api/v2/sendsms?country=91",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => $postData,
		  CURLOPT_SSL_VERIFYHOST => 0,
		  CURLOPT_SSL_VERIFYPEER => 0,
		  CURLOPT_HTTPHEADER => array(
			"authkey: 228445AqdIYICptZd5d36a66d",
			"content-type: application/json"
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		   "cURL Error #:" . $err;
		} else {
		  $response;
		}
	}
	public function index()
	{  
	  /* $km1 = $this->db->query("select user_id from tbl_user where category='1Km'")->num_rows();
	   $km3 = $this->db->query("select user_id from tbl_user where category='3Km'")->num_rows();
	   $km5 = $this->db->query("select user_id from tbl_user where category='5Km'")->num_rows();
	    
	    $pagedata['data']['remaining1km'] = 100-$km1;
	    $pagedata['data']['remaining3km'] = 100-$km3;
	    $pagedata['data']['remaining5km'] = 100-$km5;*/
	     $remainingallkm = $this->db->query("select user_id from tbl_user")->num_rows();
	      $pagedata['data']['remainingallkm'] = 300-$remainingallkm;
	    	
		$this->load->view('registration',$pagedata);
		$this->load->view('footer');
	}


	public function validmno()
	{  
	   $mno = $_POST['mno'];
		 $data = $this->db->query("select *  from tbl_registraton_nagarcycling2020 where mno='$mno'")->result_array();
		 $cnt = count($data);
		 if($cnt>'0'){
		 	echo 'exist';
		 }else{
		 	echo 'not exist';
		 }
	}

	public function validemail()
	{  

	   $email = $_POST['email'];
		 $data = $this->db->query("select *  from tbl_registraton_nagarcycling2020 where email='$email'")->result_array();
		
		 $cnt1 = count($data);
		 if($cnt1>'0'){
		 	echo 'exist';
		 }else{
		 	echo 'not exist';
		 }
	}
	
	public function confirm_form()
	{ 
		 
	   	$insert_array	=	array(
									'name'  				=> $this->input->post('name'),
									'mno'   				=> $this->input->post('mobile_no'),
									'email'   				=> $this->input->post('email'),
									'dob'   				=> date("Y-m-d", strtotime($this->input->post('dob'))),
									'category'     			=> $this->input->post('category'),
									'state'   				=> $this->input->post('state'),
									'city'   				=> $this->input->post('city'),
									'address'   			=> $this->input->post('address'),
									'pincode'   			=> $this->input->post('pincode'),
									'country'   			=> $this->input->post('country'),
									'gender'   				=> $this->input->post('gender'),
									'blood_group'   		=> $this->input->post('blood_group'),
									'tshirt_size'   		=> $this->input->post('t-shirt'),
									'school_name'   		=> $this->input->post('school_name'),
									'class'   				=> $this->input->post('class'),
									'medical_issue'   		=> $this->input->post('medical_issues'),
									'form_filler_name'   	=> $this->input->post('applicant_name'),
									'relation_with_runner'  => $this->input->post('relation'),
									'name_of_guardian'   	=> $this->input->post('parent_name')
									
									);

	   	$this->db->insert('tbl_user_temp',$insert_array);
   
     
	   	$cat = $this->input->post('category');
	   	if($cat=='1Km'){
	   	   $price_array= array('price' => '1');
	   	}
	   	else if($cat=='3Km'){
	   	   $price_array= array('price' => '1');
	   	}
	   	else if($cat=='5Km'){
	   	   $price_array= array('price' => '1');
	   	}
	   $merge = array_merge($insert_array, $price_array);
	 
	   	$data['records'] = 	$merge;
	   	$this->load->view('confirmation_form',$data);
		$this->load->view('footer');
	}

	public function welcome(){

	}
		public function payment_success()
	{  
		 
      
     $email =  $_POST['udf2'];
	  $phone =	$_POST['udf3'];
	  $txnid =	$_POST['txnid'];
	   $email =  "danish@onevoicetransmedia.com";
	  $phone =	"9762379774";
	  $txnid =	"810322";
	  $datau = $this->db->query("select *  from tbl_user where mno='$phone' and email='$email' and txnId='$txnid' order by user_id desc")->result_array();
	 $cntu = count($datau); 
	 if($cntu>0){
	 	
	     	$this->load->view('success');
			$this->load->view('footer');
	 }else{
	 
	 $data = $this->db->query("select *  from tbl_user_temp where mno='$phone' and email='$email' order by user_temp_id desc")->result_array();
	 $cnt = count($data);

	 	if($cnt>0){
	 			// code for bibcode
	 	
		$cat =$data[0]['category'];
		 $result = $this->db->query("SELECT MAX(`bib_id`) AS `bib_id` FROM `tbl_user` WHERE `category` = '$cat'")->row();
		$bib = $result->bib_id;
		
		if($bib=='' && $data[0]['category']=='1Km'){
             $bibid='1000';
		}else if($bib=='' && $data[0]['category']=='3Km'){
			 $bibid='3000';
		}else if($bib=='' && $data[0]['category']=='5Km'){
			 $bibid='5000';
		}else{
			$bibid=$bib;
		}
        $final_bib_id = $bibid+1;
			$insert_array1	=	array(
										'name'  				=> $data[0]['name'],
										'mno'   				=>$data[0]['mno'],
										'email'   				=> $data[0]['email'],
										'dob'   				=> date("Y-m-d", strtotime($data[0]['dob'])),
										'category'     			=>$data[0]['category'],
										'state'   				=> $data[0]['state'],
										'address'   			=> $data[0]['address'],
										'city'   				=> $data[0]['city'],
										'pincode'   			=> $data[0]['pincode'],
										'country'   			=> $data[0]['country'],
										'gender'   				=> $data[0]['gender'],
										'blood_group'   		=> $data[0]['blood_group'],
										'tshirt_size'   		=> $data[0]['tshirt_size'],
										'school_name'   		=> $data[0]['school_name'],
										'class'   				=>$data[0]['class'],
										'medical_issue'   		=> $data[0]['medical_issue'],
										'form_filler_name'   	=> $data[0]['form_filler_name'],
										'relation_with_runner'  => $data[0]['relation_with_runner'],
										'name_of_guardian'   	=> $data[0]['name_of_guardian'],
										'txnId'   	=> $txnid,
										'bib_id'   	=> $final_bib_id,
										
										);

			$pagedata['data']=$insert_array1;

        $emailinvoice = $this->load->view('invoicePdf',$pagedata,true);
        
		   	$this->db->insert('tbl_user',$insert_array1);
			$name = $data[0]['name'];
			$ride = $data[0]['category'];
		    $mno = $phone;	
		    $msg = "Dear $name, You Have Successfully Registered For the Urja Kids Run 2019. Your Ride - $ride,Your BIB Id:$final_bib_id. For more queries, contact us on 86002 44000 ";
			$this->message_send_bulk($msg, $mno);
			$insert_array=array(
							'number'=>$mno,
							'message'=>$msg
							);
			$this->db->insert('sms_sent',$insert_array);

	   
	  			 $subj = "Registration Successful For Urja Kids Run";
				$emailmsg = "You Have Done Successfully Registration For Urja Kids Run";
		
							$developmentMode=false;
					$mail = new PHPMailer ($developmentMode);

						if ($developmentMode) {
						$mail->SMTPOptions = [
							'ssl'=> [
							'verify_peer' => false,
							'verify_peer_name' => false,
							'allow_self_signed' => true
							]
						];
						}
				$mail->CharSet   = "UTF-8";	
			$mail->IsSMTP (); // set mailer to use SMTP
			$mail->SMTPAuth = true; // turn on SMTP authentication
			$mail->Host = "mail.deltainfomedia.com"; // specify main and backup server
	        $mail->Port = 25; // set the port to use
	        $mail->SMTPAuth = true; // turn on SMTP authentication
	        $mail->Username = "support@nagarcycling.com"; // your SMTP username or your gmail username
	        $mail->Password = "nagarcycling@123"; // your SMTP password or your gmail password
	        
	        $from = "support@nagarcycling.com"; // Reply to this email
			$name = "Urja Kids Run"; // Recipient's name
			$mail->From = $from;
			$mail->FromName = "Urja Kids Run"; // Name to indicate where the email came from when the recepient received
			
			
  			
   				$mail->AddAddress ( $email, $name );
				$mail->AddReplyTo ( "support@urjaacademy.com", "Urja Kids Run" );
				$mail->AddCC("support@urjaacademy.com","Admin");
				$mail->IsHTML ( true ); // send as HTML 
				//print_r($this->input->post('message'));exit;
				$mail->Subject =  $subj;
				$mail->Body = $emailinvoice;
			
				$mail->Send();
				$insert_array=array(
					'email'=>$email,
					'subject'=>  $subj,
					'message'=>'invoice'
					);
				$this->db->insert('email_sent',$insert_array);
			

		   	$this->load->view('success',$data);
			$this->load->view('footer');
	    	}
    	}
	}

	public function payment_fail()
	{  

		 	$this->load->view('failure');
			$this->load->view('footer');
	  
		
	}

		public function testing()
	{  
		 
      
   /*  $email =  $_POST['udf2'];
	  $phone =	$_POST['udf3'];
	  $txnid =	$_POST['txnid'];*/
	   $email =  "danish@onevoicetransmedia.com";
	  $phone =	"9762379774";
	//  $txnid =	"810322";
	 $data = $this->db->query("select *  from tbl_user where mno='9823111116' and email='jagdeep.makar@rediffmail.com' and bib_id='3003'")->result_array();
	$pagedata['data'] = $this->db->query("select *  from tbl_user where mno='9823111116' and email='jagdeep.makar@rediffmail.com' and bib_id='3003'")->result_array();
	// $data = $this->db->query("select *  from tbl_user where mno='$phone' and email='$email' and bib_id='3002'")->result_array();
	
       
		
       $emailinvoice = $this->load->view('invoicePdftest',$pagedata,true);  
        
		  
			$name = $data[0]['name'];
			$ride = $data[0]['category'];
			$final_bib_id = $data[0]['bib_id'];
		    $mno = $phone;	
		    $msg = "Dear $name, You Have Successfully Registered For the Urja Kids Run 2019. Your Ride - $ride,Your BIB Id:$final_bib_id. For more queries, contact us on 86002 44000 ";
			$this->message_send_bulk($msg, $mno);
		/*	$insert_array=array(
							'number'=>$mno,
							'message'=>$msg
							);
			$this->db->insert('sms_sent',$insert_array);*/

	   
	  			 $subj = "Registration Successful For Urja Kids Run";
				$emailmsg = "You Have Done Successfully Registration For Urja Kids Run";
		
							$developmentMode=false;
					$mail = new PHPMailer ($developmentMode);

						if ($developmentMode) {
						$mail->SMTPOptions = [
							'ssl'=> [
							'verify_peer' => false,
							'verify_peer_name' => false,
							'allow_self_signed' => true
							]
						];
						}
				$mail->CharSet   = "UTF-8";	
			$mail->IsSMTP (); // set mailer to use SMTP
			$mail->SMTPAuth = true; // turn on SMTP authentication
			$mail->Host = "mail.deltainfomedia.com"; // specify main and backup server
	        $mail->Port = 25; // set the port to use
	        $mail->SMTPAuth = true; // turn on SMTP authentication
	        $mail->Username = "support@nagarcycling.com"; // your SMTP username or your gmail username
	        $mail->Password = "nagarcycling@123"; // your SMTP password or your gmail password
	        
	        $from = "support@nagarcycling.com"; // Reply to this email
			$name = "Urja Kids Run"; // Recipient's name
			$mail->From = $from;
			$mail->FromName = "Urja Kids Run"; // Name to indicate where the email came from when the recepient received
			
			
  			
   				$mail->AddAddress ( $email, $name );
				$mail->AddReplyTo ( "support@urjaacademy.com", "Urja Kids Run" );
				$mail->AddCC("support@urjaacademy.com","Admin");
				$mail->IsHTML ( true ); // send as HTML 
				//print_r($this->input->post('message'));exit;
				$mail->Subject =  $subj;
				$mail->Body = $emailinvoice;
			
				$mail->Send();
			/*	$insert_array=array(
					'email'=>$email,
					'subject'=>  $subj,
					'message'=>'invoice'
					);
				$this->db->insert('email_sent',$insert_array);*/
			

		   	$this->load->view('success',$data);
			$this->load->view('footer');
		}

	

}
