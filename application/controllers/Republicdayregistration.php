<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH . 'core/class.phpmailer.php';
require_once APPPATH . 'core/class.smtp.php';
class Republicdayregistration extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function message_send_bulk($message1, $number) {
		//$numsStr = implode(',',$number);
		$postData = "{ 
			\"sender\": \"NGRCYC\", 
			\"route\": \"4\", 
			\"country\": \"91\", 
			\"unicode\": \"1\",
			\"sms\": [ 
						{ 
							\"message\": \"$message1\", 
							\"to\": [ $number ] 
						}
					] 
		}";
			
		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://api.msg91.com/api/v2/sendsms?country=91",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => $postData,
		  CURLOPT_SSL_VERIFYHOST => 0,
		  CURLOPT_SSL_VERIFYPEER => 0,
		  CURLOPT_HTTPHEADER => array(
			"authkey: 228445AqdIYICptZd5d36a66d",
			"content-type: application/json"
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		   "cURL Error #:" . $err;
		} else {
		  $response;
		}
	}
	public function index()
	{  
	  /* $km1 = $this->db->query("select user_id from tbl_user where category='1Km'")->num_rows();
	   $km3 = $this->db->query("select user_id from tbl_user where category='3Km'")->num_rows();
	   $km5 = $this->db->query("select user_id from tbl_user where category='5Km'")->num_rows();
	    
	    $pagedata['data']['remaining1km'] = 100-$km1;
	    $pagedata['data']['remaining3km'] = 100-$km3;
	    $pagedata['data']['remaining5km'] = 100-$km5;*/
	     $remainingallkm = $this->db->query("select user_id from tbl_registraton_republicday_nagarcycling2020")->num_rows();
	      //$pagedata['data']['remainingallkm'] = 300-$remainingallkm;
	    	
		$this->load->view('republicdayregistration');
		$this->load->view('footer');
	}



	public function smssending()
	{  
	  
		 $data = $this->db->query("select *  from  sms_sent")->result_array();
		
		 foreach($data as $row){
		 	 $msg = $row['message'];
		 	 $mno = $row['number']; 
		 	$this->message_send_bulk($msg, $mno);
		 }
		echo 'success';
	}	

	public function validmno()
	{  
	   $mno = $_POST['mno'];
		 $data = $this->db->query("select *  from tbl_registraton_republicday_nagarcycling2020 where mno='$mno'")->result_array();
		 $cnt = count($data);
		 if($cnt>'0'){
		 	echo 'exist';
		 }else{
		 	echo 'not exist';
		 }
	}

	public function validemail()
	{  

	   $email = $_POST['email'];
		 $data = $this->db->query("select *  from tbl_registraton_republicday_nagarcycling2020 where email='$email'")->result_array();
		
		 $cnt1 = count($data);
		 if($cnt1>'0'){
		 	echo 'exist';
		 }else{
		 	echo 'not exist';
		 }
	}
	
	public function confirm_form()
	{ 
		/*echo "<pre>";
		print_r($_POST);
		 echo $_POST["dob"];*/
		 
	   	$insert_array	=	array(
									'name'  						=> $this->input->post('name'),
									'email'   						=> $this->input->post('email'),
									'mno'   						=> $this->input->post('mobile_no'),
									'gender'   						=> $this->input->post('gender'),
									'dob'   						=> date('Y-m-d', strtotime($this->input->post('dob'))),
									'age'   						=> $this->input->post('age'),
									'category'     					=> $this->input->post('category'),
								    'blood_group'   				=> $this->input->post('blood_group'),
								  	'address'   					=> $this->input->post('address'),
									'city'   						=> $this->input->post('city'),
									'state'   						=> $this->input->post('state'),
									'pincode'   					=> $this->input->post('pincode'),
									'country'   					=> $this->input->post('country'),
									'tshirt_size'   				=> $this->input->post('t-shirt'),
									'emergency_contact_name'   		=> $this->input->post('emergency_name'),
									'emergency_contact_number'   	=> $this->input->post('emergency_number'),
								 );
	  
	  /* 	echo "<pre>";
		print_r($insert_array);
		die;*/

	   	$this->db->insert('tbl_registraton_republicday_nagarcycling2020_temp',$insert_array);
   
     
	   	$cat = $this->input->post('category');
	   	if($cat=='40KM'){
	   	   $price_array= array('price' => '1');
	   	}
	   	else if($cat=='10KM'){
	   	   $price_array= array('price' => '300');
	   	}
	   $merge = array_merge($insert_array, $price_array);
	 
	   	$data['records'] = 	$merge;
	   	$this->load->view('confirmation_formrepublicday',$data);
		$this->load->view('footer');
	}

	public function welcome(){

	}
		public function payment_success()
	{  
		 
      
     $email =  $_POST['udf2'];
	  $phone =	$_POST['udf3'];
	  $txnid =	$_POST['txnid'];
	 /*  $email =  "sak@gmail.com";
	  $phone =	"7896541230";
	  $txnid =	"810355";*/
	  $datau = $this->db->query("select *  from tbl_registraton_republicday_nagarcycling2020 where mno='$phone' and email='$email' and txnId='$txnid' order by user_id desc")->result_array();
	 $cntu = count($datau); 
	 if($cntu>0){
	 	
	     	$this->load->view('success');
			$this->load->view('footer');
	 }else{
	 
	 $data = $this->db->query("select *  from tbl_registraton_republicday_nagarcycling2020_temp  where  `mno`='$phone' and  `email`='$email' order by `user_temp_id` desc")->result_array();
	
	 /*echo '<pre>';
	 print_r($data);
	 die;*/
	 $cnt = count($data);

	 	if($cnt>0){
	 			// code for bibcode
	 	
		$cat =$data[0]['category'];
		 $result = $this->db->query("SELECT MAX(`bib_id`) AS `bib_id` FROM `tbl_registraton_republicday_nagarcycling2020` WHERE `category` = '$cat'")->row();
		$bib = $result->bib_id;
		
		if($bib=='' && $data[0]['category']=='20KM'){
             $bibid='2000';
		}else if($bib=='' && $data[0]['category']=='40KM'){
			 $bibid='4000';
		}
		else if($bib=='' && $data[0]['category']=='10KM'){
			 $bibid='1000';
		}
		else if($bib=='' && $data[0]['category']=='50KM'){
			 $bibid='5000';
		}else if($bib=='' && $data[0]['category']=='100KM'){
			 $bibid='10000';
		}else{
			$bibid=$bib;
		}
        $final_bib_id = $bibid+1;
			$insert_array1	=	array(
										'name'  						=> $data[0]['name'],
										'mno'   						=>$data[0]['mno'],
										'email'   						=> $data[0]['email'],
										'dob'   						=> date("Y-m-d", strtotime($data[0]['dob'])),
										'category'     					=>$data[0]['category'],
										'state'   						=> $data[0]['state'],
										'address'   					=> $data[0]['address'],
										'city'   						=> $data[0]['city'],
										'pincode'   					=> $data[0]['pincode'],
										'country'   					=> $data[0]['country'],
										'gender'   						=> $data[0]['gender'],
										'blood_group'   				=> $data[0]['blood_group'],
										'tshirt_size'   				=> $data[0]['tshirt_size'],
										'emergency_contact_name'   		=> $data[0]['emergency_contact_name'],
										'emergency_contact_number'   	=>$data[0]['emergency_contact_number'],
										'age'   						=> $data[0]['age'],
										'txnId'  					 	=> $txnid,
										'bib_id' 					  	=> $final_bib_id,
										
										);

			$pagedata['data']=$insert_array1;
				$this->db->insert('tbl_registraton_republicday_nagarcycling2020',$insert_array1);

         // $this->load->library('html2pdf');
	    
	    //Set folder to save PDF to
	   // $this->html2pdf->folder('./assets/pdfs/');
	    
	    //Set the filename to save/download as
	    //$this->html2pdf->filename('Invoice.pdf');
	    
	    //Set the paper defaults
	   // $this->html2pdf->paper('a4', 'portrait');
	    
	  /*  $data = array(
	    	'title' => 'PDF Created',
	    	'message' => 'Hello World!'
	    );*/
	    
	    //Load html view
	   // $this->html2pdf->html($this->load->view('invoicePdf',$pagedata,TRUE));
	    
	  //  $path = $this->html2pdf->create('save');
       
       $email = $insert_array1['email'];
      $name = $insert_array1['name'];
        
		 $emailinvoice = "Congratulations! You Have Successfully Registered For Republic Day Ride. Ride Category: 40KM. Keep Riding!";
	//	  $emailinvoice = "<h1>Test 1 of PHPMailer html</h1><p>This is a test</p>";
		/*	$name = $data[0]['name'];
			$ride = $data[0]['category'];*/
			//$final_bib_id = $data[0]['bib_id'];
		    $mno = $phone;	
		    //$msg = "Dear $name, You Have Successfully Registered For Ahmednagar Cycothone 2020. Your Ride - $ride,Your BIB Id:$final_bib_id. For more queries, contact us on 86002 44000 ";
		       $msg = "Congratulations! You Have Successfully Registered For Republic Day Ride. Ride Category: 40KM. Keep Riding!";
			$this->message_send_bulk($msg, $mno);
		/*	$insert_array=array(
							'number'=>$mno,
							'message'=>$msg
							);
			$this->db->insert('sms_sent',$insert_array);*/

	   
	  			 $subj = "Registration Successful For Ahmednagar Cycothone 2020";
				$emailmsg = "You Have Done Successfully Registration For Ahmednagar Cycothone 2020";
		
							$developmentMode=false;
					$mail = new PHPMailer ($developmentMode);

						if ($developmentMode) {
						$mail->SMTPOptions = [
							'ssl'=> [
							'verify_peer' => false,
							'verify_peer_name' => false,
							'allow_self_signed' => true
							]
						];
						}
			$mail->CharSet   = "UTF-8";	
			$mail->IsSMTP (); // set mailer to use SMTP
			$mail->SMTPAuth = true; // turn on SMTP authentication
			$mail->Host = "mail.nagarcycling.com"; // specify main and backup server
	        $mail->Port = 587; // set the port to use
	        $mail->SMTPAuth = true; // turn on SMTP authentication
	        $mail->Username = "support@nagarcycling.com"; // your SMTP username or your gmail username
	        $mail->Password = "nagarcycling@123"; // your SMTP password or your gmail password
	        
	        $from = "support@nagarcycling.com"; // Reply to this email
			$name = "Ahmednagar Cyclothon 2020"; // Recipient's name
			$mail->From = $from;
			$mail->FromName = "Ahmednagar Cyclothon 2020"; // Name to indicate where the email came from when the recepient received
			
		
  			
   				$mail->AddAddress ("danish@onevoicetransmedia.com", "DAMMY" );
			//	$mail->AddAddress ("surajk4437@gmail.com" );
				$mail->AddReplyTo ( "support@nagarcycling.com", "Ahmednagar Cyclothon 2020" );
				$mail->IsHTML(true); // send as HTML 
				//print_r($this->input->post('message'));exit;
				$mail->Subject =  $subj;
				$mail->Body = $emailinvoice;
		     //	$mail->addAttachment($path); 
			
	//	print_r($mail);die;	
		$mail->Send();
		
		//$Path = './assets/pdfs/Invoice.pdf';
	//	unlink($Path);
			/*	$insert_array=array(
					'email'=>$email,
					'subject'=>  $subj,
					'message'=>'invoice'
					);
				$this->db->insert('email_sent',$insert_array);*/
			

		   	$this->load->view('success',$data);
			$this->load->view('footer');
	    	}
    	}
	}

	public function payment_fail()
	{  

		 	$this->load->view('failure');
			$this->load->view('footer');
	  
		
	}

		public function testing()
	{  
		 
      
   /*  $email =  $_POST['udf2'];
	  $phone =	$_POST['udf3'];
	  $txnid =	$_POST['txnid'];*/
	   $email =  "danish@onevoicetransmedia.com";
	  $phone =	"9762379774";
	//  $txnid =	"810322";
	 $data = $this->db->query("select *  from tbl_user where mno='9823111116' and email='jagdeep.makar@rediffmail.com' and bib_id='3003'")->result_array();
	$pagedata['data'] = $this->db->query("select *  from tbl_user where mno='9823111116' and email='jagdeep.makar@rediffmail.com' and bib_id='3003'")->result_array();
	// $data = $this->db->query("select *  from tbl_user where mno='$phone' and email='$email' and bib_id='3002'")->result_array();
	
       
		
       $emailinvoice = $this->load->view('invoicePdftest',$pagedata,true);  
        
		  
			$name = $data[0]['name'];
			$ride = $data[0]['category'];
			$final_bib_id = $data[0]['bib_id'];
		    $mno = $phone;	
		    $msg = "Dear $name, You Have Successfully Registered For the Urja Kids Run 2019. Your Ride - $ride,Your BIB Id:$final_bib_id. For more queries, contact us on 86002 44000 ";
			$this->message_send_bulk($msg, $mno);
		/*	$insert_array=array(
							'number'=>$mno,
							'message'=>$msg
							);
			$this->db->insert('sms_sent',$insert_array);*/

	   
	  			 $subj = "Registration Successful For Urja Kids Run";
				$emailmsg = "You Have Done Successfully Registration For Urja Kids Run";
		
							$developmentMode=false;
					$mail = new PHPMailer ($developmentMode);

						if ($developmentMode) {
						$mail->SMTPOptions = [
							'ssl'=> [
							'verify_peer' => false,
							'verify_peer_name' => false,
							'allow_self_signed' => true
							]
						];
						}
				$mail->CharSet   = "UTF-8";	
			$mail->IsSMTP (); // set mailer to use SMTP
			$mail->SMTPAuth = true; // turn on SMTP authentication
			$mail->Host = "mail.deltainfomedia.com"; // specify main and backup server
	        $mail->Port = 25; // set the port to use
	        $mail->SMTPAuth = true; // turn on SMTP authentication
	        $mail->Username = "support@nagarcycling.com"; // your SMTP username or your gmail username
	        $mail->Password = "nagarcycling@123"; // your SMTP password or your gmail password
	        
	        $from = "support@nagarcycling.com"; // Reply to this email
			$name = "Urja Kids Run"; // Recipient's name
			$mail->From = $from;
			$mail->FromName = "Urja Kids Run"; // Name to indicate where the email came from when the recepient received
			
			
  			
   				$mail->AddAddress ( $email, $name );
				$mail->AddReplyTo ( "support@urjaacademy.com", "Urja Kids Run" );
				$mail->AddCC("support@urjaacademy.com","Admin");
				$mail->IsHTML ( true ); // send as HTML 
				//print_r($this->input->post('message'));exit;
				$mail->Subject =  $subj;
				$mail->Body = $emailinvoice;
			
				$mail->Send();
			/*	$insert_array=array(
					'email'=>$email,
					'subject'=>  $subj,
					'message'=>'invoice'
					);
				$this->db->insert('email_sent',$insert_array);*/
			

		   	$this->load->view('success',$data);
			$this->load->view('footer');
		}

	

}
