<?php
require_once APPPATH . 'core/Base_Controller.php';
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class home extends Base_Controller {

	public function __construct() {
		parent::__construct ();
		$this->load->model('api_model');
		
		//check_token
		if(!empty($_GET['type']) && $_GET['type'] == 'new_user' ){
            $this->api_model->check_token('app_user',$_GET['device_token']);
		}else if(!empty($_GET['type']) && $_GET['type'] == 'exist_user' ){
            $this->api_model->check_token('tbl_registerclone',$_GET['device_token']);
		}else{
            $response ['message'] = "fail";
			$response ['result'] =  "User type not found";
			$response ['data'] =  array();
			echo json_encode($response);
			die();
		}
    }

    function index(){
        $raceList = $this->Base_Models->CustomeQuary("SELECT *,date_format(start_date_time, '%d/%b/%Y') as start_date_time,date_format(end_date_time, '%d/%b/%Y') as end_date_time, (select image_url from images where tbl_race.race_id = images.ref_id and images.type = 1 limit 1) as image_url, (select image_url from images where tbl_race.race_id = images.ref_id and images.type = 4 limit 1) as image_url_details FROM tbl_race WHERE status = 1");
		// foreach ($raceList as $key => $value) {
        //     $event_images= $this->Base_Models->GetAllValues ( "images" ,array("ref_id"=>$value['race_id'] ,"type"=>"1")  );
        //     $raceList[$key]["images"]=$event_images;
        // }
		foreach ($raceList as $key => $value) {
            $event_images= $this->Base_Models->GetAllValues ( "tbl_race_category" ,array("race_id"=>$value['race_id'])  );
            $raceList[$key]["race_cat"]=$event_images;
        }

        $upcomevntList = $this->Base_Models->CustomeQuary("SELECT *,date_format(start_date_time, '%d/%b/%Y') as start_date_time,date_format(end_date_time, '%d/%b/%Y') as end_date_time, (select image_url from images where upcomming_events.id = images.ref_id and images.type = 6 limit 1) as image_url FROM upcomming_events WHERE status = 1");
        foreach ($upcomevntList as $key => $value) {
            $event_images= $this->Base_Models->GetAllValues ( "upcomming_events_category" ,array("race_id"=>$value['id'])  );
            $upcomevntList[$key]["race_cat"]=$event_images;
        }

        $sponsor = $this->Base_Models->CustomeQuary("SELECT image_url from images where ref_id = 1 and type = 5");
        $committee = $this->Base_Models->CustomeQuary("SELECT id, name, short_desc, long_desc, (select image_url from images where tbl_committee.id = images.ref_id and images.type = 2 limit 1) as image_url FROM tbl_committee WHERE status = 1");
        
        $response ['message'] = "done";
		$response ['result'] =  "Home details";
		$response ['data'] = $raceList;
		$response ['sponsor'] = (!empty($sponsor)) ? $sponsor : array('image_url' => '');
		$response ['upcomming_events'] = (!empty($upcomevntList)) ? $upcomevntList : array('id' => '','name' => '','title' => '','long_desc' => '','race_venue' => '','start_date_time' => '','end_date_time' => '','redirect' => '','redirect_url' => '','image_url' => '','race_cat'=>array());
		$response ['committee'] = $committee;
        echo json_encode($response);
    }

    function gallery(){

        $videos = $this->Base_Models->CustomeQuary("SELECT image_url as video_url, thumbnail from images where ref_id = 1 and type = 3");
        $imgList = $this->Base_Models->CustomeQuary("SELECT id, name FROM tbl_season WHERE status = 1");
		foreach ($imgList as $key => $value) {
            $id = $value['id'];
            $image_url= $this->Base_Models->CustomeQuary("SELECT image_url FROM images WHERE ref_id = $id and type = 4");
            $imgList[$key]["images"]=$image_url;
        }
		
        $response ['message'] = "done";
		$response ['result'] =  "Home details";
		$response ['videos'] = (!empty($videos)) ? $videos : array('video_url' => '','thumbnail' => '');
		$response ['season'] = (!empty($imgList)) ? $imgList : array('id' =>'','name' =>'','images' =>array('image_url' =>''));
        echo json_encode($response);
    }

    //get videos by id
    function videos(){
        $response ['message'] = "done";
        $response ['result'] =  "Id required";
        $videos = array();
        if(!empty($_GET['id'])){
            $id = $_GET['id'];
            $videos = $this->Base_Models->CustomeQuary("SELECT image_url as video_url, thumbnail from images where ref_id = 1 and id = $id and type = 3");
        
            $response ['message'] = "done";
            $response ['result'] =  "video details";
        }
        $response ['videos'] = (!empty($videos)) ? $videos : array('video_url' => '','thumbnail' => '');
        echo json_encode($response);
    }

    function pageData(){
        $response ['message'] = "fail";
        $response ['result'] =  "Param name is reqired";
        $response ['data'] =  array('page_name'=>"");

		if( !empty($_GET['page_name']) ){
            $response ['result'] =  "Page not found";
            if(!empty($page_data = $this->Base_Models->GetSingleDetails ( "website_data" ,array("page_name"=>$_GET['page_name']),array('page_data')))){                
                $response ['message'] = "done";
                $response ['result'] =  "Page data";
                $response ['data'] = $page_data;
            }
        }
        echo json_encode($response);
    }

    function history(){
        $response ['message'] = "fail";
        $response ['result'] =  "Param mobile_no,email is reqired";
        $response ['data'] =  array('registration_no'=>"",'bibid'=>"",'transaction_id'=>"",'total_amount'=>"",'name'=>"",'race_category'=>"",'email_addr'=>"",'mobile_no'=>"",'full_address'=>"",'state'=>"",'city'=>"",'postcode'=>"",'country'=>"",'gender'=>"",'birthdate'=>"",'age'=>"",'bloodgrp'=>"",'tshirt'=>"",'emergency_name'=>"",'emergency_number'=>"");

		if( !empty($_GET['mobile_no']) && !empty($_GET['email']) && !empty($_GET['user_id']) ){
            $select1 = array('registration_no','bibid','transaction_id','total_amount','	name','race_category','	email_addr','mobile_no','full_address','state','city','postcode','country','gender','birthdate','age','	bloodgrp','	tshirt','emergency_name','emergency_number','create_at');
            $history1 = $this->Base_Models->GetAllValues ( "tbl_registerclone" ,array("mobile_no"=>$_GET['mobile_no'],"email_addr"=>$_GET['email']),$select1);
            //$history1 = $this->Base_Models->GetAllValues ( "tbl_registerclone" ,array("mobile_no"=>'9552532828',"email_addr"=>'smbp9009@gmail.com'),$select1);
            foreach ($history1 as $key => $value) {
                $history1[$key]["type"]='exist';
            }
            
            $select2 = array('user_id as registration_no','bib_id as bibid','txnId as transaction_id','total_amount','name','category as race_category','email as email_addr','mno as mobile_no','address as full_address','state','city','pincode as postcode','country','gender','dob as birthdate','age','blood_group as bloodgrp','tshirt_size as tshirt','emergency_contact_name as emergency_name','emergency_contact_number as emergency_number','created_on as create_at');
            $history2 = $this->Base_Models->GetAllValues ( "tbl_registraton_nagarcycling2021" ,array("mno"=>$_GET['mobile_no'],"email"=>$_GET['email']),$select2);
            foreach ($history2 as $key => $value) {
                $history2[$key]["type"]='new';
            }
            
            $history3 = $this->Base_Models->GetAllValues ( "tbl_registraton_nagarcycling2021" ,array("app_user_id"=>$_GET['user_id']),$select2);
            
            $history4 = array_merge($history1,$history2);
            $history = array_merge($history4,$history3);
            $response ['message'] = "done";
            $response ['result'] =  "History details";
            $response ['data'] = $history;
        }
        echo json_encode($response);
    }

	public function participate() { // register for new user
		$response ['message'] = "fail";
		$response ['result'] =  "Param is reqired";
		$response ['data'] = array('email' => "", 'mobile_no' => "", 'temp_id' => 0 );

		if( !empty($_GET['user_id']) && !empty($_GET['name']) && !empty($_GET['mobile_no']) && !empty($_GET['email']) && !empty($_GET['birthdate'])  && !empty($_GET['gender'])  && !empty($_GET['blood_group']) && !empty($_GET['race_category']) && !empty($_GET['address']) && !empty($_GET['city']) && !empty($_GET['state']) && !empty($_GET['country']) && !empty($_GET['pincode']) && !empty($_GET['tshirt']) && !empty($_GET['emergency_name']) && !empty($_GET['emergency_number']) ){
			$type = trim($_GET['type']);
			$app_user_id = trim($_GET['user_id']);
			$name = trim($_GET['name']);
			$mobile_no = trim($_GET['mobile_no']);
			$email = trim($_GET['email']);
			$birthdate = date('Y-m-d', strtotime(trim($_GET['birthdate'])));
			$gender = trim($_GET['gender']);
			$blood_group = str_replace(" ", "+", $_GET['blood_group']);
			$race_category = trim($_GET['race_category']);
			$address = trim($_GET['address']);
			$city = trim($_GET['city']);
			$state = trim($_GET['state']);
			$country = trim($_GET['country']);
			$pincode = trim($_GET['pincode']);
			$tshirt = trim($_GET['tshirt']);
			$emergency_name = trim($_GET['emergency_name']);
            $emergency_number = trim($_GET['emergency_number']);
            
            //check if already registered
            $already_exist = $this->Base_Models->GetAllValues("tbl_registraton_nagarcycling2021",array('email'=>$email,'mno'=>$mobile_no));
            if(count($already_exist)!=0){
                $response ['result'] = "Email & Mobile already registered";
                echo json_encode ($response);
                die();
            }

            $tablevalues = array(
                'app_user_id' => $app_user_id,
                'name' => $name,
                'mno' => $mobile_no,
                'email' => $email,
                'dob' => $birthdate,
                'category' => $race_category,
                'state' => $state,
                'address' => $address,
                'city' => $city,
                'pincode' => $pincode,
                'country' => $country,
                'gender' => $gender,
                'blood_group' => $blood_group,
                'tshirt_size' => $tshirt,
                'emergency_contact_name' => $emergency_name,
                'emergency_contact_number' => $emergency_number
            );

            $this->Base_Models->AddValues ( "tbl_registraton_nagarcycling2021_temp", $tablevalues);
            $response ['message'] = "done";
		    $response ['result'] =  "Participate added";
            $response ['data'] = array('email' => $email, 'mobile_no' => $mobile_no, 'temp_id' =>$this->db->insert_id() );
        }
		echo json_encode ($response);
    }
    
    public function generate_hash_key(){
        $response ['message'] = "fail";
        $response ['result'] =  "Param is reqired";
        $response ['hashkey'] = "";
        
        if( !empty($_GET['mobile_no']) && !empty($_GET['email']) && !empty($_GET['amount']) && !empty($_GET['name']) && !empty($_GET['temp_id']) && !empty($_GET['txnid']) && !empty($_GET['productinfo'])){
            $salt="hOTRLB5LMS";
            $key="v98vlTWB";
            //$txnid =  mt_rand(100000, 999999); // six digit random no
            $txnid = $_GET['txnid'];
            //$productinfo='NagarCycling 2021';
            $productinfo=$_GET['productinfo'];
            $amount=$_GET['amount'];
            $firstname=$_GET['name'];
            $email=$_GET['email'];            
            $udf1=$_GET['temp_id'];
            $udf2=$email;
            $udf3=$_GET['mobile_no'];
            $udf4='';
            $udf5='';
            $udf6='';
            $udf7='';
            $udf8='';
            $udf9='';
            $udf10='';

            $data = $key.'|'.$txnid.'|'.$amount.'|'.$productinfo.'|'.$firstname.'|'.$email.'|'.$udf1.'|'.$udf2.'|'.$udf3.'|'.$udf4.'|'.$udf5.'|'.$udf6.'|'.$udf7.'|'.$udf8.'|'.$udf9.'|'.$udf10.'|'.$salt;
            $getHash=(hash('sha512',$data));
            
            //$hash_string = $key."|".$txnid."|".$amount."|".$productinfo."|".$firstname."|".$email."|".$udf1."|".$udf2."|".$udf3."||||||||".$salt;
            //$hash_string = $salt.'|success||||||udf5|udf4|'.$udf3.'|'.$udf2.'|'.$udf1.'|'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;
            //sha512(<SALT>|status||||||udf5|udf4|udf3|udf2|udf1|email|firstname|productinfo|amount|txnid|key)
            
            
            //$payhash_str = $key . '|' . $this->checkNull($txnId) . '|' .$this->checkNull($amount)  . '|' .$this->checkNull($productinfo)  . '|' . $this->checkNull($firstname) . '|' . $this->checkNull($email) . '|' . $this->checkNull($udf1) . '|' . $this->checkNull($udf2) . '|' . $this->checkNull($udf3) . '|' . $this->checkNull($udf4) . '|' . $this->checkNull($udf5) . '||||||'. $salt;
            //$payhash_str = $key. '|' .$txnId. '|' .$amount. '|' .$productinfo. '|' .$firstname. '|' .$email. '|' .$udf1. '|' .$udf2. '|' .$udf3. '|' .$udf4. '|' .$udf5. '||||||'. $salt;
            //$payhash_str = $key.'|123123|10|ABCD|Firstname|demo@gmail.com|2|demo@gmail.com|9175917762||||||||'.$salt;
             //$payhash_str = $salt.'|success|||||||||||email@gmail.com|firstname|hello|12|123123123|'.$key;
            
            //$getHash = hash('sha512', $payhash_str);
            //$getHash = hash('sha512', $hash_string);

            $response ['message'] = "done";
            $response ['result'] =  "Hash key generated";
            $response ['hashkey'] = $getHash;
        }
        echo json_encode ($response);
    }

    function slider(){
        $response ['message'] = "done";
        $response ['result'] =  "Id required";
        $raceList = array();
        if(!empty($_GET['id'])){
            $id = $_GET['id'];
            $raceList = $this->Base_Models->CustomeQuary("SELECT *,date_format(start_date_time, '%d/%b/%Y') as start_date_time,date_format(end_date_time, '%d/%b/%Y') as end_date_time, (select image_url from images where tbl_race.race_id = images.ref_id and images.type = 1 limit 1) as image_url, (select image_url from images where tbl_race.race_id = images.ref_id and images.type = 4 limit 1) as image_url_details FROM tbl_race WHERE race_id = $id AND status = 1");
            foreach ($raceList as $key => $value) {
                $event_images= $this->Base_Models->GetAllValues ( "tbl_race_category" ,array("race_id"=>$value['race_id'])  );
                $raceList[$key]["race_cat"]=$event_images;
            }
            $response ['message'] = "done";
            $response ['result'] =  "Slider details";
        }
		$response ['data'] = $raceList;
		echo json_encode($response);
    }

    function upcommingEvent(){
        $response ['message'] = "done";
        $response ['result'] =  "Id required";
        if(!empty($_GET['id'])){
            $id = $_GET['id'];
            $upcomevntList = $this->Base_Models->CustomeQuary("SELECT *,date_format(start_date_time, '%d/%b/%Y') as start_date_time,date_format(end_date_time, '%d/%b/%Y') as end_date_time, (select image_url from images where upcomming_events.id = images.ref_id and images.type = 6 limit 1) as image_url FROM upcomming_events WHERE id = $id AND status = 1");
            foreach ($upcomevntList as $key => $value) {
                $event_images= $this->Base_Models->GetAllValues ( "upcomming_events_category" ,array("race_id"=>$value['id'])  );
                $upcomevntList[$key]["race_cat"]=$event_images;
            }
            $response ['message'] = "done";
            $response ['result'] =  "Home details";
        }
		$response ['data'] = (!empty($upcomevntList)) ? $upcomevntList : array('id' => '','name' => '','title' => '','long_desc' => '','race_venue' => '','start_date_time' => '','end_date_time' => '','redirect' => '','redirect_url' => '','image_url' => '','race_cat'=>array());
		echo json_encode($response);
    }

    function notifications(){
        $notifications= $this->Base_Models->GetAllValues ( "user_notification" );

        foreach ($notifications as $key => $value) {
            if($notifications[$key]["type"] == '1'){
                $notifications[$key]["type"]= 'Event';
            }elseif($notifications[$key]["type"] == '2'){
                $notifications[$key]["type"]= 'Video';
            }
        }

        $response ['message'] = "done";
		$response ['result'] =  "Notification details";
		$response ['notifications'] = $notifications;
        echo json_encode($response);
    }

}
?>