<?php
require_once APPPATH . 'core/Base_Controller.php';
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class login extends Base_Controller {
	public function __construct() {
		parent::__construct ();
	}

    public function status() {
        $config_data = array_values($this->Base_Models->GetAllValues("tbl_config",array(),array('config_key','config_value')));
		$output = array();
		// If you want only a specific set of meta_key names rather than all meta_key names
		$keys_you_want = array('version_ios','version_android','maintenance_mode','is_update');
		// Loops over the result set
		foreach ($config_data as $row) {
			// If the current row holds one of the meta_key you are looking for
			if (in_array($row['config_key'], $keys_you_want)) {
				// Put it onto the output array using the meta_key as the array key
				$output[$row['config_key']] = $row['config_value'];
			}
			// Otherwise do nothing...
		}
		$response ['message'] = "done";
		$response ['result'] =  "Application status";
		if(isset($_GET['appversion'])){
			if($_GET['appversion'] < $output["version_ios"]){
				$output['is_update'] = true;
			}			
		}
		$response ['data'] =  $output;
        echo json_encode($response);
    }
	
	public function index() {
		$response ['data'] =  array('user_id'=>"",'name'=>"",'email_addr'=>"",'mobile_no'=>"",'gender'=>"",'birthdate'=>"",'bloodgrp'=>"",'device_token'=>"",'create_at'=>"");
		
		if(!empty($_GET['mobile_no'])){ // login with mobile
			
			$TableValues['mobile_no']= $_GET['mobile_no'];				
			$temp = $this->Base_Models->GetAllValues("tbl_registerclone",$TableValues); // check if alreday exist pre yr
			
			if(count($temp)==0){ //new user
				$temp2 = $this->Base_Models->GetAllValues("app_user",array('mobile_no'=>$TableValues['mobile_no']));
				
				//die($temp2[0]['user_status']);
				if(count($temp2)==0){// if not exist in new user
					$TableValues['user_is']="new_user";
					$temp = $this->Base_Models->AddValues ( "app_user", $TableValues);
					$response ['type'] = "new_user";
					$response ['message'] = "done";
					$response ['result'] =  "Redirect to register";
				}elseif($temp2[0]['user_status'] == 2){ // if new user already verified
					$otp = "";
					$send_message = "Hi, your OTP for Nagarcycling is ";
					if($temp2[0]['user_otp']=="" || $temp2[0]['user_otp']=="0"){
						
				    	// if($_GET['mobile_no'] == '9175917762'){
    					    $str = 1234; // comment after live
    					// }else{
    					//     $str = $this->generate_random_string ( 4, 'num' ); // remove comment after live
    					// }
						$otp=$str;
						$this->Base_Models->UpadateValue ( "app_user", array (
							"user_otp" =>$str
						), array (
							"user_id" => $temp2[0]['user_id']
						) );
					}else{
					    if($_GET['mobile_no'] == '9175917762'){
    					    $otp= 1234; // comment after live
    					}else{
    					    $otp=$temp2[0]['user_otp'];
    					}
					}
					$response ['type'] = "new_user";
					$response ['message'] = "done";
					$response ['result'] =  "OTP send"; //dont change this value it is flag
					$send_message = $send_message.$otp." ";
					//$this->message_send ( $send_message,  $TableValues['mobile_no']); // remove comment after live
				}elseif($temp2[0]['user_status'] == 1 || $temp2[0]['user_status'] == ""){ // if new user exist but not verified
					$response ['type'] = "new_user";
					$response ['message'] = "done";
					$response ['result'] =  "Redirect to register";
				}
			} else { // EXIST USER
				$otp = "";
				$send_message = "Hi, your OTP for Nagarcycling is ";
				if($temp[0]['user_otp']=="" || $temp[0]['user_otp']=="0"){
					// $str = $this->generate_random_string ( 4, 'num' ); // remove comment after live
					// if($_GET['mobile_no'] == '9175917762'){
					    $str = 1234; // comment after live
					// }
					$otp=$str;
					$this->Base_Models->UpadateValue ( "tbl_registerclone", array (
						"user_otp" =>$str
					), array (
						"registration_no" => $temp[0]['registration_no']
					) );
				}else{
					$otp=$temp[0]['user_otp'];
				}
				$response ['type'] = "exist_user";
				$response ['message'] = "done";
				$response ['result'] =  "OTP send"; //dont change this value it is flag
				$send_message = $send_message.$otp." ";
				//$this->message_send ( $send_message,  $TableValues['mobile_no']); // remove comment after live
			}

		} else if (!empty($_GET['email'])) { // social login

			$selectArr = array('name','email_addr','mobile_no','gender','birthdate','bloodgrp','device_token','create_at','user_status');
			
			if(!empty($_GET['email']) && !empty($_GET['social_id']) && !empty($_GET['social_type']) ) {
				$TableValues['email_addr']= $_GET['email'];				
				$temp = $this->Base_Models->GetAllValues("tbl_registerclone",$TableValues); // check if alreday exist pre yr
				
				$TableValues['social_id']= $_GET['social_id'];				
				$TableValues['social_type']= $_GET['social_type'];

				if(count($temp)==0){ //new user
					array_push($selectArr,'user_id');
					$temp2 = $this->Base_Models->GetAllValues("app_user",array('email_addr'=>$TableValues['email_addr']),$selectArr,'user_id');
					if(count($temp2)==0){
						$TableValues['user_is']="new_user";
						$temp = $this->Base_Models->AddValues ( "app_user", $TableValues);
						$response ['message'] = "done";
						$response ['result'] =  "Redirect to register";
						$response ['type'] = "new_user";
					}elseif($temp2[0]['user_status'] == 1 || $temp2[0]['user_status'] == ""){ // if new user exist but not verified
						$response ['type'] = "new_user";
						$response ['message'] = "done";
						$response ['result'] =  "Redirect to register";
					}else{
						//$TableValues['device_token']= $_GET['device_token'];
						$this->Base_Models->UpadateValue ( "app_user", array (
							"device_token" =>$_GET['device_token']
						), array (
							"user_id" => $temp2[0]['user_id']
						) );

						$temp2[0]['birthdate'] = date('d M Y',strtotime($temp2[0]['birthdate']));
						$temp2[0]['type'] = "new_user";
						$response ['data'] = $temp2[0];
						$response ['message'] = "done";
						$response ['result'] =  "Redirect to home";
						$response ['type'] = "new_user";
					}
				} else { // EXIST USER
					array_push($selectArr,'registration_no as user_id');
					$TableValues['device_token']= $_GET['device_token'];
					$temp2 = $this->Base_Models->GetAllValues("tbl_registerclone",array('email_addr'=>$TableValues['email_addr']),$selectArr,'registration_no');
					$this->Base_Models->UpadateValue ( "tbl_registerclone", $TableValues, array (
						"registration_no" => $temp2[0]['registration_no']
					) );
					$temp2[0]['birthdate'] = date('d M Y',strtotime($temp2[0]['birthdate']));
					$temp2[0]['type'] = "exist_user";
					$response ['data'] = $temp2[0];
					$response ['message'] = "done";
					$response ['result'] =  "Redirect to home";
					$response ['type'] = "exist_user";
				}
			}else{
				$response ['message'] = "fail";
				$response ['result'] =  "Param req email,social_id,social_type";
				$response ['type'] = "";
			}

		} else {
			$response ['message'] = "fail";
			$response ['result'] =  "OTP not send";
			$response ['type'] = "";
		}
		echo json_encode ( $response );
    }

	public function smstest(){
		$response = $this->message_send('test', '9175917762');
		echo json_encode ( $response );
	}

    public function otp_varification(){
		$response ['message'] = "fail";
		$response ['result'] =  "OTP not verify";
		$response ['data'] =  array('user_id'=>null,'name'=>null,'email_addr'=>null,'mobile_no'=>null,'gender'=>null,'birthdate'=>null,'bloodgrp'=>null,'device_token'=>null,'create_at'=>null);
        if( isset($_GET['mobile_no'])  && isset($_GET['device_token']) && isset($_GET['user_otp']) && isset($_GET['type'])){
			$TableValues['mobile_no']= $_GET['mobile_no'];
			$TableValues['user_otp']= $_GET['user_otp'];
			$type = $_GET['type'];
			$selectArr = array('name','email_addr','mobile_no','gender','birthdate','bloodgrp','device_token','create_at');
				
				if($type == 'new_user'){
					$cnt = $this->Base_Models->GetAllValues("app_user",$TableValues,"user_id");
					$TableValues['device_token']= $_GET['device_token'];
					$TableValues['user_otp']="";
					$TableValues['user_status']=2;
					array_push($selectArr,'user_id');

					if(count($cnt)==1){
						$this->Base_Models->UpadateValue ( "app_user",$TableValues, array (
							"user_id" => $cnt[0]['user_id']
						) );
						
						$temp = $this->Base_Models->GetAllValues("app_user",$TableValues,$selectArr);
						$temp[0]['type'] = "new_user";
						$response ['type'] = $type;
						$response ['message'] = "done";
						$response ['result'] =  "OTP verify successfully";
						// $response ['result'] = json_encode($temp[0]);
						$response ['data'] = $temp[0];
					}					
				}else if($type == 'exist_user'){
					$cnt = $this->Base_Models->GetAllValues("tbl_registerclone",$TableValues,"registration_no");
					$TableValues['device_token']= $_GET['device_token'];
					$TableValues['user_otp']="";
					$TableValues['user_status']=2;
					array_push($selectArr,'registration_no as user_id');

					if(count($cnt)==1){
						$this->Base_Models->UpadateValue ( "tbl_registerclone",$TableValues, array (
							"registration_no" => $cnt[0]['registration_no']
						) );
						
						$temp = $this->Base_Models->GetAllValues("tbl_registerclone",$TableValues,$selectArr);
						$temp[0]['type'] = "exist_user";
						$response ['type'] = $type;
						$response ['message'] = "done";
						// $response ['result'] = json_encode($temp[0]);
						$response ['result'] =  "OTP verify successfully";
						$response ['data'] = $temp[0];
					}
				}
		}       
		echo json_encode ( $response );
    }
	
	public function otp_resend(){
		$response ['message'] = "fail";
		$response ['result'] =  "Param device_token,type required";
		$response ['type'] = "";
		$send_message = "Hi, your OTP for Nagarcycling is ";						

		if(!empty($_GET['device_token']) && !empty($_GET['type']) && !empty($_GET['mobile_no'])){
			$TableValues['device_token']= $_GET['device_token'];
			$TableValues['mobile_no']= $_GET['mobile_no'];
			$type = $_GET['type'];
			$selectArr = array('mobile_no','user_otp');
				
			if($type == 'new_user'){ // if new user
				array_push($selectArr,'user_id');
				$temp2 = $this->Base_Models->GetAllValues("app_user",$TableValues,$selectArr);

				if(count($temp2)==0){
					$response ['result'] =  "User not found";						
				}else{
					if($temp2[0]['user_otp']=="" || $temp2[0]['user_otp']=="0"){
						// $str = $this->generate_random_string ( 4, 'num' );
						$str = 1234;
						$otp=$str;
						$this->Base_Models->UpadateValue ( "app_user", array (
							"user_otp" =>$str,
							"user_status" => '1'
						), array (
							"user_id" => $temp2[0]['user_id']
						) );
					}else{
						$this->Base_Models->UpadateValue ( "app_user", array (
							"user_status" => '1'
						), array (
							"user_id" => $temp2[0]['user_id']
						) );
						$otp=$temp2[0]['user_otp'];
					}
					$response ['message'] = "done";
					$response ['result'] =  "OTP send"; //dont change this value it is flag
					$response ['type'] = "new_user";

					$send_message = $send_message.$otp." ";
					//$this->message_send ( $send_message,$temp2[0]['mobile_no']);
				}
									
			}else if($type == 'exist_user'){

				array_push($selectArr,'registration_no');
				$temp = $this->Base_Models->GetAllValues("tbl_registerclone",$TableValues,$selectArr);

				if(count($temp)==0){
					$response ['result'] =  "User not found";						
				}else{
					if($temp[0]['user_otp']=="" || $temp[0]['user_otp']=="0"){
						$str = $this->generate_random_string ( 4, 'num' );
						$otp=$str;
						$this->Base_Models->UpadateValue ( "tbl_registerclone", array (
							"user_otp" =>$str,
							"user_status" => '1'
						), array (
							"registration_no" => $temp[0]['registration_no']
						) );
					}else{
						$this->Base_Models->UpadateValue ( "tbl_registerclone", array (
							"user_status" => '1'
						), array (
							"registration_no" => $temp[0]['registration_no']
						) );
						$otp=$temp[0]['user_otp'];
					}
					$response ['message'] = "done";
					$response ['result'] =  "OTP send"; //dont change this value it is flag
					$response ['type'] = "exist_user";

					$send_message = $send_message.$otp." ";
					//$this->message_send ( $send_message,$temp[0]['mobile_no']);
				}
			}
		}
		echo json_encode ( $response );
    }
}
?>