<?php
require_once APPPATH . 'core/Base_Controller.php';
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class profile extends Base_Controller {

	public function __construct() {
		parent::__construct ();
		$this->load->model('api_model');
		
		//check_token
		if(!empty($_POST['type']) && $_POST['type'] == 'new_user' ){
            $this->api_model->check_token('app_user',$_POST['device_token']);
		}else if(!empty($_POST['type']) && $_POST['type'] == 'exist_user' ){
            $this->api_model->check_token('tbl_registerclone',$_POST['device_token']);
		}else{
            $response ['message'] = "fail";
			$response ['result'] =  "User type not found";
			$response ['data'] =  array();
			echo json_encode($response);
			die();
		}
    }

    function index(){        
        $response ['message'] = "fail";
		$response ['result'] =  "Param is required";
		$response ['data'] =  array('user_id'=>null,'name'=>null,'email_addr'=>null,'mobile_no'=>null,'gender'=>null,'birthdate'=>null,'bloodgrp'=>null);
        if( isset($_POST['user_id']) && isset($_POST['type'])){
			$type = $_POST['type'];
			$selectArr = array('name','email_addr','mobile_no','gender','birthdate','bloodgrp','profile_pic');
				
            if($type == 'new_user'){
                array_push($selectArr,'user_id');
                $cnt = $this->Base_Models->GetAllValues("app_user",array('user_id' => $_POST['user_id']),$selectArr);

                if(count($cnt)==1){
                    $temp = $cnt;
                    $temp[0]['type'] = "new_user";
                    $temp[0]['profile_pic'] = base_url('/assets/images/profile/').$temp[0]['profile_pic'];
                    $response ['type'] = $type;
                    $response ['message'] = "done";
                    $response ['result'] =  "User Details";
                    $response ['data'] = $temp[0];
                }					
            }else if($type == 'exist_user'){
                array_push($selectArr,'registration_no as user_id');
                $cnt = $this->Base_Models->GetAllValues("tbl_registerclone",array('registration_no' => $_POST['user_id']),$selectArr);

                if(count($cnt)==1){                    
                    $temp = $cnt;
                    $temp[0]['type'] = "exist_user";
                    $temp[0]['profile_pic'] = base_url('/assets/images/profile/').$temp[0]['profile_pic'];
                    $response ['type'] = $type;
                    $response ['message'] = "done";
                    $response ['result'] =  "User Details";
                    $response ['data'] = $temp[0];
                }
            }
		}    
		echo json_encode ( $response );
    }

    public function update(){
		$response ['message'] = "fail";
		$response ['result'] =  "Param is required";
		$response ['data'] =  array('user_id'=>null,'name'=>null,'email_addr'=>null,'mobile_no'=>null,'gender'=>null,'birthdate'=>null,'bloodgrp'=>null);
        if( isset($_POST['user_id']) && isset($_POST['name']) && isset($_POST['email_addr']) && isset($_POST['mobile_no']) && isset($_POST['gender']) && isset($_POST['birthdate']) && isset($_POST['bloodgrp'])){
			$TableValues['name']= $_POST['name'];
			$TableValues['email_addr']= $_POST['email_addr'];
			$TableValues['mobile_no']= $_POST['mobile_no'];
			$TableValues['gender']= $_POST['gender'];
			$TableValues['birthdate']= date('Y-m-d', strtotime($_POST['birthdate']));
			$TableValues['bloodgrp']= str_replace(" ", "+", $_POST['bloodgrp']);
			$type = $_POST['type'];
			$selectArr = array('name','email_addr','mobile_no','gender','birthdate','bloodgrp');
				
            if($type == 'new_user'){
                $cnt = $this->Base_Models->GetAllValues("app_user",array('user_id' => $_POST['user_id']),"user_id");
                array_push($selectArr,'user_id');

                if(count($cnt)==1){
                    $this->Base_Models->UpadateValue ( "app_user",$TableValues, array (
                        "user_id" => $cnt[0]['user_id']
                    ) );
                    
                    $temp = $this->Base_Models->GetAllValues("app_user",$TableValues,$selectArr);
                    $temp[0]['type'] = "new_user";
                    $response ['type'] = $type;
                    $response ['message'] = "done";
                    $response ['result'] =  "updated successfully";
                    $response ['data'] = $temp[0];
                }					
            }else if($type == 'exist_user'){
                $cnt = $this->Base_Models->GetAllValues("tbl_registerclone",array('registration_no' => $_POST['user_id']),"registration_no");
                array_push($selectArr,'registration_no as user_id');

                if(count($cnt)==1){
                    $this->Base_Models->UpadateValue ( "tbl_registerclone",$TableValues, array (
                        "registration_no" => $cnt[0]['registration_no']
                    ) );
                    
                    $temp = $this->Base_Models->GetAllValues("tbl_registerclone",$TableValues,$selectArr);
                    $temp[0]['type'] = "exist_user";
                    $response ['type'] = $type;
                    $response ['message'] = "done";
                    $response ['result'] =  "updated successfully";
                    $response ['data'] = $temp[0];
                }
            }
		}    
		echo json_encode ( $response );
    }

    public function update_pic(){
        $response ['type'] = "";
        $response ['message'] = "fail";
        $response ['result'] =  "Param is required";
        $response ['data'] = array();
        if( isset($_POST['user_id']) && !empty($_FILES['image']['name'])) {
            $config['upload_path'] = 'assets/images/profile/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $this->upload->initialize($config);

            if ($this->upload->do_upload('image')) {
                $data = $this->upload->data();
                $TableValues['profile_pic'] = $data['file_name'];
                $type = $_POST['type'];
                
                if($type == 'new_user'){
                    $cnt = $this->Base_Models->GetAllValues("app_user",array('user_id' => $_POST['user_id']),array("user_id","profile_pic"));                    
                    if(count($cnt)==1){
                        $this->Base_Models->UpadateValue ( "app_user",$TableValues, array (
                            "user_id" => $cnt[0]['user_id']
                        ) );
                        $response ['type'] = $type;
                        $response ['message'] = "done";
                        $response ['result'] =  "updated successfully";
                        $response ['data'] = base_url('assets/images/profile/').$data['file_name'];

                        if(!empty($cnt[0]['profile_pic'])){
                            if(file_exists(FCPATH.''.$config['upload_path'].'/'.$cnt[0]['profile_pic'])){
                                unlink(FCPATH.''.$config['upload_path'].'/'.$cnt[0]['profile_pic']);
                            }
                        }
                    }else{
                        $response ['result'] = 'User not found';
                    }					
                }else if($type == 'exist_user'){
                    $cnt = $this->Base_Models->GetAllValues("tbl_registerclone",array('registration_no' => $_POST['user_id']),array("registration_no","profile_pic"));    
                    if(count($cnt)==1){
                        $this->Base_Models->UpadateValue ( "tbl_registerclone",$TableValues, array (
                            "registration_no" => $cnt[0]['registration_no']
                        ) );
                        $response ['type'] = $type;
                        $response ['message'] = "done";
                        $response ['result'] =  "updated successfully";
                        $response ['data'] = base_url('assets/images/profile/').$data['file_name']; 
                        
                        if(!empty($cnt[0]['profile_pic'])){
                            if(file_exists(FCPATH.''.$config['upload_path'].'/'.$cnt[0]['profile_pic'])){
                                unlink(FCPATH.''.$config['upload_path'].'/'.$cnt[0]['profile_pic']);
                            }
                        }
                    }else{
                        $response ['result'] = 'User not found';
                    }
                }
                
            }else{
                $response ['data'] = $this->upload->display_errors();
            }
        }
        echo json_encode ( $response );
    }

}
?>