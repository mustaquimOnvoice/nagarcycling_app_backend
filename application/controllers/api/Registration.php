<?php
require_once APPPATH . 'core/Base_Controller.php';
// require_once APPPATH . 'core/class.phpmailer.php';
// require_once APPPATH . 'core/class.smtp.php';
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class registration extends Base_Controller {
	public function __construct() {
		parent::__construct ();
	}

    public function index() { // register for new user
		$response ['message'] = "fail";
		$response ['result'] =  "Param is reqired";
		$response ['type'] = "new_user";

		if( !empty($_GET['name']) && !empty($_GET['mobile_no']) && !empty($_GET['email']) && !empty($_GET['birthdate'])  && !empty($_GET['gender'])  && !empty($_GET['blood_group'])   && !empty($_GET['device_token']) ){
			$name = trim($_GET['name']);
			$mobile_no = trim($_GET['mobile_no']);
			$email = trim($_GET['email']);
			$birthdate = date('Y-m-d', strtotime(trim($_GET['birthdate'])));
			$gender = trim($_GET['gender']);
			$blood_group = str_replace(" ", "+", $_GET['blood_group']);
			$device_token = trim($_GET['device_token']);

			if( !empty($_GET['social_id'] )){
				$temp = $this->Base_Models->GetAllValues("app_user",array('email_addr'=>$email));
			}else{
				$temp = $this->Base_Models->GetAllValues("app_user",array('mobile_no'=>$mobile_no));
			}		
			if(count($temp)==0){ //User not found
				$response ['result'] =  "User not found";
			} else {
				if($temp[0]['user_status'] == '2'){ // already verified by otp
					$response ['result'] =  "User already registered";
				}else{
					$otp = "";
					$send_message = "Hi your OTP for Nagarcycling is ";
					//$str = $this->generate_random_string ( 4, 'num' ); // remove comment after live
					$str = 1234; // comment after live
					$otp=$str;
					$TableValues['user_otp']=$str;
					$TableValues['user_status']="1";
					$TableValues['user_is']="new_user";
					$TableValues['name']=$name;
					$TableValues['mobile_no']=$mobile_no;
					$TableValues['email_addr']=$email;
					$TableValues['birthdate']=$birthdate;
					$TableValues['gender']=$gender;
					$TableValues['bloodgrp']=$blood_group;
					$TableValues['device_token']=$device_token;
					
					if( !empty($_GET['social_id'] )){
						$this->Base_Models->UpadateValue("app_user",$TableValues, array('email_addr'=>$email,'social_id'=>$_GET['social_id']));
					}else{
						$this->Base_Models->UpadateValue ( "app_user", $TableValues , array('mobile_no'=>$mobile_no) );
					}
					
					$send_message = $send_message.$otp." ";
					$mob = $_GET['mobile_no'];
					//$this->message_send($send_message, $mob); // remove comment after live

					$response ['message'] = "done";
					$response ['result'] =  "OTP send & Registered successfully";
					$response ['type'] = "new_user";	
				}							
			}
		}
		echo json_encode ($response);
	}

	public function payment_success_app() {
        //echo '<pre>';
        //print_r($_POST);
        //die();
        //if(!empty($_POST)){
			//die('l');die;
			$temp_id =$_POST['udf1'];
			$email =$_POST['udf2'];
			$phone =$_POST['udf3'];
			$txnid =$_POST['txnid'];
			$amount =$_POST['amount'];
			// $email =  "mustaquim.sayyed@onevoice.co.in";
			// $phone =	"8087586743";
			// $txnid =	"771092iu";
			// $temp_id =	"6";
			// $amount = 500;

			$datau = $this->db->query("select *  from tbl_registraton_nagarcycling2021 where mno='$phone' and email='$email' and txnId='$txnid' order by user_id desc")->result_array();
			$cntu = count($datau);
				if($cntu>0){	 	
					//redirect('https://www.nagarcycling.com/demo/index.php/Registration/payment_success_redirect'); 
					//echo "<script>alert('already registered')</script>";
					echo 'Already registered';
				}else{
					$data = $this->db->query("select *  from tbl_registraton_nagarcycling2021_temp where user_temp_id = '$temp_id'")->result_array();
					$cnt = count($data);

					if($cnt>0){
						// code for bibcode					
						$cat = $data[0]['category'];
						$result = $this->db->query("SELECT MAX(`bib_id`) AS `bib_id` FROM `tbl_registraton_nagarcycling2021` WHERE `category` = '$cat'")->row();
						$bib = $result->bib_id;
						
						if($bib=='' && ($data[0]['category']=='20KM' || $data[0]['category']=='20km')){
							$bibid='2000';
						}else if($bib=='' && ($data[0]['category']=='50KM' || $data[0]['category']=='50km')){
							$bibid='5000';
						}else if($bib=='' && ($data[0]['category']=='100KM' || $data[0]['category']=='100km')){
							$bibid='10000';
						}else{
							$bibid=$bib;
						}
						$final_bib_id = $bibid+1;
						$insert_array1	=	array(
													'name'  						=> $data[0]['name'],
													'mno'   						=> $data[0]['mno'],
													'email'   						=> $data[0]['email'],
													'dob'   						=> date("Y-m-d", strtotime($data[0]['dob'])),
													'category'     					=> $data[0]['category'],
													'state'   						=> $data[0]['state'],
													'address'   					=> $data[0]['address'],
													'city'   						=> $data[0]['city'],
													'pincode'   					=> $data[0]['pincode'],
													'country'   					=> $data[0]['country'],
													'gender'   						=> $data[0]['gender'],
													'blood_group'   				=> $data[0]['blood_group'],
													'tshirt_size'   				=> $data[0]['tshirt_size'],
													'emergency_contact_name'   		=> $data[0]['emergency_contact_name'],
													'emergency_contact_number'   	=> $data[0]['emergency_contact_number'],
													'age'   						=> $data[0]['age'],
													'txnId'  					 	=> $txnid,
													'total_amount'					=> $amount,
													'bib_id' 					  	=> $final_bib_id,
													'app_user_id' 					=> $data[0]['app_user_id']											
													);

						$pagedata['data']=$insert_array1;
						$this->db->insert('tbl_registraton_nagarcycling2021',$insert_array1);
					// $emailinvoice = $this->load->view('invoicePdf',$pagedata,true);
					
						$pagedata1['data'] = $this->db->query("select *  from tbl_registraton_nagarcycling2021 where mno='$phone' and email='$email' and bib_id='$final_bib_id'")->result_array();
					
						$this->load->library('html2pdf');					
						//Set folder to save PDF to
						$this->html2pdf->folder('./assets/pdfs/');					
						//Set the filename to save/download as
						$this->html2pdf->filename('Invoice.pdf');					
						//Set the paper defaults
						$this->html2pdf->paper('a4', 'portrait');					
					
						$pagedata1['data']['amount'] = $amount;
						//Load html view
						$this->html2pdf->html($this->load->view('appInvoicePdf',$pagedata1,TRUE));
						$path = $this->html2pdf->create('save'); 
						$name = $data[0]['name']; 
						$ride = $data[0]['category'];
						$mno = $phone;	
						
						$emailinvoice = "Congratulations! You Have Successfully Registered For India's Biggest Cyclothon- Ahmednagar Cyclothon 2021,Season 4.";
						$msg = "Congratulations! You Have Successfully Registered For India's Biggest Cyclothon- Ahmednagar Cyclothon 2021, Season 4. Your BIB ID is: $final_bib_id  Ride Category: $ride. Keep Riding! ";
						$this->message_send_bulk($msg, $mno);
						$insert_array=array(
										'number'=>$mno,
										'message'=>$msg
										);
						$this->db->insert('sms_sent',$insert_array);

					
						$subj = "Registration Successful For Ahmednagar Cyclothon 2021";
						$emailmsg = "You Have Done Successfully Registration For Ahmednagar Cyclothon 2021";			
						$developmentMode=false;
						$mail = new PHPMailer ($developmentMode);
							if ($developmentMode) {
								$mail->SMTPOptions = [
									'ssl'=> [
									'verify_peer' => false,
									'verify_peer_name' => false,
									'allow_self_signed' => true
									]
								];
							}
						$mail->CharSet   = "UTF-8";	
						$mail->IsSMTP (); // set mailer to use SMTP
						$mail->SMTPAuth = true; // turn on SMTP authentication
						$mail->Host = "mail.nagarcycling.com"; // specify main and backup server
						$mail->Port = 587; // set the port to use
						$mail->SMTPAuth = true; // turn on SMTP authentication
						$mail->Username = "support@nagarcycling.com"; // your SMTP username or your gmail username
						$mail->Password = "nagarcycling@123"; // your SMTP password or your gmail password
						
						$from = "support@nagarcycling.com"; // Reply to this email
						$name = "Ahmednagar Cyclothone 2021"; // Recipient's name
						$mail->From = $from;
						$mail->FromName = "Ahmednagar Cyclothone 2021"; // Name to indicate where the email came from when the recepient received
						
						
						
						$mail->AddAddress ( $email, $name );
						$mail->AddReplyTo ( "support@nagarcycling.com", "Ahmednagar Cyclothon 2021" );
						//$mail->AddCC("support@urjaacademy.com","Admin");
						$mail->IsHTML ( true ); // send as HTML 
						//print_r($this->input->post('message'));exit;
						$mail->Subject =  $subj;
						$mail->Body = $emailinvoice;
						$mail->addAttachment($path); 
						$mail->Send();

						$Path = './assets/pdfs/Invoice.pdf';
						unlink($Path);
						//$mail->Send();
						$insert_array11=array(
							'email'=>$email,
							'subject'=>  $subj,
							'message'=>'invoice'
							);
						$this->db->insert('email_sent',$insert_array11);

						redirect('https://www.nagarcycling.com/demo/index.php/Registration/payment_success_redirect'); 
					}
				}
		//}else{
		//	redirect('https://www.nagarcycling.com/');
		//}
	}

	//----------------------------------------------//
	
	
	public function message_send_bulk($message1, $number) {
		//$numsStr = implode(',',$number);
		$postData = "{ 
			\"sender\": \"NGRCYC\", 
			\"route\": \"4\", 
			\"country\": \"91\", 
			\"unicode\": \"1\",
			\"sms\": [ 
						{ 
							\"message\": \"$message1\", 
							\"to\": [ $number ] 
						}
					] 
		}";
			
		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://api.msg91.com/api/v2/sendsms?country=91",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => $postData,
		  CURLOPT_SSL_VERIFYHOST => 0,
		  CURLOPT_SSL_VERIFYPEER => 0,
		  CURLOPT_HTTPHEADER => array(
			"authkey: 228445AqdIYICptZd5d36a66d",
			"content-type: application/json"
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		   "cURL Error #:" . $err;
		} else {
		  $response;
		}
	}
	
    public function status() {
        $response ['message'] = "done";
        $response ['version'] =  "1.0";
        // $response ['maintenance_mode'] =  false;
        // $response ['data'] =  $this->Base_Models->GetSingleDetails('tbl_status', array('status' => '1') , array('title','description','(select image_url from images where type = "4" AND ref_code = tbl_status.id ORDER BY id DESC limit 1) as image'));
        echo json_encode($response);
    }
	

public function validmno()
	{  
	  //  print_r($_POST);
	   $mno = $_POST['mno'];
		 $data = $this->db->query("select *  from tbl_registraton_nagarcycling2021 where mno='$mno'")->result_array();
		 $cnt = count($data);
		if($cnt>'0'){
			$response ['message'] = "Already Registered";
			$response ['result'] = 'Already Registered';
		 
		 }else{
		 	$response ['message'] = "Not Registered";
			$response ['result'] = "Not Registered";
		 }
		 echo json_encode ($response);
	}

	public function validemail()
	{  

	   $email = $_POST['email'];
		 $data = $this->db->query("select *  from tbl_registraton_nagarcycling2021 where email='$email'")->result_array();
		
		 $cnt1 = count($data);
		 if($cnt1>'0'){
			$response ['message'] = "Already Registered";
			$response ['result'] = "Already Registered";
		 
		 }else{
		 	$response ['message'] = "Not Registered";
			$response ['result'] = "Not Registered";
		 }
		 echo json_encode ($response);
	}


   //user is registred or not
   public function isregistred()
	{
	  	$search = $_POST['search'];

		 $data = $this->db->query("select *  from tbl_registraton_nagarcycling2021 where email='$search' or mno='$search'")->result_array();
		
		 $cnt = count($data);
		 if($cnt>'0'){
			$response ['message'] = "Already Registered";
			$response ['result'] = "Already Registered";
		 
		 }else{
		 	$response ['message'] = "Not Registered";
			$response ['result'] = "Not Registered";
		 }
		 echo json_encode ($response);
	}


public function isrecord()
	{  
	   $search = $_POST['search'];

		 $data = $this->db->query("select *  from tbl_registerclone where email_addr='$search' or mobile_no='$search' order by registration_no desc limit 1")->result_array();
		  $datareg = $this->db->query("select *  from tbl_registraton_nagarcycling2021 where email='$search' or mno='$search'")->result_array();
		 $cnt = count($data);
		 $cntreg = count( $datareg);
		
		if($cntreg=='0'){	 
        		if($cnt>'0'){
        					$response ['message'] = "exist";
							$response ['result'] = "success";
							$response ['data'] = $data;
        		}
        		else{
        		$response ['message'] = "Sorry No Data Found..";
				$response ['result'] = "success";
				
        		}
		}else{
			$response ['message'] = "Already Registred..";
			$response ['result'] = "success";
		} 
		 echo json_encode ($response);		 	
   }


	public function payment_success()
	{  
		 
    //  print_r($_POST);die;
    $email =  $_POST['udf2'];
	  $phone =	$_POST['udf3'];
	  $txnid =	$_POST['txnid'];
	  /* $email =  "sak@gmail.com";
	  $phone =	"7896541230";
	  $txnid =	"810355";*/
	  $datau = $this->db->query("select *  from tbl_registraton_nagarcycling2021 where mno='$phone' and email='$email' and txnId='$txnid' order by user_id desc")->result_array();
	 $cntu = count($datau); 
	 if($cntu>0){
	 	
	     	$this->load->view('success');
			$this->load->view('footer');
	 }else{
	 
	 $data = $this->db->query("select *  from tbl_registraton_nagarcycling2021_temp  where  `mno`='$phone' and  `email`='$email' order by `user_temp_id` desc")->result_array();
	
	/* echo '<pre>';
	 print_r($data);
	 die;*/
	 $cnt = count($data);

	 	if($cnt>0){
	 			// code for bibcode
	 	
		$cat =$data[0]['category'];
		 $result = $this->db->query("SELECT MAX(`bib_id`) AS `bib_id` FROM `tbl_registraton_nagarcycling2021` WHERE `category` = '$cat'")->row();
		$bib = $result->bib_id;
		
		if($bib=='' && $data[0]['category']=='20KM'){
             $bibid='2000';
		}else if($bib=='' && $data[0]['category']=='50KM'){
			 $bibid='5000';
		}else if($bib=='' && $data[0]['category']=='100KM'){
			 $bibid='10000';
		}else{
			$bibid=$bib;
		}
         $final_bib_id = $bibid+1;
			$insert_array1	=	array(
										'name'  						=> $data[0]['name'],
										'mno'   						=>$data[0]['mno'],
										'email'   						=> $data[0]['email'],
										'dob'   						=> date("Y-m-d", strtotime($data[0]['dob'])),
										'category'     					=>$data[0]['category'],
										'state'   						=> $data[0]['state'],
										'address'   					=> $data[0]['address'],
										'city'   						=> $data[0]['city'],
										'pincode'   					=> $data[0]['pincode'],
										'country'   					=> $data[0]['country'],
										'gender'   						=> $data[0]['gender'],
										'blood_group'   				=> $data[0]['blood_group'],
										'tshirt_size'   				=> $data[0]['tshirt_size'],
										'emergency_contact_name'   		=> $data[0]['emergency_contact_name'],
										'emergency_contact_number'   	=>$data[0]['emergency_contact_number'],
										'age'   						=> $data[0]['age'],
										'txnId'  					 	=> $txnid,
										'bib_id' 					  	=> $final_bib_id,
										
										);

			$pagedata['data']=$insert_array1;
      	$this->db->insert('tbl_registraton_nagarcycling2021',$insert_array1);
       // $emailinvoice = $this->load->view('invoicePdf',$pagedata,true);
       
       	$pagedata1['data'] = $this->db->query("select *  from tbl_registraton_nagarcycling2021 where mno='$phone' and email='$email' and bib_id='$final_bib_id'")->result_array();
     
       
       
    
        $this->load->library('html2pdf');
	     
	    //Set folder to save PDF to
	    $this->html2pdf->folder('./assets/pdfs/');
	    
	    //Set the filename to save/download as
	    $this->html2pdf->filename('Invoice.pdf');
	    
	    //Set the paper defaults
	    $this->html2pdf->paper('a4', 'portrait');
	    
	  /*  $data = array(
	    	'title' => 'PDF Created',
	    	'message' => 'Hello World!'
	    );*/
	 
	    //Load html view
	     $this->html2pdf->html($this->load->view('invoicePdf',$pagedata1,TRUE));
	       $path = $this->html2pdf->create('save'); 
	
       
       
        	 $name = $data[0]['name']; 
		  
	
			$ride = $data[0]['category'];
		    $mno = $phone;	
		    
		    $emailinvoice = "Congratulations! You Have Successfully Registered For India's Biggest Cyclothon- Ahmednagar Cyclothon 2021,Season 4.";
		    $msg = "Congratulations! You Have Successfully Registered For India's Biggest Cyclothon- Ahmednagar Cyclothon 2021, Season 4. Your BIB ID is: $final_bib_id  Ride Category: $ride. Keep Riding! ";
			$this->message_send_bulk($msg, $mno);
			$insert_array=array(
							'number'=>$mno,
							'message'=>$msg
							);
			$this->db->insert('sms_sent',$insert_array);

	   
	  		  $subj = "Registration Successful For Ahmednagar Cyclothon 2021";
				$emailmsg = "You Have Done Successfully Registration For Ahmednagar Cyclothon 2021";
		
							$developmentMode=false;
					$mail = new PHPMailer ($developmentMode);

						if ($developmentMode) {
						$mail->SMTPOptions = [
							'ssl'=> [
							'verify_peer' => false,
							'verify_peer_name' => false,
							'allow_self_signed' => true
							]
						];
						}
			$mail->CharSet   = "UTF-8";	
			$mail->IsSMTP (); // set mailer to use SMTP
			$mail->SMTPAuth = true; // turn on SMTP authentication
			$mail->Host = "mail.nagarcycling.com"; // specify main and backup server
	        $mail->Port = 587; // set the port to use
	        $mail->SMTPAuth = true; // turn on SMTP authentication
	        $mail->Username = "support@nagarcycling.com"; // your SMTP username or your gmail username
	        $mail->Password = "nagarcycling@123"; // your SMTP password or your gmail password
	        
	        $from = "support@nagarcycling.com"; // Reply to this email
			$name = "Ahmednagar Cyclothone 2021"; // Recipient's name
			$mail->From = $from;
			$mail->FromName = "Ahmednagar Cyclothone 2021"; // Name to indicate where the email came from when the recepient received
			
			
  			
			$mail->AddAddress ( $email, $name );
			$mail->AddReplyTo ( "support@nagarcycling.com", "Ahmednagar Cyclothon 2021" );
			//$mail->AddCC("support@urjaacademy.com","Admin");
			$mail->IsHTML ( true ); // send as HTML 
			//print_r($this->input->post('message'));exit;
			$mail->Subject =  $subj;
			$mail->Body = $emailinvoice;
		    $mail->addAttachment($path); 
			$mail->Send();

		$Path = './assets/pdfs/Invoice.pdf';
		unlink($Path);
				//$mail->Send();
				$insert_array11=array(
					'email'=>$email,
					'subject'=>  $subj,
					'message'=>'invoice'
					);
				$this->db->insert('email_sent',$insert_array11);
			

		   	$this->load->view('success');
			$this->load->view('footer');
	    	}
    	}
	}


public function payment_fail()
	{  

		 	$this->load->view('failure');
			$this->load->view('footer');
	  
		
	}



	//Login ASM/TSM
	public function step1() {
		$response ['message'] = "fail";
		$response ['result'] =  "Param is reqired";
		if( !empty($_POST['mr_code']) && !empty($_POST['device_token']) ){
			$mr_code = trim($_POST['mr_code']);
			
			$select = array('mr_id');
			$where = array('mr_code'=>$mr_code, 'user_status'=>'2');
			$check = $this->Base_Models->GetAllValues ( "mr" ,$where, $select);//check
			if(count($check)== 0){
				$response ['result'] = 'MR code not found';
			}else{
				$response ['message'] = "done";
				$response ['result'] = 'MR code found';
			}
		}
		echo json_encode ($response);
	}
	
	public function step2() {
		$response ['message'] = "fail";
		$response ['result'] =  "Param is reqired";
		if( !empty($_POST['mr_code']) && !empty($_POST['mobile_no']) && !empty($_POST['email']) && !empty($_POST['device_token']) ){
			$mr_code = trim($_POST['mr_code']);
			$mobile_no = trim($_POST['mobile_no']);
			$email = trim($_POST['email']);
			
			//check code exist
			$select1 = array('mr_id');
			$where1 = array('mr_code'=>$mr_code, 'user_status'=>'2');
			$check1 = $this->Base_Models->GetAllValues ( "mr" ,$where1, $select1);//check
			if(count($check1)== 0){
				$response ['result'] = 'MR code not found';
			}else{			
				$select = array('mr_id','mobile_no','mr_code','fname','email','hq','state','division','designation');
				$where = array('mr_code'=>$mr_code,'mobile_no'=>$mobile_no, 'email'=>$email, 'user_status'=>'2');
				$check = $this->Base_Models->GetAllValues ( "mr" ,$where, $select);//check
				if(count($check)== 0){
					$response ['result'] = 'Mobile No. Email not match with this code: '.$mr_code;
				}else{
					$update = array ("device_token"=>$_POST['device_token']);
					$this->Base_Models->UpadateValue ( "mr",$update, $where );
					
					$response ['message'] = "done";
					$response ['result'] = 'Matched';
					$response ['data'] = $check;
				}
			}
		}
		echo json_encode ($response);
	}
}
?>