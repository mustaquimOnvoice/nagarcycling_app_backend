<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH . 'core/class.phpmailer.php';
require_once APPPATH . 'core/class.smtp.php';

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('welcome_message');
	}
	
    public function payment_success_app() {
        //echo '<pre>';
        //print_r($_POST);
        //die();
        //if(!empty($_POST)){
			//die('l');die;
			$temp_id =$_POST['udf1'];
			$email =$_POST['udf2'];
			$phone =$_POST['udf3'];
			$txnid =$_POST['txnid'];
			$amount =$_POST['amount'];
			// $email =  "mustaquim.sayyed@onevoice.co.in";
			// $phone =	"8087586743";
			// $txnid =	"771092iu";
			// $temp_id =	"6";
			// $amount = 500;

			$datau = $this->db->query("select *  from tbl_registraton_nagarcycling2021 where mno='$phone' and email='$email' and txnId='$txnid' order by user_id desc")->result_array();
			$cntu = count($datau);
				if($cntu>0){	 	
					//redirect('https://www.nagarcycling.com/demo/index.php/Registration/payment_success_redirect'); 
					//echo "<script>alert('already registered')</script>";
					echo 'Already registered';
				}else{
					$data = $this->db->query("select *  from tbl_registraton_nagarcycling2021_temp where user_temp_id = '$temp_id'")->result_array();
					$cnt = count($data);

					if($cnt>0){
						// code for bibcode					
						$cat = $data[0]['category'];
						$result = $this->db->query("SELECT MAX(`bib_id`) AS `bib_id` FROM `tbl_registraton_nagarcycling2021` WHERE `category` = '$cat'")->row();
						$bib = $result->bib_id;
						
						if($bib=='' && ($data[0]['category']=='20KM' || $data[0]['category']=='20km')){
							$bibid='2000';
						}else if($bib=='' && ($data[0]['category']=='50KM' || $data[0]['category']=='50km')){
							$bibid='5000';
						}else if($bib=='' && ($data[0]['category']=='100KM' || $data[0]['category']=='100km')){
							$bibid='10000';
						}else{
							$bibid=$bib;
						}
						$final_bib_id = $bibid+1;
						$insert_array1	=	array(
													'name'  						=> $data[0]['name'],
													'mno'   						=> $data[0]['mno'],
													'email'   						=> $data[0]['email'],
													'dob'   						=> date("Y-m-d", strtotime($data[0]['dob'])),
													'category'     					=> $data[0]['category'],
													'state'   						=> $data[0]['state'],
													'address'   					=> $data[0]['address'],
													'city'   						=> $data[0]['city'],
													'pincode'   					=> $data[0]['pincode'],
													'country'   					=> $data[0]['country'],
													'gender'   						=> $data[0]['gender'],
													'blood_group'   				=> $data[0]['blood_group'],
													'tshirt_size'   				=> $data[0]['tshirt_size'],
													'emergency_contact_name'   		=> $data[0]['emergency_contact_name'],
													'emergency_contact_number'   	=> $data[0]['emergency_contact_number'],
													'age'   						=> $data[0]['age'],
													'txnId'  					 	=> $txnid,
													'total_amount'					=> $amount,
													'bib_id' 					  	=> $final_bib_id,
													'app_user_id' 					=> $data[0]['app_user_id']											
													);

						$pagedata['data']=$insert_array1;
						$this->db->insert('tbl_registraton_nagarcycling2021',$insert_array1);
					// $emailinvoice = $this->load->view('invoicePdf',$pagedata,true);
					
						$pagedata1['data'] = $this->db->query("select *  from tbl_registraton_nagarcycling2021 where mno='$phone' and email='$email' and bib_id='$final_bib_id'")->result_array();
					
						$this->load->library('html2pdf');					
						//Set folder to save PDF to
						$this->html2pdf->folder('./assets/pdfs/');					
						//Set the filename to save/download as
						$this->html2pdf->filename('Invoice.pdf');					
						//Set the paper defaults
						$this->html2pdf->paper('a4', 'portrait');					
					
						$pagedata1['data']['amount'] = $amount;
						//Load html view
						$this->html2pdf->html($this->load->view('appInvoicePdf',$pagedata1,TRUE));
						$path = $this->html2pdf->create('save'); 
						$name = $data[0]['name']; 
						$ride = $data[0]['category'];
						$mno = $phone;	
						
						$emailinvoice = "Congratulations! You Have Successfully Registered For India's Biggest Cyclothon- Ahmednagar Cyclothon 2021,Season 4.";
						$msg = "Congratulations! You Have Successfully Registered For India's Biggest Cyclothon- Ahmednagar Cyclothon 2021, Season 4. Your BIB ID is: $final_bib_id  Ride Category: $ride. Keep Riding! ";
						$this->message_send_bulk($msg, $mno);
						$insert_array=array(
										'number'=>$mno,
										'message'=>$msg
										);
						$this->db->insert('sms_sent',$insert_array);

					
						$subj = "Registration Successful For Ahmednagar Cyclothon 2021";
						$emailmsg = "You Have Done Successfully Registration For Ahmednagar Cyclothon 2021";			
						$developmentMode=false;
						$mail = new PHPMailer ($developmentMode);
							if ($developmentMode) {
								$mail->SMTPOptions = [
									'ssl'=> [
									'verify_peer' => false,
									'verify_peer_name' => false,
									'allow_self_signed' => true
									]
								];
							}
						$mail->CharSet   = "UTF-8";	
						$mail->IsSMTP (); // set mailer to use SMTP
						$mail->SMTPAuth = true; // turn on SMTP authentication
						$mail->Host = "mail.nagarcycling.com"; // specify main and backup server
						$mail->Port = 587; // set the port to use
						$mail->SMTPAuth = true; // turn on SMTP authentication
						$mail->Username = "support@nagarcycling.com"; // your SMTP username or your gmail username
						$mail->Password = "nagarcycling@123"; // your SMTP password or your gmail password
						
						$from = "support@nagarcycling.com"; // Reply to this email
						$name = "Ahmednagar Cyclothone 2021"; // Recipient's name
						$mail->From = $from;
						$mail->FromName = "Ahmednagar Cyclothone 2021"; // Name to indicate where the email came from when the recepient received
						
						
						
						$mail->AddAddress ( $email, $name );
						$mail->AddReplyTo ( "support@nagarcycling.com", "Ahmednagar Cyclothon 2021" );
						//$mail->AddCC("support@urjaacademy.com","Admin");
						$mail->IsHTML ( true ); // send as HTML 
						//print_r($this->input->post('message'));exit;
						$mail->Subject =  $subj;
						$mail->Body = $emailinvoice;
						$mail->addAttachment($path); 
						$mail->Send();

						$Path = './assets/pdfs/Invoice.pdf';
						unlink($Path);
						//$mail->Send();
						$insert_array11=array(
							'email'=>$email,
							'subject'=>  $subj,
							'message'=>'invoice'
							);
						$this->db->insert('email_sent',$insert_array11);

						//redirect('https://www.nagarcycling.com/demo/index.php/Registration/payment_success_redirect'); 
					}
				}
		//}else{
		//	redirect('https://www.nagarcycling.com/');
		//}
	}
	
	public function message_send_bulk($message1, $number) {
		//$numsStr = implode(',',$number);
		$postData = "{ 
			\"sender\": \"NGRCYC\", 
			\"route\": \"4\", 
			\"country\": \"91\", 
			\"unicode\": \"1\",
			\"sms\": [ 
						{ 
							\"message\": \"$message1\", 
							\"to\": [ $number ] 
						}
					] 
		}";
			
		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://api.msg91.com/api/v2/sendsms?country=91",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => $postData,
		  CURLOPT_SSL_VERIFYHOST => 0,
		  CURLOPT_SSL_VERIFYPEER => 0,
		  CURLOPT_HTTPHEADER => array(
			"authkey: 228445AqdIYICptZd5d36a66d",
			"content-type: application/json"
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		   "cURL Error #:" . $err;
		} else {
		  $response;
		}
	}
	
}
