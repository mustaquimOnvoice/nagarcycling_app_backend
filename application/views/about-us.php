<!DOCTYPE html>
<html dir="ltr" lang="en">

<!-- Mirrored from html.kodesolution.live/html/health-beauty/sports-trainer/v2.0/demo/page-about1.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 27 Jul 2019 07:30:40 GMT -->
<head>

<!-- Meta Tags -->
<meta name="viewport" content="width=device-width,initial-scale=1.0"/>
<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>


<!-- Page Title -->
<title>SportsTrainer- Sports, Health, Gym & Fitness Personal Trainer HTML5 Theme</title>

<!-- Favicon and Touch Icons -->
<link href="<?php echo base_url();?>web_asset/images/favicon.png" rel="shortcut icon" type="image/png">
<link href="<?php echo base_url();?>web_asset/images/apple-touch-icon.png" rel="apple-touch-icon">
<link href="<?php echo base_url();?>web_asset/images/apple-touch-icon-72x72.png" rel="apple-touch-icon" sizes="72x72">
<link href="<?php echo base_url();?>web_asset/images/apple-touch-icon-114x114.png" rel="apple-touch-icon" sizes="114x114">
<link href="<?php echo base_url();?>web_asset/images/apple-touch-icon-144x144.png" rel="apple-touch-icon" sizes="144x144">

<!-- Stylesheet -->
<link href="<?php echo base_url();?>web_asset/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url();?>web_asset/css/jquery-ui.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url();?>web_asset/css/animate.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url();?>web_asset/css/css-plugin-collections.css" rel="stylesheet"/>
<!-- CSS | menuzord megamenu skins -->
<link id="menuzord-menu-skins" href="<?php echo base_url();?>web_asset/css/menuzord-skins/menuzord-boxed.css" rel="stylesheet"/>
<!-- CSS | Main style file -->
<link href="<?php echo base_url();?>web_asset/css/style-main.css" rel="stylesheet" type="text/css">
<!-- CSS | Theme Color -->
<link href="<?php echo base_url();?>web_asset/css/colors/theme-skin-lemon.css" rel="stylesheet" type="text/css">
<!-- CSS | Preloader Styles -->
<link href="<?php echo base_url();?>web_asset/css/preloader.css" rel="stylesheet" type="text/css">
<!-- CSS | Custom Margin Padding Collection -->
<link href="<?php echo base_url();?>web_asset/css/custom-bootstrap-margin-padding.css" rel="stylesheet" type="text/css">
<!-- CSS | Responsive media queries -->
<link href="<?php echo base_url();?>web_asset/css/responsive.css" rel="stylesheet" type="text/css">
<!-- CSS | Style css. This is the file where you can place your own custom css code. Just uncomment it and use it. -->
<!-- <link href="css/style.css" rel="stylesheet" type="text/css"> -->

<!-- external javascripts -->
<script src="<?php echo base_url();?>web_asset/js/jquery-2.2.4.min.js"></script>
<script src="<?php echo base_url();?>web_asset/js/jquery-ui.min.js"></script>
<script src="<?php echo base_url();?>web_asset/js/bootstrap.min.js"></script>
<!-- JS | jquery plugin collection for this theme -->
<script src="<?php echo base_url();?>web_asset/js/jquery-plugin-collection.js"></script>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body class="">
<div id="wrapper">
  <!-- preloader -->
  <div id="preloader">
    <div id="spinner">
      <img src="<?php echo base_url();?>web_asset/images/preloaders/1.gif" alt="">
    </div>
    <div id="disable-preloader" class="btn btn-default btn-sm">Disable Preloader</div>
  </div>
  
  <!-- Header -->
  <header id="header" class="header">
    <div class="header-top sm-text-center bg-theme-colored">
      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <nav>
              <ul class="list-inline sm-text-center text-left flip mt-5">
                <li> <a class="text-white" href="<?php echo base_url();?>index.php/contactus">Contact Us</a> </li>
                <li class="text-white">|</li>
                <li> <a class="text-white" href="<?php echo base_url();?>index.php/termsandconditions">Terms & Conditions</a> </li>
               
              </ul>
            </nav>
          </div>
          <div class="col-md-6">
            <div class="widget m-0 mt-5 no-border">
              <ul class="list-inline text-right sm-text-center">
                <li class="pl-10 pr-10 mb-0 pb-0">
                  <div class="header-widget text-white"><i class="fa fa-phone"></i> 123-456-789 </div>
                </li>
                <li class="pl-10 pr-10 mb-0 pb-0">
                  <div class="header-widget text-white"><i class="fa fa-envelope-o"></i> contact@yourdomain.com </div>
                </li>
              </ul>
            </div>
          </div>
          <div class="col-md-2 text-right flip sm-text-center">
            <div class="widget m-0">
              <ul class="styled-icons icon-dark icon-circled icon-theme-colored icon-sm">
                <li class="mb-0 pb-0"><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li class="mb-0 pb-0"><a href="#"><i class="fa fa-twitter"></i></a></li>
                <li class="mb-0 pb-0"><a href="#"><i class="fa fa-instagram"></i></a></li>
                <li class="mb-0 pb-0"><a href="#"><i class="fa fa-linkedin text-white"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="header-nav">
      <div class="header-nav-wrapper bg-light navbar-scrolltofixed">
        <div class="container">
          <div
		  class="row">
            <div class="col-md-12">
              <div id="menuzord-right" class="menuzord orange no-bg"> <a class="menuzord-brand stylish-header pull-left flip" href="javascript:void(0)">
			  <img src="<?php echo base_url();?>web_asset/images/logo-wide-white.png" alt=""></a>
                <ul class="menuzord-menu">
                  <li><a href="<?php echo base_url();?>index.php/home">Home</a>
                 </li>
                  <li class="active"><a href="<?php echo base_url();?>index.php/about-us">About Us</a></li>
                  <li><a href="<?php echo base_url();?>index.php/events">Events</a></li>
                  <li><a href="#">Sponsors</a>
				   <ul class="dropdown">
				    <li><a href="<?php echo base_url();?>index.php/sponsors2020">Sponsors 2020</a></li>
                    <li><a href="<?php echo base_url();?>index.php/sponsors2019">Sponsors 2019</a></li>
                    <li><a href="<?php echo base_url();?>index.php/sponsors2018">Sponsors 2018</a></li>
					</ul>
				  </li>
				  <li><a href="<?php echo base_url();?>index.php/photos">Free Photos</a></li>
                  <li><a href="<?php echo base_url();?>index.php/contactus">Contact Us</a></li>				  
				   <li><a href="<?php echo base_url();?>index.php/registration">Registration</a></li>
                

			   </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </header>
  
  <!-- Start main-content -->
  <div class="main-content">

    <!-- Section: inner-header -->
    <section class="inner-header divider parallax layer-overlay overlay-dark-5" data-bg-img="images/bg/bg1.jpg">
      <div class="container">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row"> 
            <div class="col-md-12">
              <h3 class="title text-white">About</h3>
              <h4 class="text-theme-colored letter-space-3 font-weight-400 text-uppercase">Only he who can see the invisible can do the impossible. </h4>
              <ol class="breadcrumb text-white mt-10">
                <li><a class="text-white" href="#">Home</a></li>
                <li><a class="text-white" href="#">Pages</a></li>
                <li class="active text-theme-colored">About </li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </section>
    
    <!-- Section: About -->
    <section id="about" class="bg-lighter">
      <div class="container">
        <div class="section-content">
          <div class="row">
            <div class="col-md-6"> <img class="img-fullwidth" src="<?php echo base_url();?>web_asset/images/about/1.jpg" alt=""> </div>
            <div class="col-md-6">
              <div class="events-venue border-theme-colored border-5px p-40 pt-0 mt-50 ml-sm-0 pt-sm-80" data-margin-left="-60px">
                <h2 class="text-uppercase letter-space-1 text-center text-theme-colored bg-lighter mb-20" data-margin-top="-40px">About Us </h2>
                <div class="pl-50 pl-sm-0 mb-20">
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi praesentium saepe, consectetur consequatur deserunt, iusto eos aspernatur labore minus odit, est! Voluptatem alias eos ut voluptatum eum numquam eligendi fuga minima possimus!</p>
                </div>
                <div class="icon-box p-0 mb-20 pl-50 pl-sm-0"> <a class="icon mb-0 mr-0 pull-left flip bg-theme-colored mt-20 sm-pull-none" href="#"> <i class="flaticon-sports-gym-4 text-white font-36"></i> </a>
                  <div class="ml-100 ml-sm-0">
                    <h5 class="icon-box-title mt-15 mb-10 text-uppercase letter-space-1"><strong>Vision</strong></h5>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam interdum diam tortor, egestas varius erat aliquam a. </p>
                  </div>
                </div>
                <div class="icon-box p-0 mb-20 pl-50 pl-sm-0"> <a class="icon mb-0 mr-0 pull-left flip bg-theme-colored mt-20 sm-pull-none" href="#"> <i class="flaticon-sports-game-1 text-white font-36"></i> </a>
                  <div class="ml-100 ml-sm-0">
                    <h5 class="icon-box-title mt-15 mb-10 text-uppercase letter-space-1"><strong>Mission</strong></h5>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam interdum diam tortor, egestas varius erat aliquam a. </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

  <!-- Section: Team -->
    <section id="trainer" class="bg-light">
      <div class="container">
	   <div class="section-title text-center">
          <div class="row">
            <div class="col-md-8 col-md-offset-2">
              <h2 class="title text-uppercase">Our <span class="text-black font-weight-300">Team</span></h2>
              <p class="text-uppercase letter-space-1">Join our Training Club and Rise to a New Challenge</p>
            </div>
          </div>
        </div>
        <div class="section-content pb-10">
          <div class="row pb-30">
            <div class="col-xs-12 col-sm-6 col-md-4 mb-30 mb-sm-30">
              <div class="bg-img-box maxwidth400">
                <div class="photo">
                  <img class="img-fullwidth" src="<?php echo base_url();?>web_asset/images/team/1.jpg" alt="">
                </div>
                <div class="style3 bg-light border-right-5px border-theme-colored">
                  <h5 class="text-gray mt-0 mb-0">Race Director</h5>
                  <h3 class="text-theme-colored mt-0 mb-5">SUNIL MENON</h3>

                  <!--<p class="mt-0">2010 Best Trainer Award</p>-->
                </div>
              </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 mb-30 mb-sm-30">
              <div class="bg-img-box maxwidth400">
                <div class="photo">
                  <img class="img-fullwidth" src="<?php echo base_url();?>web_asset/images/team/2.jpg" alt="">
                </div>
                <div class="style3 bg-light border-right-5px border-theme-colored">
                  <!--<h5 class="text-gray mt-0 mb-0">Race Expart</h5>-->
                  <h3 class="text-theme-colored mt-0 mb-5">GAURAV FIRODIA</h3>
                  <!--<p class="mt-0">2011 Uefa Championship league</p>-->
                </div>
              </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 mb-30 mb-sm-30">
              <div class="bg-img-box maxwidth400">
                <div class="photo">
                  <img class="img-fullwidth" src="<?php echo base_url();?>web_asset/images/team/3.jpg" alt="">
                </div>
                <div class="style3 bg-light border-right-5px border-theme-colored">
                  <!--<h5 class="text-gray mt-0 mb-0">Boxing Expart</h5>-->
                  <h3 class="text-theme-colored mt-0 mb-5">KALYANI FIRODIA</h3>
                  <!--<p class="mt-0">2010 Best Trainer Award</p>-->
                </div>
              </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 mb-sm-30">
              <div class="bg-img-box maxwidth400">
                <div class="photo">
                  <img class="img-fullwidth" src="<?php echo base_url();?>web_asset/images/team/4.jpg" alt="">
                </div>
                <div class="style3 bg-light border-right-5px border-theme-colored">
                  <!--<h5 class="text-gray mt-0 mb-0">GYM Expart</h5>-->
                  <h3 class="text-theme-colored mt-0 mb-5">MAHESH MULAY</h3>
                  <!--<p class="mt-0">2010 Best Trainer Award</p>-->
                </div>
              </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 mb-sm-30">
              <div class="bg-img-box maxwidth400">
                <div class="photo">
                  <img class="img-fullwidth" src="<?php echo base_url();?>web_asset/images/team/5.jpg" alt="">
                </div>
                <div class="style3 bg-light border-right-5px border-theme-colored">
                <!--  <h5 class="text-gray mt-0 mb-0">Race Expart</h5>-->
                  <h3 class="text-theme-colored mt-0 mb-5">RAVI PATRE</h3>
                  <!--<p class="mt-0">2011 Uefa Championship league</p>-->
                </div>
              </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 mb-sm-30">
              <div class="bg-img-box maxwidth400">
                <div class="photo">
                  <img class="img-fullwidth" src="<?php echo base_url();?>web_asset/images/team/6.jpg" alt="">
                </div>
                <div class="style3 bg-light border-right-5px border-theme-colored">
                  <!--<h5 class="text-gray mt-0 mb-0">Boxing Expart</h5>-->
                  <h3 class="text-theme-colored mt-0 mb-5">ABHIJEET PATHAK</h3>
                  <!--<p class="mt-0">2010 Best Trainer Award</p>-->
                </div>
              </div>
            </div>
	
</div>
  <div class="row pb-30">
	
			<div class="col-xs-12 col-sm-6 col-md-4 mb-30 mb-sm-30">
              <div class="bg-img-box maxwidth400">
                <div class="photo">
                  <img class="img-fullwidth" src="<?php echo base_url();?>web_asset/images/team/7.jpg" alt="">
                </div>
                <div class="style3 bg-light border-right-5px border-theme-colored">
                  <!--<h5 class="text-gray mt-0 mb-0">Boxing Expart</h5>-->
                  <h3 class="text-theme-colored mt-0 mb-5">DINESH SANKLECHA</h3>
                 <!-- <p class="mt-0">2010 Best Trainer Award</p>-->
                </div>
              </div>
            </div>
			
			
			<div class="col-xs-12 col-sm-6 col-md-4 mb-30 mb-sm-30">
              <div class="bg-img-box maxwidth400">
                <div class="photo">
                  <img class="img-fullwidth" src="<?php echo base_url();?>web_asset/images/team/8.jpg" alt="">
                </div>
                <div class="style3 bg-light border-right-5px border-theme-colored">
                  <!--<h5 class="text-gray mt-0 mb-0">Boxing Expart</h5>-->
                  <h3 class="text-theme-colored mt-0 mb-5">JYOTI PISORE</h3>
                  <!--<p class="mt-0">2010 Best Trainer Award</p>-->
                </div>
              </div>
            </div>
			
			
			<div class="col-xs-12 col-sm-6 col-md-4 mb-30 mb-sm-30">
              <div class="bg-img-box maxwidth400">
                <div class="photo">
                  <img class="img-fullwidth" src="<?php echo base_url();?>web_asset/images/team/9.jpg" alt="">
                </div>
                <div class="style3 bg-light border-right-5px border-theme-colored">
                  <!--<h5 class="text-gray mt-0 mb-0">Boxing Expart</h5>-->
                  <h3 class="text-theme-colored mt-0 mb-5">RAVI PISORE</h3>
                  <!--<p class="mt-0">2010 Best Trainer Award</p>-->
                </div>
              </div>
            </div>
			
			<div class="col-xs-12 col-sm-6 col-md-4 mb-sm-30">
              <div class="bg-img-box maxwidth400">
                <div class="photo">
                  <img class="img-fullwidth" src="<?php echo base_url();?>web_asset/images/team/10.jpg" alt="">
                </div>
                <div class="style3 bg-light border-right-5px border-theme-colored">
                  <!--<h5 class="text-gray mt-0 mb-0">Boxing Expart</h5>-->
                  <h3 class="text-theme-colored mt-0 mb-5">RENUKA</h3>
                  <!--<p class="mt-0">2010 Best Trainer Award</p>-->
                </div>
              </div>
            </div>
			
			<div class="col-xs-12 col-sm-6 col-md-4 mb-sm-30">
              <div class="bg-img-box maxwidth400">
                <div class="photo">
                  <img class="img-fullwidth" src="<?php echo base_url();?>web_asset/images/team/11.jpg" alt="">
                </div>
                <div class="style3 bg-light border-right-5px border-theme-colored">
                  <!--<h5 class="text-gray mt-0 mb-0">Boxing Expert</h5>-->
                  <h3 class="text-theme-colored mt-0 mb-5">TUSHAR PATWA</h3>
                  <!--<p class="mt-0">2010 Best Trainer Award</p>-->
                </div>
              </div>
            </div>
			
			
          </div>
        </div>
      </div>
    </section>
    

    <!--
	<section id="achievements" class="divider parallax layer-overlay overlay-deep" data-bg-img="images/bg/bg2.jpg">
      <div class="container pb-40">
        <div class="section-title text-center">
          <div class="row">
            <div class="col-md-8 col-md-offset-2">
              <h2 class="title text-uppercase">Our <span class="text-black font-weight-300">Achievements</span></h2>
              <p class="text-uppercase letter-space-1">Join our Training Club and Rise to a New Challenge</p>
            </div>
          </div>
        </div>
        <div class="section-content">
          <div class="row">
            <div class="col-md-12">
              <div class="owl-carousel-4col text-center">
                <div class="item">
                  <a href="#"> <img style="width: 55%; margin: auto;" src="images/achievements/1.png" alt=""></a>
                  <h5 class="letter-space-2 text-uppercase text-theme-colored">2010 Best Trainer Award</h5>
                </div>
                <div class="item">
                  <a href="#"> <img style="width: 55%; margin: auto;" src="images/achievements/2.png" alt=""></a>
                  <h5 class="letter-space-2 text-uppercase text-theme-colored">2010 Best Trainer Award</h5>
                </div>
                <div class="item">
                  <a href="#"> <img style="width: 55%; margin: auto;" src="images/achievements/3.png" alt=""></a>
                  <h5 class="letter-space-2 text-uppercase text-theme-colored">2010 Best Trainer Award</h5>
                </div>
                <div class="item">
                  <a href="#"> <img style="width: 55%; margin: auto;" src="images/achievements/4.png" alt=""></a>
                  <h5 class="letter-space-2 text-uppercase text-theme-colored">2010 Best Trainer Award</h5>
                </div>
                <div class="item">
                  <a href="#"> <img style="width: 55%; margin: auto;" src="images/achievements/5.png" alt=""></a>
                  <h5 class="letter-space-2 text-uppercase text-theme-colored">2010 Best Trainer Award</h5>
                </div>
              </div> 
            </div>
          </div>
        </div>
      </div>
    </section>-->

  </div>
  <!-- end main-content -->
  
  