<!DOCTYPE html>
<html dir="ltr" lang="en">

<!-- Mirrored from html.kodesolution.live/html/health-beauty/sports-trainer/v2.0/demo/page-gallery-grid-animation.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 27 Jul 2019 07:30:49 GMT -->
<head>

<!-- Meta Tags -->
<meta name="viewport" content="width=device-width,initial-scale=1.0"/>
<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>


<!-- Page Title -->
<title>Ahmednagar Cyclothon - Sponsors 2020</title>

<!-- Favicon and Touch Icons -->
<link href="<?php echo base_url();?>web_asset/images/favicon.png" rel="shortcut icon" type="image/png">
<link href="<?php echo base_url();?>web_asset/images/apple-touch-icon.png" rel="apple-touch-icon">
<link href="<?php echo base_url();?>web_asset/images/apple-touch-icon-72x72.png" rel="apple-touch-icon" sizes="72x72">
<link href="<?php echo base_url();?>web_asset/images/apple-touch-icon-114x114.png" rel="apple-touch-icon" sizes="114x114">
<link href="<?php echo base_url();?>web_asset/images/apple-touch-icon-144x144.png" rel="apple-touch-icon" sizes="144x144">

<!-- Stylesheet -->
<link href="<?php echo base_url();?>web_asset/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url();?>web_asset/css/jquery-ui.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url();?>web_asset/css/animate.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url();?>web_asset/css/css-plugin-collections.css" rel="stylesheet"/>
<!-- CSS | menuzord megamenu skins -->
<link id="menuzord-menu-skins" href="<?php echo base_url();?>web_asset/css/menuzord-skins/menuzord-boxed.css" rel="stylesheet"/>
<!-- CSS | Main style file -->
<link href="<?php echo base_url();?>web_asset/css/style-main.css" rel="stylesheet" type="text/css">
<!-- CSS | Theme Color -->
<link href="<?php echo base_url();?>web_asset/css/colors/theme-skin-lemon.css" rel="stylesheet" type="text/css">
<!-- CSS | Preloader Styles -->
<link href="<?php echo base_url();?>web_asset/css/preloader.css" rel="stylesheet" type="text/css">
<!-- CSS | Custom Margin Padding Collection -->
<link href="<?php echo base_url();?>web_asset/css/custom-bootstrap-margin-padding.css" rel="stylesheet" type="text/css">
<!-- CSS | Responsive media queries -->
<link href="<?php echo base_url();?>web_asset/css/responsive.css" rel="stylesheet" type="text/css">
<!-- CSS | Style css. This is the file where you can place your own custom css code. Just uncomment it and use it. -->
<!-- <link href="css/style.css" rel="stylesheet" type="text/css"> -->

<!-- external javascripts -->
<script src="<?php echo base_url();?>web_asset/js/jquery-2.2.4.min.js"></script>
<script src="<?php echo base_url();?>web_asset/js/jquery-ui.min.js"></script>
<script src="<?php echo base_url();?>web_asset/js/bootstrap.min.js"></script>
<!-- JS | jquery plugin collection for this theme -->
<script src="<?php echo base_url();?>web_asset/js/jquery-plugin-collection.js"></script>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body class="">
<div id="wrapper">
  <!-- preloader -->
<!--   <div id="preloader">
    <div id="spinner">
      <img src="<?php echo base_url();?>web_asset/images/preloaders/1.gif" alt="">
    </div>
    <div id="disable-preloader" class="btn btn-default btn-sm">Disable Preloader</div>
  </div> -->
  
  <!-- Header -->
  <header id="header" class="header">
    <div class="header-top sm-text-center bg-theme-colored">
      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <nav>
              <ul class="list-inline sm-text-center text-left flip mt-5">
                <li> <a class="text-white" href="<?php echo base_url();?>index.php/contactus">Contact Us</a> </li>
                <li class="text-white">|</li>
                <li> <a class="text-white" href="<?php echo base_url();?>index.php/termsandconditions">Terms & Conditions</a> </li>
               
              </ul>
            </nav>
          </div>
          <div class="col-md-6">
            <div class="widget m-0 mt-5 no-border">
              <ul class="list-inline text-right sm-text-center">
                <li class="pl-10 pr-10 mb-0 pb-0">
                  <div class="header-widget text-white"><i class="fa fa-phone"></i> 123-456-789 </div>
                </li>
                <li class="pl-10 pr-10 mb-0 pb-0">
                  <div class="header-widget text-white"><i class="fa fa-envelope-o"></i> contact@yourdomain.com </div>
                </li>
              </ul>
            </div>
          </div>
          <div class="col-md-2 text-right flip sm-text-center">
            <div class="widget m-0">
              <ul class="styled-icons icon-dark icon-circled icon-theme-colored icon-sm">
                <li class="mb-0 pb-0"><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li class="mb-0 pb-0"><a href="#"><i class="fa fa-twitter"></i></a></li>
                <li class="mb-0 pb-0"><a href="#"><i class="fa fa-instagram"></i></a></li>
                <li class="mb-0 pb-0"><a href="#"><i class="fa fa-linkedin text-white"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="header-nav">
      <div class="header-nav-wrapper bg-light navbar-scrolltofixed">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div id="menuzord-right" class="menuzord orange no-bg"> <a class="menuzord-brand stylish-header pull-left flip" href="javascript:void(0)"><img src="<?php echo base_url();?>web_asset/images/logo-wide-white.png" alt=""></a>
               <ul class="menuzord-menu">
                 <li><a href="<?php echo base_url();?>index.php/home">Home</a>
                 </li>
                  <li><a href="<?php echo base_url();?>index.php/aboutus">About Us</a></li>
                  <li><a href="<?php echo base_url();?>index.php/events">Events</a></li>
                  <li><a href="#">Sponsors</a>
				   <ul class="dropdown">
				    <li><a href="<?php echo base_url();?>index.php/sponsors2020">Sponsors 2020</a></li>
                    <li><a href="<?php echo base_url();?>index.php/sponsors2019">Sponsors 2019</a></li>
                    <li><a href="<?php echo base_url();?>index.php/sponsors2018">Sponsors 2018</a></li>
					</ul>
				  </li>
				  <li><a href="<?php echo base_url();?>index.php/photos">Free Photos</a></li>
                  <li class="active"><a href="<?php echo base_url();?>index.php/contactus">Contact Us</a></li>				  
				  <li><a href="<?php echo base_url();?>index.php/registration">Registration</a></li>
				</ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </header>
  
  <!-- Start main-content -->
  <div class="main-content">

    <!-- Section: inner-header -->
    <section class="inner-header divider parallax layer-overlay overlay-dark-5" data-bg-img="<?php echo base_url();?>web_asset/images/bg/bg1.jpg">
     <div class="container">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row"> 
            <div class="col-md-12">
              <h3 class="title text-white">Contact</h3>
              <h4 class="text-theme-colored letter-space-3 font-weight-400 text-uppercase">Only he who can see the invisible can do the impossible. </h4>
              <ol class="breadcrumb text-white mt-10">
                <li><a class="text-white" href="#">Home</a></li>
                <li><a class="text-white" href="#">Pages</a></li>
                <li class="active text-theme-colored">Contact </li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- Section Contact -->
    <section id="contact" class="divider bg-lighter">
      <div class="container">
        <div class="section-title">
          <div class="row">
            <div class="col-md-8 col-md-offset-2 text-center">
              <h2 class="text-uppercase title">GET IN <span class="text-black font-weight-300"> TOUCH</span></h2>
              <p class="text-uppercase letter-space-1">JOIN OUR TRAINING CLUB AND RISE TO A NEW CHALLENGE</p>
            </div>
          </div>
        </div>
        <div class="section-content">
          <div class="row">
            <div class="col-sm-12 col-md-12">
              <div class="contact-wrapper">
              <?php  if(@$this->session->userdata('success') != ''){ ?>
                <div class="alert alert-success alert-dismissable">
                   <i class="fa fa-check"></i>
                 <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
              
                 <?php  echo "Enquiry Sent Successfully"; $this->session->unset_userdata('success'); ?>
                </div>
              <?php }?>
                <!-- Contact Form -->
                <form  name="contact_form" class="form-transparent" action="<?php echo base_url();?>index.php/contactus/backendemail" method="post">
                  <div class="row">
                    <div class="col-sm-8">
                      <div class="col-sm-6">
                        <div class="form-group">
                          <input id="form_name" name="form_name" class="form-control" type="text" placeholder="Enter Name" required="">
                        </div>
                        <div class="form-group">
                          <input id="form_email" name="form_email" class="form-control required email" type="email" required="" placeholder="Enter Email">
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group">
                          <input id="form_subject" name="form_subject" class="form-control required" type="text" required="" placeholder="Enter Subject">
                        </div>
                        <div class="form-group">
                          <input id="form_phone" name="form_phone" class="form-control" type="number" placeholder="Enter Phone" required="">
                        </div>
                      </div>
                      <div class="col-sm-12">
                        <div class="form-group">  
                          <textarea id="form_message" name="form_message" required="" class="form-control required" rows="5" placeholder="Enter Message" style="height: 165px;"></textarea>
                        </div>
                        <div class="form-group mt-20">
                         
                          <input type="submit" class="btn btn-theme-colored mr-5" value="Send your message">
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-4">
                      <h3 class="mt-0 text-theme-colored font-weight-300">Contact info</h3>
                      <p>Integer tincidunt  Cras dapibus Vivamus elementum semper nisi Aenean vulputate eleifend tellus.</p>

                      <ul class="mt-30">
                        <li><i class="fa fa-phone mb-20 text-theme-colored mr-5 font-20"></i>8308054000</li>
                        <li><i class="fa fa-map-marker mb-20 text-theme-colored mr-5 font-20"></i> IMA Bhavan- Near Railway overbridge, Kalyan Road.</li>
                        <li><i class="fa fa-envelope mb-20 text-theme-colored mr-5 font-20"></i> support@nagarcycling.com</li>
                        <li><i class="fa fa-globe mb-20 text-theme-colored mr-5 font-20 fa-spin"></i> www.nagarcycling.com</li>
                        
                      </ul>
                    </div>
                  </div>
                </form>
                <!-- Contact Form Validation-->
                <script type="text/javascript">
                  $("#contact_form").validate({
                    submitHandler: function(form) {
                      var form_btn = $(form).find('button[type="submit"]');
                      var form_result_div = '#form-result';
                      $(form_result_div).remove();
                      form_btn.before('<div id="form-result" class="alert alert-success" role="alert" style="display: none;"></div>');
                      var form_btn_old_msg = form_btn.html();
                      form_btn.html(form_btn.prop('disabled', true).data("loading-text"));
                      $(form).ajaxSubmit({
                        dataType:  'json',
                        success: function(data) {
                          if( data.status == 'true' ) {
                            $(form).find('.form-control').val('');
                          }
                          form_btn.prop('disabled', false).html(form_btn_old_msg);
                          $(form_result_div).html(data.message).fadeIn('slow');
                          setTimeout(function(){ $(form_result_div).fadeOut('slow') }, 6000);
                        }
                      });
                    }
                  });
                </script>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!--======= Section Map ===== -->
    <section>
      <div class="container-fluid pt-0 pb-0">
        <div class="row">
          <div class="col-sm-12 col-md-12">
<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d7540.645861040153!2d74.759441!3d19.093484!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x3cfd599b71301bcb!2sAhmednagar+Club+Ltd.!5e0!3m2!1sen!2sin!4v1566538610289!5m2!1sen!2sin" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
         
          </div>
        </div>
      </div>
    </section>    
  </div>
  <!-- end main-content -->
  
  
  