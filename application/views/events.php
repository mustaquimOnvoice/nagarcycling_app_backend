<!DOCTYPE html>
<html dir="ltr" lang="en">

<!-- Mirrored from html.kodesolution.live/html/health-beauty/sports-trainer/v2.0/demo/page-services-style1.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 27 Jul 2019 07:30:40 GMT -->
<head>

<!-- Meta Tags -->
<meta name="viewport" content="width=device-width,initial-scale=1.0"/>
<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
<title>Ahmednagar Cyclothon - Events</title>



<!-- Favicon and Touch Icons -->
<link href="<?php echo base_url();?>web_asset/images/favicon.png" rel="shortcut icon" type="image/png">
<link href="<?php echo base_url();?>web_asset/images/apple-touch-icon.png" rel="apple-touch-icon">
<link href="<?php echo base_url();?>web_asset/images/apple-touch-icon-72x72.png" rel="apple-touch-icon" sizes="72x72">
<link href="<?php echo base_url();?>web_asset/images/apple-touch-icon-114x114.png" rel="apple-touch-icon" sizes="114x114">
<link href="<?php echo base_url();?>web_asset/images/apple-touch-icon-144x144.png" rel="apple-touch-icon" sizes="144x144">

<!-- Stylesheet -->
<link href="<?php echo base_url();?>web_asset/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url();?>web_asset/css/jquery-ui.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url();?>web_asset/css/animate.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url();?>web_asset/css/css-plugin-collections.css" rel="stylesheet"/>
<!-- CSS | menuzord megamenu skins -->
<link id="menuzord-menu-skins" href="<?php echo base_url();?>web_asset/css/menuzord-skins/menuzord-boxed.css" rel="stylesheet"/>
<!-- CSS | Main style file -->
<link href="<?php echo base_url();?>web_asset/css/style-main.css" rel="stylesheet" type="text/css">
<!-- CSS | Theme Color -->
<link href="<?php echo base_url();?>web_asset/css/colors/theme-skin-lemon.css" rel="stylesheet" type="text/css">
<!-- CSS | Preloader Styles -->
<link href="<?php echo base_url();?>web_asset/css/preloader.css" rel="stylesheet" type="text/css">
<!-- CSS | Custom Margin Padding Collection -->
<link href="<?php echo base_url();?>web_asset/css/custom-bootstrap-margin-padding.css" rel="stylesheet" type="text/css">
<!-- CSS | Responsive media queries -->
<link href="<?php echo base_url();?>web_asset/css/responsive.css" rel="stylesheet" type="text/css">
<!-- CSS | Style css. This is the file where you can place your own custom css code. Just uncomment it and use it. -->
<!-- <link href="css/style.css" rel="stylesheet" type="text/css"> -->

<!-- external javascripts -->
<script src="<?php echo base_url();?>web_asset/js/jquery-2.2.4.min.js"></script>
<script src="<?php echo base_url();?>web_asset/js/jquery-ui.min.js"></script>
<script src="<?php echo base_url();?>web_asset/js/bootstrap.min.js"></script>
<!-- JS | jquery plugin collection for this theme -->
<script src="<?php echo base_url();?>web_asset/js/jquery-plugin-collection.js"></script>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body class="">
<div id="wrapper">
  <!-- preloader -->
  <div id="preloader">
    <div id="spinner">
      <img src="<?php echo base_url();?>web_asset/images/preloaders/1.gif" alt="">
    </div>
    <div id="disable-preloader" class="btn btn-default btn-sm">Disable Preloader</div>
  </div>
  
  <!-- Header -->
  <!-- Header -->
  <header id="header" class="header">
    <div class="header-top sm-text-center bg-theme-colored">
      <div class="container">
         <div class="row">
          <div class="col-md-4">
            <nav>
              <ul class="list-inline sm-text-center text-left flip mt-5">
                <li> <a class="text-white" href="<?php echo base_url();?>index.php/contactus">Contact Us</a> </li>
                <!--<li class="text-white">|</li>
                <li> <a class="text-white" href="<?php echo base_url();?>index.php/termsandconditions">Terms & Conditions</a> </li>-->
               
              </ul>
            </nav>
          </div>
          <div class="col-md-6">
            <div class="widget m-0 mt-5 no-border">
              <ul class="list-inline text-right sm-text-center">
                <li class="pl-10 pr-10 mb-0 pb-0">
                  <div class="header-widget text-white"><i class="fa fa-phone"></i>+91 8308054000 </div>
                </li>
                <li class="pl-10 pr-10 mb-0 pb-0">
                  <div class="header-widget text-white"><i class="fa fa-envelope-o"></i> support@nagarcycling.com</div>
                </li>
              </ul>
            </div>
          </div>
          <div class="col-md-2 text-right flip sm-text-center">
            <div class="widget m-0">
              <ul class="styled-icons icon-dark icon-circled icon-theme-colored icon-sm">
                <li class="mb-0 pb-0"><a href="https://www.facebook.com/nagarcycling/" target="_blank"><i class="fa fa-facebook"></i></a></li>
                
                <li class="mb-0 pb-0"><a href="https://www.instagram.com/nagarcycling/" target="_blank"><i class="fa fa-instagram"></i></a></li>
                
              </ul>
            </div>
          </div>
        </div>
     </div>
    </div>
    <div class="header-nav">
      <div class="header-nav-wrapper bg-light navbar-scrolltofixed">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div id="menuzord-right" class="menuzord orange no-bg"> <a class="menuzord-brand stylish-header pull-left flip" href="javascript:void(0)">
			  <img src="<?php echo base_url();?>web_asset/images/logo-wide-white.png" alt=""></a>
                <ul class="menuzord-menu">
                 <li><a href="#home">Home</a>
                 </li>
                  <li><a href="<?php echo base_url();?>index.php/aboutus">About Us</a></li>
                  <li class="active"><a href="<?php echo base_url();?>index.php/events">Events</a></li>
                  <li><a href="#">Sponsors</a>
				   <ul class="dropdown">
				    <li><a href="<?php echo base_url();?>index.php/sponsors2020">Sponsors 2020</a></li>
                    <li><a href="<?php echo base_url();?>index.php/sponsors2019">Sponsors 2019</a></li>
                    <li><a href="<?php echo base_url();?>index.php/sponsors2018">Sponsors 2018</a></li>
					</ul>
				  </li>
				  <li><a href="<?php echo base_url();?>index.php/photos">Free Photos</a></li>
                  <li><a href="<?php echo base_url();?>index.php/contactus">Contact Us</a></li>				  
				   <li><a href="<?php echo base_url();?>index.php/registration">Registration</a></li>
				   </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </header>
  <!-- Start main-content -->
  <div class="main-content">

    <!-- Section: inner-header -->
    <section class="inner-header divider parallax layer-overlay overlay-dark-5" data-bg-img="images/bg/bg1.jpg">
     <div class="container">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row"> 
            <div class="col-md-12">
              <h3 class="title text-white">Events</h3>
              <h4 class="text-theme-colored letter-space-3 font-weight-400 text-uppercase">Only he who can see the invisible can do the impossible. </h4>
              <ol class="breadcrumb text-white mt-10">
                <li><a class="text-white" href="<?php echo base_url();?>index.php/home">Home</a></li>
                <li><a class="text-white" href="#">Pages</a></li>
                <li class="active text-theme-colored">Events</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- Section: Services -->
    <section id="services" data-bg-img="<?php echo base_url();?>web_asset/images/pattern/pattern8.html">
      <div class="container">
        <div class="section-content">
          <div class="row">
          <!--  <div class="col-sm-12">

              <div class="class-item box-hover-effect effect1 mb-sm-30">

                <div class="thumb"> <a href="#"><img class="img-fullwidth mb-20" src="images/services/1.jpg" alt="..."></a> </div>

                <div class="caption"> <span class="text-uppercase letter-space-1 mb-10 font-12 text-gray-silver">ipsum fugit </span>

                  <h3 class="text-uppercase letter-space-1 mt-10">Riding on <span class="text-theme-colored">Trainer</span></h3>

                   <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, alias eos dolores unde aliquid quidem soluta ducimus quod numquam veniam obcaecati ratione, tempora quibusdam </p>

                  <p>aperiam voluptates id, in consectetur amet quas voluptatem, accusantium? In dignissimos eveniet voluptatem accusamus explicabo sapiente, similique minus? Dolor, vel minima.</p>

                  <p><a href="#" class="btn btn-theme-colored btn-flat mt-10 btn-sm" role="button">Read More</a></p>

                </div>

              </div>

            </div>

            <div class="col-sm-6 col-md-4">

              <div class="class-item box-hover-effect effect1 mb-sm-30">

                <div class="thumb"> <a href="#"><img class="img-fullwidth mb-20" src="images/services/2.jpg" alt="..."></a> </div>

                <div class="caption"> <span class="text-uppercase letter-space-1 mb-10 font-12 text-gray-silver">ipsum fugit </span>

                  <h3 class="text-uppercase letter-space-1 mt-10">Start your <span class="text-theme-colored">training</span></h3>

                   <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, alias eos dolores unde aliquid quidem soluta ducimus quod numquam veniam obcaecati ratione, tempora quibusdam </p>

                  <p>aperiam voluptates id, in consectetur amet quas voluptatem, accusantium? In dignissimos eveniet voluptatem accusamus explicabo sapiente, similique minus? Dolor, vel minima.</p>

                  <p><a href="#" class="btn btn-theme-colored btn-flat mt-10 btn-sm" role="button">Read More</a></p>

                </div>

              </div>

            </div>-->

            <!--<div class="col-sm-6 col-md-4">

              <div class="class-item box-hover-effect effect1 mb-sm-30">

                <div class="thumb"> <a href="#"><img class="img-fullwidth mb-20" src="images/services/3.jpg" alt="..."></a> </div>

                <div class="caption"> <span class="text-uppercase letter-space-1 mb-10 font-12 text-gray-silver">ipsum fugit </span>

                  <h3 class="text-uppercase letter-space-1 mt-10">competitive <span class="text-theme-colored">swimming</span></h3>

                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, alias eos dolores unde aliquid quidem soluta ducimus quod numquam veniam obcaecati ratione, tempora quibusdam </p>

                  <p>aperiam voluptates id, in consectetur amet quas voluptatem, accusantium? In dignissimos eveniet voluptatem accusamus explicabo sapiente, similique minus? Dolor, vel minima.</p>

                  <p><a href="#" class="btn btn-theme-colored btn-flat mt-10 btn-sm" role="button">Read More</a></p>

                </div>

              </div>

            </div>-->
          </div>
          <div class="row mt-30">
            <div class="col-sm-4">
              <div class="class-item box-hover-effect effect1 mb-sm-30">
                <div class="thumb"> <a href="#"><img class="img-fullwidth mb-20" src="<?php echo base_url();?>web_asset/images/services/4.jpg" alt="..."></a> </div>
                
                </div>
              </div>
            
            <div class="col-sm-8">
              <div class="class-item box-hover-effect effect1 mb-sm-30">
             
                <div class="caption"> <span class="text-uppercase letter-space-1 mb-10 font-12 text-gray-silver">ipsum fugit </span>
                  <h4 class="text-uppercase letter-space-1">CYCLOTHON<span class="text-theme-colored"> EVENT</span></h4>
                   <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, alias eos dolores unde aliquid quidem soluta ducimus quod numquam veniam obcaecati ratione, tempora quibusdam </p>
                  <p>aperiam voluptates id, in consectetur amet quas voluptatem, accusantium? In dignissimos eveniet voluptatem accusamus explicabo sapiente, similique minus? Dolor, vel minima.</p>
                  <p><a href="#" class="btn btn-theme-colored btn-flat mt-10 btn-sm" role="button">Read More</a></p>
                </div>
              </div>
            </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section id="achievements" class="divider parallax layer-overlay overlay-deep" data-bg-img="<?php echo base_url();?>web_asset/images/bg/bg2.jpg">
    
    </section>

    <!-- Section: Cources -->
    <section id="courses">
	  
      <div class="container">
	   <div class="section-title text-center">
          <div class="row">
            <div class="col-md-8 col-md-offset-2">
              <h2 class="title text-uppercase">Event <span class="text-black font-weight-300">Information</span></h2>
              <p class="text-uppercase letter-space-1">Join our Training Club and Rise to a New Challenge</p>
            </div>
          </div>
        </div>
        <div class="section-content">
          <div class="row">
            <div class="col-sm-6 col-md-4 mb-30">
              <div class="card effect__hover">
                <div class="card__front bg-theme-colored">
                  <div class="card__text">
                    <div class="icon-box mb-0 mt-0 p-0"> <img class="img-responsive img-fullwidth" src="<?php echo base_url();?>web_asset/images/achievements/1.jpg" alt="">
                      <h3 class="icon-box-title text-uppercase text-white letter-space-2">Event Date</h3>
                    </div>
                  </div>
                </div>
                <div class="card__back bg-black">
                  <div class="card__text">
                    <div class="display-table-parent p-30">
                      <div class="display-table">
                        <div class="display-table-cell">
                          <h4 class="text-uppercase text-white">Event Date</h4>
                          <div class="text-gray-lightgray">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Numquam laborum deserunt debitis fuga aliquid dolor ullam sed.</p>
                          </div>
                          <a href="#" class="btn btn-sm btn-flat btn-theme-colored mt-10"> Read More </a> </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-md-4 mb-30">
              <div class="card effect__hover">
                <div class="card__front bg-theme-colored">
                  <div class="card__text">
                    <div class="icon-box mb-0 mt-0 p-0"> <img class="img-responsive img-fullwidth" src="<?php echo base_url();?>web_asset/images/achievements/2.jpg" alt="">
                      <h3 class="icon-box-title text-uppercase text-white letter-space-2">Race Categories</h3>
                    </div>
                  </div>
                </div>
                <div class="card__back bg-black">
                  <div class="card__text">
                    <div class="display-table-parent p-30">
                      <div class="display-table">
                        <div class="display-table-cell">
                          <h4 class="text-uppercase text-white">Race Categories</h4>
                          <div class="text-gray-lightgray">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Numquam laborum deserunt debitis fuga aliquid dolor ullam sed.</p>
                          </div>
                          <a href="#" class="btn btn-sm btn-flat btn-theme-colored"> Read More </a> </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-md-4 mb-30">
              <div class="card effect__hover">
                <div class="card__front bg-theme-colored">
                  <div class="card__text">
                    <div class="icon-box mb-0 mt-0 p-0"> <img class="img-responsive img-fullwidth" src="<?php echo base_url();?>web_asset/images/achievements/3.jpg" alt="">
                      <h3 class="icon-box-title text-uppercase text-white letter-space-2">Event Timing</h3>
                    </div>
                  </div>
                </div>
                <div class="card__back bg-black">
                  <div class="card__text">
                    <div class="display-table-parent p-30">
                      <div class="display-table">
                        <div class="display-table-cell">
                          <h4 class="text-uppercase text-white">Event Timing</h4>
                          <div class="text-gray-lightgray">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Numquam laborum deserunt debitis fuga aliquid dolor ullam sed.</p>
                          </div>
                          <a href="#" class="btn btn-sm btn-flat btn-theme-colored mt-10"> Read More </a> </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    

  </div>
  <!-- end main-content -->
  