 
  <!-- Footer -->
<footer class="footer divider layer-overlay overlay-dark" data-bg-img="<?php echo base_url();?>web_asset/images/bg/bg6.jpg">
    <div class="container pt-100 pb-30">
      <div class="row mb-50">
        <div class="col-sm-4 col-md-4 mb-sm-60">
          <div class="contact-icon-box p-30">
            <div class="contact-icon bg-theme-colored"> <i class="fa fa-map-marker text-white font-22"></i> </div>
            <h4 class="text-uppercase text-white">Address</h4>
            <p class="font-16">IMA Bhavan- Near Railway overbridge, Kalyan Road - Ahmednagar</p>
          </div>
        </div>
        <div class="col-sm-4 col-md-4 mb-sm-60">
          <div class="contact-icon-box p-30">
            <div class="contact-icon bg-theme-colored"> <i class="fa fa-envelope-o text-white font-22"></i> </div>
            <h4 class="text-uppercase text-white">mail us</h4>
            <p class="font-16">Get support via <br>
              <a class="" href="mailto:mail@kodesolution.com" target="_top">email : support@nagarcycling.com </a> </p>
          </div>
        </div>
        <div class="col-sm-4 col-md-4">
          <div class="contact-icon-box p-30">
            <div class="contact-icon bg-theme-colored"> <i class="fa fa-phone text-white font-22"></i> </div>
            <h4 class="text-uppercase text-white">PHONE </h4>
            <p class="font-16">+91 8308054000 <br><br>
            </p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-6 col-md-3">
          <div class="widget dark"> <img class="mt-10 mb-20" alt="" src="<?php echo base_url();?>web_asset/images/logo-wide-white.png">
            <p class="font-12">
			Ahmednagar Cycling Club actively encourages cyclists of all abilities to meet their personal goals. Whether you cycle for fun, for the health benefits or for the competition, Ahmednagar Cycling Club welcomes you.
			</p>
            <ul class="styled-icons icon-bordered small square list-inline mt-10">
              <li><a href="https://www.facebook.com/nagarcycling/"><i class="fa fa-facebook text-white"></i></a></li>
             
              <li><a href="https://instagram.com/nagarcycling/" target="_blank"><i class="fa fa-instagram text-white"></i></a></li>
              
            </ul>
          </div>
        </div>
        <div class="col-sm-6 col-md-3">
          <div class="widget dark">
           Quick Links</h5>
            <ul class="list angle-double-right list-border">
              <li><a href="<?php echo base_url();?>index.php/aboutus">About Us</a></li>
              <li><a href="<?php echo base_url();?>index.php/events">Events</a></li>
              <li><a href="<?php echo base_url();?>index.php/photos">Photos</a></li>
			  <li><a href="<?php echo base_url();?>index.php/sponsors2020">Sponsors</a></li>
              <li><a href="<?php echo base_url();?>index.php/contactus">Contact Us</a></li>
              <li><a href="<?php echo base_url();?>index.php/termsandconditions">Terms & Conditions</a></li>
            
            </ul>
          </div>
        </div>
        <div class="col-sm-6 col-md-3">
          <div class="widget dark">
          
                       <h5 class="widget-title line-bottom">Contact info</h5>
                    
                      <ul class="mt-30">
                        <li><i class="fa fa-phone mb-20 text-theme-colored mr-5 font-20"></i>8308054000</li>
                        <li><i class="fa fa-map-marker mb-20 text-theme-colored mr-5 font-20"></i> IMA Bhavan- Near Railway overbridge, Kalyan Road.</li>
                        <li><i class="fa fa-envelope mb-20 text-theme-colored mr-5 font-20"></i> support@nagarcycling.com</li>
                        <li><i class="fa fa-globe mb-20 text-theme-colored mr-5 font-20 fa-spin"></i> www.nagarcycling.com</li>
                        
                      </ul>
                    
          </div>
        </div>
        <div class="col-sm-6 col-md-3">
          <div class="widget dark">
            <h5 class="widget-title line-bottom">Follow Us</h5>
            <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FAhmednagar-Cycling-Club-1900441513602454%2F&amp;tabs=timeline&amp;width=300&amp;height=300&amp;small_header=true&amp;adapt_container_width=true&amp;hide_cover=false&amp;show_facepile=false&amp;appId=1330903350339636" width="300" height="300" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowtransparency="true"></iframe>
          </div>
        </div>
      </div>
    </div>
    <div class="container-fluid copy-right p-20">
      <div class="row text-center">
        <div class="col-md-12">
          <p class="font-11 text-white m-0">Copyright &copy;2019 Ahmednagar Nagarcycling Club. All Rights Reserved. Developed by OneVoice Transmedia Pvt. Ltd.</p>
        </div>
      </div>
    </div>
  </footer>
 <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a> </div>
<!-- end wrapper --> 

<!-- Footer Scripts --> 
<!-- JS | Calendar Event Data --> 
<script src="<?php echo base_url();?>web_asset/js/calendar-events-data.js"></script> 
<!-- JS | Custom script for all pages --> 
<script src="<?php echo base_url();?>web_asset/js/custom.js"></script>

</body>

<!-- Mirrored from html.kodesolution.live/html/health-beauty/sports-trainer/v2.0/demo/page-about1.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 27 Jul 2019 07:30:40 GMT -->
</html>