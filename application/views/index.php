<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>

<!-- Meta Tags -->
<meta name="viewport" content="width=device-width,initial-scale=1.0"/>
<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>

<!-- Page Title -->
<title>Ahmednagar Cyclothon - 2019</title>

<!-- Favicon and Touch Icons -->
<link href="<?php echo base_url();?>web_asset/images/favicon.png" rel="shortcut icon" type="image/png">
<link href="<?php echo base_url();?>web_asset/images/apple-touch-icon.png" rel="apple-touch-icon">
<link href="<?php echo base_url();?>web_asset/images/apple-touch-icon-72x72.png" rel="apple-touch-icon" sizes="72x72">
<link href="<?php echo base_url();?>web_asset/images/apple-touch-icon-114x114.png" rel="apple-touch-icon" sizes="114x114">
<link href="<?php echo base_url();?>web_asset/images/apple-touch-icon-144x144.png" rel="apple-touch-icon" sizes="144x144">

<!-- Stylesheet -->
<link href="<?php echo base_url();?>web_asset/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url();?>web_asset/css/jquery-ui.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url();?>web_asset/css/animate.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url();?>web_asset/css/css-plugin-collections.css" rel="stylesheet"/>
<!-- CSS | menuzord megamenu skins -->
<link id="menuzord-menu-skins" href="<?php echo base_url();?>web_asset/css/menuzord-skins/menuzord-rounded-boxed.css" rel="stylesheet"/>
<!-- CSS | Main style file -->
<link href="<?php echo base_url();?>web_asset/css/style-main.css" rel="stylesheet" type="text/css">
<!-- CSS | Theme Color -->
<link href="<?php echo base_url();?>web_asset/css/colors/theme-skin-lemon.css" rel="stylesheet" type="text/css">
<!-- CSS | Preloader Styles -->
<link href="<?php echo base_url();?>web_asset/css/preloader.css" rel="stylesheet" type="text/css">
<!-- CSS | Custom Margin Padding Collection -->
<link href="<?php echo base_url();?>web_asset/css/custom-bootstrap-margin-padding.css" rel="stylesheet" type="text/css">
<!-- CSS | Main style file -->
<link href="<?php echo base_url();?>web_asset/css/style-main.css" rel="stylesheet" type="text/css">
<!-- CSS | Theme Color -->
<link href="<?php echo base_url();?>web_asset/css/colors/theme-skin-lemon.css" rel="stylesheet" type="text/css">
<!-- CSS | Preloader Styles -->
<link href="<?php echo base_url();?>web_asset/css/preloader.css" rel="stylesheet" type="text/css">
<!-- CSS | Custom Margin Padding Collection -->
<link href="<?php echo base_url();?>web_asset/css/custom-bootstrap-margin-padding.css" rel="stylesheet" type="text/css">
<!-- CSS | Responsive media queries -->
<link href="<?php echo base_url();?>web_asset/css/responsive.css" rel="stylesheet" type="text/css">
<!-- CSS | Style css. This is the file where you can place your own custom css code. Just uncomment it and use it. -->
<!-- <link href="css/style.css" rel="stylesheet" type="text/css"> -->

<!-- Revolution Slider 5.x CSS settings -->
<link  href="<?php echo base_url();?>web_asset/js/revolution-slider/css/settings.css" rel="stylesheet" type="text/css"/>
<link  href="<?php echo base_url();?>web_asset/js/revolution-slider/css/layers.css" rel="stylesheet" type="text/css"/>
<link  href="<?php echo base_url();?>web_asset/js/revolution-slider/css/navigation.css" rel="stylesheet" type="text/css"/>

<!-- external javascripts -->
<script src="<?php echo base_url();?>web_asset/js/jquery-2.2.4.min.js"></script>
<script src="<?php echo base_url();?>web_asset/js/jquery-ui.min.js"></script>
<script src="<?php echo base_url();?>web_asset/js/bootstrap.min.js"></script>
<!-- JS | jquery plugin collection for this theme -->
<script src="<?php echo base_url();?>web_asset/js/jquery-plugin-collection.js"></script>

<!-- Revolution Slider 5.x SCRIPTS -->
<script src="<?php echo base_url();?>web_asset/js/revolution-slider/js/jquery.themepunch.tools.min.js"></script>
<script src="<?php echo base_url();?>web_asset/js/revolution-slider/js/jquery.themepunch.revolution.min.js"></script>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body class="">
<div id="wrapper"> 
  <!-- preloader -->
  <div id="preloader">
    <div id="spinner">
      <img src="<?php echo base_url();?>web_asset/images/preloaders/1.gif" alt="">
    </div>
    <div id="disable-preloader" class="btn btn-default btn-sm">Disable Preloader</div>
  </div>
  
  <!-- Header -->
  <header id="header" class="header">
    <div class="header-top sm-text-center bg-theme-colored">
      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <nav>
              <ul class="list-inline sm-text-center text-left flip mt-5">
                <li> <a class="text-white" href="<?php echo base_url();?>index.php/contactus">Contact Us</a> </li>
                <!--<li class="text-white">|</li>
                <li> <a class="text-white" href="<?php echo base_url();?>index.php/termsandconditions">Terms & Conditions</a> </li>-->
               
              </ul>
            </nav>
          </div>
          <div class="col-md-6">
            <div class="widget m-0 mt-5 no-border">
              <ul class="list-inline text-right sm-text-center">
                <li class="pl-10 pr-10 mb-0 pb-0">
                  <div class="header-widget text-white"><i class="fa fa-phone"></i>+91 8308054000 </div>
                </li>
                <li class="pl-10 pr-10 mb-0 pb-0">
                  <div class="header-widget text-white"><i class="fa fa-envelope-o"></i> support@nagarcycling.com</div>
                </li>
              </ul>
            </div>
          </div>
          <div class="col-md-2 text-right flip sm-text-center">
            <div class="widget m-0">
              <ul class="styled-icons icon-dark icon-circled icon-theme-colored icon-sm">
                <li class="mb-0 pb-0"><a href="https://www.facebook.com/nagarcycling/" target="_blank"><i class="fa fa-facebook"></i></a></li>
                
                <li class="mb-0 pb-0"><a href="https://www.instagram.com/nagarcycling/" target="_blank"><i class="fa fa-instagram"></i></a></li>
                
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="header-nav">
      <div class="header-nav-wrapper bg-light navbar-scrolltofixed">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div id="menuzord-right" class="menuzord orange no-bg"> <a class="menuzord-brand stylish-header pull-left flip" href="javascript:void(0)">
			  <img src="<?php echo base_url();?>web_asset/images/logo-wide-white.png" alt=""></a>
                <ul class="menuzord-menu">
                 <li><a href="<?php echo base_url();?>index.php/home">Home</a>
                 </li>
                  <li><a href="<?php echo base_url();?>index.php/aboutus">About Us</a></li>
                  <li class="active"><a href="<?php echo base_url();?>index.php/events">Events</a></li>
                  <li><a href="#">Sponsors</a>
				   <ul class="dropdown">
				    <li><a href="<?php echo base_url();?>index.php/sponsors2020">Sponsors 2020</a></li>
                    <li><a href="<?php echo base_url();?>index.php/sponsors2019">Sponsors 2019</a></li>
                    <li><a href="<?php echo base_url();?>index.php/sponsors2018">Sponsors 2018</a></li>
					</ul>
				  </li>
				  <li><a href="<?php echo base_url();?>index.php/photos">Free Photos</a></li>
                  <li><a href="<?php echo base_url();?>index.php/contactus">Contact Us</a></li>				  
				   <li><a href="<?php echo base_url();?>index.php/registration">Registration</a></li>
				   </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </header>
  
  <!-- Start main-content -->
  <div class="main-content">
    <!-- Section: home -->
    <section id="home" class="divider fullscreen no-bg layer-overlay overlay-dark-light" data-bg-img="<?php echo base_url();?>web_asset/images/bg/bg16.html">
      <div class="bg-video">
        <div id="home-video" class="video">
          <div class="player video-container" data-property="{videoURL:'0-lRjrHrVlo',containment:'#home-video',autoPlay:true, showControls:false, mute:false, startAt:0, opacity:1}"></div>
        </div>
      </div>
      <div class="display-table">
        <div class="display-table-cell">
          <div class="container pt-200 pb-200">
            <div class="row">
              <div class="col-md-12 text-center">
                <i class="flaticon-salon-scissors-and-comb-2 font-64 text-white m-0 line-height-1"></i>
                <h2 class="font-playfair font-48 font-weight-700 text-white m-0">Dont Limit your challenges,</h2>
                <h2 class="font-playfair font-48 font-weight-700 text-white m-0"><span style="color:red;">challenge</span> your limits.</h2>
                <!--<a class="btn btn-default btn-transparent btn-circled pl-30 pr-30 mt-15" href="#">View Details</a>-->
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    
    <!-- Section: About -->
    <section id="about">
      <div class="container">
        <div class="section-title text-center">
          <div class="row">
            <div class="col-md-8 col-md-offset-2">
              <h2 class="title text-uppercase">Welcome to <span class="text-black font-weight-300">Nagarcycling Club</span></h2>
              <h6 class="letter-space-8 font-weight-400 text-uppercase">Lorem ipsum dolor sit amet</h6>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Saepe perferendis eveniet odio, ea. Architecto, nulla quis laboriosam voluptatibus dolorum consequuntur voluptate minima veritatis debitis sed molestias ullam pariatur similique vel dignissimos odit, natus ipsam nam temporibus, laudantium enim, sunt dolores!</p>
            </div>
          </div>
        </div>
        <div class="section-content">
          <div class="row mt-50">
            <div class="col-sm-4">
              <div class="box-hover-effect effect1 mb-sm-30">
                <div class="thumb"> <a href="#"><img class="img-fullwidth mb-20" src="<?php echo base_url();?>web_asset/images/home_about/about1.jpg" alt="..."></a> </div>
                <div class="caption"> <span class="text-uppercase letter-space-1 mb-10 font-12 text-gray-silver">About </span>
                  <h3 class="text-uppercase letter-space-1 mt-0">Ahmednagar<span class="text-theme-colored"> Cyclothon</span></h3>
                   <p>Quam distinctio quis perspiciatis facere accusamus perferendis eligendi odit cum nemo fugit, tenetur sequi expedita earum nobis optio tempora id repellendus? Tempora illo ad, magni iusto quas, rerum debitis id et nisi eius quod dolor eos repudiandae perferendis, consequuntur illum alias. Molestiae provident voluptate corporis quae numquam eos earum porro quia officia, maiores magni accusantium iusto similique tempora.</p>
                  <p><a href="#" class="btn btn-theme-colored btn-flat mt-10 btn-sm" role="button">Read More</a></p>
                </div>
              </div>
            </div>
            <div class="col-sm-4">
              <div class="box-hover-effect effect1 mb-sm-30">
                <div class="thumb"> <a href="#"><img class="img-fullwidth mb-20" src="<?php echo base_url();?>web_asset/images/home_about/about2.jpg" alt="..."></a> </div>
                <div class="caption"> <span class="text-uppercase letter-space-1 mb-10 font-12 text-gray-silver">ipsum fugit </span>
                  <h3 class="text-uppercase letter-space-1 mt-0">Why <span class="text-theme-colored">Cycle</span></h3>
                   <p>Quam distinctio quis perspiciatis facere accusamus perferendis eligendi odit cum nemo fugit, tenetur sequi expedita earum nobis optio tempora id repellendus? Tempora illo ad, magni iusto quas, rerum debitis id et nisi eius quod dolor eos repudiandae perferendis, consequuntur illum alias. Molestiae provident voluptate corporis quae numquam eos earum porro quia officia, maiores magni accusantium iusto similique tempora.</p>
                  <p><a href="#" class="btn btn-theme-colored btn-flat mt-10 btn-sm" role="button">Read More</a></p>
                </div>
              </div>
            </div>
            <div class="col-sm-4">
              <h3 class="text-uppercase mt-0 letter-space-2">Recent Events</h3>
              <div class="bxslider" data-count="3">
                <div class="box-hover-effect effect1 mb-15">
                  <div class="thumb"> <a href="#"><img class="img-fullwidth mb-0" src="<?php echo base_url();?>web_asset/images/home_about/world_bicycleday_2_2019.jpg" alt="..."></a> </div>
                    <h5 class="text-uppercase letter-space-1 mb-0">World Bicycle Day 2019</h5>
                  <!--  <ul>
                      <li class="text-gray-silver font-12"><i class="fa fa-clock-o"></i> at 5.00 pm - 7.30 pm <i class="fa fa-map-marker ml-10"></i> 25 Newyork City.</li>
                    </ul>-->
                </div>
                <div class="box-hover-effect effect1 mb-15">
                  <div class="thumb"> <a href="#"><img class="img-fullwidth mb-0" src="<?php echo base_url();?>web_asset/images/home_about/world_bicycleday2019.jpg" alt="..."></a> </div>
                    <h5 class="text-uppercase letter-space-1 mb-0">World Bicycle Day 2019</h5>
                   <!--  <ul>
                      <li class="text-gray-silver font-12"><i class="fa fa-clock-o"></i> at 5.00 pm - 7.30 pm <i class="fa fa-map-marker ml-10"></i> 25 Newyork City.</li>
                    </ul>-->
                </div>
                <div class="box-hover-effect effect1 mb-15">
                    <div class="thumb"> <a href="#"><img class="img-fullwidth mb-0" src="<?php echo base_url();?>web_asset/images/home_about/expo2019.jpg" alt="..."></a> </div>
                    <h5 class="text-uppercase letter-space-1 mb-0">Cyclothon Expo 2019</h5>
                   <!--  <ul>
                      <li class="text-gray-silver font-12"><i class="fa fa-clock-o"></i> at 5.00 pm - 7.30 pm <i class="fa fa-map-marker ml-10"></i> 25 Newyork City.</li>
                    </ul>-->
                </div>
                <div class="box-hover-effect effect1 mb-15">
                    <div class="thumb"> <a href="#"><img class="img-fullwidth mb-0" src="<?php echo base_url();?>web_asset/images/events/3.jpg" alt="..."></a> </div>
                    <h5 class="text-uppercase letter-space-1 mb-0">adipisicing elit</h5>
                    <ul>
                      <li class="text-gray-silver font-12"><i class="fa fa-clock-o"></i> at 5.00 pm - 7.30 pm <i class="fa fa-map-marker ml-10"></i> 25 Newyork City.</li>
                    </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- Section: Clients -->
    <section class="divider parallax layer-overlay overlay-dark"  data-bg-img="<?php echo base_url();?>web_asset/images/bg/bg7.jpg" >
      <div class="container">
        <div class="section-content">
          <div class="row">
            <div class="clients-logo carousel owl-carousel-6col">
              <div class="item"> <a href="#"><img src="<?php echo base_url();?>web_asset/images/nagarcycling_sponsors/apace.png" alt=""></a> </div>
              <div class="item"> <a href="#"><img src="<?php echo base_url();?>web_asset/images/nagarcycling_sponsors/bhagirathi.png" alt=""></a> </div>
              <div class="item"> <a href="#"><img src="<?php echo base_url();?>web_asset/images/nagarcycling_sponsors/bigFM.png" alt=""></a> </div>
              <div class="item"> <a href="#"><img src="<?php echo base_url();?>web_asset/images/nagarcycling_sponsors/bmw.png" alt=""></a> </div>
              <div class="item"> <a href="#"><img src="<?php echo base_url();?>web_asset/images/nagarcycling_sponsors/castle.png" alt=""></a> </div>
              <div class="item"> <a href="#"><img src="<?php echo base_url();?>web_asset/images/nagarcycling_sponsors/changediya.png" alt=""></a> </div>
              <div class="item"> <a href="#"><img src="<?php echo base_url();?>web_asset/images/nagarcycling_sponsors/cycling_association.png" alt=""></a> </div>
              <div class="item"> <a href="#"><img src="<?php echo base_url();?>web_asset/images/nagarcycling_sponsors/enerzal.png" alt=""></a> </div>
			  
			  
			  <div class="item"> <a href="#"><img src="<?php echo base_url();?>web_asset/images/nagarcycling_sponsors/golds_gym.png" alt=""></a> </div>
			  <div class="item"> <a href="#"><img src="<?php echo base_url();?>web_asset/images/nagarcycling_sponsors/kalpattaru.png" alt=""></a> </div>
			  <div class="item"> <a href="#"><img src="<?php echo base_url();?>web_asset/images/nagarcycling_sponsors/lt.png" alt=""></a> </div>
			  <div class="item"> <a href="#"><img src="<?php echo base_url();?>web_asset/images/nagarcycling_sponsors/marathacc.png" alt=""></a> </div>
			  <div class="item"> <a href="#"><img src="<?php echo base_url();?>web_asset/images/nagarcycling_sponsors/medical_assoc.png" alt=""></a> </div>
			  <div class="item"> <a href="#"><img src="<?php echo base_url();?>web_asset/images/nagarcycling_sponsors/ontherun.png" alt=""></a> </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    
    <!-- Section: Pricing Table -->
    <!--<section id="pricing">
      <div class="container pb-40">
        <div class="section-title">
          <div class="row">
            <div class="col-md-8 col-md-offset-2 text-center">
              <h2 class="text-uppercase title">Membership <span class="text-black font-weight-300"> Packages</span></h2>
              <p class="text-uppercase letter-space-1">Join our Training Club and Rise to a New Challenge</p>
            </div>
          </div>
        </div>
        <div class="section-content">
          <div class="row">
            <div class="col-xs-12 col-md-4 col-lg-4 hvr-float-shadow mb-sm-30">
              <div class="pricing-table text-center pt-10 pb-0 mt-sm-0 maxwidth400 border-1px border-theme-colored">
                <div class="icon-box text-center pt-20 p-0 mb-0"> <a class="icon icon-circled icon-border-effect border-1px effect-circle icon-xl" href="#"> <i class="flaticon-sports-gym-4 text-theme-colored"></i> </a> </div>
                <h3 class="package-type text-uppercase letter-space-2">Beginner</h3>
                <div class="price-amount">
                  <div class="font-weight-700 text-theme-colored">35<sup>$</sup> <span class="font-14">monthly</span></div>
                </div>
                <ul class="table-list mt-0 no-bg">
                  <li>Free Consultation</li>
                  <li>Fitness Assessment</li>
                  <li>24 Hour Gym</li>
                  <li>Nutrional Plan: No</li>
                </ul>
                <a class="btn btn-lg btn-theme-colored text-uppercase btn-block pt-20 pb-20 btn-flat" href="#">Signup</a>
              </div>
            </div>
            <div class="col-xs-12 col-md-4 col-lg-4 hvr-float-shadow mb-sm-30">
              <div class="pricing-table text-center pt-10 pb-0 mt-sm-0 maxwidth400 border-1px border-theme-colored">
                <div class="icon-box text-center pt-20 p-0 mb-0"> <a class="icon icon-circled icon-border-effect border-1px effect-circle icon-xl" href="#"> <i class="flaticon-sports-game-1 text-theme-colored"></i> </a> </div>
                <h3 class="package-type text-uppercase letter-space-2">Advanced</h3>
                <div class="price-amount">
                  <div class="font-weight-700 text-theme-colored">75<sup>$</sup> <span class="font-14">monthly</span></div>
                </div>
                <ul class="table-list mt-0 no-bg">
                  <li>Free Consultation</li>
                  <li>Fitness Assessment</li>
                  <li>24 Hour Gym</li>
                  <li>Nutrional Plan: No</li>
                </ul>
                <a class="btn btn-lg btn-theme-colored text-uppercase btn-block pt-20 pb-20 btn-flat" href="#">Signup</a>
              </div>
            </div>
            <div class="col-xs-12 col-md-4 col-lg-4 hvr-float-shadow mb-sm-30">
              <div class="pricing-table text-center pt-10 pb-0 mt-sm-0 maxwidth400 border-1px border-theme-colored">
                <div class="icon-box text-center pt-20 p-0 mb-0"> <a class="icon icon-circled icon-border-effect border-1px effect-circle icon-xl" href="#"> <i class="flaticon-sports-cup-1 text-theme-colored"></i> </a> </div>
                <h3 class="package-type text-uppercase letter-space-2">professional</h3>
                <div class="price-amount">
                  <div class="font-weight-700 text-theme-colored">125<sup>$</sup> <span class="font-14">monthly</span></div>
                </div>
                <ul class="table-list mt-0 no-bg">
                  <li>Free Consultation</li>
                  <li>Fitness Assessment</li>
                  <li>24 Hour Gym</li>
                  <li>Nutrional Plan: No</li>
                </ul>
                <a class="btn btn-lg btn-theme-colored text-uppercase btn-block pt-20 pb-20 btn-flat" href="#">Signup</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>-->
    
    <!-- Section: Team -->
    <!--<section id="trainer" class="bg-light">
      <div class="container pb-70">
      	<div class="section-title text-center">
          <div class="row">
            <div class="col-md-8 col-md-offset-2">
              <h2 class="title text-uppercase">Our Sports<span class="text-black font-weight-300"> Trainers</span></h2>
              <p class="text-uppercase letter-space-1">Join our Training Club and Rise to a New Challenge</p>
            </div>
          </div>
        </div>
        <div class="section-content">
          <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-4">
              <div class="bg-img-box maxwidth400">
                <div class="photo">
                  <img class="img-fullwidth" src="images/team/1.jpg" alt="">
                </div>
                <div class="style3 bg-light border-right-5px border-theme-colored">
                  <h5 class="text-gray mt-0 mb-0">GYM Expart</h5>
                  <h3 class="text-theme-colored mt-0 mb-5">John Smith</h3>
                  <p class="mt-0">2010 Best Trainer Award</p>
                </div>
              </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
              <div class="bg-img-box maxwidth400">
                <div class="photo">
                  <img class="img-fullwidth" src="images/team/2.jpg" alt="">
                </div>
                <div class="style3 bg-light border-right-5px border-theme-colored">
                  <h5 class="text-gray mt-0 mb-0">Race Expart</h5>
                  <h3 class="text-theme-colored mt-0 mb-5">John Smith</h3>
                  <p class="mt-0">2011 Uefa Championship league</p>
                </div>
              </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
              <div class="bg-img-box maxwidth400">
                <div class="photo">
                  <img class="img-fullwidth" src="images/team/3.jpg" alt="">
                </div>
                <div class="style3 bg-light border-right-5px border-theme-colored">
                  <h5 class="text-gray mt-0 mb-0">Boxing Expart</h5>
                  <h3 class="text-theme-colored mt-0 mb-5">John Smith</h3>
                  <p class="mt-0">2010 Best Trainer Award</p>
                </div>
              </div>
            </div>
          </div>
          <div class="row mt-30">
            <div class="col-xs-12 col-sm-6 col-md-4">
              <div class="bg-img-box maxwidth400">
                <div class="photo">
                  <img class="img-fullwidth" src="images/team/4.jpg" alt="">
                </div>
                <div class="style3 bg-light border-right-5px border-theme-colored">
                  <h5 class="text-gray mt-0 mb-0">GYM Expart</h5>
                  <h3 class="text-theme-colored mt-0 mb-5">John Smith</h3>
                  <p class="mt-0">2010 Best Trainer Award</p>
                </div>
              </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
              <div class="bg-img-box maxwidth400">
                <div class="photo">
                  <img class="img-fullwidth" src="images/team/5.jpg" alt="">
                </div>
                <div class="style3 bg-light border-right-5px border-theme-colored">
                  <h5 class="text-gray mt-0 mb-0">Race Expart</h5>
                  <h3 class="text-theme-colored mt-0 mb-5">John Smith</h3>
                  <p class="mt-0">2011 Uefa Championship league</p>
                </div>
              </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
              <div class="bg-img-box maxwidth400">
                <div class="photo">
                  <img class="img-fullwidth" src="images/team/6.jpg" alt="">
                </div>
                <div class="style3 bg-light border-right-5px border-theme-colored">
                  <h5 class="text-gray mt-0 mb-0">Boxing Expart</h5>
                  <h3 class="text-theme-colored mt-0 mb-5">John Smith</h3>
                  <p class="mt-0">2010 Best Trainer Award</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>-->

    <!--<section id="achievements" class="divider parallax layer-overlay overlay-deep" data-bg-img="images/bg/bg2.jpg">
      <div class="container pt-60 pb-60">
        <div class="section-title text-center">
          <div class="row">
            <div class="col-md-8 col-md-offset-2">
              <h2 class="title text-uppercase">Our <span class="text-black font-weight-300">Achievements</span></h2>
              <p class="text-uppercase letter-space-1">Join our Training Club and Rise to a New Challenge</p>
            </div>
          </div>
        </div>
        <div class="section-content">
          <div class="row">
            <div class="col-md-12">
              <div class="owl-carousel-4col text-center" data-nav="true">
                <div class="item">
                  <a href="#"> <img style="width: 55%; margin: auto;" src="images/achievements/1.png" alt=""></a>
                  <h5 class="letter-space-2 text-uppercase text-theme-colored">2010 Best Trainer Award</h5>
                </div>
                <div class="item">
                  <a href="#"> <img style="width: 55%; margin: auto;" src="images/achievements/2.png" alt=""></a>
                  <h5 class="letter-space-2 text-uppercase text-theme-colored">2010 Best Trainer Award</h5>
                </div>
                <div class="item">
                  <a href="#"> <img style="width: 55%; margin: auto;" src="images/achievements/3.png" alt=""></a>
                  <h5 class="letter-space-2 text-uppercase text-theme-colored">2010 Best Trainer Award</h5>
                </div>
                <div class="item">
                  <a href="#"> <img style="width: 55%; margin: auto;" src="images/achievements/4.png" alt=""></a>
                  <h5 class="letter-space-2 text-uppercase text-theme-colored">2010 Best Trainer Award</h5>
                </div>
                <div class="item">
                  <a href="#"> <img style="width: 55%; margin: auto;" src="images/achievements/5.png" alt=""></a>
                  <h5 class="letter-space-2 text-uppercase text-theme-colored">2010 Best Trainer Award</h5>
                </div>
              </div> 
            </div>
          </div>
        </div>
      </div>
    </section>-->

    <!-- Section: Gallery & -->
    <section id="gallery" class="bg-lighter">
      <div class="container-fluid pt-70 pb-0">
        <div class="section-title text-center">
          <div class="row">
            <div class="col-md-8 col-md-offset-2">
              <h2 class="text-uppercase title" >Our <span class="text-black font-weight-300">Gallery</span></h2>
              <p class="text-uppercase letter-space-4">Join our Nagarcycling club and be healthy.</p>
            </div>
          </div>
        </div>
        <div class="section-content">
          <div class="row">
            <div class="col-md-12">
              <!-- Portfolio Gallery Grid -->
              <div id="grid" class="gallery-isotope grid-4 gutter clearfix">
                <!-- Portfolio Item Start -->
                <div class="gallery-item photography">
                  <div class="thumb">
                    <img class="img-fullwidth" src="<?php echo base_url();?>web_asset/images/gallery/1.jpg" alt="project">
                    <div class="overlay-shade"></div>
                    <div class="text-holder text-center">
                      <h5 class="title">Hair Style Title</h5>
                    </div>
                    <div class="icons-holder">
                      <div class="icons-holder-inner">
                        <div class="styled-icons icon-sm icon-dark icon-circled icon-theme-colored">
                          <a data-lightbox="image" href="<?php echo base_url();?>web_asset/images/gallery/1.jpg"><i class="fa fa-plus"></i></a>
                         <!-- <a href="#"><i class="fa fa-link"></i></a>-->
                        </div>
                      </div>
                    </div>
                    <a class="hover-link" data-lightbox="image" href="<?php echo base_url();?>web_asset/images/gallery/1.jpg">View more</a>
                  </div>
                </div>
                <!-- Portfolio Item End -->
                
                <!-- Portfolio Item Start -->
                <div class="gallery-item branding">
                  <div class="thumb">
                    <img class="img-fullwidth" src="<?php echo base_url();?>web_asset/images/gallery/2.jpg" alt="project">
                    <div class="overlay-shade"></div>
                    <div class="text-holder text-center">
                      <h5 class="title">Hair Style Title</h5>
                    </div>
                    <div class="icons-holder">
                      <div class="icons-holder-inner">
                        <div class="styled-icons icon-sm icon-dark icon-circled icon-theme-colored">
                          <a data-lightbox="image" href="<?php echo base_url();?>web_asset/images/gallery/2.jpg"><i class="fa fa-plus"></i></a>
                          <a href="#"><i class="fa fa-heart-o"></i></a>
                        </div>
                      </div>
                    </div>
                    <a class="hover-link" data-lightbox="image" href="<?php echo base_url();?>web_asset/images/gallery/2.jpg">View more</a>
                  </div>
                </div>
                <!-- Portfolio Item End -->

                <!-- Portfolio Item Start -->
                <div class="gallery-item design">
                  <div class="thumb">
                    <img class="img-fullwidth" src="<?php echo base_url();?>web_asset/images/gallery/3.jpg" alt="project">
                    <div class="overlay-shade"></div>
                    <div class="text-holder text-center">
                      <h5 class="title">Hair Style Title</h5>
                    </div>
                    <div class="icons-holder">
                      <div class="icons-holder-inner">
                        <div class="styled-icons icon-sm icon-dark icon-circled icon-theme-colored">
                          <a data-lightbox="image" href="<?php echo base_url();?>web_asset/images/gallery/3.jpg"><i class="fa fa-plus"></i></a>
                        </div>
                      </div>
                    </div>
                    <a class="hover-link" data-lightbox="image" href="<?php echo base_url();?>web_asset/images/gallery/3.jpg">View more</a>
                  </div>
                </div>
                <!-- Portfolio Item End -->

                <!-- Portfolio Item Start -->
                <div class="gallery-item photography">
                  <div class="thumb">
                    <img class="img-fullwidth" src="<?php echo base_url();?>web_asset/images/gallery/4.jpg" alt="project">
                    <div class="overlay-shade"></div>
                    <div class="text-holder text-center">
                      <h5 class="title">Hair Style Title</h5>
                    </div>
                    <div class="icons-holder">
                      <div class="icons-holder-inner">
                        <div class="styled-icons icon-sm icon-dark icon-circled icon-theme-colored">
                         <a data-lightbox="image" href="<?php echo base_url();?>web_asset/images/gallery/4.jpg"><i class="fa fa-plus"></i></a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- Portfolio Item End -->

                <!-- Portfolio Item Start -->
                <div class="gallery-item branding">
                  <div class="thumb">
                    <img class="img-fullwidth" src="<?php echo base_url();?>web_asset/images/gallery/5.jpg" alt="project">
                    <div class="overlay-shade"></div>
                    <div class="text-holder text-center">
                      <h5 class="title">Hair Style Title</h5>
                    </div>
                    <div class="icons-holder">
                      <div class="icons-holder-inner">
                        <div class="styled-icons icon-sm icon-dark icon-circled icon-theme-colored">
                          <a data-lightbox="image" href="<?php echo base_url();?>web_asset/images/gallery/5.jpg"><i class="fa fa-plus"></i></a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- Portfolio Item End -->

                <!-- Portfolio Item Start -->
                <div class="gallery-item design">
                  <div class="thumb">
                    <div class="flexslider-wrapper">
                      <div class="flexslider">
                        <ul class="slides">
                          <li><a href="<?php echo base_url();?>web_asset/images/gallery/1.jpg" title="Portfolio Gallery - Photo 1"><img src="<?php echo base_url();?>web_asset/images/gallery/1.jpg" alt=""></a></li>
                          <li><a href="<?php echo base_url();?>web_asset/images/gallery/2.jpg" title="Portfolio Gallery - Photo 2"><img src="<?php echo base_url();?>web_asset/images/gallery/2.jpg" alt=""></a></li>
                          <li><a href="<?php echo base_url();?>web_asset/images/gallery/3.jpg" title="Portfolio Gallery - Photo 3"><img src="<?php echo base_url();?>web_asset/images/gallery/3.jpg" alt=""></a></li>
                        </ul>
                      </div>
                    </div>
                    <div class="overlay-shade"></div>
                    <div class="text-holder text-center">
                      <h5 class="title">Hair Style Title</h5>
                    </div>
                    <div class="icons-holder">
                      <div class="icons-holder-inner">
                        <div class="styled-icons icon-sm icon-dark icon-circled icon-theme-colored">
                          <a href="#"><i class="fa fa-picture-o"></i></a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- Portfolio Item End -->

                <!-- Portfolio Item Start -->
                <div class="gallery-item branding">
                  <div class="thumb">
                    <img class="img-fullwidth" src="<?php echo base_url();?>web_asset/images/gallery/7.jpg" alt="project">
                    <div class="overlay-shade"></div>
                    <div class="text-holder text-center">
                      <h5 class="title">Hair Style Title</h5>
                    </div>
                    <div class="icons-holder">
                      <div class="icons-holder-inner">
                        <div class="styled-icons icon-sm icon-dark icon-circled icon-theme-colored">
                          <a data-lightbox="image" href="<?php echo base_url();?>web_asset/images/gallery/7.jpg"><i class="fa fa-plus"></i></a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- Portfolio Item End --> 

                <!-- Portfolio Item Start -->
                <div class="gallery-item branding">
                  <div class="thumb">
                    <img class="img-fullwidth" src="<?php echo base_url();?>web_asset/images/gallery/8.jpg" alt="project">
                    <div class="overlay-shade"></div>
                    <div class="text-holder text-center">
                      <h5 class="title">Hair Style Title</h5>
                    </div>
                    <div class="icons-holder">
                      <div class="icons-holder-inner">
                        <div class="styled-icons icon-sm icon-dark icon-circled icon-theme-colored">
                          <a data-lightbox="image" href="<?php echo base_url();?>web_asset/images/gallery/2.jpg"><i class="fa fa-plus"></i></a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- Portfolio Item End -->
              </div>
              <!-- End Portfolio Gallery Grid -->
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- Section: Features -->
    <!--<section id="features" class="bg-lighter">
      <div class="container pb-40">
        <div class="section-title text-center">
          <div class="row">
            <div class="col-md-8 col-md-offset-2">
              <h2 class="text-uppercase title">Our <span class="text-black font-weight-300"> Features</span></h2>
              <p class="text-uppercase letter-space-1">Join our Training Club and Rise to a New Challenge</p>
            </div>
          </div>
        </div>
        <div class="section-content">
          <div class="row multi-row-clearfix">
            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
              <div class="icon-box text-center">
                <a class="icon bg-theme-colored icon-circled icon-border-effect effect-circle icon-xl" href="#"> <i class="flaticon-sports-strength text-white"></i> </a>
                <h4 class="Personal trainer text-uppercase"><strong>Weight Loss Specialized</strong></h4>
                <p>Eleifend lobortis bibendum volutpat est senectus duis convallis augue hendrerit senectus duis</p>
              </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
              <div class="icon-box text-center"> 
                <a class="icon bg-theme-colored icon-circled icon-border-effect effect-circle icon-xl" href="#"> <i class="flaticon-sports-sports-6 text-white"></i> </a>
                <h4 class="icon-box-title mt-15 mb-10 text-uppercase"><strong>Golf Fitness Specialized</strong></h4>
                <p>Eleifend lobortis bibendum volutpat est senectus duis convallis augue hendrerit senectus duis</p>
              </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
              <div class="icon-box text-center"> 
                <a class="icon bg-theme-colored icon-circled icon-border-effect effect-circle icon-xl" href="#"> <i class="flaticon-sports-sport text-white"></i> </a>
                <h4 class="icon-box-title mt-15 mb-10 text-uppercase"><strong>Youth Exercise Specialized</strong></h4>
                <p>Eleifend lobortis bibendum volutpat est senectus duis convallis augue hendrerit senectus duis</p>
              </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
              <div class="icon-box text-center"> 
                <a class="icon bg-theme-colored icon-circled icon-border-effect effect-circle icon-xl" href="#"> <i class="flaticon-sports-sports-1 text-white"></i> </a>
                <h4 class="icon-box-title mt-15 mb-10 text-uppercase"><strong>Behavior Specialized</strong></h4>
                <p>Eleifend lobortis bibendum volutpat est senectus duis convallis augue hendrerit senectus duis</p>
              </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
              <div class="icon-box text-center"> 
                <a class="icon bg-theme-colored icon-circled icon-border-effect effect-circle icon-xl" href="#"> <i class="flaticon-sports-symbol text-white"></i> </a>
                <h4 class="icon-box-title mt-15 mb-10 text-uppercase"><strong>Life Time Academy</strong></h4>
                <p>Eleifend lobortis bibendum volutpat est senectus duis convallis augue hendrerit senectus duis</p>
              </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
              <div class="icon-box text-center"> 
                <a class="icon bg-theme-colored icon-circled icon-border-effect effect-circle icon-xl" href="#"> <i class="flaticon-sports-player text-white"></i> </a>
                <h4 class="icon-box-title mt-15 mb-10 text-uppercase"><strong>Truly Personal</strong></h4>
                <p>Eleifend lobortis bibendum volutpat est senectus duis convallis augue hendrerit senectus duis</p>
              </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
              <div class="icon-box text-center"> 
                <a class="icon bg-theme-colored icon-circled icon-border-effect effect-circle icon-xl" href="#"> <i class="flaticon-sports-signs text-white"></i> </a>
                <h4 class="icon-box-title mt-15 mb-10 text-uppercase"><strong>Certifications</strong></h4>
                <p>Eleifend lobortis bibendum volutpat est senectus duis convallis augue hendrerit senectus duis</p>
              </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
              <div class="icon-box text-center"> 
                <a class="icon bg-theme-colored icon-circled icon-border-effect effect-circle icon-xl" href="#"> <i class="flaticon-sports-gym-2 text-white"></i> </a>
                <h4 class="icon-box-title mt-15 mb-10 text-uppercase"><strong>Insurance</strong></h4>
                <p>Eleifend lobortis bibendum volutpat est senectus duis convallis augue hendrerit senectus duis</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>-->

    <!-- Section: Blog   -->
    <section id="blog">
      <div class="container-fluid pb-40">
        <div class="section-title text-center">
          <div class="row">
            <div class="col-md-8 col-md-offset-2">
              <h2 class="title text-uppercase">Our <span class="text-black font-weight-300">Testimonials</span></h2>
              <p class="text-uppercase letter-space-1">Join our Nagarcycling Club and Rise to a New Challenge</p>
            </div>
          </div>
        </div>
        <div class="section-content">
          <div class="row">
          	<div class="col-md-12">
	            <div class="owl-carousel-4col">
	              <div class="items">
	                <div class="schedule-box maxwidth500 mb-30 bg-lighter">
	                  <div class="thumb"> <img class="img-fullwidth" alt="" src="<?php echo base_url();?>web_asset/images/testimonials/testimonial1.jpg">
	                  <!--  <div class="overlay"> <a href="#"><i class="fa fa-search mr-5 bg-theme-colored"></i></a> </div>-->
	                  </div>
	                  <div class="schedule-details clearfix p-20  pt-10">
	                    <h5 class="font-16 title text-uppercase"><a href="#">Atharv Paranjpe</a></h5>
	                   <!-- <p>with <a href="#">Corks</a> &amp; <a href="#">Camelia</a></p>
	                    <ul class="list-inline font-10 mt-15 mb-20 text-gray-silver">
	                      <li><i class="fa fa-calendar mr-5"></i> DEC 31/2016</li>
	                      <li><i class="fa fa-map-marker mr-5"></i> 89 Newyork City.</li>
	                    </ul>-->
	                    <p>"Participated in the cycle marathon recently organised by the club. The entire event was very beautifully and professionally organised."
						</p>
	                  
	                  </div>
	                </div>
	              </div>
	              <div class="items">
	                <div class="schedule-box maxwidth500 mb-30 bg-lighter">
	                  <div class="thumb"> <img class="img-fullwidth" alt="" src="<?php echo base_url();?>web_asset/images/blog/2.jpg">
	                    <div class="overlay"> <a href="#"><i class="fa fa-search mr-5 bg-theme-colored"></i></a> </div>
	                  </div>
	                  <div class="schedule-details clearfix p-20 pt-10">
	                    <h5 class="font-16 title text-uppercase"><a href="#">voluptatum nemo</a></h5>
	                    <!-- <p>with <a href="#">Corks</a> &amp; <a href="#">Camelia</a></p>
	                    <ul class="list-inline font-10 mt-15 mb-20 text-gray-silver">
	                      <li><i class="fa fa-calendar mr-5"></i> DEC 31/2016</li>
	                      <li><i class="fa fa-map-marker mr-5"></i> 89 Newyork City.</li>
	                    </ul>-->
	                    <p>Lorem ipsum dolor sit amet elit. Cum veritatis sequi nulla nihil, dolor voluptatum nemo adipisci eligendi! Sed nisi perferendis, totam harum dicta.</p>
	                  
	                  </div>
	                </div>
	              </div>
	              <div class="items">
	                <div class="schedule-box maxwidth500 mb-30 bg-lighter">
	                  <div class="thumb"> <img class="img-fullwidth" alt="" src="<?php echo base_url();?>web_asset/images/blog/3.jpg">
	                    <div class="overlay"> <a href="#"><i class="fa fa-search mr-5 bg-theme-colored"></i></a> </div>
	                  </div>
	                  <div class="schedule-details clearfix p-20 pt-10">
	                    <h5 class="font-16 title text-uppercase"><a href="#">Nisi perferendis</a></h5>
	                    <!-- <p>with <a href="#">Corks</a> &amp; <a href="#">Camelia</a></p>
	                    <ul class="list-inline font-10 mt-15 mb-20 text-gray-silver">
	                      <li><i class="fa fa-calendar mr-5"></i> DEC 31/2016</li>
	                      <li><i class="fa fa-map-marker mr-5"></i> 89 Newyork City.</li>
	                    </ul>-->
	                    <p>Lorem ipsum dolor sit amet elit. Cum veritatis sequi nulla nihil, dolor voluptatum nemo adipisci eligendi! Sed nisi perferendis, totam harum dicta.</p>
	                   
	                  </div>
	                </div>
	              </div>
	              <div class="items">
	                <div class="schedule-box maxwidth500 mb-30 bg-lighter">
	                  <div class="thumb"> <img class="img-fullwidth" alt="" src="<?php echo base_url();?>web_asset/images/blog/4.jpg">
	                    <div class="overlay"> <a href="#"><i class="fa fa-search mr-5 bg-theme-colored"></i></a> </div>
	                  </div>
	                  <div class="schedule-details clearfix p-20 pt-10">
	                    <h5 class="font-16 title text-uppercase"><a href="#">Totam harum dicta</a></h5>
	                   <!-- <p>with <a href="#">Corks</a> &amp; <a href="#">Camelia</a></p>
	                    <ul class="list-inline font-10 mt-15 mb-20 text-gray-silver">
	                      <li><i class="fa fa-calendar mr-5"></i> DEC 31/2016</li>
	                      <li><i class="fa fa-map-marker mr-5"></i> 89 Newyork City.</li>
	                    </ul>-->
	                    <p>Lorem ipsum dolor sit amet elit. Cum veritatis sequi nulla nihil, dolor voluptatum nemo adipisci eligendi! Sed nisi perferendis, totam harum dicta.</p>
	                   
	                  </div>
	                </div>
	              </div>
	              <div class="items">
	                <div class="schedule-box maxwidth500 mb-30 bg-lighter">
	                  <div class="thumb"> <img class="img-fullwidth" alt="" src="<?php echo base_url();?>web_asset/images/blog/5.jpg">
	                    <div class="overlay"> <a href="#"><i class="fa fa-search mr-5 bg-theme-colored"></i></a> </div>
	                  </div>
	                  <div class="schedule-details clearfix p-20 pt-10">
	                    <h5 class="font-16 title text-uppercase"><a href="#">Kick Class</a></h5>
	                    <!-- <p>with <a href="#">Corks</a> &amp; <a href="#">Camelia</a></p>
	                    <ul class="list-inline font-10 mt-15 mb-20 text-gray-silver">
	                      <li><i class="fa fa-calendar mr-5"></i> DEC 31/2016</li>
	                      <li><i class="fa fa-map-marker mr-5"></i> 89 Newyork City.</li>
	                    </ul>-->
	                    <p>Lorem ipsum dolor sit amet elit. Cum veritatis sequi nusslla nihil, dolor voluptatum nemo adipisci eligendi! Sed nisi perferendis, totam harum dicta.</p>
	                   
	                  </div>
	                </div>
	              </div>
	            </div>
          	</div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <!-- end main-content -->
  
  