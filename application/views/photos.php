<!DOCTYPE html>
<html dir="ltr" lang="en">

<!-- Mirrored from html.kodesolution.live/html/health-beauty/sports-trainer/v2.0/demo/page-gallery-grid-animation.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 27 Jul 2019 07:30:49 GMT -->
<head>

<!-- Meta Tags -->
<meta name="viewport" content="width=device-width,initial-scale=1.0"/>
<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>


<!-- Page Title -->
<title>Ahmednagar Cyclothon - Photos</title>

<!-- Favicon and Touch Icons -->
<link href="<?php echo base_url();?>web_asset/images/favicon.png" rel="shortcut icon" type="image/png">
<link href="<?php echo base_url();?>web_asset/images/apple-touch-icon.png" rel="apple-touch-icon">
<link href="<?php echo base_url();?>web_asset/images/apple-touch-icon-72x72.png" rel="apple-touch-icon" sizes="72x72">
<link href="<?php echo base_url();?>web_asset/images/apple-touch-icon-114x114.png" rel="apple-touch-icon" sizes="114x114">
<link href="<?php echo base_url();?>web_asset/images/apple-touch-icon-144x144.png" rel="apple-touch-icon" sizes="144x144">

<!-- Stylesheet -->
<link href="<?php echo base_url();?>web_asset/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url();?>web_asset/css/jquery-ui.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url();?>web_asset/css/animate.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url();?>web_asset/css/css-plugin-collections.css" rel="stylesheet"/>
<!-- CSS | menuzord megamenu skins -->
<link id="menuzord-menu-skins" href="<?php echo base_url();?>web_asset/css/menuzord-skins/menuzord-boxed.css" rel="stylesheet"/>
<!-- CSS | Main style file -->
<link href="<?php echo base_url();?>web_asset/css/style-main.css" rel="stylesheet" type="text/css">
<!-- CSS | Theme Color -->
<link href="<?php echo base_url();?>web_asset/css/colors/theme-skin-lemon.css" rel="stylesheet" type="text/css">
<!-- CSS | Preloader Styles -->
<link href="<?php echo base_url();?>web_asset/css/preloader.css" rel="stylesheet" type="text/css">
<!-- CSS | Custom Margin Padding Collection -->
<link href="<?php echo base_url();?>web_asset/css/custom-bootstrap-margin-padding.css" rel="stylesheet" type="text/css">
<!-- CSS | Responsive media queries -->
<link href="<?php echo base_url();?>web_asset/css/responsive.css" rel="stylesheet" type="text/css">
<!-- CSS | Style css. This is the file where you can place your own custom css code. Just uncomment it and use it. -->
<!-- <link href="css/style.css" rel="stylesheet" type="text/css"> -->

<!-- external javascripts -->
<script src="<?php echo base_url();?>web_asset/js/jquery-2.2.4.min.js"></script>
<script src="<?php echo base_url();?>web_asset/js/jquery-ui.min.js"></script>
<script src="<?php echo base_url();?>web_asset/js/bootstrap.min.js"></script>
<!-- JS | jquery plugin collection for this theme -->
<script src="<?php echo base_url();?>web_asset/js/jquery-plugin-collection.js"></script>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body class="">
<div id="wrapper">
  <!-- preloader -->
  <div id="preloader">
    <div id="spinner">
      <img src="<?php echo base_url();?>web_asset/images/preloaders/1.gif" alt="">
    </div>
    <div id="disable-preloader" class="btn btn-default btn-sm">Disable Preloader</div>
  </div>
  
  <!-- Header -->
  <header id="header" class="header">
    <div class="header-top sm-text-center bg-theme-colored">
      <div class="container">
      <div class="row">
          <div class="col-md-4">
            <nav>
              <ul class="list-inline sm-text-center text-left flip mt-5">
                <li> <a class="text-white" href="<?php echo base_url();?>index.php/contactus">Contact Us</a> </li>
                <!--<li class="text-white">|</li>
                <li> <a class="text-white" href="<?php echo base_url();?>index.php/termsandconditions">Terms & Conditions</a> </li>-->
               
              </ul>
            </nav>
          </div>
          <div class="col-md-6">
            <div class="widget m-0 mt-5 no-border">
              <ul class="list-inline text-right sm-text-center">
                <li class="pl-10 pr-10 mb-0 pb-0">
                  <div class="header-widget text-white"><i class="fa fa-phone"></i>+91 8308054000 </div>
                </li>
                <li class="pl-10 pr-10 mb-0 pb-0">
                  <div class="header-widget text-white"><i class="fa fa-envelope-o"></i> support@nagarcycling.com</div>
                </li>
              </ul>
            </div>
          </div>
          <div class="col-md-2 text-right flip sm-text-center">
            <div class="widget m-0">
              <ul class="styled-icons icon-dark icon-circled icon-theme-colored icon-sm">
                <li class="mb-0 pb-0"><a href="https://www.facebook.com/nagarcycling/" target="_blank"><i class="fa fa-facebook"></i></a></li>
                
                <li class="mb-0 pb-0"><a href="https://www.instagram.com/nagarcycling/" target="_blank"><i class="fa fa-instagram"></i></a></li>
                
              </ul>
            </div>
          </div>
        </div>
     </div>
    </div>
    <div class="header-nav">
      <div class="header-nav-wrapper bg-light navbar-scrolltofixed">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div id="menuzord-right" class="menuzord orange no-bg"> <a class="menuzord-brand stylish-header pull-left flip" href="javascript:void(0)">
			  <img src="<?php echo base_url();?>web_asset/images/logo-wide-white.png" alt=""></a>
                <ul class="menuzord-menu">
                 <li><a href="<?php echo base_url();?>index.php/home">Home</a>
                 </li>
                  <li><a href="<?php echo base_url();?>index.php/aboutus">About Us</a></li>
                  <li><a href="<?php echo base_url();?>index.php/events">Events</a></li>
                  <li><a href="#">Sponsors</a>
				   <ul class="dropdown">
				    <li><a href="<?php echo base_url();?>index.php/sponsors2020">Sponsors 2020</a></li>
                    <li><a href="<?php echo base_url();?>index.php/sponsors2019">Sponsors 2019</a></li>
                    <li><a href="<?php echo base_url();?>index.php/sponsors2018">Sponsors 2018</a></li>
					</ul>
				  </li>
				  <li class="active"><a href="<?php echo base_url();?>index.php/photos">Free Photos</a></li>
                  <li><a href="<?php echo base_url();?>index.php/contactus">Contact Us</a></li>				  
				  <li><a href="<?php echo base_url();?>index.php/registration">Registration</a></li>
				</ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </header>
  
  <!-- Start main-content -->
  <div class="main-content">
    <!-- Section: inner-header -->
    <section class="inner-header divider parallax layer-overlay overlay-white-8" data-bg-img="<?php echo base_url();?>web_asset/images/bg/bg6.jpg">
      <div class="container pt-60 pb-60">
        <!-- Section Content -->
        <div class="section-content">
          <div class="row">
            <div class="col-md-12 text-center">
              <h2 class="title">Gallery</h2>
              <ol class="breadcrumb text-center text-black mt-10">
                <li><a href="#">Home</a></li>
                <li><a href="#">Pages</a></li>
                <li class="active text-theme-colored">Gallery</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section>
      <div class="container">
        <div class="row">
          <div class="col-md-12">
           

            <div class="separator separator-rouned">
              <i class="fa fa-cog fa-spin"></i>
            </div>

            <div class="heading-line-bottom">
               <h4 class="heading-title">Gallery</h4>
            </div>

            <!-- Portfolio Gallery Grid -->
            <div class="gallery-isotope grid-4 gutter-small clearfix" data-lightbox="gallery">
              <!-- Portfolio Item Start -->
              <div class="gallery-item">
                <div class="thumb">
                  <img class="img-fullwidth" src="<?php echo base_url();?>web_asset/images/gallery/1.jpg" alt="project">
                  <div class="overlay-shade"></div>
                  <div class="text-holder">
                    <div class="title text-center">Sample Title</div>
                  </div>
                  <div class="icons-holder">
                    <div class="icons-holder-inner">
                      <div class="styled-icons icon-sm icon-dark icon-circled icon-theme-colored">
                        <a href="<?php echo base_url();?>web_asset/images/gallery/1.jpg" data-lightbox-gallery="gallery" title="Your Title Here"><i class="fa fa-picture-o"></i></a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- Portfolio Item End -->
              
              <!-- Portfolio Item Start -->
              <div class="gallery-item">
                <div class="thumb">
                  <img class="img-fullwidth" src="<?php echo base_url();?>web_asset/images/gallery/2.jpg" alt="project">
                  <div class="overlay-shade"></div>
                  <div class="text-holder">
                    <div class="title text-center">Sample Title</div>
                  </div>
                  <div class="icons-holder">
                    <div class="icons-holder-inner">
                      <div class="styled-icons icon-sm icon-dark icon-circled icon-theme-colored">
                        <a href="<?php echo base_url();?>web_asset/images/gallery/2.jpg" data-lightbox-gallery="gallery" title="Your Title Here"><i class="fa fa-picture-o"></i></a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- Portfolio Item End -->
              
              <!-- Portfolio Item Start -->
              <div class="gallery-item">
                <div class="thumb">
                  <img class="img-fullwidth" src="<?php echo base_url();?>web_asset/images/gallery/3.jpg" alt="project">
                  <div class="overlay-shade"></div>
                  <div class="text-holder">
                    <div class="title text-center">Sample Title</div>
                  </div>
                  <div class="icons-holder">
                    <div class="icons-holder-inner">
                      <div class="styled-icons icon-sm icon-dark icon-circled icon-theme-colored">
                        <a href="<?php echo base_url();?>web_asset/images/gallery/3.jpg" data-lightbox-gallery="gallery" title="Your Title Here"><i class="fa fa-picture-o"></i></a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- Portfolio Item End -->
              
              <!-- Portfolio Item Start -->
              <div class="gallery-item">
                <div class="thumb">
                  <img class="img-fullwidth" src="<?php echo base_url();?>web_asset/images/gallery/4.jpg" alt="project">
                  <div class="overlay-shade"></div>
                  <div class="text-holder">
                    <div class="title text-center">Sample Title</div>
                  </div>
                  <div class="icons-holder">
                    <div class="icons-holder-inner">
                      <div class="styled-icons icon-sm icon-dark icon-circled icon-theme-colored">
                        <a href="<?php echo base_url();?>web_asset/images/gallery/4.jpg" data-lightbox-gallery="gallery" title="Your Title Here"><i class="fa fa-picture-o"></i></a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- Portfolio Item End -->
              
              <!-- Portfolio Item Start -->
              <div class="gallery-item">
                <div class="thumb">
                  <img class="img-fullwidth" src="<?php echo base_url();?>web_asset/images/gallery/5.jpg" alt="project">
                  <div class="overlay-shade"></div>
                  <div class="text-holder">
                    <div class="title text-center">Sample Title</div>
                  </div>
                  <div class="icons-holder">
                    <div class="icons-holder-inner">
                      <div class="styled-icons icon-sm icon-dark icon-circled icon-theme-colored">
                        <a href="<?php echo base_url();?>web_asset/images/gallery/5.jpg" data-lightbox-gallery="gallery" title="Your Title Here"><i class="fa fa-picture-o"></i></a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- Portfolio Item End -->
              
              <!-- Portfolio Item Start -->
              <div class="gallery-item">
                <div class="thumb">
                  <img class="img-fullwidth" src="<?php echo base_url();?>web_asset/images/gallery/6.jpg" alt="project">
                  <div class="overlay-shade"></div>
                  <div class="text-holder">
                    <div class="title text-center">Sample Title</div>
                  </div>
                  <div class="icons-holder">
                    <div class="icons-holder-inner">
                      <div class="styled-icons icon-sm icon-dark icon-circled icon-theme-colored">
                        <a href="<?php echo base_url();?>web_asset/images/gallery/6.jpg" data-lightbox-gallery="gallery" title="Your Title Here"><i class="fa fa-picture-o"></i></a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- Portfolio Item End -->
              
              <!-- Portfolio Item Start -->
              <div class="gallery-item">
                <div class="thumb">
                  <img class="img-fullwidth" src="<?php echo base_url();?>web_asset/images/gallery/7.jpg" alt="project">
                  <div class="overlay-shade"></div>
                  <div class="text-holder">
                    <div class="title text-center">Sample Title</div>
                  </div>
                  <div class="icons-holder">
                    <div class="icons-holder-inner">
                      <div class="styled-icons icon-sm icon-dark icon-circled icon-theme-colored">
                        <a href="<?php echo base_url();?>web_asset/images/gallery/7.jpg" data-lightbox-gallery="gallery" title="Your Title Here"><i class="fa fa-picture-o"></i></a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- Portfolio Item End -->
              
              <!-- Portfolio Item Start -->
              <div class="gallery-item">
                <div class="thumb">
                  <img class="img-fullwidth" src="<?php echo base_url();?>web_asset/images/gallery/8.jpg" alt="project">
                  <div class="overlay-shade"></div>
                  <div class="text-holder">
                    <div class="title text-center">Sample Title</div>
                  </div>
                  <div class="icons-holder">
                    <div class="icons-holder-inner">
                      <div class="styled-icons icon-sm icon-dark icon-circled icon-theme-colored">
                        <a href="<?php echo base_url();?>web_asset/images/gallery/8.jpg" data-lightbox-gallery="gallery" title="Your Title Here"><i class="fa fa-picture-o"></i></a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- Portfolio Item End -->
            </div>
            <!-- End Portfolio Gallery Grid -->

           
          </div>
        </div>
      </div>
    </section>
  </div>
  <!-- end main-content -->
  
 