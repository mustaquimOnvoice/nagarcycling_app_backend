<!DOCTYPE html>
<html dir="ltr" lang="en">

<!-- Mirrored from html.kodesolution.live/html/health-beauty/sports-trainer/v2.0/demo/form-job-apply-style1.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 27 Jul 2019 07:30:32 GMT -->
<head>

<!-- Meta Tags -->
<meta name="viewport" content="width=device-width,initial-scale=1.0"/>
<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>


<!-- Page Title -->
<title>Ahmednagar Cyclothon - Registration</title>

<!-- Favicon and Touch Icons -->
<link href="<?php echo base_url();?>web_asset/images/favicon.png" rel="shortcut icon" type="image/png">
<link href="<?php echo base_url();?>web_asset/images/apple-touch-icon.png" rel="apple-touch-icon">
<link href="<?php echo base_url();?>web_asset/images/apple-touch-icon-72x72.png" rel="apple-touch-icon" sizes="72x72">
<link href="<?php echo base_url();?>web_asset/images/apple-touch-icon-114x114.png" rel="apple-touch-icon" sizes="114x114">
<link href="<?php echo base_url();?>web_asset/images/apple-touch-icon-144x144.png" rel="apple-touch-icon" sizes="144x144">

<!-- Stylesheet -->
<link href="<?php echo base_url();?>web_asset/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url();?>web_asset/css/jquery-ui.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url();?>web_asset/css/animate.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url();?>web_asset/css/css-plugin-collections.css" rel="stylesheet"/>
<!-- CSS | menuzord megamenu skins -->
<link id="menuzord-menu-skins" href="<?php echo base_url();?>web_asset/css/menuzord-skins/menuzord-rounded-boxed.css" rel="stylesheet"/>
<!-- CSS | Main style file -->
<link href="<?php echo base_url();?>web_asset/css/style-main.css" rel="stylesheet" type="text/css">
<!-- CSS | Theme Color -->
<link href="<?php echo base_url();?>web_asset/css/colors/theme-skin-lemon.css" rel="stylesheet" type="text/css">
<!-- CSS | Preloader Styles -->
<link href="<?php echo base_url();?>web_asset/css/preloader.css" rel="stylesheet" type="text/css">
<!-- CSS | Custom Margin Padding Collection -->
<link href="<?php echo base_url();?>web_asset/css/custom-bootstrap-margin-padding.css" rel="stylesheet" type="text/css">
<!-- CSS | Main style file -->
<link href="<?php echo base_url();?>web_asset/css/style-main.css" rel="stylesheet" type="text/css">
<!-- CSS | Theme Color -->
<link href="<?php echo base_url();?>web_asset/css/colors/theme-skin-lemon.css" rel="stylesheet" type="text/css">
<!-- CSS | Preloader Styles -->
<link href="<?php echo base_url();?>web_asset/css/preloader.css" rel="stylesheet" type="text/css">
<!-- CSS | Custom Margin Padding Collection -->
<link href="<?php echo base_url();?>web_asset/css/custom-bootstrap-margin-padding.css" rel="stylesheet" type="text/css">
<!-- CSS | Responsive media queries -->
<link href="<?php echo base_url();?>web_asset/css/responsive.css" rel="stylesheet" type="text/css">
<!-- CSS | Style css. This is the file where you can place your own custom css code. Just uncomment it and use it. -->
<!-- <link href="css/style.css" rel="stylesheet" type="text/css"> -->
<!-- external javascripts -->
<script src="<?php echo base_url();?>web_asset/js/jquery-2.2.4.min.js"></script>
<script src="<?php echo base_url();?>web_asset/js/jquery-ui.min.js"></script>
<script src="<?php echo base_url();?>web_asset/js/bootstrap.min.js"></script>

<!-- JS | jquery plugin collection for this theme 
<script src="<?php echo base_url();?>web_asset/js/jquery-plugin-collection.js"></script>-->

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<style>
  .text-theme-colored {
color: #ff0000 !important;
}
.blinking{
animation:blinkingText 1.2s infinite;
}
@keyframes blinkingText{
0%{ color: red; }
49%{ color: transparent; }
50%{ color: transparent; }
99%{ color:transparent; }
100%{ color: #000; }
}

</style>
</head>
<body class="">
<div id="wrapper"> 
  <!-- preloader -->
 <!--  <div id="preloader">
    <div id="spinner">
      <img src="<?php // echo base_url();?>web_asset/images/preloaders/1.gif" alt="">
    </div>
    <div id="disable-preloader" class="btn btn-default btn-sm">Disable Preloader</div>
  </div> -->
  
   <!-- Header -->
  <header id="header" class="header">
    <div class="header-top sm-text-center bg-theme-colored">
      <div class="container">
      <div class="row">
          <div class="col-md-4">
            <nav>
              <ul class="list-inline sm-text-center text-left flip mt-5">
                <li> <a class="text-white" href="<?php echo base_url();?>index.php/contactus">Contact Us</a> </li>
                <!--<li class="text-white">|</li>
                <li> <a class="text-white" href="<?php echo base_url();?>index.php/termsandconditions">Terms & Conditions</a> </li>-->
               
              </ul>
            </nav>
          </div>
          <div class="col-md-6">
            <div class="widget m-0 mt-5 no-border">
              <ul class="list-inline text-right sm-text-center">
                <li class="pl-10 pr-10 mb-0 pb-0">
                  <!--<div class="header-widget text-white"><i class="fa fa-phone"></i>+91 8308054000 </div>-->
                  <div class="header-widget text-white"><i class="fa fa-phone"></i>+91 8600244000 </div>
                </li>
                <li class="pl-10 pr-10 mb-0 pb-0">
                  <div class="header-widget text-white"><i class="fa fa-envelope-o"></i> support@nagarcycling.com</div>
                </li>
              </ul>
            </div>
          </div>
          <div class="col-md-2 text-right flip sm-text-center">
            <div class="widget m-0">
              <ul class="styled-icons icon-dark icon-circled icon-theme-colored icon-sm">
                <li class="mb-0 pb-0"><a href="https://www.facebook.com/nagarcycling/" target="_blank"><i class="fa fa-facebook"></i></a></li>
                
                <li class="mb-0 pb-0"><a href="https://www.instagram.com/nagarcycling/" target="_blank"><i class="fa fa-instagram"></i></a></li>
                
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="header-nav">
      <div class="header-nav-wrapper bg-light navbar-scrolltofixed">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div id="menuzord-right" class="menuzord orange no-bg"> <a class="menuzord-brand stylish-header pull-left flip" href="javascript:void(0)"><img src="<?php echo base_url();?>web_asset/images/logo-wide-white.png" alt=""></a>
                <ul class="menuzord-menu">
                 <li><a href="<?php echo base_url();?>index.php/home">Home</a>
                 </li>
                  <li><a href="<?php echo base_url();?>index.php/aboutus">About Us</a></li>
                  <li><a href="<?php echo base_url();?>index.php/events">Events</a></li>
                  <li><a href="#">Sponsors</a>
				   <ul class="dropdown">
				    <!--<li><a href="<?php echo base_url();?>index.php/sponsors2020">Sponsors 2020</a></li>-->
                    <li><a href="<?php echo base_url();?>index.php/sponsors2019">Cyclothon Sponsors 2019</a></li>
                    <li><a href="<?php echo base_url();?>index.php/sponsors2018">Cyclothon Sponsors 2018</a></li>
					</ul>
				  </li>
				  <li><a href="<?php echo base_url();?>index.php/photos">Gallery</a></li>
                  <li><a href="<?php echo base_url();?>index.php/contactus">Contact Us</a></li>				  
				  <li  class="active"><a href="<?php echo base_url();?>index.php/registration">Registrations </a></li>
				 <!-- <li  class="active"><a href="<?php echo base_url();?>index.php/registration">Register Here</a></li>-->
				</ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </header>
  <style >
  #home{
      background-image: url(https://www.nagarcycling.com/web_asset/images/bg/bg1.jpg);
    background-position: 50% 61px;
    }
  .footer{
    background-image: url("https://www.nagarcycling.com/web_asset/images/footer/footer-nagarcycling.png");
  }  
</style>
  <!-- Start main-content -->
  <div class="main-content">

    <!-- Section: inner-header -->
  <section id="home" class="divider parallax layer-overlay"  data-bg-img="<?php echo base_url();?>web_asset/images/bg/bg1.jpg">
      <div class="display-table">
        <div class="display-table-cell">
          <div class="container">
              <div class="col-md-8 col-md-push-2">
               <!-- <div class="text-center mb-60"><a href="#" class=""><img alt="" src="<?php echo base_url();?>web_asset/images/logo-wide.png"></a>
                </div>-->
                <div class="bg-lightest border-1px p-30 mb-0">
                  <center>   <h3 class="text-theme-colored mt-0 pt-5">Register For Ahmednagar Cyclothon 2020</h3></center>
                  <?php //echo '<pre>';print_r($info) ?>
                <!--    <center>   <h3 class="mt-0 pt-5">Registrations Closed </h3></center> -->
                 <!--  <center>   <h3 class="mt-0 pt-5">Last Few Hours Left To Register </h3></center>-->
                 <!--  <h4 class="text-theme-colored" style="text-align:center;" id="demo"></h4>-->
               <!-- <h4  style="text-align:center;">Only <b style="color:red !important;"> <?php //echo @$data['remainingallkm']; ?></b><b>  Registrations Left</b>  </h4>-->
                  <!--  <p><b>Registrations Available:</b> </p><p> 1KM = <?php // echo @$data['remaining1km']; ?> Registrations /  3KM = <?php // echo @$data['remaining3km']; ?> Registrations / 5KM = <?php // echo @$data['remaining5km']; ?> Registrations</p> -->     <hr>
                  <form  id="job_apply_form1" name="job_apply_form" action="<?php echo base_url();?>index.php/registration/confirm_form" method="post" enctype="multipart/form-data">
                    <div class="row">
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label for="form_name">Full Name <small>*</small></label>
                          <input id="name" name="name" type="text" placeholder="Enter Full Name" required="" value="<?php echo @$info[0]['name'];?>"class="form-control">
                            <p class="name_err" style="display:none;color:red;"><b>Name Required..!</b></p>
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label for="form_email">Email <small>*</small></label>
                          <input id="email" name="email" class="form-control required email" onchange="return validemail();"  value="<?php echo @$info[0]['email_addr'];?>" type="email" placeholder="Enter Email">
                           <p class="email_err" style="display:none;color:red;"><b>Email Required..!</b></p>
                           <p class="invalidemail_err" style="display:none;color:red;"><b>Valid Email Required..!</b></p>
                           <p class="validemail_err" style="display:none;color:red;"><b>Email Already Exist..!</b></p>
                        </div>
                      </div>
                    </div>
					
					<div class="row">
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label for="form_name">Mobile Number<small>*</small></label>
                          <input id="mobile_no" name="mobile_no" type="number" onchange="return validmobile();" value="<?php echo @$info[0]['mobile_no'];?>" placeholder="Enter Mobile Number"  required="" class="form-control">
                           <p class="mno_err" style="display:none;color:red;"><b>Mobile Number Required..!</b></p>
                           <p class="mno_lengtherr" style="display:none;color:red;"><b>Enter A Valid Mobile Number..!</b></p>
                           <p class="validmno_err" style="display:none;color:red;"><b>Mobile Number Already Exist..!</b></p>
                        </div>
                      </div>
                       <div class="col-sm-6">
                        <div class="form-group">
                          <label for="gender">Gender <small>*</small></label>
                          <select id="gender" name="gender" class="form-control required">
                            <option value="">Select Gender</option>
                            <option <?php if(@$info[0]['gender']=="male"){?>selected="selected"<?php }?> value="Male">Male</option>
                            <option <?php if(@$info[0]['gender']=="female"){?>selected="selected"<?php }?>value="Female">Female</option>
                          </select>
                           <p class="gender_err" style="display:none;color:red;"><b>Gender Required..!</b></p>
                        </div>
                      </div>
                    </div>
					
					<div class="row">
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label for="form_name">Date of Birth<small>*</small></label>
                           <input type="text" value="<?php echo $info[0]['birthdate'];?>" name="dob" id="dob" onclick="return getdate();" placeholder="Date of Birth" required="" class="form-control ">
                            <input type="date" style="display:none;" name="dob" id="dob1" onchange="return countage();" placeholder="Date of Birth"  class="form-control ">
                             <input type="hidden" value="<?php   $exp_date = str_replace('/', '-', $info[0]['birthdate']);  echo  date('Y-m-d', strtotime($exp_date));?>" name="dob" id="dob3" onclick="return getdate();" placeholder="Date of Birth" required="" class="form-control ">
                        </div>
                        <p class="min_age_err" style="display:none;color:red;"><b>Age Should Be Atlist 18..!</b></p>
                  <!--    <p class="max_age_err" style="display:none;color:red;"><b>Age Limit Is 12..!</b></p> -->
                      <p class="dob_err" style="display:none;color:red;"><b>Date of Birth Required..!</b></p>
                     <!--  <p id="agecnt" style="color:green;font-weight: bold;"></p>  -->
                  
                      </div>
                      <div class="col-sm-6">
                       <div class="form-group">
                          <label for="age">Age <small>*</small></label>
                          <input id="age" name="age" class="form-control required" value="<?php echo @$info[0]['age'];?>" placeholder="Enter Age" >
                           <p class="age" style="display:none;color:red;"><b>Age Required..!</b></p>
                        </div>
                      </div>
                    </div>
					  
					<div class="row">  
					 <div class="col-sm-6">
                       <div class="form-group">
                          <label for="form_name"> Race Category<small>*</small></label>
                         	<select id="category" name="category" class="form-control required">
                            <option value="">Select Race Category</option>
                            <option  <?php if(@$info[0]['race_category']=="20KM"){?>selected="selected"<?php }?> value="20KM">20KM</option>
                            <option  <?php if(@$info[0]['race_category']=="50KM"){?>selected="selected"<?php }?> value="50KM">50KM</option>
                            <option  <?php if(@$info[0]['race_category']=="100KM"){?>selected="selected"<?php }?> value="100KM">100KM</option>
                          </select>
                           <p class="category_err" style="display:none;color:red;"><b>Race Category Required..!</b></p>
                             <p id="caterr" style="color:red;font-weight: bold;"></p> 
                        </div>
                      </div>
                        <div class="col-sm-6">
                        <div class="form-group">
                          <label for="blood_group">Blood Group <small>*</small></label>
                          <select id="blood_group" name="blood_group" class="form-control required">
                            <option value="">Select Blood Group</option>
                            <option  <?php if(@$info[0]['bloodgrp']=="O+"){?>selected="selected"<?php }?> value="O+">O+</option>
                            <option  <?php if(@$info[0]['bloodgrp']=="O-"){?>selected="selected"<?php }?>value="O-">O-</option>
						              	<option  <?php if(@$info[0]['bloodgrp']=="A+"){?>selected="selected"<?php }?> value="A+">A+</option>
                            <option  <?php if(@$info[0]['bloodgrp']=="A-"){?>selected="selected"<?php }?> value="A-">A-</option>
							              <option  <?php if(@$info[0]['bloodgrp']=="B+"){?>selected="selected"<?php }?> value="B+">B+</option>
                            <option  <?php if(@$info[0]['bloodgrp']=="B-"){?>selected="selected"<?php }?> value="B-">B-</option>
						              	<option  <?php if(@$info[0]['bloodgrp']=="AB+"){?>selected="selected"<?php }?> value="AB+">AB+</option>
                            <option  <?php if(@$info[0]['bloodgrp']=="AB-"){?>selected="selected"<?php }?> value="AB-">AB-</option>
                          </select>
                          <p class="blood_group_err" style="display:none;color:red;"><b>Blood Group Required..!</b></p>
                        </div>
                      </div>
                    </div>  
					<div class="row">
                      <div class="col-sm-12">
                        <div class="form-group">
                          <label for="form_name">Address <small>*</small></label>
                          <input id="address" name="address" type="text" value="<?php echo $info[0]['full_address'];?>" placeholder="Enter Address" required="" class="form-control">
                           <p class="address_err" style="display:none;color:red;"><b>Address Required..!</b></p>
                        </div>
                      </div>
                    </div>

                   <div class="row">
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label for="form_name">City <small>*</small></label>
                          <input id="city" name="city" type="text" placeholder="Enter City" value="<?php echo $info[0]['city'];?>" required="" class="form-control">
                           <p class="city_err" style="display:none;color:red;"><b>City Required..!</b></p>
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label for="form_email">State <small>*</small></label>
                          <input id="state" name="state" class="form-control required" value="<?php echo $info[0]['state'];?>" placeholder="Enter State">
                            <p class="state_err" style="display:none;color:red;"><b>State Required..!</b></p>
                        </div>
                      </div>
                    </div> 
					
					<div class="row">
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label for="form_name">Pin Code <small>*</small></label>
                          <input id="pincode" name="pincode" type="number" value="<?php echo $info[0]['postcode'];?>" placeholder="Enter Pin Code" required="" class="form-control">
                          <p class="pincode_err" style="display:none;color:red;"><b>Pin Code Required..!</b></p>
                           <p class="pincode_validerr" style="display:none;color:red;"><b>Incorrect Pin Code ..!</b></p>
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label for="form_email">Country <small>*</small></label>
                          <input id="country" name="country" class="form-control required" value="<?php echo $info[0]['country'];?>" placeholder="Enter Country">
                            <p class="country_err" style="display:none;color:red;"><b>Country Required..!</b></p>
                        </div>
                      </div>
                    </div>
					
					
                    <div class="row">               
                    
                      <div class="col-sm-6">
                       <div class="form-group">
                          <label for="t-shirt">T-Shirt Size <small>*</small></label><a style="cursor: pointer;" data-toggle="modal" data-target="#myModal"><b style="color:red;"> Size Chart</b></a>
						             <select id="t-shirt" name="t-shirt" class="form-control required">
                            <option value="">Select Tshirt Size</option>
                            <option  <?php if(@$info[0]['tshirt']=="S"){?>selected="selected"<?php }?>  value="S">S</option>
                            <option  <?php if(@$info[0]['tshirt']=="M"){?>selected="selected"<?php }?>  value="M">M</option>
							              <option  <?php if(@$info[0]['tshirt']=="L"){?>selected="selected"<?php }?>  value="L">L</option>
                            <option  <?php if(@$info[0]['tshirt']=="XL"){?>selected="selected"<?php }?>  value="XXL">XL</option>
                            <option  <?php if(@$info[0]['tshirt']=="XXL"){?>selected="selected"<?php }?>  value="XXL">XXL</option>
						              	<option  <?php if(@$info[0]['tshirt']=="XXXL"){?>selected="selected"<?php }?>  value="XXL">XXXL</option>
							            
                          </select>
                             <p class="tshirt_err" style="display:none;color:red;"><b>T-Shirt Size Required..!</b></p>
                        </div>
                      </div>
                       <div class="col-sm-6">
                       <div class="form-group">
                          <label for="school_name">Emergency Contact Name <small>*</small></label>
                          <input id="emergency_name" name="emergency_name" value="<?php echo $info[0]['emergency_name'];?>" class="form-control required" placeholder="Emergency Contact Name ">
                           <p class="emergency_name_err" style="display:none;color:red;"><b>Emergency Contact Name Required..!</b></p>
                        </div>
                      </div>
                    </div>
					
					<div class="row">
					
                      <div class="col-sm-6">
                       <div class="form-group">
                          <label for="class">Emergency Number <small>*</small></label>
                          <input id="emergency_number" name="emergency_number"value="<?php echo $info[0]['emergency_number'];?>" class="form-control required" placeholder="Enter Emergency Number">
                            <p class="emergency_number_err" style="display:none;color:red;"><b>Emergency Number Required..!</b></p>
                             <p class="emergency_number_lengtherr" style="display:none;color:red;"><b>Enter A Valid Emergency Mobile Number..!</b></p>
                        </div>
                      </div>
                    </div>
                    <div class="row">
					 
                     
                    </div>
					<?php  ?>
					
                    <div class="form-group">
                      <input id="form_botcheck" name="form_botcheck" class="form-control" type="hidden" value="" />
                      <button type="submit" id="btnSubmit" class="btn btn-block btn-dark btn-theme-colored btn-sm mt-20 pt-10 pb-10" onclick="return validation();" data-loading-text="Please wait...">Submit</button>
                    </div>
                  </form>
                        <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
     
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Size Chart</h4>
        </div>
        <div class="modal-body">
         <img src="<?php echo base_url();?>web_asset/images/sizechart.png" alt=""> 
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
                  <!-- Job Form Validation-->
                  <script type="text/javascript">
                     var dd   = $("#dob").val();
                     $("#dob1").val(dd);
                    function getdate(){
                          $('#dob').hide();
                           $('#dob').val('');
                           $('#age').val('0');
                          $('#dob1').show();
             }
                 
                    $("#job_apply_form").validate({
                      submitHandler: function(form) {
                        var form_btn = $(form).find('button[type="submit"]');
                        var form_result_div = '#form-result';
                        $(form_result_div).remove();
                        form_btn.before('<div id="form-result" class="alert alert-success" role="alert" style="display: none;"></div>');
                        var form_btn_old_msg = form_btn.html();
                        form_btn.html(form_btn.prop('disabled', true).data("loading-text"));
                        $(form).ajaxSubmit({
                          dataType:  'json',
                          success: function(data) {
                           
                            if( data.status == 'true' ) {
                              $(form).find('.form-control').val('');
                            }
                            form_btn.prop('disabled', false).html(form_btn_old_msg);
                            $(form_result_div).html(data.message).fadeIn('slow');
                            setTimeout(function(){ $(form_result_div).fadeOut('slow') }, 6000);
                          }
                        });
                      }
                    });
                    

         function validemail(){
      var email = $("#email").val();
      var email_regex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i;
      if(email ==''){
                      $('.email_err').show();
                       setTimeout(function() {
                    $('.email_err').fadeOut('slow');
                      }, 2000);
                      return false;
                 }else  
                if(!email_regex.test(email)){ 
                    $('.invalidemail_err').show();
                       setTimeout(function() {
                    $('.invalidemail_err').fadeOut('slow');
                      }, 2000);

                   return false;  
              }else{
                   $.ajax({
                       type: "POST",
                       url: "<?php echo base_url()?>index.php/Registration/validemail",
                       data: {'email':email},
                       success: function(result1)
                       {
                         if(result1 =='exist'){
                          $('.validemail_err').show();
                             setTimeout(function() {
                          $('.validemail_err').fadeOut('slow');
                            }, 2000);
                            return false;
                          }
                       } 
                   });

                 }
       
        }



         function validmobile(){
    
      var mno   = $("#mobile_no").val();
      var mnolength = mno.length;

      if(mno ==''){
                      $('.mno_err').show();
                       setTimeout(function() {
                    $('.mno_err').fadeOut('slow');
                      }, 2000);
                      return false;
                 }
                 else if(mnolength != 10){
                     $('.mno_lengtherr').show();
                       setTimeout(function() {
                    $('.mno_lengtherr').fadeOut('slow');
                      }, 2000);
                      return false;
                 }else{
                   $.ajax({
                 type: "POST",
                 url: "<?php echo base_url()?>index.php/Registration/validmno",
                 data: {'mno':mno},
                 success: function(result)
                 {
                   if(result =='exist'){
                    $('.validmno_err').show();
                       setTimeout(function() {
                    $('.validmno_err').fadeOut('slow');
                      }, 2000);
                      return false;
                    }
                 } 
                   });

                 }
       
        }


        function countage(){
        var dob = $("#dob1").val();
         $("#dob").val(dob);
       /* var remaining1km = $("#remaining1km").val();
        var remaining3km = $("#remaining3km").val();
        var remaining5km = $("#remaining5km").val();
*/    
 		/*var remainingallkm = $("#remainingallkm").val();   
 */
        var birthdate = new Date(dob); // 
        var cur = new Date("2020/02/16");
        var diff = cur-birthdate; // This is the difference in milliseconds
        var age = Math.floor(diff/31557600000); // Divide by 1000*60*60*24*365.25
         
            if(age <18){
               $('.min_age_err').show();
               $('#agecnt').hide();
            }
            else{

            	 document.getElementById("dob3").value = dob;
               document.getElementById("age").value = age;
            	  $('.min_age_err').hide();
            }
            
	}
            function validation(){
              var name  = $("#name").val();
              var email = $("#email").val();
              var mno   = $("#mobile_no").val();
              var mnolength = mno.length;
              var gender = $("#gender").val();
              var dob   = $("#dob").val();
              var dob1   = $("#dob1").val();
              var category = $("#category").val();
              var address = $("#address").val();
              var city = $("#city").val();
              var state = $("#state").val();
              var pincode = $("#pincode").val();
              var country = $("#country").val();
              var blood_group = $("#blood_group").val();
              var tshirt = $("#t-shirt").val();
              var age1 = $("#age").val();
            
              var emergency_name = $("#emergency_name").val();
              var emergency_number = $("#emergency_number").val();
               var emergency_number_length = emergency_number.length;
         
               var email_regex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i;
                if(name ==''){
                   $('.name_err').show();
                  setTimeout(function() {
                    $('.name_err').fadeOut('slow');
                      }, 2000);
                   return false;
                }else if(email ==''){
                    $('.email_err').show();
                       setTimeout(function() {
                    $('.email_err').fadeOut('slow');
                      }, 2000);
                     return false;
                }else  
                if(!email_regex.test(email)){ 
                    $('.invalidemail_err').show();
                       setTimeout(function() {
                    $('.invalidemail_err').fadeOut('slow');
                      }, 2000);

                   return false;  
              }else{
                   $.ajax({
                       type: "POST",
                       url: "<?php echo base_url()?>index.php/Registration/validemail",
                       data: {'email':email},
                       success: function(result1)
                       {
                         if(result1 =='exist'){
                          $('.validemail_err').show();
                             setTimeout(function() {
                          $('.validemail_err').fadeOut('slow');
                            }, 2000);
                            return false;
                          }
                       } 
                   });

                 }
               
                if(mno =='' || mno < '0'){
                    $('.mno_err').show();
                       setTimeout(function() {
                    $('.mno_err').fadeOut('slow');
                      }, 2000);
                      return false;
                } else if(mnolength != 10){
                     $('.mno_lengtherr').show();
                       setTimeout(function() {
                    $('.mno_lengtherr').fadeOut('slow');
                      }, 2000);
                      return false;
                 }else{
                   $.ajax({
                       type: "POST",
                       url: "<?php echo base_url()?>index.php/Registration/validmno",
                       data: {'mno':mno},
                       success: function(result)
                       {
                         if(result =='exist'){
                          $('.validmno_err').show();
                             setTimeout(function() {
                          $('.validmno_err').fadeOut('slow');
                            }, 2000);
                            return false;
                          }
                       } 
                   });

                 }


                 if(gender == ''){ 
                    $('.gender_err').show();
                  
                        setTimeout(function() {
                          $('.gender_err').fadeOut('slow');
                      }, 2000);
                          return false;
                }else if(dob == '' && dob1 == ''){
                    $('.dob_err').show();
                    
                        setTimeout(function() {
                          $('.dob_err').fadeOut('slow');
                      }, 2000);
                        return false;
                }


       /*    var remaining1km = $("#remaining1km").val();
        var remaining3km = $("#remaining3km").val();
        var remaining5km = $("#remaining5km").val();*/
       // var remainingallkm = $("#remainingallkm").val(); 
      if(dob == '')
      {
         var birthdate = new Date(dob1);
      }else{
         var birthdate = new Date(dob); // 
      }
       
        var cur = new Date("2020/02/16");
        var diff = cur-birthdate; // This is the difference in milliseconds
        var age = Math.floor(diff/31557600000); // Divide by 1000*60*60*24*365.25
          
            if(age="NaN"){
             //  document.getElementById("age").val(age1);
            }else if(age <18){
               $('.min_age_err').show();
               $('#agecnt').hide();
                return false;
            }
            else{
            	 document.getElementById("age").val(age);;
            	  $('.min_age_err').hide();
            }


                 if(category == ''){
                    $('.category_err').show();
                     setTimeout(function() {
                          $('.category_err').fadeOut('slow');
                      }, 2000);
                         return false;
                }else if(blood_group == ''){
                    $('.blood_group_err').show();
                        setTimeout(function() {
                          $('.blood_group_err').fadeOut('slow');
                      }, 2000);
                        return false;
                }else if(address == ''){
                    $('.address_err').show();
                  
                        setTimeout(function() {
                          $('.address_err').fadeOut('slow');
                      }, 2000);
                         return false; 
                }else
                 if(city == ''){
                    $('.city_err').show();
                  
                        setTimeout(function() {
                          $('.city_err').fadeOut('slow');
                      }, 2000);
                         return false; 
                }else if(state == ''){
                    $('.state_err').show();
                      setTimeout(function() {
                          $('.state_err').fadeOut('slow');
                      }, 2000);
                         return false;
                }else if(pincode == ''){
                    $('.pincode_err').show();
                        setTimeout(function() {
                          $('.pincode_err').fadeOut('slow');
                      }, 2000);
                        return false;
                }else if(pincode.length !=6 ){
                    $('.pincode_validerr').show();
                        setTimeout(function() {
                          $('.pincode_validerr').fadeOut('slow');
                      }, 2000);
                        return false;
                }else if(country == ''){
                    $('.country_err').show();
                        setTimeout(function() {
                          $('.country_err').fadeOut('slow');
                      }, 2000);
                        return false;
                }else if(tshirt == ''){
                    $('.tshirt_err').show();
                        setTimeout(function() {
                          $('.tshirt_err').fadeOut('slow');
                      }, 2000);
                        return false;
                }else if(age1 == '' || age1<18 ||age1.length>2){
                    $('.age_err').show();
                        setTimeout(function() {
                          $('.age_err').fadeOut('slow');
                      }, 2000);
                        return false;
                }else if(emergency_name == ''){
                    $('.emergency_name_err').show();
                        setTimeout(function() {
                          $('.emergency_name_err').fadeOut('slow');
                      }, 2000);
                        return false;
                }else if(emergency_number == ''){
                    $('.emergency_number_err').show();
                        setTimeout(function() {
                          $('.emergency_number_err').fadeOut('slow');
                      }, 2000);
                        return false;
                }else if(emergency_number_length != 10){
                     $('.emergency_number_lengtherr').show();
                       setTimeout(function() {
                    $('.emergency_number_lengtherr').fadeOut('slow');
                      }, 2000);
                      return false;
                 }else{
                   return true;
                  $("#btnSubmit").attr("disabled", true);
                }
        }     
      
      
      // Set the date we're counting down to
var countDownDate = new Date("Oct 31, 2019 23:59:59").getTime();

/*// Update the count down every 1 second
var x = setInterval(function() {

  // Get today's date and time
  var now = new Date().getTime();
    
  // Find the distance between now and the count down date
  var distance = countDownDate - now;
    
  // Time calculations for days, hours, minutes and seconds
  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  var seconds = Math.floor((distance % (1000 * 60)) / 1000);
    
  // Output the result in an element with id="demo"
  document.getElementById("demo").innerHTML =  hours + " Hr "
  + minutes + " Min " + seconds + " Sec ";
    
  // If the count down is over, write some text 
  if (distance < 0) {
    clearInterval(x);
    
    var job_apply_form1 = document.getElementById("job_apply_form1");
    document.getElementById("demo").innerHTML = "Registrations Closed";
       $("#btnSubmit").attr("disabled", true);
        job_apply_form1.style.display = "none";
  }
}, 1000);*/

                  </script>
                </div>
              </div>
           
          </div>
        </div>
      </div>
    </section>
    <!-- Section: inner-header -->
  </div>  <!-- end main-content -->  
  
  
  <!-- Footer -->
<footer class="footer divider layer-overlay overlay-dark" data-bg-img="<?php echo base_url();?>web_asset/images/bg/bg6.jpg">
    <div class="container pt-100 pb-30">
      <div class="row mb-50">
        <div class="col-sm-4 col-md-4 mb-sm-60">
          <div class="contact-icon-box p-30">
            <div class="contact-icon bg-theme-colored"> <i class="fa fa-map-marker text-white font-22"></i> </div>
            <h4 class="text-uppercase text-white">Address</h4>
            <p class="font-16">IMA Bhavan- Near Railway overbridge, Kalyan Road - Ahmednagar</p>
          </div>
        </div>
        <div class="col-sm-4 col-md-4 mb-sm-60">
          <div class="contact-icon-box p-30">
            <div class="contact-icon bg-theme-colored"> <i class="fa fa-envelope-o text-white font-22"></i> </div>
            <h4 class="text-uppercase text-white">mail us</h4>
            <p class="font-16">Get support via <br>
              <a class="" href="mailto:mail@kodesolution.com" target="_top">email : support@nagarcycling.com </a> </p>
          </div>
        </div>
        <div class="col-sm-4 col-md-4">
          <div class="contact-icon-box p-30">
            <div class="contact-icon bg-theme-colored"> <i class="fa fa-phone text-white font-22"></i> </div>
            <h4 class="text-uppercase text-white">PHONE </h4>
            <p class="font-16">+91 8308054000 <br><br>
            </p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-6 col-md-3">
          <div class="widget dark"> <img class="mt-10 mb-20" alt="" src="<?php echo base_url();?>web_asset/images/logo-wide-white.png">
            <p class="font-12">
      Ahmednagar Cycling Club actively encourages cyclists of all abilities to meet their personal goals. Whether you cycle for fun, for the health benefits or for the competition, Ahmednagar Cycling Club welcomes you.
      </p>
            <ul class="styled-icons icon-bordered small square list-inline mt-10">
              <li><a href="https://www.facebook.com/nagarcycling/"><i class="fa fa-facebook text-white"></i></a></li>
             
              <li><a href="https://instagram.com/nagarcycling/" target="_blank"><i class="fa fa-instagram text-white"></i></a></li>
              
            </ul>
          </div>
        </div>
        <div class="col-sm-6 col-md-3">
          <div class="widget dark">
           Quick Links</h5>
            <ul class="list angle-double-right list-border">
              <li><a href="<?php echo base_url();?>index.php/aboutus">About Us</a></li>
              <li><a href="<?php echo base_url();?>index.php/events">Events</a></li>
              <li><a href="<?php echo base_url();?>index.php/photos">Photos</a></li>
        <li><a href="<?php echo base_url();?>index.php/sponsors2020">Sponsors</a></li>
              <li><a href="<?php echo base_url();?>index.php/contactus">Contact Us</a></li>
              <li><a href="<?php echo base_url();?>index.php/termsandconditions">Terms & Conditions</a></li>
            
            </ul>
          </div>
        </div>
        <div class="col-sm-6 col-md-3">
          <div class="widget dark">
          
                       <h5 class="widget-title line-bottom">Contact info</h5>
                    
                      <ul class="mt-30">
                        <li><i class="fa fa-phone mb-20 text-theme-colored mr-5 font-20"></i>8308054000</li>
                        <li><i class="fa fa-map-marker mb-20 text-theme-colored mr-5 font-20"></i> IMA Bhavan- Near Railway overbridge, Kalyan Road.</li>
                        <li><i class="fa fa-envelope mb-20 text-theme-colored mr-5 font-20"></i> support@nagarcycling.com</li>
                        <li><i class="fa fa-globe mb-20 text-theme-colored mr-5 font-20 fa-spin"></i> www.nagarcycling.com</li>
                        
                      </ul>
                    
          </div>
        </div>
        <div class="col-sm-6 col-md-3">
          <div class="widget dark">
            <h5 class="widget-title line-bottom">Follow Us</h5>
            <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FAhmednagar-Cycling-Club-1900441513602454%2F&amp;tabs=timeline&amp;width=300&amp;height=300&amp;small_header=true&amp;adapt_container_width=true&amp;hide_cover=false&amp;show_facepile=false&amp;appId=1330903350339636" width="300" height="300" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowtransparency="true"></iframe>
          </div>
        </div>
      </div>
    </div>
    <div class="container-fluid copy-right p-20">
      <div class="row text-center">
        <div class="col-md-12">
          <p class="font-11 text-white m-0">Copyright &copy;2019 Ahmednagar Nagarcycling Club. All Rights Reserved. Developed by OneVoice Transmedia Pvt. Ltd.</p>
        </div>
      </div>
    </div>
  </footer>
 <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a> </div>
<!-- end wrapper --> 

<!-- Footer Scripts --> 
<!-- JS | Calendar Event Data --> 
<script src="<?php echo base_url();?>web_asset/js/calendar-events-data.js"></script> 
<!-- JS | Custom script for all pages --> 
 <!-- <script src="<?php echo base_url();?>web_asset/js/custom.js"></script>  -->

</body>

<!-- Mirrored from html.kodesolution.live/html/health-beauty/sports-trainer/v2.0/demo/page-about1.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 27 Jul 2019 07:30:40 GMT -->
</html>